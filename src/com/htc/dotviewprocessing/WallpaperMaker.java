package com.htc.dotviewprocessing;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.util.Log;
import com.htc.mosaic.LinearMosaic;
import com.htc.mosaic.MosaicMaker;
import java.nio.ByteBuffer;

public class WallpaperMaker
{
  String TAG = "WallpaperMaker";

  public Bitmap convertDotViewWallpaper(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    if (paramBitmap == null)
    {
      Log.i(this.TAG, "Illegal source bitmap(=NULL)");
      return null;
    }
    if ((paramInt1 <= 0) || (paramInt2 <= 0))
    {
      Log.i(this.TAG, "Illegal resolution.(width = " + paramInt1 + ", height = " + paramInt2);
      return null;
    }
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
    ByteBuffer localByteBuffer = ByteBuffer.allocate(4 * (paramBitmap.getWidth() * paramBitmap.getHeight()));
    localByteBuffer.rewind();
    paramBitmap.copyPixelsToBuffer(localByteBuffer);
    byte[] arrayOfByte = ImageProcessingJNI.doMinimumFilter(2, paramBitmap.getWidth(), paramBitmap.getHeight(), localByteBuffer.array());
    localByteBuffer.rewind();
    localByteBuffer.put(arrayOfByte);
    localByteBuffer.rewind();
    localBitmap.copyPixelsFromBuffer(localByteBuffer);
    return new MosaicMaker().doLinearMosaic(localBitmap, paramInt1, paramInt2).getMosaic();
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotviewprocessing.WallpaperMaker
 * JD-Core Version:    0.6.2
 */