package com.htc.dotviewprocessing;

public class ImageProcessingJNI
{
  static
  {
    System.loadLibrary("DotView");
  }

  public static native byte[] doMinimumFilter(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfByte);
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotviewprocessing.ImageProcessingJNI
 * JD-Core Version:    0.6.2
 */