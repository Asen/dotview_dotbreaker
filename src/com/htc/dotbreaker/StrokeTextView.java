package com.htc.dotbreaker;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint.Style;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

public class StrokeTextView extends TextView
{
  private TextPaint m_StrokePaint;

  public StrokeTextView(Context paramContext)
  {
    super(paramContext);
    initalStrokePaint();
  }

  public StrokeTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initalStrokePaint();
  }

  public StrokeTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initalStrokePaint();
  }

  public void initalOriginalTextView()
  {
    if (this.m_StrokePaint == null)
      this.m_StrokePaint = new TextPaint();
    this.m_StrokePaint.setTextSize(getTextSize());
    this.m_StrokePaint.setTypeface(getTypeface());
    this.m_StrokePaint.setFlags(getPaintFlags());
  }

  public void initalStrokePaint()
  {
    initalOriginalTextView();
    Resources localResources = getResources();
    this.m_StrokePaint.setStyle(Paint.Style.STROKE);
    this.m_StrokePaint.setColor(localResources.getColor(2130968606));
    this.m_StrokePaint.setStrokeWidth(localResources.getDimensionPixelSize(2131099716));
  }

  protected void onDraw(Canvas paramCanvas)
  {
    CharSequence localCharSequence = getText();
    if ((localCharSequence != null) && (localCharSequence.length() > 0))
    {
      String str = localCharSequence.toString();
      paramCanvas.drawText(str, (getWidth() - this.m_StrokePaint.measureText(str)) / 2.0F, getBaseline(), this.m_StrokePaint);
    }
    super.onDraw(paramCanvas);
  }

  public void setStrokeColor(int paramInt)
  {
    initalStrokePaint();
    float f = this.m_StrokePaint.getStrokeWidth();
    this.m_StrokePaint.setColor(paramInt);
    this.m_StrokePaint.setStrokeWidth(f);
    invalidate();
  }

  public void setStrokeWidth(float paramFloat)
  {
    int i = this.m_StrokePaint.getColor();
    initalStrokePaint();
    this.m_StrokePaint.setColor(i);
    this.m_StrokePaint.setStrokeWidth(paramFloat);
    invalidate();
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.StrokeTextView
 * JD-Core Version:    0.6.2
 */