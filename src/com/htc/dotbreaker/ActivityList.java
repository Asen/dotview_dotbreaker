package com.htc.dotbreaker;

import android.app.Activity;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ActivityList
{
  private static ActivityList instance;
  private List<Activity> list = new LinkedList();

  public static ActivityList getInstance()
  {
    try
    {
      if (instance == null)
        instance = new ActivityList();
      ActivityList localActivityList = instance;
      return localActivityList;
    }
    finally
    {
    }
  }

  public void addActivity(Activity paramActivity)
  {
    this.list.add(paramActivity);
  }

  public void exit()
  {
    try
    {
      Iterator localIterator = this.list.iterator();
      while (localIterator.hasNext())
      {
        Activity localActivity = (Activity)localIterator.next();
        if (localActivity != null)
          localActivity.finish();
      }
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      return;
      return;
    }
    finally
    {
      System.exit(0);
    }
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.ActivityList
 * JD-Core Version:    0.6.2
 */