package com.htc.dotbreaker;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.view.View;

public class GameOverView extends View
{
  final boolean F = false;
  final boolean T = true;
  boolean[][] WORD_GAME_OVER2_FOR_LOOK = { { 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0 }, { 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0 }, { 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1 }, { 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0 }, { 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0 }, { 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, { 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, { 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0 }, { 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0 }, { 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0 }, { 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1 } };
  final boolean[][] WORD_GAME_OVER_FOR_LOOK = { { 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1 }, { 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0 }, { 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1 }, { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0 }, { 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0 }, { 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0 }, { 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0 }, { 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0 }, { 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0 } };
  final boolean[][] WORD_GMAE_OVER = BrickView.toConvertMatrix(this.WORD_GAME_OVER_FOR_LOOK);
  final boolean[][] WORD_GMAE_OVER2 = BrickView.toConvertMatrix(this.WORD_GAME_OVER2_FOR_LOOK);
  final int WORD_GMAE_OVER2_LOC_X = this.inner_frame_margin_left;
  final int WORD_GMAE_OVER2_LOC_Y = this.inner_frame_margin_top;
  final int WORD_GMAE_OVER_LOC_X = this.inner_frame_margin_left + this.res.getDimensionPixelSize(2131099694) * this.dot_pixel;
  final int WORD_GMAE_OVER_LOC_Y = this.inner_frame_margin_top + this.res.getDimensionPixelSize(2131099695) * this.dot_pixel;
  final boolean[][] WORD_RESTART = BrickView.toConvertMatrix(this.WORD_RESTART_FOR_LOOK);
  final boolean[][] WORD_RESTART_FOR_LOOK = { { 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1 }, { 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0 }, { 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0 }, { 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0 }, { 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0 } };
  final int WORD_RESTART_LOC_X = this.inner_frame_margin_left + this.res.getDimensionPixelSize(2131099696) * this.dot_pixel;
  final int WORD_RESTART_LOC_Y = this.inner_frame_margin_top + this.res.getDimensionPixelSize(2131099697) * this.dot_pixel;
  int dot_pixel = this.res.getDimensionPixelSize(2131099738);
  int inner_frame_margin_left = this.res.getDimensionPixelSize(2131099735);
  int inner_frame_margin_top = this.res.getDimensionPixelSize(2131099734);
  Resources res = getResources();

  public GameOverView(Context paramContext)
  {
    super(paramContext);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    BrickView.onDrawMatrix(paramCanvas, this.WORD_GMAE_OVER, this.WORD_GMAE_OVER_LOC_X, this.WORD_GMAE_OVER_LOC_Y, 36);
    if (!BrickView.hint_animation)
      BrickView.onDrawMatrix(paramCanvas, this.WORD_RESTART, this.WORD_RESTART_LOC_X, this.WORD_RESTART_LOC_Y, 37);
    while (!BrickView.hint_animation)
      return;
    if (BrickView.hint_animation_number < 5)
    {
      BrickView.onDrawMatrix(paramCanvas, BrickView.MARK_START, BrickView.MARK_START_LOC_X, BrickView.MARK_START_LOC_Y, 41);
      BrickView.onDrawMatrix(paramCanvas, BrickView.toConvertMatrix(BrickView.HINT_ANIMATION[BrickView.hint_animation_number]), BrickView.HINT_ANIMATION_LOC_X, BrickView.HINT_ANIMATION_LOC_Y, 41);
      return;
    }
    BrickView.onDrawMatrix(paramCanvas, BrickView.MARK_EXIT, BrickView.MARK_EXIT_LOC_X, BrickView.MARK_EXIT_LOC_Y, 42);
    BrickView.onDrawMatrix(paramCanvas, BrickView.toConvertMatrix(BrickView.HINT_ANIMATION[BrickView.hint_animation_number]), BrickView.HINT_ANIMATION_LOC_X, BrickView.HINT_ANIMATION_LOC_Y, 42);
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.GameOverView
 * JD-Core Version:    0.6.2
 */