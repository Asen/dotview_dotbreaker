package com.htc.dotbreaker;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import java.text.DecimalFormat;

public class Settings extends Activity
{
  CheckBox Music;
  CheckBox Sound;
  CheckBox Vibrate;
  Button button_ball;
  Button button_board;
  Button button_moveAngle;
  private CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener()
  {
    public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
    {
      if (Settings.this.Sound.isChecked() == true)
      {
        Settings.this.sharedprefs_editor.putBoolean("checkbox_Sound", true);
        if (Settings.this.Music.isChecked() != true)
          break label110;
        Settings.this.sharedprefs_editor.putBoolean("checkbox_Music", true);
      }
      while (true)
      {
        if (Settings.this.Vibrate.isChecked() != true)
          break label129;
        Settings.this.sharedprefs_editor.putBoolean("checkbox_Vibrate", true);
        return;
        Settings.this.sharedprefs_editor.putBoolean("checkbox_Sound", false);
        break;
        label110: Settings.this.sharedprefs_editor.putBoolean("checkbox_Music", false);
      }
      label129: Settings.this.sharedprefs_editor.putBoolean("checkbox_Vibrate", false);
    }
  };
  SharedPreferences sharedprefs = null;
  SharedPreferences sharedprefs1 = null;
  SharedPreferences.Editor sharedprefs1_editor = null;
  SharedPreferences.Editor sharedprefs_editor = null;

  private void set_checkboxlistener()
  {
    this.Sound.setOnCheckedChangeListener(this.listener);
    this.Music.setOnCheckedChangeListener(this.listener);
    this.Vibrate.setOnCheckedChangeListener(this.listener);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903046);
    this.sharedprefs = getSharedPreferences("PREFS_Settings", 0);
    this.sharedprefs_editor = this.sharedprefs.edit();
    this.Sound = ((CheckBox)findViewById(2131361896));
    this.Music = ((CheckBox)findViewById(2131361898));
    this.Vibrate = ((CheckBox)findViewById(2131361900));
    if (this.sharedprefs.getBoolean("checkbox_Sound", false))
    {
      this.Sound.setChecked(true);
      if (!this.sharedprefs.getBoolean("checkbox_Music", false))
        break label351;
      this.Music.setChecked(true);
      label120: if (!this.sharedprefs.getBoolean("checkbox_Vibrate", false))
        break label362;
      this.Vibrate.setChecked(true);
    }
    while (true)
    {
      set_checkboxlistener();
      this.sharedprefs1 = getSharedPreferences("PREFS_MyStatus", 0);
      this.sharedprefs1_editor = this.sharedprefs1.edit();
      this.button_ball = ((Button)findViewById(2131361903));
      this.button_ball.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          switch (Settings.this.sharedprefs1.getInt("SpeedOfBall", BrickView.ball_speed))
          {
          default:
            Settings.this.sharedprefs1_editor.clear();
          case 50:
          case 60:
          case 70:
          case 80:
          case 90:
          case 100:
          }
          while (true)
          {
            Settings.this.sharedprefs1_editor.commit();
            Settings.this.button_ball.setText(Integer.toString(Settings.this.sharedprefs1.getInt("SpeedOfBall", BrickView.ball_speed)));
            return;
            Settings.this.sharedprefs1_editor.putInt("SpeedOfBall", 10 + Settings.this.sharedprefs1.getInt("SpeedOfBall", BrickView.ball_speed));
            continue;
            Settings.this.sharedprefs1_editor.putInt("SpeedOfBall", 50);
          }
        }
      });
      this.button_board = ((Button)findViewById(2131361905));
      this.button_board.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          switch (Settings.this.sharedprefs1.getInt("SpeedOfBoard", BrickView.board_speed))
          {
          default:
            Settings.this.sharedprefs1_editor.clear();
          case 50:
          case 60:
          case 70:
          case 80:
          case 90:
          case 100:
          }
          while (true)
          {
            Settings.this.sharedprefs1_editor.commit();
            Settings.this.button_board.setText(Integer.toString(Settings.this.sharedprefs1.getInt("SpeedOfBoard", BrickView.board_speed)));
            return;
            Settings.this.sharedprefs1_editor.putInt("SpeedOfBoard", 10 + Settings.this.sharedprefs1.getInt("SpeedOfBoard", BrickView.board_speed));
            continue;
            Settings.this.sharedprefs1_editor.putInt("SpeedOfBoard", 50);
          }
        }
      });
      this.button_moveAngle = ((Button)findViewById(2131361907));
      this.button_moveAngle.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          switch ((int)(10.0F * Settings.this.sharedprefs1.getFloat("MoveAngleOfBoard", BrickView.board_moveAngle)))
          {
          default:
            Settings.this.sharedprefs1_editor.clear();
          case 3:
          case 4:
          case 5:
          case 6:
          case 7:
          case 8:
          case 9:
          case 10:
          }
          while (true)
          {
            Settings.this.sharedprefs1_editor.commit();
            DecimalFormat localDecimalFormat = new DecimalFormat("###,##0.0");
            Settings.this.button_moveAngle.setText(localDecimalFormat.format(Settings.this.sharedprefs1.getFloat("MoveAngleOfBoard", BrickView.board_moveAngle)));
            return;
            Settings.this.sharedprefs1_editor.putFloat("MoveAngleOfBoard", 0.1F + Settings.this.sharedprefs1.getFloat("MoveAngleOfBoard", BrickView.board_moveAngle));
            continue;
            Settings.this.sharedprefs1_editor.putFloat("MoveAngleOfBoard", 0.3F);
          }
        }
      });
      this.button_ball.setText(Integer.toString(this.sharedprefs1.getInt("SpeedOfBall", BrickView.ball_speed)));
      this.button_board.setText(Integer.toString(this.sharedprefs1.getInt("SpeedOfBoard", BrickView.board_speed)));
      DecimalFormat localDecimalFormat = new DecimalFormat("###,##0.0");
      this.button_moveAngle.setText(localDecimalFormat.format(this.sharedprefs1.getFloat("MoveAngleOfBoard", BrickView.board_moveAngle)));
      return;
      this.Sound.setChecked(false);
      break;
      label351: this.Music.setChecked(false);
      break label120;
      label362: this.Vibrate.setChecked(false);
    }
  }

  protected void onDestroy()
  {
    super.onDestroy();
    this.sharedprefs_editor.commit();
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.Settings
 * JD-Core Version:    0.6.2
 */