package com.htc.dotbreaker;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.view.View;

public class GameWinView extends View
{
  final boolean F = false;
  final boolean T = true;
  final boolean[][] WORD_NEXT = BrickView.toConvertMatrix(this.WORD_NEXT_FOR_LOOK);
  final boolean[][] WORD_NEXT_FOR_LOOK = { { 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1 }, { 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0 }, { 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0 }, { 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0 }, { 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0 } };
  final int WORD_NEXT_LOC_X = this.inner_frame_margin_left + this.res.getDimensionPixelSize(2131099692) * this.dot_pixel;
  final int WORD_NEXT_LOC_Y = this.inner_frame_margin_top + this.res.getDimensionPixelSize(2131099693) * this.dot_pixel;
  final boolean[][] WORD_YOU_WIN = BrickView.toConvertMatrix(this.WORD_YOU_WIN_FOR_LOOK);
  final boolean[][] WORD_YOU_WIN2 = BrickView.toConvertMatrix(this.WORD_YOU_WIN2_FOR_LOOK);
  boolean[][] WORD_YOU_WIN2_FOR_LOOK = { { 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0 }, { 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0 }, { 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 } };
  final int WORD_YOU_WIN2_LOC_X = this.inner_frame_margin_left;
  final int WORD_YOU_WIN2_LOC_Y = this.inner_frame_margin_top;
  final boolean[][] WORD_YOU_WIN_FOR_LOOK = { { 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1 }, { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 }, { 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1 }, { 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 }, { 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1 }, { 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1 }, { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 }, { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 }, { 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1 } };
  final int WORD_YOU_WIN_LOC_X = this.inner_frame_margin_left + this.res.getDimensionPixelSize(2131099690) * this.dot_pixel;
  final int WORD_YOU_WIN_LOC_Y = this.inner_frame_margin_top + this.res.getDimensionPixelSize(2131099691) * this.dot_pixel;
  int dot_pixel = this.res.getDimensionPixelSize(2131099738);
  int inner_frame_margin_left = this.res.getDimensionPixelSize(2131099735);
  int inner_frame_margin_top = this.res.getDimensionPixelSize(2131099734);
  Resources res = getResources();

  public GameWinView(Context paramContext)
  {
    super(paramContext);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    BrickView.onDrawMatrix(paramCanvas, this.WORD_YOU_WIN, this.WORD_YOU_WIN_LOC_X, this.WORD_YOU_WIN_LOC_Y, 38);
    if (!BrickView.hint_animation)
      BrickView.onDrawMatrix(paramCanvas, this.WORD_NEXT, this.WORD_NEXT_LOC_X, this.WORD_NEXT_LOC_Y, 39);
    while (!BrickView.hint_animation)
      return;
    paramCanvas.drawRect(0.0F, 0.0F, BrickView.SCREEN_WALL_RIGHT, BrickView.SCREEN_WALL_BOTTOM, BrickView.a_Paint[35]);
    if (BrickView.hint_animation_number < 5)
    {
      BrickView.onDrawMatrix(paramCanvas, BrickView.MARK_START, BrickView.MARK_START_LOC_X, BrickView.MARK_START_LOC_Y, 41);
      BrickView.onDrawMatrix(paramCanvas, BrickView.toConvertMatrix(BrickView.HINT_ANIMATION[BrickView.hint_animation_number]), BrickView.HINT_ANIMATION_LOC_X, BrickView.HINT_ANIMATION_LOC_Y, 41);
      return;
    }
    BrickView.onDrawMatrix(paramCanvas, BrickView.MARK_EXIT, BrickView.MARK_EXIT_LOC_X, BrickView.MARK_EXIT_LOC_Y, 42);
    BrickView.onDrawMatrix(paramCanvas, BrickView.toConvertMatrix(BrickView.HINT_ANIMATION[BrickView.hint_animation_number]), BrickView.HINT_ANIMATION_LOC_X, BrickView.HINT_ANIMATION_LOC_Y, 42);
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.GameWinView
 * JD-Core Version:    0.6.2
 */