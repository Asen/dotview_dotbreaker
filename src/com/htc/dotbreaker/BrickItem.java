package com.htc.dotbreaker;

import android.graphics.Paint;

public class BrickItem
{
  int bottom;
  boolean exist;
  int left;
  Paint paint;
  int right;
  int top;

  BrickItem(boolean paramBoolean, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.exist = paramBoolean;
    this.paint = paramPaint;
    this.left = paramInt1;
    this.top = paramInt2;
    this.right = paramInt3;
    this.bottom = paramInt4;
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.BrickItem
 * JD-Core Version:    0.6.2
 */