package com.htc.dotbreaker;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class MyStatus extends Activity
{
  private static final String TAG = "HtcDotBreaker";
  static final int UIOPTIONS_FULLSCREEN_HIDENAVIGATION = 4102;
  public static Typeface mTextFontChinese;
  public static Typeface mTextFontEnglish;
  public static Typeface mTextFontNumber;
  public static int mTextSizeChinese;
  public static int mTextSizeEnglish;
  public static int mTextSizeNumber;
  private Resources mRes;
  StrokeTextView strokeTextView_title;
  TextView textView_BallTravelled;
  TextView textView_BallTravelled_Numeric;
  TextView textView_BestScore;
  TextView textView_BestScore_Numeric;
  TextView textView_BricksHit;
  TextView textView_BricksHit_Numeric;
  TextView textView_Death;
  TextView textView_Death_Numeric;
  TextView textView_HITS;
  TextView textView_HOURS;
  TextView textView_LEVEL;
  TextView textView_LevelPass;
  TextView textView_LevelPass_Numeric;
  TextView textView_MILES;
  TextView textView_MINS;
  TextView textView_PTS;
  TextView textView_PlayTime;
  TextView textView_PlayTime_Numeric_H;
  TextView textView_PlayTime_Numeric_M;
  TextView textView_TIMES;
  TextView text_BallTravelled;
  TextView text_BestScore;
  TextView text_BricksHit;
  TextView text_Deaths;
  TextView text_LevelPass;
  TextView text_PlayTime;
  int tmp_BallTravelled;
  int tmp_BestScore;
  int tmp_BricksHit;
  int tmp_Deaths;
  int tmp_LevelPass;
  int tmp_PlayTime;

  Typeface LoadTypeFacefromApk(String paramString)
  {
    Typeface localTypeface;
    try
    {
      Context localContext2 = createPackageContext("com.htc.dotmatrix", 2);
      localObject = localContext2;
      localTypeface = null;
      if (localObject == null);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException1)
    {
      try
      {
        while (true)
        {
          Object localObject;
          localTypeface = Typeface.createFromAsset(((Context)localObject).getAssets(), paramString);
          Log.i("HtcDotBreaker", "LoadTypeFacefromApk:" + paramString + "=" + localTypeface);
          return localTypeface;
          localNameNotFoundException1 = localNameNotFoundException1;
          Log.i("HtcDotBreaker", "Error e1", localNameNotFoundException1);
          try
          {
            Context localContext1 = createPackageContext("com.htc.dotmatrix_odm", 2);
            localObject = localContext1;
          }
          catch (PackageManager.NameNotFoundException localNameNotFoundException2)
          {
            Log.i("HtcDotBreaker", "Error e2", localNameNotFoundException2);
            localObject = null;
          }
        }
      }
      catch (Exception localException)
      {
        Log.i("HtcDotBreaker", "Error3", localException);
      }
    }
    return localTypeface;
  }

  void initFont()
  {
    mTextFontEnglish = LoadTypeFacefromApk("DFPixelFont.ttf");
    mTextSizeEnglish = getResources().getDimensionPixelSize(2131099741);
    mTextFontChinese = LoadTypeFacefromApk("DFPixelFontGB-A.ttf");
    mTextSizeChinese = getResources().getDimensionPixelSize(2131099743);
    mTextFontNumber = LoadTypeFacefromApk("Lynnpixel03.ttf");
    mTextSizeNumber = getResources().getDimensionPixelSize(2131099745);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Log.d("Alvis", "MyStatus.onCreate()\tbegin.");
    ActivityList.getInstance().addActivity(this);
    getWindow().addFlags(1024);
    if (Build.VERSION.SDK_INT >= 19)
    {
      Window localWindow2 = getWindow();
      localWindow2.addFlags(67108864);
      localWindow2.addFlags(134217728);
    }
    if (Build.VERSION.SDK_INT >= 19)
    {
      Window localWindow1 = getWindow();
      localWindow1.setFlags(67108864, 67108864);
      localWindow1.setFlags(134217728, 134217728);
    }
    ActionBar localActionBar = getActionBar();
    localActionBar.setDisplayShowHomeEnabled(false);
    localActionBar.setTitle("");
    localActionBar.setDisplayHomeAsUpEnabled(true);
    localActionBar.setBackgroundDrawable(getResources().getDrawable(2130968588));
    setContentView(2130903045);
    this.mRes = getResources();
    int i = this.mRes.getDimensionPixelSize(2131099714);
    int j = this.mRes.getDimensionPixelSize(2131099715);
    ((ImageView)findViewById(Resources.getSystem().getIdentifier("up", "id", "android"))).setPadding(i, 0, j, 0);
    Log.d("Alvis", "MyStatus.onCreate()\t1.");
    SharedPreferences localSharedPreferences = getSharedPreferences("PREFS_MyStatus", 0);
    this.tmp_BestScore = localSharedPreferences.getInt("BestScore", 0);
    this.tmp_BricksHit = localSharedPreferences.getInt("BricksHit", 0);
    this.tmp_PlayTime = localSharedPreferences.getInt("PlayTime", 0);
    this.tmp_Deaths = localSharedPreferences.getInt("Deaths", 0);
    this.tmp_LevelPass = localSharedPreferences.getInt("LevelPass", 0);
    this.tmp_BallTravelled = localSharedPreferences.getInt("BallTravelled", 0);
    Log.d("Alvis", "MyStatus.onCreate()\t2.");
    this.strokeTextView_title = ((StrokeTextView)findViewById(2131361874));
    label660: int k;
    label698: int m;
    if (GameLevel.isCJText((String)this.strokeTextView_title.getText()))
    {
      this.strokeTextView_title.setTypeface(GameLevel.mTextFontChinese);
      this.strokeTextView_title.setStrokeColor(this.mRes.getColor(2130968607));
      this.strokeTextView_title.setStrokeWidth(this.mRes.getDimension(2131099717));
      this.textView_BestScore = ((TextView)findViewById(2131361875));
      this.textView_BestScore_Numeric = ((TextView)findViewById(2131361876));
      this.textView_BricksHit = ((TextView)findViewById(2131361878));
      this.textView_BricksHit_Numeric = ((TextView)findViewById(2131361879));
      this.textView_PlayTime = ((TextView)findViewById(2131361881));
      this.textView_MINS = ((TextView)findViewById(2131361882));
      this.textView_HOURS = ((TextView)findViewById(2131361884));
      this.textView_PlayTime_Numeric_M = ((TextView)findViewById(2131361883));
      this.textView_PlayTime_Numeric_H = ((TextView)findViewById(2131361885));
      this.textView_Death = ((TextView)findViewById(2131361886));
      this.textView_Death_Numeric = ((TextView)findViewById(2131361887));
      this.textView_LevelPass = ((TextView)findViewById(2131361889));
      this.textView_LevelPass_Numeric = ((TextView)findViewById(2131361890));
      this.textView_BallTravelled = ((TextView)findViewById(2131361892));
      this.textView_MILES = ((TextView)findViewById(2131361893));
      this.textView_BallTravelled_Numeric = ((TextView)findViewById(2131361894));
      this.textView_BestScore_Numeric.setText(Integer.toString(this.tmp_BestScore));
      this.textView_BricksHit_Numeric.setText(Integer.toString(this.tmp_BricksHit));
      if (this.tmp_PlayTime / 3600 != 0)
        break label1122;
      this.textView_PlayTime_Numeric_H.setText("");
      this.textView_HOURS.setText("");
      this.textView_PlayTime_Numeric_M.setText(Integer.toString(this.tmp_PlayTime % 3600 / 60));
      this.textView_Death_Numeric.setText(Integer.toString(this.tmp_Deaths));
      k = 2;
      if (k <= 20)
      {
        String str = "GameStatus[" + k + "]";
        if (k != 1)
          break label1143;
        m = 1;
        label743: if (localSharedPreferences.getInt(str, m) != 4)
          break label1149;
        this.tmp_LevelPass = (k - 1);
      }
      this.textView_LevelPass_Numeric.setText(Integer.toString(this.tmp_LevelPass));
      this.textView_BallTravelled_Numeric.setText(Integer.toString(this.tmp_BallTravelled));
      initFont();
      if (!GameLevel.isCJText((String)this.textView_BestScore.getText()))
        break label1155;
      this.textView_BestScore.setTypeface(GameLevel.mTextFontChinese);
      ((ViewGroup.MarginLayoutParams)this.textView_BestScore.getLayoutParams()).setMargins(0, this.mRes.getDimensionPixelSize(2131099731), 0, 0);
      label850: this.textView_BestScore_Numeric.setTypeface(GameLevel.mTextFontEnglish);
      if (!GameLevel.isCJText((String)this.textView_BricksHit.getText()))
        break label1187;
      this.textView_BricksHit.setTypeface(GameLevel.mTextFontChinese);
      label886: this.textView_BricksHit_Numeric.setTypeface(GameLevel.mTextFontEnglish);
      if (!GameLevel.isCJText((String)this.textView_PlayTime.getText()))
        break label1219;
      this.textView_PlayTime.setTypeface(GameLevel.mTextFontChinese);
      label922: this.textView_MINS.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_HOURS.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_PlayTime_Numeric_M.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_PlayTime_Numeric_H.setTypeface(GameLevel.mTextFontEnglish);
      if (!GameLevel.isCJText((String)this.textView_Death.getText()))
        break label1251;
      this.textView_Death.setTypeface(GameLevel.mTextFontChinese);
      label988: this.textView_Death_Numeric.setTypeface(GameLevel.mTextFontEnglish);
      if (!GameLevel.isCJText((String)this.textView_LevelPass.getText()))
        break label1283;
      this.textView_LevelPass.setTypeface(GameLevel.mTextFontChinese);
      label1024: this.textView_LevelPass_Numeric.setTypeface(GameLevel.mTextFontEnglish);
      if (!GameLevel.isCJText((String)this.textView_BallTravelled.getText()))
        break label1315;
      this.textView_BallTravelled.setTypeface(GameLevel.mTextFontChinese);
    }
    while (true)
    {
      this.textView_MILES.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_BallTravelled_Numeric.setTypeface(GameLevel.mTextFontEnglish);
      Log.d("Alvis", "MyStatus.onCreate()\tend.");
      return;
      this.strokeTextView_title.setTypeface(GameLevel.mTextFontEnglish);
      this.strokeTextView_title.setTextSize(0, this.mRes.getDimensionPixelSize(2131099766));
      break;
      label1122: this.textView_PlayTime_Numeric_H.setText(Integer.toString(this.tmp_PlayTime / 3600));
      break label660;
      label1143: m = 4;
      break label743;
      label1149: k++;
      break label698;
      label1155: this.textView_BestScore.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_BestScore.setTextSize(0, this.mRes.getDimensionPixelSize(2131099767));
      break label850;
      label1187: this.textView_BricksHit.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_BricksHit.setTextSize(0, this.mRes.getDimensionPixelSize(2131099768));
      break label886;
      label1219: this.textView_PlayTime.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_PlayTime.setTextSize(0, this.mRes.getDimensionPixelSize(2131099769));
      break label922;
      label1251: this.textView_Death.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_Death.setTextSize(0, this.mRes.getDimensionPixelSize(2131099768));
      break label988;
      label1283: this.textView_LevelPass.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_LevelPass.setTextSize(0, this.mRes.getDimensionPixelSize(2131099769));
      break label1024;
      label1315: this.textView_BallTravelled.setTypeface(GameLevel.mTextFontEnglish);
      this.textView_BallTravelled.setTextSize(0, this.mRes.getDimensionPixelSize(2131099768));
    }
  }

  protected void onDestroy()
  {
    super.onDestroy();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332:
    }
    onBackPressed();
    return true;
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.MyStatus
 * JD-Core Version:    0.6.2
 */