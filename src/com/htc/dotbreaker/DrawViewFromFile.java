package com.htc.dotbreaker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;

public class DrawViewFromFile extends View
{
  Bitmap mbitmap;

  public DrawViewFromFile(Context paramContext, Bitmap paramBitmap)
  {
    super(paramContext);
    this.mbitmap = paramBitmap;
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    paramCanvas.drawBitmap(this.mbitmap, 0.0F, 0.0F, null);
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.DrawViewFromFile
 * JD-Core Version:    0.6.2
 */