package com.htc.dotbreaker.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import java.lang.reflect.Array;

public abstract class DotImage extends RelativeLayout
{
  private static final String LOG_PREFIX = "[DotImage] ";
  protected static int sBackgroundColor;
  protected static int sDotPixelHeight;
  protected static int sDotPixelWidth;
  protected static int sInnerFrameHeight;
  protected static int sInnerFrameWidth;
  private static boolean sIsInit = false;
  protected static Paint sPaint = new Paint();
  protected int mColSize;
  protected int[][] mImgDotMatrix = (int[][])null;
  protected int mRowSize;

  public DotImage(Context paramContext)
  {
    super(paramContext);
    init();
  }

  public DotImage(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }

  public DotImage(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init();
  }

  private void init()
  {
    if (getClass() != null);
    Resources localResources;
    for (String str = getClass().getName(); ; str = "")
    {
      Log.d("DotBreaker", "[DotImage] init " + str);
      localResources = getResources();
      if (localResources != null)
        break;
      Log.d("DotBreaker", "[DotImage] init, res is null!!");
      return;
    }
    if (!sIsInit)
    {
      sIsInit = true;
      sDotPixelWidth = localResources.getDimensionPixelSize(2131099738);
      sDotPixelHeight = localResources.getDimensionPixelSize(2131099739);
      sInnerFrameWidth = localResources.getDimensionPixelSize(2131099732);
      sInnerFrameHeight = localResources.getDimensionPixelSize(2131099733);
      if (sPaint != null)
        sPaint.setAntiAlias(true);
      sBackgroundColor = -16777216;
    }
    setBackgroundColor(0);
  }

  protected void createImgDotMatrix(int paramInt1, int paramInt2)
  {
    if ((paramInt2 % sDotPixelHeight != 0) || (paramInt1 % sDotPixelWidth != 0))
    {
      Log.w("DotBreaker", "[DotImage] none divisible!!");
      return;
    }
    this.mColSize = (paramInt1 / sDotPixelWidth);
    this.mRowSize = (paramInt2 / sDotPixelHeight);
    int[] arrayOfInt = { this.mRowSize, this.mColSize };
    this.mImgDotMatrix = ((int[][])Array.newInstance(Integer.TYPE, arrayOfInt));
    setLayoutParams(new RelativeLayout.LayoutParams(paramInt1, paramInt2));
  }

  protected abstract void initImgDotMatrix();

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (getVisibility() != 0);
    while (true)
    {
      return;
      if ((this.mImgDotMatrix != null) && (sPaint != null))
      {
        int i = 0;
        int j = 0;
        int k = sDotPixelWidth;
        int m = sDotPixelHeight;
        for (int n = 0; n < this.mRowSize; n++)
        {
          for (int i1 = 0; i1 < this.mColSize; i1++)
          {
            setPaintColor(this.mImgDotMatrix[n][i1]);
            paramCanvas.drawRect(i, j, k, m, sPaint);
            i += sDotPixelWidth;
            k += sDotPixelWidth;
          }
          i = 0;
          k = sDotPixelWidth;
          j += sDotPixelHeight;
          m += sDotPixelHeight;
        }
      }
    }
  }

  public void resetImgDotMatrixValue()
  {
    if (this.mImgDotMatrix != null)
      for (int i = 0; i < this.mRowSize; i++)
        for (int j = 0; j < this.mColSize; j++)
          this.mImgDotMatrix[i][j] = sBackgroundColor;
  }

  protected void setPaintColor(int paramInt)
  {
    sPaint.setColor(paramInt);
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.ui.DotImage
 * JD-Core Version:    0.6.2
 */