package com.htc.dotbreaker.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import com.htc.dotbreaker.utils.DotBreakerUtils;
import java.util.ArrayList;

public class CustomizedImage extends DotImage
{
  public static final int DIM_30_PERCENT = 1;
  private static final String LOG_PREFIX = "[CustomizedImage] ";
  private static final int WHAT_UI_PLAY_ANIMATION = 1001;
  private int mAnimIdx = 0;
  private AnimationFinishCallBack mAnimationFinishCallBack = null;
  private int mCurrentMode = 0;
  protected int mDuration = 0;
  protected int mFrameCount = 1;
  protected ArrayList<int[][]> mImgDotMatrixList = null;
  private Handler mUIHandler = new MyUIHandler(null);
  private boolean mbAnimRepeat = true;
  protected boolean mbStartAnimation = true;

  public CustomizedImage(Context paramContext)
  {
    super(paramContext);
    init();
  }

  public CustomizedImage(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }

  public CustomizedImage(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init();
  }

  private void animationfinish()
  {
    if (this.mAnimationFinishCallBack != null)
      this.mAnimationFinishCallBack.onAnimationFinish();
  }

  private void init()
  {
  }

  protected void initImgDotMatrix()
  {
    this.mAnimIdx = 0;
    if ((this.mImgDotMatrixList != null) && (!this.mImgDotMatrixList.isEmpty()))
      this.mImgDotMatrix = ((int[][])this.mImgDotMatrixList.get(this.mAnimIdx));
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if ((this.mImgDotMatrixList == null) || (!this.mbStartAnimation));
    do
    {
      do
        return;
      while ((this.mFrameCount <= 1) || (this.mDuration < 0));
      this.mAnimIdx = (1 + this.mAnimIdx);
      if (this.mAnimIdx == this.mImgDotMatrixList.size())
      {
        this.mAnimIdx = 0;
        if (!this.mbAnimRepeat)
        {
          animationfinish();
          return;
        }
      }
      this.mImgDotMatrix = ((int[][])this.mImgDotMatrixList.get(this.mAnimIdx));
    }
    while (this.mUIHandler == null);
    this.mbStartAnimation = false;
    this.mUIHandler.removeMessages(1001);
    this.mUIHandler.sendEmptyMessageDelayed(1001, this.mDuration / this.mFrameCount);
  }

  public void setAnimationFinishCallBack(AnimationFinishCallBack paramAnimationFinishCallBack)
  {
    this.mAnimationFinishCallBack = paramAnimationFinishCallBack;
  }

  public void setDotMatrixList(ArrayList<int[][]> paramArrayList, boolean paramBoolean)
  {
    if (paramArrayList == this.mImgDotMatrixList);
    do
    {
      return;
      this.mImgDotMatrixList = paramArrayList;
      if (this.mImgDotMatrixList == null)
        break;
      this.mDuration = 0;
      this.mFrameCount = this.mImgDotMatrixList.size();
      Log.d("DotBreaker", "[CustomizedImage] setDotMatrixList, mFrameCount: " + this.mFrameCount);
    }
    while (this.mImgDotMatrixList.isEmpty());
    int[][] arrayOfInt = (int[][])this.mImgDotMatrixList.get(0);
    if (arrayOfInt != null)
    {
      this.mRowSize = arrayOfInt.length;
      this.mColSize = arrayOfInt[0].length;
    }
    while (true)
    {
      this.mbAnimRepeat = paramBoolean;
      initImgDotMatrix();
      invalidate();
      return;
      Log.d("DotBreaker", "[CustomizedImage] setDotMatrixList, dotMatrixList is null!!");
    }
  }

  public void setDotMatrixList(ArrayList<int[][]> paramArrayList, boolean paramBoolean, int paramInt)
  {
    setDotMatrixList(paramArrayList, paramBoolean);
    this.mDuration = paramInt;
  }

  protected void setPaintColor(int paramInt)
  {
    switch (this.mCurrentMode)
    {
    default:
      sPaint.setColor(paramInt);
      return;
    case 1:
    }
    sPaint.setColor(DotBreakerUtils.adjustColorAlpha(paramInt, 0.3F));
  }

  public void updateCurrentMode(int paramInt)
  {
    this.mCurrentMode = paramInt;
    invalidate();
  }

  public static abstract interface AnimationFinishCallBack
  {
    public abstract void onAnimationFinish();
  }

  private class MyUIHandler extends Handler
  {
    private MyUIHandler()
    {
    }

    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default:
        return;
      case 1001:
      }
      CustomizedImage.this.mbStartAnimation = true;
      CustomizedImage.this.invalidate();
    }
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.ui.CustomizedImage
 * JD-Core Version:    0.6.2
 */