package com.htc.dotbreaker;

import android.graphics.Paint;

public class Board extends BrickItem
{
  int move_unit_h;
  int width;

  Board(boolean paramBoolean, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    super(paramBoolean, paramPaint, paramInt1, paramInt2, paramInt3, paramInt4);
    this.width = paramInt5;
    this.move_unit_h = paramInt6;
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.Board
 * JD-Core Version:    0.6.2
 */