package com.htc.dotbreaker;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;
import com.htc.dotbreaker.ui.CustomizedImage;
import com.htc.dotbreaker.utils.DotBreakerUtils;
import java.io.IOException;
import java.util.List;

public class GameActivity extends Activity
  implements SensorEventListener
{
  static final String ACTION_COVEROPEN = "com.htc.cover.closed";
  private static final String ANIMATION_FINAL_WIN = "open_cover_animation";
  private static final String ANIMATION_LEVEL = "level_animation";
  private static final String ANIMATION_LOSE = "lose_animation";
  private static final String ANIMATION_WIN = "win_animation";
  static final String DOTVIEWACTION_STRING = "com.htc.intent.action.dotviewgameservice";
  private static final String GAME_ID = "activityName";
  public static final String KEY_POLE_N = "north";
  public static final String KEY_STATE = "state";
  public static final String LOG_PREFIX = "[GameActivity] ";
  static final int MSG_DOTVIEW_CLIENT_PAUSE = 4;
  static final int MSG_DOTVIEW_CLIENT_REGISTER = 1;
  static final int MSG_DOTVIEW_CLIENT_RESUME = 3;
  static final int MSG_DOTVIEW_CLIENT_UNREGISTER = 2;
  static final int MSG_DOTVIEW_SERVCER_REGISTER_SUCCESS = 101;
  static final int MSG_DOTVIEW_SERVER_PUASE_END = 107;
  static final int MSG_DOTVIEW_SERVER_PUASE_START = 106;
  static final int MSG_DOTVIEW_SERVER_REGISTER_FAIL = 102;
  static final int MSG_DOTVIEW_SERVER_REMOVECURRENTGAME = 105;
  static final int MSG_DOTVIEW_SERVER_UNREGISTER_FAIL = 104;
  static final int MSG_DOTVIEW_SERVER_UNREGISTER_SUCCESS = 103;
  static final String PERMISSION_DOTVIEW_GAME = "com.htc.permission.dotviewgame";
  private static final String TAG = "HtcDotBreaker";
  private static Sensor aSensor;
  private static SensorManager aSensorManager;
  static float[] gravity_sensor = new float[3];
  private static SharedPreferences sharedprefs = null;
  private static SharedPreferences.Editor sharedprefs_editor = null;
  static final int x = 0;
  static final int y = 1;
  static final int z = 2;
  float SLIDE_MIN_DISTANCE;
  BrickView brickView;
  CountDownTimer check_game_complete_timer = new CountDownTimer(1000L, BrickView.ball_speed)
  {
    public void onFinish()
    {
      Log.d("HtcDotBreaker", "BrickView.game_over=" + BrickView.game_over);
      GameActivity.this.check_game_complete();
    }

    public void onTick(long paramAnonymousLong)
    {
      if (BrickView.game_level_complete)
        GameActivity.this.toGameWinPage();
      while (!BrickView.game_over)
        return;
      GameActivity.this.toGameOverPage();
    }
  };
  String currentGestureDetected;
  GameOverView gameOverView;
  GameStartsLevelView gameStartsLevelView;
  GameWinView gameWinView;
  CountDownTimer hint_animation_timer = new CountDownTimer(2100L, 200L)
  {
    public void onFinish()
    {
      BrickView.hint_animation = false;
      GameActivity.this.mView.invalidate();
    }

    public void onTick(long paramAnonymousLong)
    {
      BrickView.hint_animation_number = 1 + BrickView.hint_animation_number;
      BrickView.hint_animation_number %= 10;
      GameActivity.this.mView.invalidate();
    }
  };
  CustomizedImage mAnimationFinalWin = null;
  CustomizedImage mAnimationLevel = null;
  CustomizedImage mAnimationLose = null;
  CustomizedImage mAnimationWin = null;
  BroadcastReceiver mCoverStateReceiver = new BroadcastReceiver()
  {
    boolean mFirstNear = false;

    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      String str1 = paramAnonymousIntent.getAction();
      boolean bool1;
      if (!paramAnonymousIntent.getBooleanExtra("north", false))
      {
        bool1 = true;
        boolean bool2 = paramAnonymousIntent.getBooleanExtra("state", false);
        Log.i("HtcDotBreaker", "onReceive:" + str1 + " HWFULLTYPE:" + bool1 + " close=" + bool2);
        if ((!bool1) || (!bool2))
          break label153;
        GameActivity.this.setMaxBright();
        label89: if ("com.htc.cover.closed" == str1)
        {
          if (!bool2)
            break label163;
          this.mFirstNear = true;
          GameActivity.this.getWindow().getDecorView().setSystemUiVisibility(0x1000 | (0x4 | (0x2 | GameActivity.this.getWindow().getDecorView().getSystemUiVisibility())));
          GameActivity.this.checkViewStatus();
        }
      }
      label153: label163: 
      while (!this.mFirstNear)
      {
        return;
        bool1 = false;
        break;
        GameActivity.this.restoreBright();
        break label89;
      }
      GameActivity.this.stop_timer();
      GameActivity.this.onBackPressed();
      GameActivity.this.finish();
      String str2 = GameActivity.this.mRes.getString(2131230776);
      Toast.makeText(GameActivity.this, str2, 1).show();
    }
  };
  private GestureDetector mDetector;
  DotViewCommunity mDotViewCommunity = new DotViewCommunity();
  private GestureDetector.SimpleOnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener()
  {
    public boolean onFling(MotionEvent paramAnonymousMotionEvent1, MotionEvent paramAnonymousMotionEvent2, float paramAnonymousFloat1, float paramAnonymousFloat2)
    {
      GameActivity.this.currentGestureDetected = (paramAnonymousMotionEvent1.toString() + " " + paramAnonymousMotionEvent2.toString());
      float f1 = paramAnonymousMotionEvent1.getY();
      float f2 = paramAnonymousMotionEvent2.getY();
      if ((f2 >= f1) && (f2 - f1 > GameActivity.this.SLIDE_MIN_DISTANCE))
        if (GameActivity.this.mTipsView != null)
          Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_down  in View: drawView");
      while (true)
      {
        return true;
        if (GameActivity.this.gameStartsLevelView != null)
        {
          Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_down  in View: gameStartsLevelView");
        }
        else if (GameActivity.this.brickView != null)
        {
          Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_down  in View: brickView");
          if (GameActivity.this.brickView.game_pause)
            GameActivity.this.brickView.toExitBrick();
          else
            GameActivity.this.brickView.toGamePause();
        }
        else if (GameActivity.this.gameWinView != null)
        {
          Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_down  in View: gameWinView");
          if (GameActivity.preferencesGet("CongratulationMessage", false))
          {
            GameActivity.preferencesEditor("CongratulationMessage", false);
            GameActivity.preferencesEditorCommit();
          }
          GameActivity.this.hint_animation_stop();
          BrickView.hint_animation = false;
          ActivityList.getInstance().exit();
        }
        else if (GameActivity.this.gameOverView != null)
        {
          Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_down  in View: gameOverView");
          GameActivity.this.hint_animation_stop();
          BrickView.hint_animation = false;
          GameActivity.this.gameOverView.invalidate();
          ActivityList.getInstance().exit();
          continue;
          if ((f2 < f1) && (f1 - f2 > GameActivity.this.SLIDE_MIN_DISTANCE))
            if (GameActivity.this.mTipsView != null)
            {
              Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_up  in View: drawView");
            }
            else if (GameActivity.this.gameStartsLevelView != null)
            {
              Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_up  in View: gameStartsLevelView");
            }
            else if (GameActivity.this.brickView != null)
            {
              Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_up  in View: brickView");
              if (GameActivity.this.brickView.game_pause)
                GameActivity.this.brickView.toGameResume();
              else
                GameActivity.this.brickView.toStartGame();
            }
            else if (GameActivity.this.gameWinView != null)
            {
              Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_up  in View: gameWinView");
              if (GameActivity.preferencesGet("CongratulationMessage", false))
              {
                GameActivity.this.toGameWin_FinalWin();
              }
              else
              {
                GameActivity.this.hint_animation_stop();
                BrickView.hint_animation = false;
                GameActivity.this.nextLevel();
              }
            }
            else if (GameActivity.this.gameOverView != null)
            {
              Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onFling.swipe_up  in View: gameOverView");
              GameActivity.this.hint_animation_stop();
              BrickView.hint_animation = false;
              GameActivity.this.gameOverView.invalidate();
              GameActivity.this.restartLevel();
            }
        }
      }
    }

    public boolean onSingleTapUp(MotionEvent paramAnonymousMotionEvent)
    {
      GameActivity.this.currentGestureDetected = paramAnonymousMotionEvent.toString();
      Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onSingleTapUp degin.");
      float f1 = paramAnonymousMotionEvent.getX();
      float f2 = paramAnonymousMotionEvent.getY();
      Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onSingleTapUp, e.x = " + f1 + ", e.y = " + f2);
      if (GameActivity.this.mTipsView != null)
        Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onSingleTapUp  in View: drawView");
      while (true)
      {
        Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onSingleTapUp end.");
        return true;
        if (GameActivity.this.gameStartsLevelView != null)
        {
          Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onSingleTapUp  in View: gameStartsLevelView");
        }
        else if (GameActivity.this.brickView != null)
        {
          Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onSingleTapUp  in View: brickView");
          if (!GameActivity.this.brickView.ball_alive)
            GameActivity.this.brickView.toStartGame();
          else if ((GameActivity.this.brickView.game_pause) && (f1 > BrickView.MARK_PAUSE_LOC_X) && (f1 < BrickView.MARK_PAUSE_LOC_X + 5 * BrickView.BLOCK_WIDTH) && (f2 > BrickView.MARK_PAUSE_LOC_Y) && (f2 < BrickView.MARK_PAUSE_LOC_Y + 5 * BrickView.BLOCK_WIDTH) && (!BrickView.hint_animation))
            GameActivity.this.hint_animation_show(GameActivity.this.brickView);
        }
        else if (GameActivity.this.gameWinView != null)
        {
          Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onSingleTapUp  in View: gameWinView");
          if ((f1 > GameActivity.this.gameWinView.WORD_NEXT_LOC_X) && (f1 < GameActivity.this.gameWinView.WORD_NEXT_LOC_X + 15 * BrickView.BLOCK_WIDTH) && (f2 > GameActivity.this.gameWinView.WORD_NEXT_LOC_Y) && (f2 < GameActivity.this.gameWinView.WORD_NEXT_LOC_Y + 5 * BrickView.BLOCK_WIDTH) && (!BrickView.hint_animation))
            if (GameActivity.preferencesGet("CongratulationMessage", false))
              GameActivity.this.toGameWin_FinalWin();
            else
              GameActivity.this.hint_animation_show(GameActivity.this.gameWinView);
        }
        else if (GameActivity.this.gameOverView != null)
        {
          Log.d("HtcDotBreaker", "[GameActivity] mGestureListener.onSingleTapUp  in View: gameOverView");
          if ((f1 > GameActivity.this.gameOverView.WORD_RESTART_LOC_X) && (f1 < GameActivity.this.gameOverView.WORD_RESTART_LOC_X + 27 * BrickView.BLOCK_WIDTH) && (f2 > GameActivity.this.gameOverView.WORD_RESTART_LOC_Y) && (f2 < GameActivity.this.gameOverView.WORD_RESTART_LOC_Y + 5 * BrickView.BLOCK_WIDTH) && (!BrickView.hint_animation))
            GameActivity.this.hint_animation_show(GameActivity.this.gameOverView);
        }
      }
    }
  };
  RelativeLayout mLayout;
  private Resources mRes = null;
  View mTipsView;
  View mView;
  float tmp_onTouchEvent_X;
  float tmp_onTouchEvent_Y;
  CountDownTimer toGamePageTimer;

  private void checkViewStatus()
  {
    if (this.mTipsView != null)
      toGameStartPage();
    do
    {
      return;
      if (this.gameStartsLevelView != null)
      {
        toGameStartPage();
        return;
      }
      if (this.brickView != null)
      {
        toGamePage();
        return;
      }
      if (this.gameWinView != null)
      {
        toGameWinPage();
        return;
      }
      if (this.gameOverView != null)
      {
        toGameOverPage();
        return;
      }
    }
    while (this.mAnimationFinalWin == null);
    toGameWin_FinalWin();
  }

  private void check_game_complete()
  {
    this.check_game_complete_timer.cancel();
    this.check_game_complete_timer.start();
  }

  private RelativeLayout.LayoutParams getRelativeLayoutParams()
  {
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(DotBreakerUtils.getDotViewInnerFrameWidth(this.mRes), DotBreakerUtils.getDotViewInnerFrameHeight(this.mRes));
    localLayoutParams.leftMargin = DotBreakerUtils.getDotViewInnerFrameMarginLeft(this.mRes);
    localLayoutParams.topMargin = DotBreakerUtils.getDotViewInnerFrameMarginTop(this.mRes);
    return localLayoutParams;
  }

  private void hint_animation_show(View paramView)
  {
    BrickView.hint_animation = true;
    BrickView.hint_animation_number = -1 + BrickView.HINT_ANIMATION.length;
    hint_animation_stop();
    this.mView = paramView;
    this.hint_animation_timer.start();
  }

  private void hint_animation_stop()
  {
    this.mView = null;
    this.hint_animation_timer.cancel();
  }

  private void nextLevel()
  {
    Log.d("HtcDotBreaker", "GameActivity.nextLevel\tbegin.");
    toGameStartPage();
    Log.d("HtcDotBreaker", "GameActivity.nextLevel\tend.");
  }

  protected static void preferencesEditor(String paramString, int paramInt)
  {
    sharedprefs_editor.putInt(paramString, paramInt);
  }

  protected static void preferencesEditor(String paramString, boolean paramBoolean)
  {
    sharedprefs_editor.putBoolean(paramString, paramBoolean);
  }

  protected static void preferencesEditorApply()
  {
    sharedprefs_editor.apply();
  }

  protected static void preferencesEditorCommit()
  {
    sharedprefs_editor.commit();
  }

  protected static float preferencesGet(String paramString, float paramFloat)
  {
    return sharedprefs.getFloat(paramString, paramFloat);
  }

  protected static int preferencesGet(String paramString, int paramInt)
  {
    return sharedprefs.getInt(paramString, paramInt);
  }

  protected static boolean preferencesGet(String paramString, boolean paramBoolean)
  {
    return sharedprefs.getBoolean(paramString, paramBoolean);
  }

  private void restartLevel()
  {
    Log.d("HtcDotBreaker", "GameActivity.restartLevel\tbegin.");
    toGameStartPage();
    Log.d("HtcDotBreaker", "GameActivity.restartLevel\tend.");
  }

  private void setGameLevelStatus()
  {
    int i = 1;
    if (i <= 20)
    {
      SharedPreferences localSharedPreferences = sharedprefs;
      String str = "GameStatus[" + i + "]";
      if (i == 1);
      for (int j = 1; ; j = 4)
      {
        if (localSharedPreferences.getInt(str, j) == 4)
          break label102;
        preferencesEditor("GameStatus[" + i + "]", 2);
        i++;
        break;
      }
    }
    label102: preferencesEditor("GameStatus[" + BrickView.brick_game_level + "]", 3);
    preferencesEditorCommit();
  }

  private void stop_timer()
  {
    this.toGamePageTimer.cancel();
    this.check_game_complete_timer.cancel();
    this.hint_animation_timer.cancel();
  }

  private void toGameOverPage()
  {
    Log.d("HtcDotBreaker", "toGameOverPage()\tbegin.");
    if (this.brickView == null)
      Log.d("HtcDotBreaker", "brickView == null");
    while (true)
    {
      if (this.brickView != null)
      {
        preferencesEditor("Current_GameState", false);
        if (this.brickView.score != null)
          preferencesEditor("BestScore", Math.max(this.brickView.score.numeric, preferencesGet("BestScore", 0)));
        preferencesEditor("BricksHit", preferencesGet("BricksHit", 0) + this.brickView.mBricksHit);
        preferencesEditor("PlayTime", preferencesGet("PlayTime", 0) + this.brickView.tmp_playTime);
        preferencesEditor("Deaths", 1 + preferencesGet("Deaths", 0));
        preferencesEditor("BallTravelled", preferencesGet("BallTravelled", 0) + this.brickView.ball_travelled);
        preferencesEditorCommit();
      }
      BrickView.keep_score = -1;
      this.mLayout.removeAllViews();
      this.mTipsView = null;
      this.gameStartsLevelView = null;
      this.check_game_complete_timer.cancel();
      this.brickView = null;
      this.gameWinView = null;
      this.mAnimationWin = null;
      this.mAnimationLose = new CustomizedImage(this);
      if ((this.mAnimationLose != null) && (this.mRes != null))
        this.mAnimationLose.setLayoutParams(getRelativeLayoutParams());
      try
      {
        this.mAnimationLose.setDotMatrixList(DotBreakerUtils.readPNGtoAnimation(this, true, "lose_animation", 30, 48, 27), true, 5000);
        this.gameOverView = new GameOverView(this);
        this.mLayout.addView(this.mAnimationLose);
        this.mLayout.addView(this.gameOverView);
        BrickView.game_over = false;
        Log.d("HtcDotBreaker", "toGameOverPage()\tend.");
        return;
        if (this.brickView.score != null)
          continue;
        Log.d("HtcDotBreaker", "brickView.score == null");
      }
      catch (IOException localIOException)
      {
        while (true)
          localIOException.printStackTrace();
      }
    }
  }

  private void toGamePage()
  {
    Log.d("HtcDotBreaker", "GameActivity.toGamePage\tbegin.");
    this.mLayout.removeAllViews();
    this.mAnimationLevel = null;
    this.mTipsView = null;
    this.gameStartsLevelView = null;
    this.mAnimationWin = null;
    this.gameWinView = null;
    this.mAnimationLose = null;
    this.gameOverView = null;
    if (this.brickView == null)
      this.brickView = new BrickView(this, 1080, 1920);
    this.mLayout.addView(this.brickView);
    check_game_complete();
    Log.d("HtcDotBreaker", "GameActivity.toGamePage\tend.");
  }

  private void toGameStartPage()
  {
    Log.d("HtcDotBreaker", "GameActivity.toGameStartPage\tbegin.");
    this.mLayout.removeAllViews();
    this.mTipsView = null;
    this.gameStartsLevelView = new GameStartsLevelView(this);
    this.brickView = null;
    this.mAnimationWin = null;
    this.gameWinView = null;
    this.mAnimationLose = null;
    this.gameOverView = null;
    this.mAnimationLevel = new CustomizedImage(this);
    if ((this.mAnimationLevel != null) && (this.mRes != null))
      this.mAnimationLevel.setLayoutParams(getRelativeLayoutParams());
    try
    {
      this.mAnimationLevel.setDotMatrixList(DotBreakerUtils.readPNGtoAnimation(this, true, "level_animation", 66, 48, 27), false, 1500);
      this.gameStartsLevelView = new GameStartsLevelView(this);
      this.mLayout.addView(this.mAnimationLevel);
      this.mLayout.addView(this.gameStartsLevelView);
      new CountDownTimer(1000L, 1000L)
      {
        public void onFinish()
        {
          if (GameActivity.this.gameStartsLevelView != null)
            GameActivity.this.gameStartsLevelView.updateLevelNum(false);
        }

        public void onTick(long paramAnonymousLong)
        {
        }
      }
      .start();
      this.toGamePageTimer = new CountDownTimer(3000L, 3000L)
      {
        public void onFinish()
        {
          GameActivity.this.toGamePage();
        }

        public void onTick(long paramAnonymousLong)
        {
        }
      }
      .start();
      setGameLevelStatus();
      Log.d("HtcDotBreaker", "GameActivity.toGameStartPage\tend.");
      return;
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
  }

  private void toGameWinPage()
  {
    Log.d("HtcDotBreaker", "toGameWinPage()\tbegin.");
    if (this.brickView == null)
      Log.d("HtcDotBreaker", "brickView == null");
    if (this.brickView != null)
    {
      preferencesEditor("Current_GameState", false);
      preferencesEditor("BricksHit", preferencesGet("BricksHit", 0) + this.brickView.mBricksHit);
      preferencesEditor("PlayTime", preferencesGet("PlayTime", 0) + this.brickView.tmp_playTime);
      preferencesEditor("LevelPass", 1 + preferencesGet("LevelPass", 0));
      preferencesEditor("BallTravelled", preferencesGet("BallTravelled", 0) + this.brickView.ball_travelled);
      preferencesEditorCommit();
    }
    this.mLayout.removeAllViews();
    this.mTipsView = null;
    this.gameStartsLevelView = null;
    this.check_game_complete_timer.cancel();
    this.brickView = null;
    this.mAnimationLose = null;
    this.gameOverView = null;
    this.mAnimationWin = new CustomizedImage(this);
    if ((this.mAnimationWin != null) && (this.mRes != null))
      this.mAnimationWin.setLayoutParams(getRelativeLayoutParams());
    try
    {
      this.mAnimationWin.setDotMatrixList(DotBreakerUtils.readPNGtoAnimation(this, true, "win_animation", 30, 48, 27), true, 5000);
      this.gameWinView = new GameWinView(this);
      this.mLayout.addView(this.mAnimationWin);
      this.mLayout.addView(this.gameWinView);
      BrickView.game_level_complete = false;
      Log.d("HtcDotBreaker", "toGameWinPage()\tend.");
      return;
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
  }

  private void toGameWin_FinalWin()
  {
    this.mLayout.removeAllViews();
    this.mTipsView = null;
    this.gameStartsLevelView = null;
    this.brickView = null;
    this.mAnimationWin = null;
    this.gameWinView = null;
    this.mAnimationLose = null;
    this.gameOverView = null;
    this.mAnimationFinalWin = new CustomizedImage(this);
    if ((this.mAnimationFinalWin != null) && (this.mRes != null))
      this.mAnimationFinalWin.setLayoutParams(getRelativeLayoutParams());
    try
    {
      this.mAnimationFinalWin.setDotMatrixList(DotBreakerUtils.readPNGtoAnimation(this, true, "open_cover_animation", 31, 48, 27), true, 2000);
      this.mLayout.addView(this.mAnimationFinalWin);
      return;
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
  }

  public boolean isTopActivity(String paramString)
  {
    List localList = ((ActivityManager)getApplicationContext().getSystemService("activity")).getRunningTasks(1);
    String str;
    if ((localList != null) && (localList.size() > 0))
    {
      ComponentName localComponentName = ((ActivityManager.RunningTaskInfo)localList.get(0)).topActivity;
      if (localComponentName != null)
      {
        str = localComponentName.getPackageName();
        bool = false;
        if (paramString != null)
        {
          bool = false;
          if (str != null)
            if (paramString.compareTo(str) != 0)
              break label166;
        }
      }
    }
    label166: for (boolean bool = true; ; bool = false)
    {
      Log.d("HtcDotBreaker", "isTopActivity send: " + paramString + ", top: " + str + ", match: " + bool);
      return bool;
      Log.e("HtcDotBreaker", "TopActivity is null");
      str = null;
      break;
      Log.e("HtcDotBreaker", "TopActivity is 0 task");
      str = null;
      break;
    }
  }

  public void onAccuracyChanged(Sensor paramSensor, int paramInt)
  {
  }

  public void onBackPressed()
  {
    Log.d("HtcDotBreaker", "GameActivity.onBackPressed()\tbegin.");
    super.onBackPressed();
    if (this.brickView != null)
      this.brickView.onBackPressed();
    while (true)
    {
      Log.d("HtcDotBreaker", "GameActivity.onBackPressed()\tend.");
      return;
      if (this.gameWinView != null)
      {
        hint_animation_stop();
        BrickView.hint_animation = false;
      }
      else if (this.gameOverView != null)
      {
        hint_animation_stop();
        BrickView.hint_animation = false;
      }
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Log.i("HtcDotBreaker", "onCreate");
    ActivityList.getInstance().addActivity(this);
    getWindow().addFlags(1024);
    requestWindowFeature(1);
    setContentView(2130903041);
    getWindow().addFlags(128);
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.screenBrightness = 1.0F;
    getWindow().setAttributes(localLayoutParams);
    this.mRes = getResources();
    this.SLIDE_MIN_DISTANCE = this.mRes.getDimension(2131099740);
    aSensorManager = (SensorManager)getSystemService("sensor");
    aSensor = aSensorManager.getDefaultSensor(1);
    aSensorManager.registerListener(this, aSensor, 3);
    this.mLayout = ((RelativeLayout)findViewById(2131361794));
    this.mTipsView = getLayoutInflater().inflate(2130903047, null);
    RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-1, -1);
    this.mLayout.addView(this.mTipsView, localLayoutParams1);
    ((AnimationDrawable)((ImageView)findViewById(2131361909)).getDrawable()).start();
    sharedprefs = getSharedPreferences("PREFS_MyStatus", 0);
    sharedprefs_editor = sharedprefs.edit();
    this.mDetector = new GestureDetector(getApplicationContext(), this.mGestureListener);
    this.mDotViewCommunity.bindDotViewService();
  }

  protected void onDestroy()
  {
    Log.i("HtcDotBreaker", "onDestroy");
    this.mDotViewCommunity.doUnbindDotViewService();
    super.onDestroy();
  }

  protected void onPause()
  {
    super.onPause();
    Log.i("HtcDotBreaker", "onPause, top: " + isTopActivity(getPackageName()));
    unregisterReceiver(this.mCoverStateReceiver);
    this.mDotViewCommunity.pause();
  }

  protected void onResume()
  {
    super.onResume();
    Log.i("HtcDotBreaker", "onResume");
    IntentFilter localIntentFilter = new IntentFilter("com.htc.cover.closed");
    if (this.mCoverStateReceiver != null)
      registerReceiver(this.mCoverStateReceiver, localIntentFilter);
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.flags = (0x80000 | localLayoutParams.flags);
    this.mDotViewCommunity.resume();
  }

  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    gravity_sensor[0] = paramSensorEvent.values[0];
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    this.mDetector.onTouchEvent(paramMotionEvent);
    return super.onTouchEvent(paramMotionEvent);
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if ((Build.VERSION.SDK_INT >= 19) && (paramBoolean) && (this.mTipsView == null))
      getWindow().getDecorView().setSystemUiVisibility(0x1000 | (0x4 | (0x2 | getWindow().getDecorView().getSystemUiVisibility())));
  }

  void restoreBright()
  {
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.screenBrightness = -1.0F;
    getWindow().setAttributes(localLayoutParams);
  }

  void setMaxBright()
  {
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.screenBrightness = 1.0F;
    getWindow().setAttributes(localLayoutParams);
  }

  public class DotViewCommunity
  {
    static final int MSG_INNER_RETRY_RESUME = 1;
    static final int MSG_SET_VALUE = 3;
    Handler mInnerHandler = new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        Log.i("HtcDotBreaker", "mInnerMessenger, msg.what: " + paramAnonymousMessage.what);
        switch (paramAnonymousMessage.what)
        {
        default:
          super.handleMessage(paramAnonymousMessage);
        case 1:
        }
        int i;
        int j;
        do
        {
          return;
          if (GameActivity.DotViewCommunity.this.mService != null)
          {
            GameActivity.DotViewCommunity.this.resume();
            return;
          }
          i = paramAnonymousMessage.arg1;
          j = paramAnonymousMessage.arg2;
        }
        while (i >= 5);
        int k = i + 1;
        int m = j + 100;
        if (GameActivity.DotViewCommunity.this.mInnerHandler.hasMessages(1))
          GameActivity.DotViewCommunity.this.mInnerHandler.removeMessages(1);
        GameActivity.DotViewCommunity.this.mInnerHandler.sendMessageDelayed(Message.obtain(null, 1, k, m), m);
      }
    };
    boolean mIsBound = false;
    Messenger mMessenger = new Messenger(new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        Log.i("HtcDotBreaker", "handleMessage: " + paramAnonymousMessage.what);
        switch (paramAnonymousMessage.what)
        {
        default:
          super.handleMessage(paramAnonymousMessage);
          return;
        case 3:
        }
        String str = "Got:" + paramAnonymousMessage.arg1;
        Bundle localBundle = paramAnonymousMessage.getData();
        if (localBundle != null)
          str = str + localBundle.getString("status");
        Log.i("HtcDotBreaker", "text=" + str);
        Toast.makeText(GameActivity.this, str, 0).show();
      }
    });
    Messenger mService = null;
    DotViewSericeConnection mServiceConnect = new DotViewSericeConnection();

    public DotViewCommunity()
    {
    }

    public boolean bindDotViewService()
    {
      StringBuilder localStringBuilder = new StringBuilder().append("bindDotService: ");
      if (!this.mIsBound);
      for (boolean bool1 = true; ; bool1 = false)
      {
        Log.i("HtcDotBreaker", bool1);
        boolean bool2 = this.mIsBound;
        boolean bool3 = false;
        if (!bool2)
        {
          this.mIsBound = true;
          Intent localIntent = new Intent();
          localIntent.setClassName("com.htc.dotmatrix", "com.htc.dotmatrix.GameService");
          bool3 = GameActivity.this.bindService(localIntent, this.mServiceConnect, 1);
        }
        return bool3;
      }
    }

    public void doUnbindDotViewService()
    {
      Log.i("HtcDotBreaker", "doUnbindDotViewService:" + this.mIsBound);
      if ((!this.mIsBound) || (this.mService != null));
      try
      {
        Message localMessage = Message.obtain(null, 2);
        Bundle localBundle = new Bundle();
        localBundle.putString("activityName", GameActivity.class.getName());
        localMessage.setData(localBundle);
        this.mService.send(localMessage);
        GameActivity.this.unbindService(this.mServiceConnect);
        this.mIsBound = false;
        return;
      }
      catch (RemoteException localRemoteException)
      {
        while (true)
          Log.i("HtcDotBreaker", "send server2 fail:" + localRemoteException, localRemoteException);
      }
    }

    public boolean pause()
    {
      Log.i("HtcDotBreaker", "pause");
      Messenger localMessenger = this.mService;
      boolean bool = false;
      if (localMessenger != null);
      try
      {
        Message localMessage = Message.obtain(null, 4);
        Bundle localBundle = new Bundle();
        localBundle.putString("activityName", GameActivity.class.getName());
        localMessage.setData(localBundle);
        this.mService.send(localMessage);
        bool = true;
        if (this.mInnerHandler.hasMessages(1))
          this.mInnerHandler.removeMessages(1);
        return bool;
      }
      catch (RemoteException localRemoteException)
      {
        Log.i("HtcDotBreaker", "send server fail:" + localRemoteException, localRemoteException);
      }
      return bool;
    }

    public boolean resume()
    {
      Log.i("HtcDotBreaker", "resume");
      if (this.mService != null)
        try
        {
          Message localMessage = Message.obtain(null, 3);
          Bundle localBundle = new Bundle();
          localBundle.putString("activityName", GameActivity.class.getName());
          localMessage.setData(localBundle);
          this.mService.send(localMessage);
          return true;
        }
        catch (RemoteException localRemoteException)
        {
          Log.i("HtcDotBreaker", "send server fail:" + localRemoteException, localRemoteException);
          return false;
        }
      if (this.mInnerHandler.hasMessages(1))
        this.mInnerHandler.removeMessages(1);
      this.mInnerHandler.sendMessageDelayed(Message.obtain(null, 1, 0, 100), 100);
      return false;
    }

    class DotViewSericeConnection
      implements ServiceConnection
    {
      DotViewSericeConnection()
      {
      }

      public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
      {
        Log.i("HtcDotBreaker", "onServiceConnected: " + paramComponentName);
        GameActivity.DotViewCommunity.this.mService = new Messenger(paramIBinder);
        try
        {
          Message localMessage = Message.obtain(null, 1);
          localMessage.replyTo = GameActivity.DotViewCommunity.this.mMessenger;
          Bundle localBundle = new Bundle();
          localBundle.putString("activityName", GameActivity.class.getName());
          localMessage.setData(localBundle);
          GameActivity.DotViewCommunity.this.mService.send(localMessage);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          Log.i("HtcDotBreaker", "send server fail:" + localRemoteException, localRemoteException);
        }
      }

      public void onServiceDisconnected(ComponentName paramComponentName)
      {
        Log.i("HtcDotBreaker", "onServiceDisconnected:" + paramComponentName);
        GameActivity.DotViewCommunity.this.mService = null;
      }
    }
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.GameActivity
 * JD-Core Version:    0.6.2
 */