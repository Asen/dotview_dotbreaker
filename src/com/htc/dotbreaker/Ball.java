package com.htc.dotbreaker;

import android.graphics.Paint;

public class Ball extends BrickItem
{
  int height;
  int move_unit_horizontal;
  int move_unit_vertical;
  int width;

  Ball(boolean paramBoolean, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
  {
    super(paramBoolean, paramPaint, paramInt1, paramInt2, paramInt3, paramInt4);
    this.width = paramInt5;
    this.height = paramInt6;
    this.move_unit_horizontal = paramInt7;
    this.move_unit_vertical = paramInt8;
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.Ball
 * JD-Core Version:    0.6.2
 */