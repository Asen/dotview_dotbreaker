package com.htc.dotbreaker;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;

public class GameLevel extends Activity
{
  private static final String LOG_PREFIX = "[GameLevel] ";
  public static final String TAG = "HtcDotBreaker";
  static final int locked = 4;
  public static Typeface mTextFontChinese;
  public static Typeface mTextFontEnglish;
  public static Typeface mTextFontNumber;
  public static int mTextSizeChinese = 0;
  public static int mTextSizeEnglish = 0;
  public static int mTextSizeNumber = 0;
  private static ArrayList<Character.UnicodeBlock> msCJBlockList = null;
  static final int ongoing = 3;
  static final int passed = 2;
  static final int unlocked = 1;
  View[] bg_level = new View[21];
  CheckBox dontShow_dialog;
  ImageView[] imageView_level = new ImageView[21];
  private Resources mRes;
  MenuItem menuOverflow;
  SharedPreferences sharedprefs = null;
  SharedPreferences.Editor sharedprefs_editor = null;
  StrokeTextView strokeTextView_title;
  StrokeTextView[] textView_level = new StrokeTextView[21];

  private static void addBlockChinese(ArrayList<Character.UnicodeBlock> paramArrayList)
  {
    paramArrayList.add(Character.UnicodeBlock.CJK_RADICALS_SUPPLEMENT);
    paramArrayList.add(Character.UnicodeBlock.KANGXI_RADICALS);
    paramArrayList.add(Character.UnicodeBlock.BOPOMOFO);
    paramArrayList.add(Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A);
    paramArrayList.add(Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS);
    paramArrayList.add(Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS);
  }

  private static void addBlockJapanese(ArrayList<Character.UnicodeBlock> paramArrayList)
  {
    paramArrayList.add(Character.UnicodeBlock.HIRAGANA);
    paramArrayList.add(Character.UnicodeBlock.KATAKANA);
    paramArrayList.add(Character.UnicodeBlock.KATAKANA_PHONETIC_EXTENSIONS);
  }

  private static ArrayList<Character.UnicodeBlock> getCJBlockList()
  {
    if ((msCJBlockList == null) || (msCJBlockList.size() == 0))
    {
      Log.d("HtcDotBreaker", "[GameLevel] block null, generate");
      msCJBlockList = new ArrayList();
      addBlockChinese(msCJBlockList);
      addBlockJapanese(msCJBlockList);
    }
    return msCJBlockList;
  }

  private void initGameLevelStatus()
  {
    if (this.sharedprefs.getBoolean("GameStatusInit", false))
      return;
    this.sharedprefs_editor.putBoolean("GameStatusInit", true);
    int i = 1;
    if (i <= 20)
    {
      SharedPreferences.Editor localEditor = this.sharedprefs_editor;
      String str = "GameStatus[" + i + "]";
      if (i == 1);
      for (int j = 1; ; j = 4)
      {
        localEditor.putInt(str, j);
        i++;
        break;
      }
    }
    this.sharedprefs_editor.commit();
  }

  public static boolean isCJText(String paramString)
  {
    if ((paramString == null) || (paramString.length() == 0))
      Log.d("HtcDotBreaker", "[GameLevel] isCJText, text is null!!");
    while (true)
    {
      return false;
      msCJBlockList = getCJBlockList();
      if (msCJBlockList == null)
      {
        Log.d("HtcDotBreaker", "[GameLevel] isCJText, msCJKBlockList is null!!");
        return false;
      }
      for (int i = 0; i < paramString.length(); i++)
      {
        int j = Character.codePointAt(paramString, i);
        if ((isInBlockList(msCJBlockList, j)) || (isInJapaneseUnMappedRange(j)))
          return true;
        int k = Character.charCount(j);
        if (k > 1)
          i += k - 1;
      }
    }
  }

  private static boolean isInBlockList(ArrayList<Character.UnicodeBlock> paramArrayList, int paramInt)
  {
    if ((paramArrayList == null) || (paramArrayList.size() == 0));
    Character.UnicodeBlock localUnicodeBlock;
    Iterator localIterator;
    do
      while (!localIterator.hasNext())
      {
        return false;
        localUnicodeBlock = Character.UnicodeBlock.of(paramInt);
        localIterator = paramArrayList.iterator();
      }
    while (!localUnicodeBlock.equals((Character.UnicodeBlock)localIterator.next()));
    return true;
  }

  private static boolean isInJapaneseUnMappedRange(int paramInt)
  {
    return (paramInt <= 65439L) && (paramInt >= 65381L);
  }

  private void setGameLevelStatus()
  {
    Log.d("HtcDotBreaker", "GameLevel.setGameLevelStatus()\tbegin.");
    if (this.menuOverflow != null)
      this.menuOverflow.setIcon(2130837508);
    int i = 1;
    if (i <= 20)
    {
      SharedPreferences localSharedPreferences = this.sharedprefs;
      String str = "GameStatus[" + i + "]";
      int k;
      label75: int m;
      if (i == 1)
      {
        k = 1;
        m = localSharedPreferences.getInt(str, k);
        Log.d("HtcDotBreaker", "GameStatus[" + i + "]=" + m + ".");
        if (m != 1)
          break label209;
        this.bg_level[i].setBackgroundResource(2130837532);
        this.textView_level[i].setTextColor(this.mRes.getColor(2130968590));
        this.textView_level[i].setStrokeColor(-16777216);
        this.imageView_level[i].setVisibility(4);
        Log.d("HtcDotBreaker", "unlocked");
      }
      while (true)
      {
        i++;
        break;
        k = 4;
        break label75;
        label209: if (m == 2)
        {
          this.bg_level[i].setBackgroundResource(2130837532);
          this.textView_level[i].setTextColor(this.mRes.getColor(2130968590));
          this.textView_level[i].setStrokeColor(-16777216);
          this.imageView_level[i].setVisibility(4);
          Log.d("HtcDotBreaker", "passed");
        }
        else if (m == 3)
        {
          this.bg_level[i].setBackgroundResource(2130837532);
          this.textView_level[i].setTextColor(this.mRes.getColor(2130968590));
          this.textView_level[i].setStrokeColor(-16777216);
          this.imageView_level[i].setImageResource(2130837533);
          this.imageView_level[i].setVisibility(0);
          Log.d("HtcDotBreaker", "ongoing");
        }
        else if (m == 4)
        {
          this.bg_level[i].setBackgroundResource(2130837531);
          this.textView_level[i].setTextColor(0);
          this.textView_level[i].setStrokeColor(0);
          this.imageView_level[i].setVisibility(4);
          Log.d("HtcDotBreaker", "locked");
        }
      }
    }
    int j = this.sharedprefs.getInt("PlayedGameLevel", -1);
    if (j != -1)
    {
      this.bg_level[j].setBackgroundResource(2130837535);
      this.textView_level[j].setTextColor(this.mRes.getColor(2130968591));
      this.imageView_level[j].setImageResource(2130837534);
    }
    Log.d("HtcDotBreaker", "GameLevel.setGameLevelStatus()\tend.");
  }

  private void showdialog_battery()
  {
    if (this.sharedprefs.getBoolean("checkbox_DontShowDialogBattery", false))
    {
      startActivity(new Intent(this, GameActivity.class));
      return;
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    LayoutInflater localLayoutInflater = LayoutInflater.from(this);
    localBuilder.setTitle(this.mRes.getString(2131230770));
    View localView = localLayoutInflater.inflate(2130903040, null);
    localBuilder.setView(localView);
    ((TextView)localView.findViewById(2131361792)).setText(this.mRes.getString(2131230771));
    this.dontShow_dialog = ((CheckBox)localView.findViewById(2131361793));
    this.dontShow_dialog.setText(this.mRes.getString(2131230772));
    localBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        GameLevel.this.startActivity(new Intent(GameLevel.this, GameActivity.class));
      }
    });
    localBuilder.create().show();
    if (this.sharedprefs.getBoolean("checkbox_DontShowDialogBattery", false))
      this.dontShow_dialog.setChecked(true);
    while (true)
    {
      this.dontShow_dialog.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
      {
        public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
        {
          if (GameLevel.this.dontShow_dialog.isChecked() == true)
            GameLevel.this.sharedprefs_editor.putBoolean("checkbox_DontShowDialogBattery", true);
          while (true)
          {
            GameLevel.this.sharedprefs_editor.commit();
            return;
            GameLevel.this.sharedprefs_editor.putBoolean("checkbox_DontShowDialogBattery", false);
          }
        }
      });
      return;
      this.dontShow_dialog.setChecked(false);
    }
  }

  private void showdialog_congratulation()
  {
    if (this.sharedprefs.getBoolean("checkbox_DontShowDialogCongratulation", false))
      Log.d("HtcDotBreaker", "GameActivity.showdialog_congratulation\tcheckbox_DontShowDialogCongratulation=" + this.sharedprefs.getBoolean("checkbox_DontShowDialogCongratulation", false));
    while (!this.sharedprefs.getBoolean("CongratulationMessage", false))
      return;
    Log.d("HtcDotBreaker", "GameActivity.showdialog_congratulation\tCongratulationMessage=" + this.sharedprefs.getBoolean("CongratulationMessage", false));
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    View localView = LayoutInflater.from(this).inflate(2130903040, null);
    localBuilder.setTitle(this.mRes.getString(2131230773));
    localBuilder.setView(localView);
    ((TextView)localView.findViewById(2131361792)).setText(this.mRes.getString(2131230774));
    this.dontShow_dialog = ((CheckBox)localView.findViewById(2131361793));
    this.dontShow_dialog.setText(this.mRes.getString(2131230775));
    localBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        GameLevel.this.sharedprefs_editor.putBoolean("CongratulationMessage", false);
        GameLevel.this.sharedprefs_editor.commit();
      }
    });
    localBuilder.setCancelable(false);
    localBuilder.create().show();
    if (this.sharedprefs.getBoolean("checkbox_DontShowDialogCongratulation", false))
      this.dontShow_dialog.setChecked(true);
    while (true)
    {
      this.dontShow_dialog.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
      {
        public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
        {
          if (GameLevel.this.dontShow_dialog.isChecked() == true)
            GameLevel.this.sharedprefs_editor.putBoolean("checkbox_DontShowDialogCongratulation", true);
          while (true)
          {
            GameLevel.this.sharedprefs_editor.commit();
            return;
            GameLevel.this.sharedprefs_editor.putBoolean("checkbox_DontShowDialogCongratulation", false);
          }
        }
      });
      return;
      this.dontShow_dialog.setChecked(false);
    }
  }

  private void start_Brick_Game(int paramInt)
  {
    int i = 1;
    Log.d("HtcDotBreaker", "GameLevel.start_Brick_Game\tbegin.");
    SharedPreferences localSharedPreferences = this.sharedprefs;
    String str = "GameStatus[" + paramInt + "]";
    if (paramInt == i);
    while (localSharedPreferences.getInt(str, i) == 4)
    {
      return;
      i = 4;
    }
    BrickView.brick_game_level = paramInt;
    showdialog_battery();
  }

  Typeface LoadTypeFacefromApk(String paramString)
  {
    Typeface localTypeface;
    try
    {
      Context localContext2 = createPackageContext("com.htc.dotmatrix", 2);
      localObject = localContext2;
      localTypeface = null;
      if (localObject == null);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException1)
    {
      try
      {
        while (true)
        {
          Object localObject;
          localTypeface = Typeface.createFromAsset(((Context)localObject).getAssets(), paramString);
          Log.i("HtcDotBreaker", "LoadTypeFacefromApk:" + paramString + "=" + localTypeface);
          return localTypeface;
          localNameNotFoundException1 = localNameNotFoundException1;
          Log.i("HtcDotBreaker", "Error e1", localNameNotFoundException1);
          try
          {
            Context localContext1 = createPackageContext("com.htc.dotmatrix_odm", 2);
            localObject = localContext1;
          }
          catch (PackageManager.NameNotFoundException localNameNotFoundException2)
          {
            Log.i("HtcDotBreaker", "Error e2", localNameNotFoundException2);
            localObject = null;
          }
        }
      }
      catch (Exception localException)
      {
        Log.i("HtcDotBreaker", "Error3", localException);
      }
    }
    return localTypeface;
  }

  void initFont()
  {
    mTextFontEnglish = Typeface.createFromAsset(getAssets(), "standard0765-webfont.ttf");
    mTextSizeEnglish = this.mRes.getDimensionPixelSize(2131099741);
    mTextFontChinese = LoadTypeFacefromApk("DFPixelFontGB-A.ttf");
    mTextSizeChinese = this.mRes.getDimensionPixelSize(2131099743);
    mTextFontNumber = LoadTypeFacefromApk("Lynnpixel03.ttf");
    mTextSizeNumber = this.mRes.getDimensionPixelSize(2131099745);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Log.d("HtcDotBreaker", "GameLevel.onCreate()\tbegin.");
    ActivityList.getInstance().addActivity(this);
    getWindow().addFlags(1024);
    if (Build.VERSION.SDK_INT >= 19)
    {
      Window localWindow = getWindow();
      localWindow.addFlags(67108864);
      localWindow.addFlags(134217728);
    }
    setContentView(2130903044);
    this.sharedprefs = getSharedPreferences("PREFS_MyStatus", 0);
    this.sharedprefs_editor = this.sharedprefs.edit();
    ActionBar localActionBar = getActionBar();
    localActionBar.setDisplayShowHomeEnabled(false);
    localActionBar.setTitle("");
    localActionBar.setDisplayHomeAsUpEnabled(true);
    localActionBar.setBackgroundDrawable(new ColorDrawable(0));
    this.mRes = getResources();
    int i = this.mRes.getDimensionPixelSize(2131099714);
    int j = this.mRes.getDimensionPixelSize(2131099715);
    ((ImageView)findViewById(Resources.getSystem().getIdentifier("up", "id", "android"))).setPadding(i, 0, j, 0);
    initFont();
    this.strokeTextView_title = ((StrokeTextView)findViewById(2131361813));
    if (isCJText((String)this.strokeTextView_title.getText()))
      this.strokeTextView_title.setTypeface(mTextFontChinese);
    while (true)
    {
      this.strokeTextView_title.setStrokeColor(this.mRes.getColor(2130968607));
      this.strokeTextView_title.setStrokeWidth(this.mRes.getDimension(2131099717));
      this.bg_level[1] = findViewById(2131361814);
      this.bg_level[2] = findViewById(2131361817);
      this.bg_level[3] = findViewById(2131361820);
      this.bg_level[4] = findViewById(2131361823);
      this.bg_level[5] = findViewById(2131361826);
      this.bg_level[6] = findViewById(2131361829);
      this.bg_level[7] = findViewById(2131361832);
      this.bg_level[8] = findViewById(2131361835);
      this.bg_level[9] = findViewById(2131361838);
      this.bg_level[10] = findViewById(2131361841);
      this.bg_level[11] = findViewById(2131361844);
      this.bg_level[12] = findViewById(2131361847);
      this.bg_level[13] = findViewById(2131361850);
      this.bg_level[14] = findViewById(2131361853);
      this.bg_level[15] = findViewById(2131361856);
      this.bg_level[16] = findViewById(2131361859);
      this.bg_level[17] = findViewById(2131361862);
      this.bg_level[18] = findViewById(2131361865);
      this.bg_level[19] = findViewById(2131361868);
      this.bg_level[20] = findViewById(2131361871);
      this.textView_level[1] = ((StrokeTextView)findViewById(2131361815));
      this.textView_level[2] = ((StrokeTextView)findViewById(2131361818));
      this.textView_level[3] = ((StrokeTextView)findViewById(2131361821));
      this.textView_level[4] = ((StrokeTextView)findViewById(2131361824));
      this.textView_level[5] = ((StrokeTextView)findViewById(2131361827));
      this.textView_level[6] = ((StrokeTextView)findViewById(2131361830));
      this.textView_level[7] = ((StrokeTextView)findViewById(2131361833));
      this.textView_level[8] = ((StrokeTextView)findViewById(2131361836));
      this.textView_level[9] = ((StrokeTextView)findViewById(2131361839));
      this.textView_level[10] = ((StrokeTextView)findViewById(2131361842));
      this.textView_level[11] = ((StrokeTextView)findViewById(2131361845));
      this.textView_level[12] = ((StrokeTextView)findViewById(2131361848));
      this.textView_level[13] = ((StrokeTextView)findViewById(2131361851));
      this.textView_level[14] = ((StrokeTextView)findViewById(2131361854));
      this.textView_level[15] = ((StrokeTextView)findViewById(2131361857));
      this.textView_level[16] = ((StrokeTextView)findViewById(2131361860));
      this.textView_level[17] = ((StrokeTextView)findViewById(2131361863));
      this.textView_level[18] = ((StrokeTextView)findViewById(2131361866));
      this.textView_level[19] = ((StrokeTextView)findViewById(2131361869));
      this.textView_level[20] = ((StrokeTextView)findViewById(2131361872));
      this.imageView_level[1] = ((ImageView)findViewById(2131361816));
      this.imageView_level[2] = ((ImageView)findViewById(2131361819));
      this.imageView_level[3] = ((ImageView)findViewById(2131361822));
      this.imageView_level[4] = ((ImageView)findViewById(2131361825));
      this.imageView_level[5] = ((ImageView)findViewById(2131361828));
      this.imageView_level[6] = ((ImageView)findViewById(2131361831));
      this.imageView_level[7] = ((ImageView)findViewById(2131361834));
      this.imageView_level[8] = ((ImageView)findViewById(2131361837));
      this.imageView_level[9] = ((ImageView)findViewById(2131361840));
      this.imageView_level[10] = ((ImageView)findViewById(2131361843));
      this.imageView_level[11] = ((ImageView)findViewById(2131361846));
      this.imageView_level[12] = ((ImageView)findViewById(2131361849));
      this.imageView_level[13] = ((ImageView)findViewById(2131361852));
      this.imageView_level[14] = ((ImageView)findViewById(2131361855));
      this.imageView_level[15] = ((ImageView)findViewById(2131361858));
      this.imageView_level[16] = ((ImageView)findViewById(2131361861));
      this.imageView_level[17] = ((ImageView)findViewById(2131361864));
      this.imageView_level[18] = ((ImageView)findViewById(2131361867));
      this.imageView_level[19] = ((ImageView)findViewById(2131361870));
      this.imageView_level[20] = ((ImageView)findViewById(2131361873));
      for (int k = 1; k <= 20; k++)
        this.textView_level[k].setTypeface(mTextFontEnglish);
      this.strokeTextView_title.setTypeface(mTextFontEnglish);
      this.strokeTextView_title.setTextSize(0, this.mRes.getDimensionPixelSize(2131099766));
    }
    for (int m = 1; m <= 20; m++)
    {
      final int n = m;
      this.textView_level[m].setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          SharedPreferences localSharedPreferences = GameLevel.this.sharedprefs;
          String str = "GameStatus[" + n + "]";
          int i;
          int j;
          if (n == 1)
          {
            i = 1;
            j = localSharedPreferences.getInt(str, i);
            if (j != 1)
              break label121;
            GameLevel.this.sharedprefs_editor.putInt("PlayedGameLevel", n);
            GameLevel.this.sharedprefs_editor.commit();
          }
          while (true)
          {
            GameLevel.this.setGameLevelStatus();
            GameLevel.this.start_Brick_Game(n);
            return;
            i = 4;
            break;
            label121: if (j == 2)
            {
              GameLevel.this.sharedprefs_editor.putInt("PlayedGameLevel", n);
              GameLevel.this.sharedprefs_editor.commit();
            }
            else if (j == 3)
            {
              GameLevel.this.imageView_level[n].setImageResource(2130837534);
              GameLevel.this.sharedprefs_editor.putInt("PlayedGameLevel", n);
              GameLevel.this.sharedprefs_editor.commit();
            }
            else if (j != 4);
          }
        }
      });
    }
    Log.d("HtcDotBreaker", "GameLevel.onCreate()\tend.");
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131296256, paramMenu);
    return true;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    if (i == 2131361914)
      startActivity(new Intent(this, MyStatus.class));
    while (i != 16908332)
      return super.onOptionsItemSelected(paramMenuItem);
    onBackPressed();
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    Log.d("HtcDotBreaker", "GameLevel.onResume()\tbegin.");
    initGameLevelStatus();
    setGameLevelStatus();
    if (this.sharedprefs.getBoolean("CongratulationMessage", false))
      showdialog_congratulation();
    Log.d("HtcDotBreaker", "GameLevel.onResume()\tend.");
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.GameLevel
 * JD-Core Version:    0.6.2
 */