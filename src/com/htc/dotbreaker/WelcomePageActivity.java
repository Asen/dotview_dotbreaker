package com.htc.dotbreaker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

public class WelcomePageActivity extends Activity
{
  CountDownTimer mCountDownTimer;

  public void onBackPressed()
  {
    Log.d("HtcDotBreaker", "WelcomePageActivity.onBackPressed()\tbegin.");
    super.onBackPressed();
    this.mCountDownTimer.cancel();
    Log.d("HtcDotBreaker", "WelcomePageActivity.onBackPressed()\tend.");
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().getDecorView().setSystemUiVisibility(0x1000 | (0x4 | (0x2 | getWindow().getDecorView().getSystemUiVisibility())));
    setContentView(2130903043);
    ((RelativeLayout)findViewById(2131361812)).addView(new DrawViewFromFile(this, BitmapFactory.decodeResource(getResources(), 2130837537)));
    this.mCountDownTimer = new CountDownTimer(2000L, 2000L)
    {
      public void onFinish()
      {
        WelcomePageActivity.this.finish();
        WelcomePageActivity.this.startActivity(new Intent(WelcomePageActivity.this, GameLevel.class));
      }

      public void onTick(long paramAnonymousLong)
      {
      }
    }
    .start();
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.WelcomePageActivity
 * JD-Core Version:    0.6.2
 */