package com.htc.dotbreaker;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import com.htc.dotbreaker.utils.DotBreakerUtils;
import com.htc.dotviewprocessing.WallpaperMaker;
import java.lang.reflect.Array;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class BrickView extends View
{
  static final int BACKGROUND = 0;
  static final int BALL = 10;
  static final int BLACK = 0;
  static int BLOCK_HEIGHT = 0;
  static int BLOCK_WIDTH = 0;
  static final int BLUE = 1;
  static final int BOARD = 6;
  static final int CYAN = 2;
  static final int DKGRAY = 3;
  static final int DarkenBackground = 35;
  static final int DarkenBackground_Color = -2013265920;
  static boolean F = false;
  static final int GAME_OVER = 10;
  static final int GRAY = 4;
  static final int GREEN = 5;
  static final int HEART = 8;
  static boolean[][][] HINT_ANIMATION;
  static int HINT_ANIMATION_LOC_X = 0;
  static int HINT_ANIMATION_LOC_Y = 0;
  static final int HINT_START = 11;
  static final int INDIGO = 33;
  static final int INDIGO_Color = -11861886;
  static final int KEEP_LIFE_INIT = 0;
  static final int KEEP_SCORE_INIT = -1;
  static final int LIFE = 8;
  public static final String LOG_PREFIX = "[BrickView] ";
  static final int LTGRAY = 6;
  static final int MAGENTA = 7;
  static boolean[][] MARK_EXIT;
  static boolean[][] MARK_EXIT_FOR_LOOK;
  static int MARK_EXIT_LOC_X = 0;
  static int MARK_EXIT_LOC_Y = 0;
  static boolean[][] MARK_PAUSE;
  static boolean[][] MARK_PAUSE_FOR_LOOK;
  static int MARK_PAUSE_LOC_X = 0;
  static int MARK_PAUSE_LOC_Y = 0;
  static boolean[][] MARK_START;
  static boolean[][] MARK_START_FOR_LOOK;
  static int MARK_START_LOC_X = 0;
  static int MARK_START_LOC_Y = 0;
  static final int MAX_GAME_LEVEL = 20;
  static final int Mark_Exit = 42;
  static final int Mark_Pause = 40;
  static final int Mark_Start = 41;
  static final int ORANGE = 29;
  static final int ORANGE_Color = -23296;
  static final int RED = 8;
  static final int SCORE = 40;
  static int SCREEN_WALL_BOTTOM = 0;
  static final int SCREEN_WALL_LEFT = 0;
  static int SCREEN_WALL_RIGHT = 0;
  static final int SCREEN_WALL_TOP = 0;
  static boolean[][][] START_ANIMATION = arrayOfBoolean102;
  static final int Score_Life_Line = 43;
  static boolean T = false;
  static final int TRANSPARENT = 9;
  static final int VIOLET = 34;
  static final int VIOLET_Color = -1146130;
  static final int WHITE = 10;
  static final int Word_GameOver = 36;
  static final int Word_Next = 39;
  static final int Word_Restart = 37;
  static final int Word_YouWin = 38;
  static final int YELLOW = 11;
  static Paint[] a_Paint;
  static Timer ball_Timer;
  static TimerTask ball_TimerTask;
  static int ball_speed = 0;
  static Timer board_Timer;
  static TimerTask board_TimerTask;
  static float board_moveAngle = 0.0F;
  static int board_speed = 0;
  static int brick_game_level = 0;
  static boolean game_level_complete = false;
  static boolean game_over = false;
  static boolean hint_animation = false;
  static int hint_animation_number = 0;
  static final int hint_animation_number_swipe_down_start = 5;
  static int keep_life;
  protected static int keep_score;
  static boolean mode_setting;
  private static boolean set_static_variable;
  static int start_animation_number;
  final int BALL_MOVE_UNIT_HORIZONTAL_NEGATIVE;
  final int BALL_MOVE_UNIT_HORIZONTAL_POSITIVE;
  final int BALL_MOVE_UNIT_VERTICAL_NEGATIVE;
  final int BALL_MOVE_UNIT_VERTICAL_POSITIVE;
  final int BLOCK_NUMBER_COLUMN;
  final int BLOCK_NUMBER_ROW;
  private final int BRICKS_AREA_BOTTOM;
  private final int BRICKS_AREA_LEFT;
  private final int BRICKS_AREA_RIGHT;
  private final int BRICKS_AREA_SIZE_HORIZONTAL;
  private final int BRICKS_AREA_SIZE_VERTICAL;
  private final int BRICKS_AREA_TOP;
  private final int COLOR_MASK;
  boolean[][] FAILING_DOWN_HEART;
  boolean[][] FAILING_DOWN_HEART_FOR_LOOK;
  final int KEEP_LIFE_DEFAULT;
  private int LIFE_BITS;
  final boolean[][] LIFE_HEART;
  final boolean[][] LIFE_HEART_FOR_LOOK;
  private final int MAX_LIFE;
  final int MAX_NUMBER_OF_BALLS;
  private int MAX_NUMBER_OF_BONUS;
  final int Mark_Exit_Color;
  final int Mark_Pause_Color;
  final int Mark_Start_Color;
  boolean[][][] NUMERIC_MORE_K;
  int NUMERIC_MORE_K_LOC_X;
  int NUMERIC_MORE_K_LOC_Y;
  private final int SCORE_ADD_LIFE_BONUS;
  private int SCORE_BITS;
  private final int SIX;
  private final int SPEED_BONUS;
  private int START_ANIMATION_LOC_X;
  private int START_ANIMATION_LOC_Y;
  final int Score_Life_Line_Color;
  int WALL_BOTTOM;
  int WALL_LEFT;
  int WALL_RIGHT;
  int WALL_TOP;
  final int Word_GameOver_Color;
  final int Word_Next_Color;
  final int Word_Restart_Color;
  final int Word_YouWin_Color;
  boolean[][][] a_Bricks_exist;
  Ball[] a_ball;
  protected BrickItem[] a_bonus;
  protected CountDownTimer[] a_bonus_timer;
  BrickItem[][] a_bricks;
  boolean[][][] a_numeric;
  Handler ball_TimerTask_Handler;
  boolean ball_alive;
  int ball_travelled;
  private Board board;
  Handler board_TimerTask_Handler;
  private int bonus_22BLUE;
  private boolean bonus_22BLUE_change_color;
  private boolean bonus_22RED;
  private boolean bonus_22YELLOW;
  private CountDownTimer bonus_item_timer_change_color;
  int bricks_Current_Rectangle_Boundary_Bottom;
  int bricks_Current_Rectangle_Boundary_Left;
  int bricks_Current_Rectangle_Boundary_Right;
  int bricks_Current_Rectangle_Boundary_Top;
  int bricks_shiftoffset_horizontal;
  int bricks_shiftoffset_vertical;
  final int current_game_level;
  int dot_pixel_height = this.res.getDimensionPixelSize(2131099739);
  int dot_pixel_width = this.res.getDimensionPixelSize(2131099738);
  boolean[][] game_over_word;
  boolean game_pause;
  CountDownTimer hint_animation_timer;
  int inner_frame_height = this.res.getDimensionPixelSize(2131099733);
  int inner_frame_margin_bottom = this.res.getDimensionPixelSize(2131099737);
  int inner_frame_margin_left = this.res.getDimensionPixelSize(2131099735);
  int inner_frame_margin_right = this.res.getDimensionPixelSize(2131099736);
  int inner_frame_margin_top = this.res.getDimensionPixelSize(2131099734);
  int inner_frame_width = this.res.getDimensionPixelSize(2131099732);
  Numeric life;
  Bitmap mBitmap4827;
  int mBricksHit;
  int number_of_balls_alive;
  private int number_of_bonus;
  CountDownTimer playTime_timer;
  Resources res = getResources();
  Numeric score;
  boolean start_animation;
  CountDownTimer start_animation_timer;
  float tmp_onTouchEvent_X;
  float tmp_onTouchEvent_Y;
  int tmp_playTime;
  private int tmp_score_add_life_check;

  static
  {
    F = false;
    boolean[][] arrayOfBoolean1 = new boolean[5][];
    boolean[] arrayOfBoolean2 = new boolean[5];
    arrayOfBoolean2[0] = T;
    arrayOfBoolean2[1] = T;
    arrayOfBoolean2[2] = F;
    arrayOfBoolean2[3] = T;
    arrayOfBoolean2[4] = T;
    arrayOfBoolean1[0] = arrayOfBoolean2;
    boolean[] arrayOfBoolean3 = new boolean[5];
    arrayOfBoolean3[0] = T;
    arrayOfBoolean3[1] = T;
    arrayOfBoolean3[2] = F;
    arrayOfBoolean3[3] = T;
    arrayOfBoolean3[4] = T;
    arrayOfBoolean1[1] = arrayOfBoolean3;
    boolean[] arrayOfBoolean4 = new boolean[5];
    arrayOfBoolean4[0] = T;
    arrayOfBoolean4[1] = T;
    arrayOfBoolean4[2] = F;
    arrayOfBoolean4[3] = T;
    arrayOfBoolean4[4] = T;
    arrayOfBoolean1[2] = arrayOfBoolean4;
    boolean[] arrayOfBoolean5 = new boolean[5];
    arrayOfBoolean5[0] = T;
    arrayOfBoolean5[1] = T;
    arrayOfBoolean5[2] = F;
    arrayOfBoolean5[3] = T;
    arrayOfBoolean5[4] = T;
    arrayOfBoolean1[3] = arrayOfBoolean5;
    boolean[] arrayOfBoolean6 = new boolean[5];
    arrayOfBoolean6[0] = T;
    arrayOfBoolean6[1] = T;
    arrayOfBoolean6[2] = F;
    arrayOfBoolean6[3] = T;
    arrayOfBoolean6[4] = T;
    arrayOfBoolean1[4] = arrayOfBoolean6;
    MARK_PAUSE_FOR_LOOK = arrayOfBoolean1;
    MARK_PAUSE = toConvertMatrix(MARK_PAUSE_FOR_LOOK);
    boolean[][] arrayOfBoolean7 = new boolean[7][];
    boolean[] arrayOfBoolean8 = new boolean[4];
    arrayOfBoolean8[0] = T;
    arrayOfBoolean8[1] = F;
    arrayOfBoolean8[2] = F;
    arrayOfBoolean8[3] = F;
    arrayOfBoolean7[0] = arrayOfBoolean8;
    boolean[] arrayOfBoolean9 = new boolean[4];
    arrayOfBoolean9[0] = T;
    arrayOfBoolean9[1] = T;
    arrayOfBoolean9[2] = F;
    arrayOfBoolean9[3] = F;
    arrayOfBoolean7[1] = arrayOfBoolean9;
    boolean[] arrayOfBoolean10 = new boolean[4];
    arrayOfBoolean10[0] = T;
    arrayOfBoolean10[1] = T;
    arrayOfBoolean10[2] = T;
    arrayOfBoolean10[3] = F;
    arrayOfBoolean7[2] = arrayOfBoolean10;
    boolean[] arrayOfBoolean11 = new boolean[4];
    arrayOfBoolean11[0] = T;
    arrayOfBoolean11[1] = T;
    arrayOfBoolean11[2] = T;
    arrayOfBoolean11[3] = T;
    arrayOfBoolean7[3] = arrayOfBoolean11;
    boolean[] arrayOfBoolean12 = new boolean[4];
    arrayOfBoolean12[0] = T;
    arrayOfBoolean12[1] = T;
    arrayOfBoolean12[2] = T;
    arrayOfBoolean12[3] = F;
    arrayOfBoolean7[4] = arrayOfBoolean12;
    boolean[] arrayOfBoolean13 = new boolean[4];
    arrayOfBoolean13[0] = T;
    arrayOfBoolean13[1] = T;
    arrayOfBoolean13[2] = F;
    arrayOfBoolean13[3] = F;
    arrayOfBoolean7[5] = arrayOfBoolean13;
    boolean[] arrayOfBoolean14 = new boolean[4];
    arrayOfBoolean14[0] = T;
    arrayOfBoolean14[1] = F;
    arrayOfBoolean14[2] = F;
    arrayOfBoolean14[3] = F;
    arrayOfBoolean7[6] = arrayOfBoolean14;
    MARK_START_FOR_LOOK = arrayOfBoolean7;
    MARK_START = toConvertMatrix(MARK_START_FOR_LOOK);
    boolean[][] arrayOfBoolean15 = new boolean[5][];
    boolean[] arrayOfBoolean16 = new boolean[5];
    arrayOfBoolean16[0] = T;
    arrayOfBoolean16[1] = F;
    arrayOfBoolean16[2] = F;
    arrayOfBoolean16[3] = F;
    arrayOfBoolean16[4] = T;
    arrayOfBoolean15[0] = arrayOfBoolean16;
    boolean[] arrayOfBoolean17 = new boolean[5];
    arrayOfBoolean17[0] = F;
    arrayOfBoolean17[1] = T;
    arrayOfBoolean17[2] = F;
    arrayOfBoolean17[3] = T;
    arrayOfBoolean17[4] = F;
    arrayOfBoolean15[1] = arrayOfBoolean17;
    boolean[] arrayOfBoolean18 = new boolean[5];
    arrayOfBoolean18[0] = F;
    arrayOfBoolean18[1] = F;
    arrayOfBoolean18[2] = T;
    arrayOfBoolean18[3] = F;
    arrayOfBoolean18[4] = F;
    arrayOfBoolean15[2] = arrayOfBoolean18;
    boolean[] arrayOfBoolean19 = new boolean[5];
    arrayOfBoolean19[0] = F;
    arrayOfBoolean19[1] = T;
    arrayOfBoolean19[2] = F;
    arrayOfBoolean19[3] = T;
    arrayOfBoolean19[4] = F;
    arrayOfBoolean15[3] = arrayOfBoolean19;
    boolean[] arrayOfBoolean20 = new boolean[5];
    arrayOfBoolean20[0] = T;
    arrayOfBoolean20[1] = F;
    arrayOfBoolean20[2] = F;
    arrayOfBoolean20[3] = F;
    arrayOfBoolean20[4] = T;
    arrayOfBoolean15[4] = arrayOfBoolean20;
    MARK_EXIT_FOR_LOOK = arrayOfBoolean15;
    MARK_EXIT = toConvertMatrix(MARK_EXIT_FOR_LOOK);
    hint_animation = false;
    hint_animation_number = 0;
    boolean[][][] arrayOfBoolean21 = new boolean[10][][];
    boolean[][] arrayOfBoolean22 = new boolean[7][];
    boolean[] arrayOfBoolean23 = new boolean[5];
    arrayOfBoolean23[0] = F;
    arrayOfBoolean23[1] = F;
    arrayOfBoolean23[2] = F;
    arrayOfBoolean23[3] = F;
    arrayOfBoolean23[4] = F;
    arrayOfBoolean22[0] = arrayOfBoolean23;
    boolean[] arrayOfBoolean24 = new boolean[5];
    arrayOfBoolean24[0] = F;
    arrayOfBoolean24[1] = F;
    arrayOfBoolean24[2] = F;
    arrayOfBoolean24[3] = F;
    arrayOfBoolean24[4] = F;
    arrayOfBoolean22[1] = arrayOfBoolean24;
    boolean[] arrayOfBoolean25 = new boolean[5];
    arrayOfBoolean25[0] = F;
    arrayOfBoolean25[1] = F;
    arrayOfBoolean25[2] = F;
    arrayOfBoolean25[3] = F;
    arrayOfBoolean25[4] = F;
    arrayOfBoolean22[2] = arrayOfBoolean25;
    boolean[] arrayOfBoolean26 = new boolean[5];
    arrayOfBoolean26[0] = F;
    arrayOfBoolean26[1] = F;
    arrayOfBoolean26[2] = F;
    arrayOfBoolean26[3] = F;
    arrayOfBoolean26[4] = F;
    arrayOfBoolean22[3] = arrayOfBoolean26;
    boolean[] arrayOfBoolean27 = new boolean[5];
    arrayOfBoolean27[0] = F;
    arrayOfBoolean27[1] = F;
    arrayOfBoolean27[2] = T;
    arrayOfBoolean27[3] = F;
    arrayOfBoolean27[4] = F;
    arrayOfBoolean22[4] = arrayOfBoolean27;
    boolean[] arrayOfBoolean28 = new boolean[5];
    arrayOfBoolean28[0] = F;
    arrayOfBoolean28[1] = T;
    arrayOfBoolean28[2] = F;
    arrayOfBoolean28[3] = T;
    arrayOfBoolean28[4] = F;
    arrayOfBoolean22[5] = arrayOfBoolean28;
    boolean[] arrayOfBoolean29 = new boolean[5];
    arrayOfBoolean29[0] = T;
    arrayOfBoolean29[1] = F;
    arrayOfBoolean29[2] = F;
    arrayOfBoolean29[3] = F;
    arrayOfBoolean29[4] = T;
    arrayOfBoolean22[6] = arrayOfBoolean29;
    arrayOfBoolean21[0] = arrayOfBoolean22;
    boolean[][] arrayOfBoolean30 = new boolean[7][];
    boolean[] arrayOfBoolean31 = new boolean[5];
    arrayOfBoolean31[0] = F;
    arrayOfBoolean31[1] = F;
    arrayOfBoolean31[2] = F;
    arrayOfBoolean31[3] = F;
    arrayOfBoolean31[4] = F;
    arrayOfBoolean30[0] = arrayOfBoolean31;
    boolean[] arrayOfBoolean32 = new boolean[5];
    arrayOfBoolean32[0] = F;
    arrayOfBoolean32[1] = F;
    arrayOfBoolean32[2] = F;
    arrayOfBoolean32[3] = F;
    arrayOfBoolean32[4] = F;
    arrayOfBoolean30[1] = arrayOfBoolean32;
    boolean[] arrayOfBoolean33 = new boolean[5];
    arrayOfBoolean33[0] = F;
    arrayOfBoolean33[1] = F;
    arrayOfBoolean33[2] = T;
    arrayOfBoolean33[3] = F;
    arrayOfBoolean33[4] = F;
    arrayOfBoolean30[2] = arrayOfBoolean33;
    boolean[] arrayOfBoolean34 = new boolean[5];
    arrayOfBoolean34[0] = F;
    arrayOfBoolean34[1] = T;
    arrayOfBoolean34[2] = F;
    arrayOfBoolean34[3] = T;
    arrayOfBoolean34[4] = F;
    arrayOfBoolean30[3] = arrayOfBoolean34;
    boolean[] arrayOfBoolean35 = new boolean[5];
    arrayOfBoolean35[0] = T;
    arrayOfBoolean35[1] = F;
    arrayOfBoolean35[2] = T;
    arrayOfBoolean35[3] = F;
    arrayOfBoolean35[4] = T;
    arrayOfBoolean30[4] = arrayOfBoolean35;
    boolean[] arrayOfBoolean36 = new boolean[5];
    arrayOfBoolean36[0] = F;
    arrayOfBoolean36[1] = T;
    arrayOfBoolean36[2] = F;
    arrayOfBoolean36[3] = T;
    arrayOfBoolean36[4] = F;
    arrayOfBoolean30[5] = arrayOfBoolean36;
    boolean[] arrayOfBoolean37 = new boolean[5];
    arrayOfBoolean37[0] = T;
    arrayOfBoolean37[1] = F;
    arrayOfBoolean37[2] = F;
    arrayOfBoolean37[3] = F;
    arrayOfBoolean37[4] = T;
    arrayOfBoolean30[6] = arrayOfBoolean37;
    arrayOfBoolean21[1] = arrayOfBoolean30;
    boolean[][] arrayOfBoolean38 = new boolean[7][];
    boolean[] arrayOfBoolean39 = new boolean[5];
    arrayOfBoolean39[0] = F;
    arrayOfBoolean39[1] = F;
    arrayOfBoolean39[2] = T;
    arrayOfBoolean39[3] = F;
    arrayOfBoolean39[4] = F;
    arrayOfBoolean38[0] = arrayOfBoolean39;
    boolean[] arrayOfBoolean40 = new boolean[5];
    arrayOfBoolean40[0] = F;
    arrayOfBoolean40[1] = T;
    arrayOfBoolean40[2] = F;
    arrayOfBoolean40[3] = T;
    arrayOfBoolean40[4] = F;
    arrayOfBoolean38[1] = arrayOfBoolean40;
    boolean[] arrayOfBoolean41 = new boolean[5];
    arrayOfBoolean41[0] = T;
    arrayOfBoolean41[1] = F;
    arrayOfBoolean41[2] = T;
    arrayOfBoolean41[3] = F;
    arrayOfBoolean41[4] = T;
    arrayOfBoolean38[2] = arrayOfBoolean41;
    boolean[] arrayOfBoolean42 = new boolean[5];
    arrayOfBoolean42[0] = F;
    arrayOfBoolean42[1] = T;
    arrayOfBoolean42[2] = F;
    arrayOfBoolean42[3] = T;
    arrayOfBoolean42[4] = F;
    arrayOfBoolean38[3] = arrayOfBoolean42;
    boolean[] arrayOfBoolean43 = new boolean[5];
    arrayOfBoolean43[0] = T;
    arrayOfBoolean43[1] = F;
    arrayOfBoolean43[2] = F;
    arrayOfBoolean43[3] = F;
    arrayOfBoolean43[4] = T;
    arrayOfBoolean38[4] = arrayOfBoolean43;
    boolean[] arrayOfBoolean44 = new boolean[5];
    arrayOfBoolean44[0] = F;
    arrayOfBoolean44[1] = F;
    arrayOfBoolean44[2] = F;
    arrayOfBoolean44[3] = F;
    arrayOfBoolean44[4] = F;
    arrayOfBoolean38[5] = arrayOfBoolean44;
    boolean[] arrayOfBoolean45 = new boolean[5];
    arrayOfBoolean45[0] = F;
    arrayOfBoolean45[1] = F;
    arrayOfBoolean45[2] = F;
    arrayOfBoolean45[3] = F;
    arrayOfBoolean45[4] = F;
    arrayOfBoolean38[6] = arrayOfBoolean45;
    arrayOfBoolean21[2] = arrayOfBoolean38;
    boolean[][] arrayOfBoolean46 = new boolean[7][];
    boolean[] arrayOfBoolean47 = new boolean[5];
    arrayOfBoolean47[0] = F;
    arrayOfBoolean47[1] = F;
    arrayOfBoolean47[2] = T;
    arrayOfBoolean47[3] = F;
    arrayOfBoolean47[4] = F;
    arrayOfBoolean46[0] = arrayOfBoolean47;
    boolean[] arrayOfBoolean48 = new boolean[5];
    arrayOfBoolean48[0] = F;
    arrayOfBoolean48[1] = T;
    arrayOfBoolean48[2] = F;
    arrayOfBoolean48[3] = T;
    arrayOfBoolean48[4] = F;
    arrayOfBoolean46[1] = arrayOfBoolean48;
    boolean[] arrayOfBoolean49 = new boolean[5];
    arrayOfBoolean49[0] = T;
    arrayOfBoolean49[1] = F;
    arrayOfBoolean49[2] = F;
    arrayOfBoolean49[3] = F;
    arrayOfBoolean49[4] = T;
    arrayOfBoolean46[2] = arrayOfBoolean49;
    boolean[] arrayOfBoolean50 = new boolean[5];
    arrayOfBoolean50[0] = F;
    arrayOfBoolean50[1] = F;
    arrayOfBoolean50[2] = F;
    arrayOfBoolean50[3] = F;
    arrayOfBoolean50[4] = F;
    arrayOfBoolean46[3] = arrayOfBoolean50;
    boolean[] arrayOfBoolean51 = new boolean[5];
    arrayOfBoolean51[0] = F;
    arrayOfBoolean51[1] = F;
    arrayOfBoolean51[2] = F;
    arrayOfBoolean51[3] = F;
    arrayOfBoolean51[4] = F;
    arrayOfBoolean46[4] = arrayOfBoolean51;
    boolean[] arrayOfBoolean52 = new boolean[5];
    arrayOfBoolean52[0] = F;
    arrayOfBoolean52[1] = F;
    arrayOfBoolean52[2] = F;
    arrayOfBoolean52[3] = F;
    arrayOfBoolean52[4] = F;
    arrayOfBoolean46[5] = arrayOfBoolean52;
    boolean[] arrayOfBoolean53 = new boolean[5];
    arrayOfBoolean53[0] = F;
    arrayOfBoolean53[1] = F;
    arrayOfBoolean53[2] = F;
    arrayOfBoolean53[3] = F;
    arrayOfBoolean53[4] = F;
    arrayOfBoolean46[6] = arrayOfBoolean53;
    arrayOfBoolean21[3] = arrayOfBoolean46;
    boolean[][] arrayOfBoolean54 = new boolean[7][];
    boolean[] arrayOfBoolean55 = new boolean[5];
    arrayOfBoolean55[0] = F;
    arrayOfBoolean55[1] = F;
    arrayOfBoolean55[2] = F;
    arrayOfBoolean55[3] = F;
    arrayOfBoolean55[4] = F;
    arrayOfBoolean54[0] = arrayOfBoolean55;
    boolean[] arrayOfBoolean56 = new boolean[5];
    arrayOfBoolean56[0] = F;
    arrayOfBoolean56[1] = F;
    arrayOfBoolean56[2] = F;
    arrayOfBoolean56[3] = F;
    arrayOfBoolean56[4] = F;
    arrayOfBoolean54[1] = arrayOfBoolean56;
    boolean[] arrayOfBoolean57 = new boolean[5];
    arrayOfBoolean57[0] = F;
    arrayOfBoolean57[1] = F;
    arrayOfBoolean57[2] = F;
    arrayOfBoolean57[3] = F;
    arrayOfBoolean57[4] = F;
    arrayOfBoolean54[2] = arrayOfBoolean57;
    boolean[] arrayOfBoolean58 = new boolean[5];
    arrayOfBoolean58[0] = F;
    arrayOfBoolean58[1] = F;
    arrayOfBoolean58[2] = F;
    arrayOfBoolean58[3] = F;
    arrayOfBoolean58[4] = F;
    arrayOfBoolean54[3] = arrayOfBoolean58;
    boolean[] arrayOfBoolean59 = new boolean[5];
    arrayOfBoolean59[0] = F;
    arrayOfBoolean59[1] = F;
    arrayOfBoolean59[2] = F;
    arrayOfBoolean59[3] = F;
    arrayOfBoolean59[4] = F;
    arrayOfBoolean54[4] = arrayOfBoolean59;
    boolean[] arrayOfBoolean60 = new boolean[5];
    arrayOfBoolean60[0] = F;
    arrayOfBoolean60[1] = F;
    arrayOfBoolean60[2] = F;
    arrayOfBoolean60[3] = F;
    arrayOfBoolean60[4] = F;
    arrayOfBoolean54[5] = arrayOfBoolean60;
    boolean[] arrayOfBoolean61 = new boolean[5];
    arrayOfBoolean61[0] = F;
    arrayOfBoolean61[1] = F;
    arrayOfBoolean61[2] = F;
    arrayOfBoolean61[3] = F;
    arrayOfBoolean61[4] = F;
    arrayOfBoolean54[6] = arrayOfBoolean61;
    arrayOfBoolean21[4] = arrayOfBoolean54;
    boolean[][] arrayOfBoolean62 = new boolean[7][];
    boolean[] arrayOfBoolean63 = new boolean[5];
    arrayOfBoolean63[0] = T;
    arrayOfBoolean63[1] = F;
    arrayOfBoolean63[2] = F;
    arrayOfBoolean63[3] = F;
    arrayOfBoolean63[4] = T;
    arrayOfBoolean62[0] = arrayOfBoolean63;
    boolean[] arrayOfBoolean64 = new boolean[5];
    arrayOfBoolean64[0] = F;
    arrayOfBoolean64[1] = T;
    arrayOfBoolean64[2] = F;
    arrayOfBoolean64[3] = T;
    arrayOfBoolean64[4] = F;
    arrayOfBoolean62[1] = arrayOfBoolean64;
    boolean[] arrayOfBoolean65 = new boolean[5];
    arrayOfBoolean65[0] = F;
    arrayOfBoolean65[1] = F;
    arrayOfBoolean65[2] = T;
    arrayOfBoolean65[3] = F;
    arrayOfBoolean65[4] = F;
    arrayOfBoolean62[2] = arrayOfBoolean65;
    boolean[] arrayOfBoolean66 = new boolean[5];
    arrayOfBoolean66[0] = F;
    arrayOfBoolean66[1] = F;
    arrayOfBoolean66[2] = F;
    arrayOfBoolean66[3] = F;
    arrayOfBoolean66[4] = F;
    arrayOfBoolean62[3] = arrayOfBoolean66;
    boolean[] arrayOfBoolean67 = new boolean[5];
    arrayOfBoolean67[0] = F;
    arrayOfBoolean67[1] = F;
    arrayOfBoolean67[2] = F;
    arrayOfBoolean67[3] = F;
    arrayOfBoolean67[4] = F;
    arrayOfBoolean62[4] = arrayOfBoolean67;
    boolean[] arrayOfBoolean68 = new boolean[5];
    arrayOfBoolean68[0] = F;
    arrayOfBoolean68[1] = F;
    arrayOfBoolean68[2] = F;
    arrayOfBoolean68[3] = F;
    arrayOfBoolean68[4] = F;
    arrayOfBoolean62[5] = arrayOfBoolean68;
    boolean[] arrayOfBoolean69 = new boolean[5];
    arrayOfBoolean69[0] = F;
    arrayOfBoolean69[1] = F;
    arrayOfBoolean69[2] = F;
    arrayOfBoolean69[3] = F;
    arrayOfBoolean69[4] = F;
    arrayOfBoolean62[6] = arrayOfBoolean69;
    arrayOfBoolean21[5] = arrayOfBoolean62;
    boolean[][] arrayOfBoolean70 = new boolean[7][];
    boolean[] arrayOfBoolean71 = new boolean[5];
    arrayOfBoolean71[0] = T;
    arrayOfBoolean71[1] = F;
    arrayOfBoolean71[2] = F;
    arrayOfBoolean71[3] = F;
    arrayOfBoolean71[4] = T;
    arrayOfBoolean70[0] = arrayOfBoolean71;
    boolean[] arrayOfBoolean72 = new boolean[5];
    arrayOfBoolean72[0] = F;
    arrayOfBoolean72[1] = T;
    arrayOfBoolean72[2] = F;
    arrayOfBoolean72[3] = T;
    arrayOfBoolean72[4] = F;
    arrayOfBoolean70[1] = arrayOfBoolean72;
    boolean[] arrayOfBoolean73 = new boolean[5];
    arrayOfBoolean73[0] = T;
    arrayOfBoolean73[1] = F;
    arrayOfBoolean73[2] = T;
    arrayOfBoolean73[3] = F;
    arrayOfBoolean73[4] = T;
    arrayOfBoolean70[2] = arrayOfBoolean73;
    boolean[] arrayOfBoolean74 = new boolean[5];
    arrayOfBoolean74[0] = F;
    arrayOfBoolean74[1] = T;
    arrayOfBoolean74[2] = F;
    arrayOfBoolean74[3] = T;
    arrayOfBoolean74[4] = F;
    arrayOfBoolean70[3] = arrayOfBoolean74;
    boolean[] arrayOfBoolean75 = new boolean[5];
    arrayOfBoolean75[0] = F;
    arrayOfBoolean75[1] = F;
    arrayOfBoolean75[2] = T;
    arrayOfBoolean75[3] = F;
    arrayOfBoolean75[4] = F;
    arrayOfBoolean70[4] = arrayOfBoolean75;
    boolean[] arrayOfBoolean76 = new boolean[5];
    arrayOfBoolean76[0] = F;
    arrayOfBoolean76[1] = F;
    arrayOfBoolean76[2] = F;
    arrayOfBoolean76[3] = F;
    arrayOfBoolean76[4] = F;
    arrayOfBoolean70[5] = arrayOfBoolean76;
    boolean[] arrayOfBoolean77 = new boolean[5];
    arrayOfBoolean77[0] = F;
    arrayOfBoolean77[1] = F;
    arrayOfBoolean77[2] = F;
    arrayOfBoolean77[3] = F;
    arrayOfBoolean77[4] = F;
    arrayOfBoolean70[6] = arrayOfBoolean77;
    arrayOfBoolean21[6] = arrayOfBoolean70;
    boolean[][] arrayOfBoolean78 = new boolean[7][];
    boolean[] arrayOfBoolean79 = new boolean[5];
    arrayOfBoolean79[0] = F;
    arrayOfBoolean79[1] = F;
    arrayOfBoolean79[2] = F;
    arrayOfBoolean79[3] = F;
    arrayOfBoolean79[4] = F;
    arrayOfBoolean78[0] = arrayOfBoolean79;
    boolean[] arrayOfBoolean80 = new boolean[5];
    arrayOfBoolean80[0] = F;
    arrayOfBoolean80[1] = F;
    arrayOfBoolean80[2] = F;
    arrayOfBoolean80[3] = F;
    arrayOfBoolean80[4] = F;
    arrayOfBoolean78[1] = arrayOfBoolean80;
    boolean[] arrayOfBoolean81 = new boolean[5];
    arrayOfBoolean81[0] = T;
    arrayOfBoolean81[1] = F;
    arrayOfBoolean81[2] = F;
    arrayOfBoolean81[3] = F;
    arrayOfBoolean81[4] = T;
    arrayOfBoolean78[2] = arrayOfBoolean81;
    boolean[] arrayOfBoolean82 = new boolean[5];
    arrayOfBoolean82[0] = F;
    arrayOfBoolean82[1] = T;
    arrayOfBoolean82[2] = F;
    arrayOfBoolean82[3] = T;
    arrayOfBoolean82[4] = F;
    arrayOfBoolean78[3] = arrayOfBoolean82;
    boolean[] arrayOfBoolean83 = new boolean[5];
    arrayOfBoolean83[0] = T;
    arrayOfBoolean83[1] = F;
    arrayOfBoolean83[2] = T;
    arrayOfBoolean83[3] = F;
    arrayOfBoolean83[4] = T;
    arrayOfBoolean78[4] = arrayOfBoolean83;
    boolean[] arrayOfBoolean84 = new boolean[5];
    arrayOfBoolean84[0] = F;
    arrayOfBoolean84[1] = T;
    arrayOfBoolean84[2] = F;
    arrayOfBoolean84[3] = T;
    arrayOfBoolean84[4] = F;
    arrayOfBoolean78[5] = arrayOfBoolean84;
    boolean[] arrayOfBoolean85 = new boolean[5];
    arrayOfBoolean85[0] = F;
    arrayOfBoolean85[1] = F;
    arrayOfBoolean85[2] = T;
    arrayOfBoolean85[3] = F;
    arrayOfBoolean85[4] = F;
    arrayOfBoolean78[6] = arrayOfBoolean85;
    arrayOfBoolean21[7] = arrayOfBoolean78;
    boolean[][] arrayOfBoolean86 = new boolean[7][];
    boolean[] arrayOfBoolean87 = new boolean[5];
    arrayOfBoolean87[0] = F;
    arrayOfBoolean87[1] = F;
    arrayOfBoolean87[2] = F;
    arrayOfBoolean87[3] = F;
    arrayOfBoolean87[4] = F;
    arrayOfBoolean86[0] = arrayOfBoolean87;
    boolean[] arrayOfBoolean88 = new boolean[5];
    arrayOfBoolean88[0] = F;
    arrayOfBoolean88[1] = F;
    arrayOfBoolean88[2] = F;
    arrayOfBoolean88[3] = F;
    arrayOfBoolean88[4] = F;
    arrayOfBoolean86[1] = arrayOfBoolean88;
    boolean[] arrayOfBoolean89 = new boolean[5];
    arrayOfBoolean89[0] = F;
    arrayOfBoolean89[1] = F;
    arrayOfBoolean89[2] = F;
    arrayOfBoolean89[3] = F;
    arrayOfBoolean89[4] = F;
    arrayOfBoolean86[2] = arrayOfBoolean89;
    boolean[] arrayOfBoolean90 = new boolean[5];
    arrayOfBoolean90[0] = F;
    arrayOfBoolean90[1] = F;
    arrayOfBoolean90[2] = F;
    arrayOfBoolean90[3] = F;
    arrayOfBoolean90[4] = F;
    arrayOfBoolean86[3] = arrayOfBoolean90;
    boolean[] arrayOfBoolean91 = new boolean[5];
    arrayOfBoolean91[0] = T;
    arrayOfBoolean91[1] = F;
    arrayOfBoolean91[2] = F;
    arrayOfBoolean91[3] = F;
    arrayOfBoolean91[4] = T;
    arrayOfBoolean86[4] = arrayOfBoolean91;
    boolean[] arrayOfBoolean92 = new boolean[5];
    arrayOfBoolean92[0] = F;
    arrayOfBoolean92[1] = T;
    arrayOfBoolean92[2] = F;
    arrayOfBoolean92[3] = T;
    arrayOfBoolean92[4] = F;
    arrayOfBoolean86[5] = arrayOfBoolean92;
    boolean[] arrayOfBoolean93 = new boolean[5];
    arrayOfBoolean93[0] = F;
    arrayOfBoolean93[1] = F;
    arrayOfBoolean93[2] = T;
    arrayOfBoolean93[3] = F;
    arrayOfBoolean93[4] = F;
    arrayOfBoolean86[6] = arrayOfBoolean93;
    arrayOfBoolean21[8] = arrayOfBoolean86;
    boolean[][] arrayOfBoolean94 = new boolean[7][];
    boolean[] arrayOfBoolean95 = new boolean[5];
    arrayOfBoolean95[0] = F;
    arrayOfBoolean95[1] = F;
    arrayOfBoolean95[2] = F;
    arrayOfBoolean95[3] = F;
    arrayOfBoolean95[4] = F;
    arrayOfBoolean94[0] = arrayOfBoolean95;
    boolean[] arrayOfBoolean96 = new boolean[5];
    arrayOfBoolean96[0] = F;
    arrayOfBoolean96[1] = F;
    arrayOfBoolean96[2] = F;
    arrayOfBoolean96[3] = F;
    arrayOfBoolean96[4] = F;
    arrayOfBoolean94[1] = arrayOfBoolean96;
    boolean[] arrayOfBoolean97 = new boolean[5];
    arrayOfBoolean97[0] = F;
    arrayOfBoolean97[1] = F;
    arrayOfBoolean97[2] = F;
    arrayOfBoolean97[3] = F;
    arrayOfBoolean97[4] = F;
    arrayOfBoolean94[2] = arrayOfBoolean97;
    boolean[] arrayOfBoolean98 = new boolean[5];
    arrayOfBoolean98[0] = F;
    arrayOfBoolean98[1] = F;
    arrayOfBoolean98[2] = F;
    arrayOfBoolean98[3] = F;
    arrayOfBoolean98[4] = F;
    arrayOfBoolean94[3] = arrayOfBoolean98;
    boolean[] arrayOfBoolean99 = new boolean[5];
    arrayOfBoolean99[0] = F;
    arrayOfBoolean99[1] = F;
    arrayOfBoolean99[2] = F;
    arrayOfBoolean99[3] = F;
    arrayOfBoolean99[4] = F;
    arrayOfBoolean94[4] = arrayOfBoolean99;
    boolean[] arrayOfBoolean100 = new boolean[5];
    arrayOfBoolean100[0] = F;
    arrayOfBoolean100[1] = F;
    arrayOfBoolean100[2] = F;
    arrayOfBoolean100[3] = F;
    arrayOfBoolean100[4] = F;
    arrayOfBoolean94[5] = arrayOfBoolean100;
    boolean[] arrayOfBoolean101 = new boolean[5];
    arrayOfBoolean101[0] = F;
    arrayOfBoolean101[1] = F;
    arrayOfBoolean101[2] = F;
    arrayOfBoolean101[3] = F;
    arrayOfBoolean101[4] = F;
    arrayOfBoolean94[6] = arrayOfBoolean101;
    arrayOfBoolean21[9] = arrayOfBoolean94;
    HINT_ANIMATION = arrayOfBoolean21;
    a_Paint = new Paint[44];
    board_speed = 40;
    board_moveAngle = 0.4F;
    ball_speed = 70;
    mode_setting = false;
    start_animation_number = 0;
    boolean[][][] arrayOfBoolean102 = new boolean[5][][];
    boolean[][] arrayOfBoolean103 = new boolean[7][];
    boolean[] arrayOfBoolean104 = new boolean[5];
    arrayOfBoolean104[0] = F;
    arrayOfBoolean104[1] = F;
    arrayOfBoolean104[2] = F;
    arrayOfBoolean104[3] = F;
    arrayOfBoolean104[4] = F;
    arrayOfBoolean103[0] = arrayOfBoolean104;
    boolean[] arrayOfBoolean105 = new boolean[5];
    arrayOfBoolean105[0] = F;
    arrayOfBoolean105[1] = F;
    arrayOfBoolean105[2] = F;
    arrayOfBoolean105[3] = F;
    arrayOfBoolean105[4] = F;
    arrayOfBoolean103[1] = arrayOfBoolean105;
    boolean[] arrayOfBoolean106 = new boolean[5];
    arrayOfBoolean106[0] = F;
    arrayOfBoolean106[1] = F;
    arrayOfBoolean106[2] = F;
    arrayOfBoolean106[3] = F;
    arrayOfBoolean106[4] = F;
    arrayOfBoolean103[2] = arrayOfBoolean106;
    boolean[] arrayOfBoolean107 = new boolean[5];
    arrayOfBoolean107[0] = F;
    arrayOfBoolean107[1] = F;
    arrayOfBoolean107[2] = F;
    arrayOfBoolean107[3] = F;
    arrayOfBoolean107[4] = F;
    arrayOfBoolean103[3] = arrayOfBoolean107;
    boolean[] arrayOfBoolean108 = new boolean[5];
    arrayOfBoolean108[0] = F;
    arrayOfBoolean108[1] = F;
    arrayOfBoolean108[2] = T;
    arrayOfBoolean108[3] = F;
    arrayOfBoolean108[4] = F;
    arrayOfBoolean103[4] = arrayOfBoolean108;
    boolean[] arrayOfBoolean109 = new boolean[5];
    arrayOfBoolean109[0] = F;
    arrayOfBoolean109[1] = T;
    arrayOfBoolean109[2] = F;
    arrayOfBoolean109[3] = T;
    arrayOfBoolean109[4] = F;
    arrayOfBoolean103[5] = arrayOfBoolean109;
    boolean[] arrayOfBoolean110 = new boolean[5];
    arrayOfBoolean110[0] = T;
    arrayOfBoolean110[1] = F;
    arrayOfBoolean110[2] = F;
    arrayOfBoolean110[3] = F;
    arrayOfBoolean110[4] = T;
    arrayOfBoolean103[6] = arrayOfBoolean110;
    arrayOfBoolean102[0] = arrayOfBoolean103;
    boolean[][] arrayOfBoolean111 = new boolean[7][];
    boolean[] arrayOfBoolean112 = new boolean[5];
    arrayOfBoolean112[0] = F;
    arrayOfBoolean112[1] = F;
    arrayOfBoolean112[2] = F;
    arrayOfBoolean112[3] = F;
    arrayOfBoolean112[4] = F;
    arrayOfBoolean111[0] = arrayOfBoolean112;
    boolean[] arrayOfBoolean113 = new boolean[5];
    arrayOfBoolean113[0] = F;
    arrayOfBoolean113[1] = F;
    arrayOfBoolean113[2] = F;
    arrayOfBoolean113[3] = F;
    arrayOfBoolean113[4] = F;
    arrayOfBoolean111[1] = arrayOfBoolean113;
    boolean[] arrayOfBoolean114 = new boolean[5];
    arrayOfBoolean114[0] = F;
    arrayOfBoolean114[1] = F;
    arrayOfBoolean114[2] = T;
    arrayOfBoolean114[3] = F;
    arrayOfBoolean114[4] = F;
    arrayOfBoolean111[2] = arrayOfBoolean114;
    boolean[] arrayOfBoolean115 = new boolean[5];
    arrayOfBoolean115[0] = F;
    arrayOfBoolean115[1] = T;
    arrayOfBoolean115[2] = F;
    arrayOfBoolean115[3] = T;
    arrayOfBoolean115[4] = F;
    arrayOfBoolean111[3] = arrayOfBoolean115;
    boolean[] arrayOfBoolean116 = new boolean[5];
    arrayOfBoolean116[0] = T;
    arrayOfBoolean116[1] = F;
    arrayOfBoolean116[2] = T;
    arrayOfBoolean116[3] = F;
    arrayOfBoolean116[4] = T;
    arrayOfBoolean111[4] = arrayOfBoolean116;
    boolean[] arrayOfBoolean117 = new boolean[5];
    arrayOfBoolean117[0] = F;
    arrayOfBoolean117[1] = T;
    arrayOfBoolean117[2] = F;
    arrayOfBoolean117[3] = T;
    arrayOfBoolean117[4] = F;
    arrayOfBoolean111[5] = arrayOfBoolean117;
    boolean[] arrayOfBoolean118 = new boolean[5];
    arrayOfBoolean118[0] = T;
    arrayOfBoolean118[1] = F;
    arrayOfBoolean118[2] = F;
    arrayOfBoolean118[3] = F;
    arrayOfBoolean118[4] = T;
    arrayOfBoolean111[6] = arrayOfBoolean118;
    arrayOfBoolean102[1] = arrayOfBoolean111;
    boolean[][] arrayOfBoolean119 = new boolean[7][];
    boolean[] arrayOfBoolean120 = new boolean[5];
    arrayOfBoolean120[0] = F;
    arrayOfBoolean120[1] = F;
    arrayOfBoolean120[2] = T;
    arrayOfBoolean120[3] = F;
    arrayOfBoolean120[4] = F;
    arrayOfBoolean119[0] = arrayOfBoolean120;
    boolean[] arrayOfBoolean121 = new boolean[5];
    arrayOfBoolean121[0] = F;
    arrayOfBoolean121[1] = T;
    arrayOfBoolean121[2] = F;
    arrayOfBoolean121[3] = T;
    arrayOfBoolean121[4] = F;
    arrayOfBoolean119[1] = arrayOfBoolean121;
    boolean[] arrayOfBoolean122 = new boolean[5];
    arrayOfBoolean122[0] = T;
    arrayOfBoolean122[1] = F;
    arrayOfBoolean122[2] = T;
    arrayOfBoolean122[3] = F;
    arrayOfBoolean122[4] = T;
    arrayOfBoolean119[2] = arrayOfBoolean122;
    boolean[] arrayOfBoolean123 = new boolean[5];
    arrayOfBoolean123[0] = F;
    arrayOfBoolean123[1] = T;
    arrayOfBoolean123[2] = F;
    arrayOfBoolean123[3] = T;
    arrayOfBoolean123[4] = F;
    arrayOfBoolean119[3] = arrayOfBoolean123;
    boolean[] arrayOfBoolean124 = new boolean[5];
    arrayOfBoolean124[0] = T;
    arrayOfBoolean124[1] = F;
    arrayOfBoolean124[2] = F;
    arrayOfBoolean124[3] = F;
    arrayOfBoolean124[4] = T;
    arrayOfBoolean119[4] = arrayOfBoolean124;
    boolean[] arrayOfBoolean125 = new boolean[5];
    arrayOfBoolean125[0] = F;
    arrayOfBoolean125[1] = F;
    arrayOfBoolean125[2] = F;
    arrayOfBoolean125[3] = F;
    arrayOfBoolean125[4] = F;
    arrayOfBoolean119[5] = arrayOfBoolean125;
    boolean[] arrayOfBoolean126 = new boolean[5];
    arrayOfBoolean126[0] = F;
    arrayOfBoolean126[1] = F;
    arrayOfBoolean126[2] = F;
    arrayOfBoolean126[3] = F;
    arrayOfBoolean126[4] = F;
    arrayOfBoolean119[6] = arrayOfBoolean126;
    arrayOfBoolean102[2] = arrayOfBoolean119;
    boolean[][] arrayOfBoolean127 = new boolean[7][];
    boolean[] arrayOfBoolean128 = new boolean[5];
    arrayOfBoolean128[0] = F;
    arrayOfBoolean128[1] = F;
    arrayOfBoolean128[2] = T;
    arrayOfBoolean128[3] = F;
    arrayOfBoolean128[4] = F;
    arrayOfBoolean127[0] = arrayOfBoolean128;
    boolean[] arrayOfBoolean129 = new boolean[5];
    arrayOfBoolean129[0] = F;
    arrayOfBoolean129[1] = T;
    arrayOfBoolean129[2] = F;
    arrayOfBoolean129[3] = T;
    arrayOfBoolean129[4] = F;
    arrayOfBoolean127[1] = arrayOfBoolean129;
    boolean[] arrayOfBoolean130 = new boolean[5];
    arrayOfBoolean130[0] = T;
    arrayOfBoolean130[1] = F;
    arrayOfBoolean130[2] = F;
    arrayOfBoolean130[3] = F;
    arrayOfBoolean130[4] = T;
    arrayOfBoolean127[2] = arrayOfBoolean130;
    boolean[] arrayOfBoolean131 = new boolean[5];
    arrayOfBoolean131[0] = F;
    arrayOfBoolean131[1] = F;
    arrayOfBoolean131[2] = F;
    arrayOfBoolean131[3] = F;
    arrayOfBoolean131[4] = F;
    arrayOfBoolean127[3] = arrayOfBoolean131;
    boolean[] arrayOfBoolean132 = new boolean[5];
    arrayOfBoolean132[0] = F;
    arrayOfBoolean132[1] = F;
    arrayOfBoolean132[2] = F;
    arrayOfBoolean132[3] = F;
    arrayOfBoolean132[4] = F;
    arrayOfBoolean127[4] = arrayOfBoolean132;
    boolean[] arrayOfBoolean133 = new boolean[5];
    arrayOfBoolean133[0] = F;
    arrayOfBoolean133[1] = F;
    arrayOfBoolean133[2] = F;
    arrayOfBoolean133[3] = F;
    arrayOfBoolean133[4] = F;
    arrayOfBoolean127[5] = arrayOfBoolean133;
    boolean[] arrayOfBoolean134 = new boolean[5];
    arrayOfBoolean134[0] = F;
    arrayOfBoolean134[1] = F;
    arrayOfBoolean134[2] = F;
    arrayOfBoolean134[3] = F;
    arrayOfBoolean134[4] = F;
    arrayOfBoolean127[6] = arrayOfBoolean134;
    arrayOfBoolean102[3] = arrayOfBoolean127;
    boolean[][] arrayOfBoolean135 = new boolean[7][];
    boolean[] arrayOfBoolean136 = new boolean[5];
    arrayOfBoolean136[0] = F;
    arrayOfBoolean136[1] = F;
    arrayOfBoolean136[2] = F;
    arrayOfBoolean136[3] = F;
    arrayOfBoolean136[4] = F;
    arrayOfBoolean135[0] = arrayOfBoolean136;
    boolean[] arrayOfBoolean137 = new boolean[5];
    arrayOfBoolean137[0] = F;
    arrayOfBoolean137[1] = F;
    arrayOfBoolean137[2] = F;
    arrayOfBoolean137[3] = F;
    arrayOfBoolean137[4] = F;
    arrayOfBoolean135[1] = arrayOfBoolean137;
    boolean[] arrayOfBoolean138 = new boolean[5];
    arrayOfBoolean138[0] = F;
    arrayOfBoolean138[1] = F;
    arrayOfBoolean138[2] = F;
    arrayOfBoolean138[3] = F;
    arrayOfBoolean138[4] = F;
    arrayOfBoolean135[2] = arrayOfBoolean138;
    boolean[] arrayOfBoolean139 = new boolean[5];
    arrayOfBoolean139[0] = F;
    arrayOfBoolean139[1] = F;
    arrayOfBoolean139[2] = F;
    arrayOfBoolean139[3] = F;
    arrayOfBoolean139[4] = F;
    arrayOfBoolean135[3] = arrayOfBoolean139;
    boolean[] arrayOfBoolean140 = new boolean[5];
    arrayOfBoolean140[0] = F;
    arrayOfBoolean140[1] = F;
    arrayOfBoolean140[2] = F;
    arrayOfBoolean140[3] = F;
    arrayOfBoolean140[4] = F;
    arrayOfBoolean135[4] = arrayOfBoolean140;
    boolean[] arrayOfBoolean141 = new boolean[5];
    arrayOfBoolean141[0] = F;
    arrayOfBoolean141[1] = F;
    arrayOfBoolean141[2] = F;
    arrayOfBoolean141[3] = F;
    arrayOfBoolean141[4] = F;
    arrayOfBoolean135[5] = arrayOfBoolean141;
    boolean[] arrayOfBoolean142 = new boolean[5];
    arrayOfBoolean142[0] = F;
    arrayOfBoolean142[1] = F;
    arrayOfBoolean142[2] = F;
    arrayOfBoolean142[3] = F;
    arrayOfBoolean142[4] = F;
    arrayOfBoolean135[6] = arrayOfBoolean142;
    arrayOfBoolean102[4] = arrayOfBoolean135;
  }

  public BrickView(Context paramContext, int paramInt1, int paramInt2)
  {
    super(paramContext);
    boolean[][][] arrayOfBoolean1 = new boolean[10][][];
    boolean[][] arrayOfBoolean2 = new boolean[5][];
    boolean[] arrayOfBoolean3 = new boolean[3];
    arrayOfBoolean3[0] = T;
    arrayOfBoolean3[1] = T;
    arrayOfBoolean3[2] = T;
    arrayOfBoolean2[0] = arrayOfBoolean3;
    boolean[] arrayOfBoolean4 = new boolean[3];
    arrayOfBoolean4[0] = T;
    arrayOfBoolean4[1] = F;
    arrayOfBoolean4[2] = T;
    arrayOfBoolean2[1] = arrayOfBoolean4;
    boolean[] arrayOfBoolean5 = new boolean[3];
    arrayOfBoolean5[0] = T;
    arrayOfBoolean5[1] = F;
    arrayOfBoolean5[2] = T;
    arrayOfBoolean2[2] = arrayOfBoolean5;
    boolean[] arrayOfBoolean6 = new boolean[3];
    arrayOfBoolean6[0] = T;
    arrayOfBoolean6[1] = F;
    arrayOfBoolean6[2] = T;
    arrayOfBoolean2[3] = arrayOfBoolean6;
    boolean[] arrayOfBoolean7 = new boolean[3];
    arrayOfBoolean7[0] = T;
    arrayOfBoolean7[1] = T;
    arrayOfBoolean7[2] = T;
    arrayOfBoolean2[4] = arrayOfBoolean7;
    arrayOfBoolean1[0] = arrayOfBoolean2;
    boolean[][] arrayOfBoolean8 = new boolean[5][];
    boolean[] arrayOfBoolean9 = new boolean[3];
    arrayOfBoolean9[0] = F;
    arrayOfBoolean9[1] = T;
    arrayOfBoolean9[2] = F;
    arrayOfBoolean8[0] = arrayOfBoolean9;
    boolean[] arrayOfBoolean10 = new boolean[3];
    arrayOfBoolean10[0] = F;
    arrayOfBoolean10[1] = T;
    arrayOfBoolean10[2] = F;
    arrayOfBoolean8[1] = arrayOfBoolean10;
    boolean[] arrayOfBoolean11 = new boolean[3];
    arrayOfBoolean11[0] = F;
    arrayOfBoolean11[1] = T;
    arrayOfBoolean11[2] = F;
    arrayOfBoolean8[2] = arrayOfBoolean11;
    boolean[] arrayOfBoolean12 = new boolean[3];
    arrayOfBoolean12[0] = F;
    arrayOfBoolean12[1] = T;
    arrayOfBoolean12[2] = F;
    arrayOfBoolean8[3] = arrayOfBoolean12;
    boolean[] arrayOfBoolean13 = new boolean[3];
    arrayOfBoolean13[0] = F;
    arrayOfBoolean13[1] = T;
    arrayOfBoolean13[2] = F;
    arrayOfBoolean8[4] = arrayOfBoolean13;
    arrayOfBoolean1[1] = arrayOfBoolean8;
    boolean[][] arrayOfBoolean14 = new boolean[5][];
    boolean[] arrayOfBoolean15 = new boolean[3];
    arrayOfBoolean15[0] = T;
    arrayOfBoolean15[1] = T;
    arrayOfBoolean15[2] = T;
    arrayOfBoolean14[0] = arrayOfBoolean15;
    boolean[] arrayOfBoolean16 = new boolean[3];
    arrayOfBoolean16[0] = F;
    arrayOfBoolean16[1] = F;
    arrayOfBoolean16[2] = T;
    arrayOfBoolean14[1] = arrayOfBoolean16;
    boolean[] arrayOfBoolean17 = new boolean[3];
    arrayOfBoolean17[0] = T;
    arrayOfBoolean17[1] = T;
    arrayOfBoolean17[2] = T;
    arrayOfBoolean14[2] = arrayOfBoolean17;
    boolean[] arrayOfBoolean18 = new boolean[3];
    arrayOfBoolean18[0] = T;
    arrayOfBoolean18[1] = F;
    arrayOfBoolean18[2] = F;
    arrayOfBoolean14[3] = arrayOfBoolean18;
    boolean[] arrayOfBoolean19 = new boolean[3];
    arrayOfBoolean19[0] = T;
    arrayOfBoolean19[1] = T;
    arrayOfBoolean19[2] = T;
    arrayOfBoolean14[4] = arrayOfBoolean19;
    arrayOfBoolean1[2] = arrayOfBoolean14;
    boolean[][] arrayOfBoolean20 = new boolean[5][];
    boolean[] arrayOfBoolean21 = new boolean[3];
    arrayOfBoolean21[0] = T;
    arrayOfBoolean21[1] = T;
    arrayOfBoolean21[2] = T;
    arrayOfBoolean20[0] = arrayOfBoolean21;
    boolean[] arrayOfBoolean22 = new boolean[3];
    arrayOfBoolean22[0] = F;
    arrayOfBoolean22[1] = F;
    arrayOfBoolean22[2] = T;
    arrayOfBoolean20[1] = arrayOfBoolean22;
    boolean[] arrayOfBoolean23 = new boolean[3];
    arrayOfBoolean23[0] = T;
    arrayOfBoolean23[1] = T;
    arrayOfBoolean23[2] = T;
    arrayOfBoolean20[2] = arrayOfBoolean23;
    boolean[] arrayOfBoolean24 = new boolean[3];
    arrayOfBoolean24[0] = F;
    arrayOfBoolean24[1] = F;
    arrayOfBoolean24[2] = T;
    arrayOfBoolean20[3] = arrayOfBoolean24;
    boolean[] arrayOfBoolean25 = new boolean[3];
    arrayOfBoolean25[0] = T;
    arrayOfBoolean25[1] = T;
    arrayOfBoolean25[2] = T;
    arrayOfBoolean20[4] = arrayOfBoolean25;
    arrayOfBoolean1[3] = arrayOfBoolean20;
    boolean[][] arrayOfBoolean26 = new boolean[5][];
    boolean[] arrayOfBoolean27 = new boolean[3];
    arrayOfBoolean27[0] = T;
    arrayOfBoolean27[1] = F;
    arrayOfBoolean27[2] = T;
    arrayOfBoolean26[0] = arrayOfBoolean27;
    boolean[] arrayOfBoolean28 = new boolean[3];
    arrayOfBoolean28[0] = T;
    arrayOfBoolean28[1] = F;
    arrayOfBoolean28[2] = T;
    arrayOfBoolean26[1] = arrayOfBoolean28;
    boolean[] arrayOfBoolean29 = new boolean[3];
    arrayOfBoolean29[0] = T;
    arrayOfBoolean29[1] = T;
    arrayOfBoolean29[2] = T;
    arrayOfBoolean26[2] = arrayOfBoolean29;
    boolean[] arrayOfBoolean30 = new boolean[3];
    arrayOfBoolean30[0] = F;
    arrayOfBoolean30[1] = F;
    arrayOfBoolean30[2] = T;
    arrayOfBoolean26[3] = arrayOfBoolean30;
    boolean[] arrayOfBoolean31 = new boolean[3];
    arrayOfBoolean31[0] = F;
    arrayOfBoolean31[1] = F;
    arrayOfBoolean31[2] = T;
    arrayOfBoolean26[4] = arrayOfBoolean31;
    arrayOfBoolean1[4] = arrayOfBoolean26;
    boolean[][] arrayOfBoolean32 = new boolean[5][];
    boolean[] arrayOfBoolean33 = new boolean[3];
    arrayOfBoolean33[0] = T;
    arrayOfBoolean33[1] = T;
    arrayOfBoolean33[2] = T;
    arrayOfBoolean32[0] = arrayOfBoolean33;
    boolean[] arrayOfBoolean34 = new boolean[3];
    arrayOfBoolean34[0] = T;
    arrayOfBoolean34[1] = F;
    arrayOfBoolean34[2] = F;
    arrayOfBoolean32[1] = arrayOfBoolean34;
    boolean[] arrayOfBoolean35 = new boolean[3];
    arrayOfBoolean35[0] = T;
    arrayOfBoolean35[1] = T;
    arrayOfBoolean35[2] = T;
    arrayOfBoolean32[2] = arrayOfBoolean35;
    boolean[] arrayOfBoolean36 = new boolean[3];
    arrayOfBoolean36[0] = F;
    arrayOfBoolean36[1] = F;
    arrayOfBoolean36[2] = T;
    arrayOfBoolean32[3] = arrayOfBoolean36;
    boolean[] arrayOfBoolean37 = new boolean[3];
    arrayOfBoolean37[0] = T;
    arrayOfBoolean37[1] = T;
    arrayOfBoolean37[2] = T;
    arrayOfBoolean32[4] = arrayOfBoolean37;
    arrayOfBoolean1[5] = arrayOfBoolean32;
    boolean[][] arrayOfBoolean38 = new boolean[5][];
    boolean[] arrayOfBoolean39 = new boolean[3];
    arrayOfBoolean39[0] = T;
    arrayOfBoolean39[1] = T;
    arrayOfBoolean39[2] = T;
    arrayOfBoolean38[0] = arrayOfBoolean39;
    boolean[] arrayOfBoolean40 = new boolean[3];
    arrayOfBoolean40[0] = T;
    arrayOfBoolean40[1] = F;
    arrayOfBoolean40[2] = F;
    arrayOfBoolean38[1] = arrayOfBoolean40;
    boolean[] arrayOfBoolean41 = new boolean[3];
    arrayOfBoolean41[0] = T;
    arrayOfBoolean41[1] = T;
    arrayOfBoolean41[2] = T;
    arrayOfBoolean38[2] = arrayOfBoolean41;
    boolean[] arrayOfBoolean42 = new boolean[3];
    arrayOfBoolean42[0] = T;
    arrayOfBoolean42[1] = F;
    arrayOfBoolean42[2] = T;
    arrayOfBoolean38[3] = arrayOfBoolean42;
    boolean[] arrayOfBoolean43 = new boolean[3];
    arrayOfBoolean43[0] = T;
    arrayOfBoolean43[1] = T;
    arrayOfBoolean43[2] = T;
    arrayOfBoolean38[4] = arrayOfBoolean43;
    arrayOfBoolean1[6] = arrayOfBoolean38;
    boolean[][] arrayOfBoolean44 = new boolean[5][];
    boolean[] arrayOfBoolean45 = new boolean[3];
    arrayOfBoolean45[0] = T;
    arrayOfBoolean45[1] = T;
    arrayOfBoolean45[2] = T;
    arrayOfBoolean44[0] = arrayOfBoolean45;
    boolean[] arrayOfBoolean46 = new boolean[3];
    arrayOfBoolean46[0] = T;
    arrayOfBoolean46[1] = F;
    arrayOfBoolean46[2] = T;
    arrayOfBoolean44[1] = arrayOfBoolean46;
    boolean[] arrayOfBoolean47 = new boolean[3];
    arrayOfBoolean47[0] = F;
    arrayOfBoolean47[1] = F;
    arrayOfBoolean47[2] = T;
    arrayOfBoolean44[2] = arrayOfBoolean47;
    boolean[] arrayOfBoolean48 = new boolean[3];
    arrayOfBoolean48[0] = F;
    arrayOfBoolean48[1] = F;
    arrayOfBoolean48[2] = T;
    arrayOfBoolean44[3] = arrayOfBoolean48;
    boolean[] arrayOfBoolean49 = new boolean[3];
    arrayOfBoolean49[0] = F;
    arrayOfBoolean49[1] = F;
    arrayOfBoolean49[2] = T;
    arrayOfBoolean44[4] = arrayOfBoolean49;
    arrayOfBoolean1[7] = arrayOfBoolean44;
    boolean[][] arrayOfBoolean50 = new boolean[5][];
    boolean[] arrayOfBoolean51 = new boolean[3];
    arrayOfBoolean51[0] = T;
    arrayOfBoolean51[1] = T;
    arrayOfBoolean51[2] = T;
    arrayOfBoolean50[0] = arrayOfBoolean51;
    boolean[] arrayOfBoolean52 = new boolean[3];
    arrayOfBoolean52[0] = T;
    arrayOfBoolean52[1] = F;
    arrayOfBoolean52[2] = T;
    arrayOfBoolean50[1] = arrayOfBoolean52;
    boolean[] arrayOfBoolean53 = new boolean[3];
    arrayOfBoolean53[0] = T;
    arrayOfBoolean53[1] = T;
    arrayOfBoolean53[2] = T;
    arrayOfBoolean50[2] = arrayOfBoolean53;
    boolean[] arrayOfBoolean54 = new boolean[3];
    arrayOfBoolean54[0] = T;
    arrayOfBoolean54[1] = F;
    arrayOfBoolean54[2] = T;
    arrayOfBoolean50[3] = arrayOfBoolean54;
    boolean[] arrayOfBoolean55 = new boolean[3];
    arrayOfBoolean55[0] = T;
    arrayOfBoolean55[1] = T;
    arrayOfBoolean55[2] = T;
    arrayOfBoolean50[4] = arrayOfBoolean55;
    arrayOfBoolean1[8] = arrayOfBoolean50;
    boolean[][] arrayOfBoolean56 = new boolean[5][];
    boolean[] arrayOfBoolean57 = new boolean[3];
    arrayOfBoolean57[0] = T;
    arrayOfBoolean57[1] = T;
    arrayOfBoolean57[2] = T;
    arrayOfBoolean56[0] = arrayOfBoolean57;
    boolean[] arrayOfBoolean58 = new boolean[3];
    arrayOfBoolean58[0] = T;
    arrayOfBoolean58[1] = F;
    arrayOfBoolean58[2] = T;
    arrayOfBoolean56[1] = arrayOfBoolean58;
    boolean[] arrayOfBoolean59 = new boolean[3];
    arrayOfBoolean59[0] = T;
    arrayOfBoolean59[1] = T;
    arrayOfBoolean59[2] = T;
    arrayOfBoolean56[2] = arrayOfBoolean59;
    boolean[] arrayOfBoolean60 = new boolean[3];
    arrayOfBoolean60[0] = F;
    arrayOfBoolean60[1] = F;
    arrayOfBoolean60[2] = T;
    arrayOfBoolean56[3] = arrayOfBoolean60;
    boolean[] arrayOfBoolean61 = new boolean[3];
    arrayOfBoolean61[0] = T;
    arrayOfBoolean61[1] = T;
    arrayOfBoolean61[2] = T;
    arrayOfBoolean56[4] = arrayOfBoolean61;
    arrayOfBoolean1[9] = arrayOfBoolean56;
    this.a_numeric = arrayOfBoolean1;
    boolean[][][] arrayOfBoolean62 = new boolean[2][][];
    boolean[][] arrayOfBoolean63 = new boolean[5][];
    boolean[] arrayOfBoolean64 = new boolean[3];
    arrayOfBoolean64[0] = F;
    arrayOfBoolean64[1] = F;
    arrayOfBoolean64[2] = F;
    arrayOfBoolean63[0] = arrayOfBoolean64;
    boolean[] arrayOfBoolean65 = new boolean[3];
    arrayOfBoolean65[0] = F;
    arrayOfBoolean65[1] = T;
    arrayOfBoolean65[2] = F;
    arrayOfBoolean63[1] = arrayOfBoolean65;
    boolean[] arrayOfBoolean66 = new boolean[3];
    arrayOfBoolean66[0] = T;
    arrayOfBoolean66[1] = T;
    arrayOfBoolean66[2] = T;
    arrayOfBoolean63[2] = arrayOfBoolean66;
    boolean[] arrayOfBoolean67 = new boolean[3];
    arrayOfBoolean67[0] = F;
    arrayOfBoolean67[1] = T;
    arrayOfBoolean67[2] = F;
    arrayOfBoolean63[3] = arrayOfBoolean67;
    boolean[] arrayOfBoolean68 = new boolean[3];
    arrayOfBoolean68[0] = F;
    arrayOfBoolean68[1] = F;
    arrayOfBoolean68[2] = F;
    arrayOfBoolean63[4] = arrayOfBoolean68;
    arrayOfBoolean62[0] = arrayOfBoolean63;
    boolean[][] arrayOfBoolean69 = new boolean[5][];
    boolean[] arrayOfBoolean70 = new boolean[3];
    arrayOfBoolean70[0] = T;
    arrayOfBoolean70[1] = F;
    arrayOfBoolean70[2] = F;
    arrayOfBoolean69[0] = arrayOfBoolean70;
    boolean[] arrayOfBoolean71 = new boolean[3];
    arrayOfBoolean71[0] = T;
    arrayOfBoolean71[1] = F;
    arrayOfBoolean71[2] = T;
    arrayOfBoolean69[1] = arrayOfBoolean71;
    boolean[] arrayOfBoolean72 = new boolean[3];
    arrayOfBoolean72[0] = T;
    arrayOfBoolean72[1] = T;
    arrayOfBoolean72[2] = F;
    arrayOfBoolean69[2] = arrayOfBoolean72;
    boolean[] arrayOfBoolean73 = new boolean[3];
    arrayOfBoolean73[0] = T;
    arrayOfBoolean73[1] = F;
    arrayOfBoolean73[2] = T;
    arrayOfBoolean69[3] = arrayOfBoolean73;
    boolean[] arrayOfBoolean74 = new boolean[3];
    arrayOfBoolean74[0] = T;
    arrayOfBoolean74[1] = F;
    arrayOfBoolean74[2] = T;
    arrayOfBoolean69[4] = arrayOfBoolean74;
    arrayOfBoolean62[1] = arrayOfBoolean69;
    this.NUMERIC_MORE_K = arrayOfBoolean62;
    this.NUMERIC_MORE_K_LOC_X = (this.inner_frame_margin_left + this.res.getDimensionPixelSize(2131099673) * this.dot_pixel_width);
    this.NUMERIC_MORE_K_LOC_Y = (this.inner_frame_margin_top + this.res.getDimensionPixelSize(2131099674) * this.dot_pixel_height);
    this.Word_GameOver_Color = this.res.getColor(2130968608);
    this.Word_Restart_Color = this.res.getColor(2130968609);
    this.Word_YouWin_Color = this.res.getColor(2130968610);
    this.Word_Next_Color = this.res.getColor(2130968611);
    this.Mark_Pause_Color = this.res.getColor(2130968612);
    this.Mark_Start_Color = this.res.getColor(2130968613);
    this.Mark_Exit_Color = this.res.getColor(2130968614);
    this.Score_Life_Line_Color = this.res.getColor(2130968615);
    this.COLOR_MASK = 16777215;
    this.BLOCK_NUMBER_ROW = this.res.getDimensionPixelSize(2131099664);
    this.BLOCK_NUMBER_COLUMN = this.res.getDimensionPixelSize(2131099665);
    this.SCORE_BITS = 4;
    this.SCORE_ADD_LIFE_BONUS = 50;
    this.MAX_LIFE = 20;
    this.tmp_score_add_life_check = 0;
    this.LIFE_BITS = 2;
    boolean[][] arrayOfBoolean75 = new boolean[5][];
    boolean[] arrayOfBoolean76 = new boolean[5];
    arrayOfBoolean76[0] = F;
    arrayOfBoolean76[1] = T;
    arrayOfBoolean76[2] = F;
    arrayOfBoolean76[3] = T;
    arrayOfBoolean76[4] = F;
    arrayOfBoolean75[0] = arrayOfBoolean76;
    boolean[] arrayOfBoolean77 = new boolean[5];
    arrayOfBoolean77[0] = T;
    arrayOfBoolean77[1] = T;
    arrayOfBoolean77[2] = T;
    arrayOfBoolean77[3] = T;
    arrayOfBoolean77[4] = T;
    arrayOfBoolean75[1] = arrayOfBoolean77;
    boolean[] arrayOfBoolean78 = new boolean[5];
    arrayOfBoolean78[0] = T;
    arrayOfBoolean78[1] = T;
    arrayOfBoolean78[2] = T;
    arrayOfBoolean78[3] = T;
    arrayOfBoolean78[4] = T;
    arrayOfBoolean75[2] = arrayOfBoolean78;
    boolean[] arrayOfBoolean79 = new boolean[5];
    arrayOfBoolean79[0] = F;
    arrayOfBoolean79[1] = T;
    arrayOfBoolean79[2] = T;
    arrayOfBoolean79[3] = T;
    arrayOfBoolean79[4] = F;
    arrayOfBoolean75[3] = arrayOfBoolean79;
    boolean[] arrayOfBoolean80 = new boolean[5];
    arrayOfBoolean80[0] = F;
    arrayOfBoolean80[1] = F;
    arrayOfBoolean80[2] = T;
    arrayOfBoolean80[3] = F;
    arrayOfBoolean80[4] = F;
    arrayOfBoolean75[4] = arrayOfBoolean80;
    this.LIFE_HEART_FOR_LOOK = arrayOfBoolean75;
    this.LIFE_HEART = toConvertMatrix(this.LIFE_HEART_FOR_LOOK);
    this.KEEP_LIFE_DEFAULT = 5;
    this.BRICKS_AREA_SIZE_HORIZONTAL = this.res.getDimensionPixelSize(2131099666);
    this.BRICKS_AREA_SIZE_VERTICAL = this.res.getDimensionPixelSize(2131099666);
    this.a_bricks = ((BrickItem[][])Array.newInstance(BrickItem.class, new int[] { this.BRICKS_AREA_SIZE_HORIZONTAL, this.BRICKS_AREA_SIZE_VERTICAL }));
    this.a_ball = new Ball[4];
    this.MAX_NUMBER_OF_BALLS = 4;
    this.number_of_balls_alive = 1;
    this.ball_travelled = 0;
    this.a_bonus = new BrickItem[7];
    this.a_bonus_timer = new CountDownTimer[7];
    this.MAX_NUMBER_OF_BONUS = 5;
    this.number_of_bonus = 0;
    this.SIX = 6;
    this.SPEED_BONUS = 100;
    this.bonus_22YELLOW = false;
    this.bonus_22RED = false;
    this.bonus_22BLUE_change_color = false;
    this.bonus_22BLUE = 1;
    boolean[][] arrayOfBoolean81 = new boolean[4][];
    boolean[] arrayOfBoolean82 = new boolean[5];
    arrayOfBoolean82[0] = F;
    arrayOfBoolean82[1] = T;
    arrayOfBoolean82[2] = F;
    arrayOfBoolean82[3] = T;
    arrayOfBoolean82[4] = F;
    arrayOfBoolean81[0] = arrayOfBoolean82;
    boolean[] arrayOfBoolean83 = new boolean[5];
    arrayOfBoolean83[0] = T;
    arrayOfBoolean83[1] = T;
    arrayOfBoolean83[2] = T;
    arrayOfBoolean83[3] = T;
    arrayOfBoolean83[4] = T;
    arrayOfBoolean81[1] = arrayOfBoolean83;
    boolean[] arrayOfBoolean84 = new boolean[5];
    arrayOfBoolean84[0] = F;
    arrayOfBoolean84[1] = T;
    arrayOfBoolean84[2] = T;
    arrayOfBoolean84[3] = T;
    arrayOfBoolean84[4] = F;
    arrayOfBoolean81[2] = arrayOfBoolean84;
    boolean[] arrayOfBoolean85 = new boolean[5];
    arrayOfBoolean85[0] = F;
    arrayOfBoolean85[1] = F;
    arrayOfBoolean85[2] = T;
    arrayOfBoolean85[3] = F;
    arrayOfBoolean85[4] = F;
    arrayOfBoolean81[3] = arrayOfBoolean85;
    this.FAILING_DOWN_HEART_FOR_LOOK = arrayOfBoolean81;
    this.FAILING_DOWN_HEART = toConvertMatrix(this.FAILING_DOWN_HEART_FOR_LOOK);
    this.start_animation = false;
    boolean[][] arrayOfBoolean86 = new boolean[27][];
    boolean[] arrayOfBoolean87 = new boolean[27];
    arrayOfBoolean87[0] = F;
    arrayOfBoolean87[1] = F;
    arrayOfBoolean87[2] = F;
    arrayOfBoolean87[3] = F;
    arrayOfBoolean87[4] = F;
    arrayOfBoolean87[5] = F;
    arrayOfBoolean87[6] = F;
    arrayOfBoolean87[7] = F;
    arrayOfBoolean87[8] = F;
    arrayOfBoolean87[9] = F;
    arrayOfBoolean87[10] = F;
    arrayOfBoolean87[11] = F;
    arrayOfBoolean87[12] = F;
    arrayOfBoolean87[13] = F;
    arrayOfBoolean87[14] = F;
    arrayOfBoolean87[15] = F;
    arrayOfBoolean87[16] = F;
    arrayOfBoolean87[17] = F;
    arrayOfBoolean87[18] = F;
    arrayOfBoolean87[19] = F;
    arrayOfBoolean87[20] = F;
    arrayOfBoolean87[21] = F;
    arrayOfBoolean87[22] = F;
    arrayOfBoolean87[23] = F;
    arrayOfBoolean87[24] = F;
    arrayOfBoolean87[25] = F;
    arrayOfBoolean87[26] = F;
    arrayOfBoolean86[0] = arrayOfBoolean87;
    boolean[] arrayOfBoolean88 = new boolean[27];
    arrayOfBoolean88[0] = F;
    arrayOfBoolean88[1] = F;
    arrayOfBoolean88[2] = F;
    arrayOfBoolean88[3] = F;
    arrayOfBoolean88[4] = F;
    arrayOfBoolean88[5] = F;
    arrayOfBoolean88[6] = F;
    arrayOfBoolean88[7] = F;
    arrayOfBoolean88[8] = F;
    arrayOfBoolean88[9] = F;
    arrayOfBoolean88[10] = F;
    arrayOfBoolean88[11] = F;
    arrayOfBoolean88[12] = F;
    arrayOfBoolean88[13] = F;
    arrayOfBoolean88[14] = F;
    arrayOfBoolean88[15] = F;
    arrayOfBoolean88[16] = F;
    arrayOfBoolean88[17] = F;
    arrayOfBoolean88[18] = F;
    arrayOfBoolean88[19] = F;
    arrayOfBoolean88[20] = F;
    arrayOfBoolean88[21] = F;
    arrayOfBoolean88[22] = F;
    arrayOfBoolean88[23] = F;
    arrayOfBoolean88[24] = F;
    arrayOfBoolean88[25] = F;
    arrayOfBoolean88[26] = F;
    arrayOfBoolean86[1] = arrayOfBoolean88;
    boolean[] arrayOfBoolean89 = new boolean[27];
    arrayOfBoolean89[0] = F;
    arrayOfBoolean89[1] = F;
    arrayOfBoolean89[2] = F;
    arrayOfBoolean89[3] = F;
    arrayOfBoolean89[4] = F;
    arrayOfBoolean89[5] = F;
    arrayOfBoolean89[6] = F;
    arrayOfBoolean89[7] = F;
    arrayOfBoolean89[8] = F;
    arrayOfBoolean89[9] = F;
    arrayOfBoolean89[10] = F;
    arrayOfBoolean89[11] = F;
    arrayOfBoolean89[12] = F;
    arrayOfBoolean89[13] = F;
    arrayOfBoolean89[14] = F;
    arrayOfBoolean89[15] = F;
    arrayOfBoolean89[16] = F;
    arrayOfBoolean89[17] = F;
    arrayOfBoolean89[18] = F;
    arrayOfBoolean89[19] = F;
    arrayOfBoolean89[20] = F;
    arrayOfBoolean89[21] = F;
    arrayOfBoolean89[22] = F;
    arrayOfBoolean89[23] = F;
    arrayOfBoolean89[24] = F;
    arrayOfBoolean89[25] = F;
    arrayOfBoolean89[26] = F;
    arrayOfBoolean86[2] = arrayOfBoolean89;
    boolean[] arrayOfBoolean90 = new boolean[27];
    arrayOfBoolean90[0] = F;
    arrayOfBoolean90[1] = F;
    arrayOfBoolean90[2] = F;
    arrayOfBoolean90[3] = F;
    arrayOfBoolean90[4] = F;
    arrayOfBoolean90[5] = F;
    arrayOfBoolean90[6] = F;
    arrayOfBoolean90[7] = F;
    arrayOfBoolean90[8] = F;
    arrayOfBoolean90[9] = F;
    arrayOfBoolean90[10] = F;
    arrayOfBoolean90[11] = F;
    arrayOfBoolean90[12] = F;
    arrayOfBoolean90[13] = F;
    arrayOfBoolean90[14] = F;
    arrayOfBoolean90[15] = F;
    arrayOfBoolean90[16] = F;
    arrayOfBoolean90[17] = F;
    arrayOfBoolean90[18] = F;
    arrayOfBoolean90[19] = F;
    arrayOfBoolean90[20] = F;
    arrayOfBoolean90[21] = F;
    arrayOfBoolean90[22] = F;
    arrayOfBoolean90[23] = F;
    arrayOfBoolean90[24] = F;
    arrayOfBoolean90[25] = F;
    arrayOfBoolean90[26] = F;
    arrayOfBoolean86[3] = arrayOfBoolean90;
    boolean[] arrayOfBoolean91 = new boolean[27];
    arrayOfBoolean91[0] = F;
    arrayOfBoolean91[1] = F;
    arrayOfBoolean91[2] = F;
    arrayOfBoolean91[3] = F;
    arrayOfBoolean91[4] = F;
    arrayOfBoolean91[5] = F;
    arrayOfBoolean91[6] = F;
    arrayOfBoolean91[7] = F;
    arrayOfBoolean91[8] = F;
    arrayOfBoolean91[9] = F;
    arrayOfBoolean91[10] = F;
    arrayOfBoolean91[11] = F;
    arrayOfBoolean91[12] = F;
    arrayOfBoolean91[13] = F;
    arrayOfBoolean91[14] = F;
    arrayOfBoolean91[15] = F;
    arrayOfBoolean91[16] = F;
    arrayOfBoolean91[17] = F;
    arrayOfBoolean91[18] = F;
    arrayOfBoolean91[19] = F;
    arrayOfBoolean91[20] = F;
    arrayOfBoolean91[21] = F;
    arrayOfBoolean91[22] = F;
    arrayOfBoolean91[23] = F;
    arrayOfBoolean91[24] = F;
    arrayOfBoolean91[25] = F;
    arrayOfBoolean91[26] = F;
    arrayOfBoolean86[4] = arrayOfBoolean91;
    boolean[] arrayOfBoolean92 = new boolean[27];
    arrayOfBoolean92[0] = F;
    arrayOfBoolean92[1] = F;
    arrayOfBoolean92[2] = F;
    arrayOfBoolean92[3] = F;
    arrayOfBoolean92[4] = F;
    arrayOfBoolean92[5] = F;
    arrayOfBoolean92[6] = F;
    arrayOfBoolean92[7] = F;
    arrayOfBoolean92[8] = F;
    arrayOfBoolean92[9] = F;
    arrayOfBoolean92[10] = F;
    arrayOfBoolean92[11] = F;
    arrayOfBoolean92[12] = F;
    arrayOfBoolean92[13] = F;
    arrayOfBoolean92[14] = F;
    arrayOfBoolean92[15] = F;
    arrayOfBoolean92[16] = F;
    arrayOfBoolean92[17] = F;
    arrayOfBoolean92[18] = F;
    arrayOfBoolean92[19] = F;
    arrayOfBoolean92[20] = F;
    arrayOfBoolean92[21] = F;
    arrayOfBoolean92[22] = F;
    arrayOfBoolean92[23] = F;
    arrayOfBoolean92[24] = F;
    arrayOfBoolean92[25] = F;
    arrayOfBoolean92[26] = F;
    arrayOfBoolean86[5] = arrayOfBoolean92;
    boolean[] arrayOfBoolean93 = new boolean[27];
    arrayOfBoolean93[0] = F;
    arrayOfBoolean93[1] = F;
    arrayOfBoolean93[2] = F;
    arrayOfBoolean93[3] = F;
    arrayOfBoolean93[4] = F;
    arrayOfBoolean93[5] = F;
    arrayOfBoolean93[6] = F;
    arrayOfBoolean93[7] = F;
    arrayOfBoolean93[8] = F;
    arrayOfBoolean93[9] = F;
    arrayOfBoolean93[10] = F;
    arrayOfBoolean93[11] = F;
    arrayOfBoolean93[12] = F;
    arrayOfBoolean93[13] = F;
    arrayOfBoolean93[14] = F;
    arrayOfBoolean93[15] = F;
    arrayOfBoolean93[16] = F;
    arrayOfBoolean93[17] = F;
    arrayOfBoolean93[18] = F;
    arrayOfBoolean93[19] = F;
    arrayOfBoolean93[20] = F;
    arrayOfBoolean93[21] = F;
    arrayOfBoolean93[22] = F;
    arrayOfBoolean93[23] = F;
    arrayOfBoolean93[24] = F;
    arrayOfBoolean93[25] = F;
    arrayOfBoolean93[26] = F;
    arrayOfBoolean86[6] = arrayOfBoolean93;
    boolean[] arrayOfBoolean94 = new boolean[27];
    arrayOfBoolean94[0] = F;
    arrayOfBoolean94[1] = F;
    arrayOfBoolean94[2] = F;
    arrayOfBoolean94[3] = F;
    arrayOfBoolean94[4] = F;
    arrayOfBoolean94[5] = F;
    arrayOfBoolean94[6] = F;
    arrayOfBoolean94[7] = F;
    arrayOfBoolean94[8] = F;
    arrayOfBoolean94[9] = F;
    arrayOfBoolean94[10] = F;
    arrayOfBoolean94[11] = F;
    arrayOfBoolean94[12] = F;
    arrayOfBoolean94[13] = F;
    arrayOfBoolean94[14] = F;
    arrayOfBoolean94[15] = F;
    arrayOfBoolean94[16] = F;
    arrayOfBoolean94[17] = F;
    arrayOfBoolean94[18] = F;
    arrayOfBoolean94[19] = F;
    arrayOfBoolean94[20] = F;
    arrayOfBoolean94[21] = F;
    arrayOfBoolean94[22] = F;
    arrayOfBoolean94[23] = F;
    arrayOfBoolean94[24] = F;
    arrayOfBoolean94[25] = F;
    arrayOfBoolean94[26] = F;
    arrayOfBoolean86[7] = arrayOfBoolean94;
    boolean[] arrayOfBoolean95 = new boolean[27];
    arrayOfBoolean95[0] = F;
    arrayOfBoolean95[1] = F;
    arrayOfBoolean95[2] = F;
    arrayOfBoolean95[3] = F;
    arrayOfBoolean95[4] = F;
    arrayOfBoolean95[5] = F;
    arrayOfBoolean95[6] = F;
    arrayOfBoolean95[7] = F;
    arrayOfBoolean95[8] = F;
    arrayOfBoolean95[9] = F;
    arrayOfBoolean95[10] = F;
    arrayOfBoolean95[11] = F;
    arrayOfBoolean95[12] = F;
    arrayOfBoolean95[13] = F;
    arrayOfBoolean95[14] = F;
    arrayOfBoolean95[15] = F;
    arrayOfBoolean95[16] = F;
    arrayOfBoolean95[17] = F;
    arrayOfBoolean95[18] = F;
    arrayOfBoolean95[19] = F;
    arrayOfBoolean95[20] = F;
    arrayOfBoolean95[21] = F;
    arrayOfBoolean95[22] = F;
    arrayOfBoolean95[23] = F;
    arrayOfBoolean95[24] = F;
    arrayOfBoolean95[25] = F;
    arrayOfBoolean95[26] = F;
    arrayOfBoolean86[8] = arrayOfBoolean95;
    boolean[] arrayOfBoolean96 = new boolean[27];
    arrayOfBoolean96[0] = F;
    arrayOfBoolean96[1] = T;
    arrayOfBoolean96[2] = T;
    arrayOfBoolean96[3] = T;
    arrayOfBoolean96[4] = T;
    arrayOfBoolean96[5] = F;
    arrayOfBoolean96[6] = F;
    arrayOfBoolean96[7] = F;
    arrayOfBoolean96[8] = F;
    arrayOfBoolean96[9] = T;
    arrayOfBoolean96[10] = T;
    arrayOfBoolean96[11] = F;
    arrayOfBoolean96[12] = F;
    arrayOfBoolean96[13] = F;
    arrayOfBoolean96[14] = T;
    arrayOfBoolean96[15] = T;
    arrayOfBoolean96[16] = F;
    arrayOfBoolean96[17] = F;
    arrayOfBoolean96[18] = T;
    arrayOfBoolean96[19] = T;
    arrayOfBoolean96[20] = F;
    arrayOfBoolean96[21] = T;
    arrayOfBoolean96[22] = T;
    arrayOfBoolean96[23] = T;
    arrayOfBoolean96[24] = T;
    arrayOfBoolean96[25] = T;
    arrayOfBoolean96[26] = T;
    arrayOfBoolean86[9] = arrayOfBoolean96;
    boolean[] arrayOfBoolean97 = new boolean[27];
    arrayOfBoolean97[0] = T;
    arrayOfBoolean97[1] = T;
    arrayOfBoolean97[2] = T;
    arrayOfBoolean97[3] = T;
    arrayOfBoolean97[4] = T;
    arrayOfBoolean97[5] = T;
    arrayOfBoolean97[6] = F;
    arrayOfBoolean97[7] = F;
    arrayOfBoolean97[8] = F;
    arrayOfBoolean97[9] = T;
    arrayOfBoolean97[10] = T;
    arrayOfBoolean97[11] = F;
    arrayOfBoolean97[12] = F;
    arrayOfBoolean97[13] = F;
    arrayOfBoolean97[14] = T;
    arrayOfBoolean97[15] = T;
    arrayOfBoolean97[16] = F;
    arrayOfBoolean97[17] = F;
    arrayOfBoolean97[18] = T;
    arrayOfBoolean97[19] = T;
    arrayOfBoolean97[20] = F;
    arrayOfBoolean97[21] = T;
    arrayOfBoolean97[22] = T;
    arrayOfBoolean97[23] = F;
    arrayOfBoolean97[24] = F;
    arrayOfBoolean97[25] = F;
    arrayOfBoolean97[26] = F;
    arrayOfBoolean86[10] = arrayOfBoolean97;
    boolean[] arrayOfBoolean98 = new boolean[27];
    arrayOfBoolean98[0] = T;
    arrayOfBoolean98[1] = T;
    arrayOfBoolean98[2] = F;
    arrayOfBoolean98[3] = F;
    arrayOfBoolean98[4] = F;
    arrayOfBoolean98[5] = F;
    arrayOfBoolean98[6] = F;
    arrayOfBoolean98[7] = F;
    arrayOfBoolean98[8] = T;
    arrayOfBoolean98[9] = T;
    arrayOfBoolean98[10] = T;
    arrayOfBoolean98[11] = T;
    arrayOfBoolean98[12] = F;
    arrayOfBoolean98[13] = F;
    arrayOfBoolean98[14] = T;
    arrayOfBoolean98[15] = T;
    arrayOfBoolean98[16] = F;
    arrayOfBoolean98[17] = F;
    arrayOfBoolean98[18] = T;
    arrayOfBoolean98[19] = T;
    arrayOfBoolean98[20] = F;
    arrayOfBoolean98[21] = T;
    arrayOfBoolean98[22] = T;
    arrayOfBoolean98[23] = F;
    arrayOfBoolean98[24] = F;
    arrayOfBoolean98[25] = F;
    arrayOfBoolean98[26] = F;
    arrayOfBoolean86[11] = arrayOfBoolean98;
    boolean[] arrayOfBoolean99 = new boolean[27];
    arrayOfBoolean99[0] = T;
    arrayOfBoolean99[1] = T;
    arrayOfBoolean99[2] = F;
    arrayOfBoolean99[3] = T;
    arrayOfBoolean99[4] = T;
    arrayOfBoolean99[5] = T;
    arrayOfBoolean99[6] = F;
    arrayOfBoolean99[7] = F;
    arrayOfBoolean99[8] = T;
    arrayOfBoolean99[9] = F;
    arrayOfBoolean99[10] = F;
    arrayOfBoolean99[11] = T;
    arrayOfBoolean99[12] = F;
    arrayOfBoolean99[13] = F;
    arrayOfBoolean99[14] = T;
    arrayOfBoolean99[15] = T;
    arrayOfBoolean99[16] = T;
    arrayOfBoolean99[17] = T;
    arrayOfBoolean99[18] = T;
    arrayOfBoolean99[19] = T;
    arrayOfBoolean99[20] = F;
    arrayOfBoolean99[21] = T;
    arrayOfBoolean99[22] = T;
    arrayOfBoolean99[23] = T;
    arrayOfBoolean99[24] = T;
    arrayOfBoolean99[25] = T;
    arrayOfBoolean99[26] = T;
    arrayOfBoolean86[12] = arrayOfBoolean99;
    boolean[] arrayOfBoolean100 = new boolean[27];
    arrayOfBoolean100[0] = T;
    arrayOfBoolean100[1] = T;
    arrayOfBoolean100[2] = F;
    arrayOfBoolean100[3] = F;
    arrayOfBoolean100[4] = T;
    arrayOfBoolean100[5] = T;
    arrayOfBoolean100[6] = F;
    arrayOfBoolean100[7] = T;
    arrayOfBoolean100[8] = T;
    arrayOfBoolean100[9] = F;
    arrayOfBoolean100[10] = F;
    arrayOfBoolean100[11] = T;
    arrayOfBoolean100[12] = T;
    arrayOfBoolean100[13] = F;
    arrayOfBoolean100[14] = T;
    arrayOfBoolean100[15] = F;
    arrayOfBoolean100[16] = T;
    arrayOfBoolean100[17] = T;
    arrayOfBoolean100[18] = F;
    arrayOfBoolean100[19] = T;
    arrayOfBoolean100[20] = F;
    arrayOfBoolean100[21] = T;
    arrayOfBoolean100[22] = T;
    arrayOfBoolean100[23] = F;
    arrayOfBoolean100[24] = F;
    arrayOfBoolean100[25] = F;
    arrayOfBoolean100[26] = F;
    arrayOfBoolean86[13] = arrayOfBoolean100;
    boolean[] arrayOfBoolean101 = new boolean[27];
    arrayOfBoolean101[0] = T;
    arrayOfBoolean101[1] = T;
    arrayOfBoolean101[2] = T;
    arrayOfBoolean101[3] = T;
    arrayOfBoolean101[4] = T;
    arrayOfBoolean101[5] = T;
    arrayOfBoolean101[6] = F;
    arrayOfBoolean101[7] = T;
    arrayOfBoolean101[8] = T;
    arrayOfBoolean101[9] = T;
    arrayOfBoolean101[10] = T;
    arrayOfBoolean101[11] = T;
    arrayOfBoolean101[12] = T;
    arrayOfBoolean101[13] = F;
    arrayOfBoolean101[14] = T;
    arrayOfBoolean101[15] = F;
    arrayOfBoolean101[16] = F;
    arrayOfBoolean101[17] = F;
    arrayOfBoolean101[18] = F;
    arrayOfBoolean101[19] = T;
    arrayOfBoolean101[20] = F;
    arrayOfBoolean101[21] = T;
    arrayOfBoolean101[22] = T;
    arrayOfBoolean101[23] = F;
    arrayOfBoolean101[24] = F;
    arrayOfBoolean101[25] = F;
    arrayOfBoolean101[26] = F;
    arrayOfBoolean86[14] = arrayOfBoolean101;
    boolean[] arrayOfBoolean102 = new boolean[27];
    arrayOfBoolean102[0] = F;
    arrayOfBoolean102[1] = T;
    arrayOfBoolean102[2] = T;
    arrayOfBoolean102[3] = T;
    arrayOfBoolean102[4] = T;
    arrayOfBoolean102[5] = F;
    arrayOfBoolean102[6] = F;
    arrayOfBoolean102[7] = T;
    arrayOfBoolean102[8] = T;
    arrayOfBoolean102[9] = F;
    arrayOfBoolean102[10] = F;
    arrayOfBoolean102[11] = T;
    arrayOfBoolean102[12] = T;
    arrayOfBoolean102[13] = F;
    arrayOfBoolean102[14] = T;
    arrayOfBoolean102[15] = F;
    arrayOfBoolean102[16] = F;
    arrayOfBoolean102[17] = F;
    arrayOfBoolean102[18] = F;
    arrayOfBoolean102[19] = T;
    arrayOfBoolean102[20] = F;
    arrayOfBoolean102[21] = T;
    arrayOfBoolean102[22] = T;
    arrayOfBoolean102[23] = T;
    arrayOfBoolean102[24] = T;
    arrayOfBoolean102[25] = T;
    arrayOfBoolean102[26] = T;
    arrayOfBoolean86[15] = arrayOfBoolean102;
    boolean[] arrayOfBoolean103 = new boolean[27];
    arrayOfBoolean103[0] = F;
    arrayOfBoolean103[1] = F;
    arrayOfBoolean103[2] = F;
    arrayOfBoolean103[3] = F;
    arrayOfBoolean103[4] = F;
    arrayOfBoolean103[5] = F;
    arrayOfBoolean103[6] = F;
    arrayOfBoolean103[7] = F;
    arrayOfBoolean103[8] = F;
    arrayOfBoolean103[9] = F;
    arrayOfBoolean103[10] = F;
    arrayOfBoolean103[11] = F;
    arrayOfBoolean103[12] = F;
    arrayOfBoolean103[13] = F;
    arrayOfBoolean103[14] = F;
    arrayOfBoolean103[15] = F;
    arrayOfBoolean103[16] = F;
    arrayOfBoolean103[17] = F;
    arrayOfBoolean103[18] = F;
    arrayOfBoolean103[19] = F;
    arrayOfBoolean103[20] = F;
    arrayOfBoolean103[21] = F;
    arrayOfBoolean103[22] = F;
    arrayOfBoolean103[23] = F;
    arrayOfBoolean103[24] = F;
    arrayOfBoolean103[25] = F;
    arrayOfBoolean103[26] = F;
    arrayOfBoolean86[16] = arrayOfBoolean103;
    boolean[] arrayOfBoolean104 = new boolean[27];
    arrayOfBoolean104[0] = F;
    arrayOfBoolean104[1] = F;
    arrayOfBoolean104[2] = F;
    arrayOfBoolean104[3] = F;
    arrayOfBoolean104[4] = F;
    arrayOfBoolean104[5] = F;
    arrayOfBoolean104[6] = F;
    arrayOfBoolean104[7] = F;
    arrayOfBoolean104[8] = F;
    arrayOfBoolean104[9] = F;
    arrayOfBoolean104[10] = F;
    arrayOfBoolean104[11] = F;
    arrayOfBoolean104[12] = F;
    arrayOfBoolean104[13] = F;
    arrayOfBoolean104[14] = F;
    arrayOfBoolean104[15] = F;
    arrayOfBoolean104[16] = F;
    arrayOfBoolean104[17] = F;
    arrayOfBoolean104[18] = F;
    arrayOfBoolean104[19] = F;
    arrayOfBoolean104[20] = F;
    arrayOfBoolean104[21] = F;
    arrayOfBoolean104[22] = F;
    arrayOfBoolean104[23] = F;
    arrayOfBoolean104[24] = F;
    arrayOfBoolean104[25] = F;
    arrayOfBoolean104[26] = F;
    arrayOfBoolean86[17] = arrayOfBoolean104;
    boolean[] arrayOfBoolean105 = new boolean[27];
    arrayOfBoolean105[0] = F;
    arrayOfBoolean105[1] = T;
    arrayOfBoolean105[2] = T;
    arrayOfBoolean105[3] = T;
    arrayOfBoolean105[4] = T;
    arrayOfBoolean105[5] = F;
    arrayOfBoolean105[6] = F;
    arrayOfBoolean105[7] = T;
    arrayOfBoolean105[8] = T;
    arrayOfBoolean105[9] = F;
    arrayOfBoolean105[10] = F;
    arrayOfBoolean105[11] = T;
    arrayOfBoolean105[12] = T;
    arrayOfBoolean105[13] = F;
    arrayOfBoolean105[14] = T;
    arrayOfBoolean105[15] = T;
    arrayOfBoolean105[16] = T;
    arrayOfBoolean105[17] = T;
    arrayOfBoolean105[18] = T;
    arrayOfBoolean105[19] = T;
    arrayOfBoolean105[20] = F;
    arrayOfBoolean105[21] = T;
    arrayOfBoolean105[22] = T;
    arrayOfBoolean105[23] = T;
    arrayOfBoolean105[24] = T;
    arrayOfBoolean105[25] = T;
    arrayOfBoolean105[26] = F;
    arrayOfBoolean86[18] = arrayOfBoolean105;
    boolean[] arrayOfBoolean106 = new boolean[27];
    arrayOfBoolean106[0] = T;
    arrayOfBoolean106[1] = T;
    arrayOfBoolean106[2] = T;
    arrayOfBoolean106[3] = T;
    arrayOfBoolean106[4] = T;
    arrayOfBoolean106[5] = T;
    arrayOfBoolean106[6] = F;
    arrayOfBoolean106[7] = T;
    arrayOfBoolean106[8] = T;
    arrayOfBoolean106[9] = F;
    arrayOfBoolean106[10] = F;
    arrayOfBoolean106[11] = T;
    arrayOfBoolean106[12] = T;
    arrayOfBoolean106[13] = F;
    arrayOfBoolean106[14] = T;
    arrayOfBoolean106[15] = T;
    arrayOfBoolean106[16] = F;
    arrayOfBoolean106[17] = F;
    arrayOfBoolean106[18] = F;
    arrayOfBoolean106[19] = F;
    arrayOfBoolean106[20] = F;
    arrayOfBoolean106[21] = T;
    arrayOfBoolean106[22] = T;
    arrayOfBoolean106[23] = F;
    arrayOfBoolean106[24] = F;
    arrayOfBoolean106[25] = F;
    arrayOfBoolean106[26] = T;
    arrayOfBoolean86[19] = arrayOfBoolean106;
    boolean[] arrayOfBoolean107 = new boolean[27];
    arrayOfBoolean107[0] = T;
    arrayOfBoolean107[1] = T;
    arrayOfBoolean107[2] = F;
    arrayOfBoolean107[3] = F;
    arrayOfBoolean107[4] = T;
    arrayOfBoolean107[5] = T;
    arrayOfBoolean107[6] = F;
    arrayOfBoolean107[7] = T;
    arrayOfBoolean107[8] = T;
    arrayOfBoolean107[9] = F;
    arrayOfBoolean107[10] = F;
    arrayOfBoolean107[11] = T;
    arrayOfBoolean107[12] = T;
    arrayOfBoolean107[13] = F;
    arrayOfBoolean107[14] = T;
    arrayOfBoolean107[15] = T;
    arrayOfBoolean107[16] = F;
    arrayOfBoolean107[17] = F;
    arrayOfBoolean107[18] = F;
    arrayOfBoolean107[19] = F;
    arrayOfBoolean107[20] = F;
    arrayOfBoolean107[21] = T;
    arrayOfBoolean107[22] = T;
    arrayOfBoolean107[23] = F;
    arrayOfBoolean107[24] = F;
    arrayOfBoolean107[25] = F;
    arrayOfBoolean107[26] = T;
    arrayOfBoolean86[20] = arrayOfBoolean107;
    boolean[] arrayOfBoolean108 = new boolean[27];
    arrayOfBoolean108[0] = T;
    arrayOfBoolean108[1] = T;
    arrayOfBoolean108[2] = F;
    arrayOfBoolean108[3] = F;
    arrayOfBoolean108[4] = T;
    arrayOfBoolean108[5] = T;
    arrayOfBoolean108[6] = F;
    arrayOfBoolean108[7] = F;
    arrayOfBoolean108[8] = T;
    arrayOfBoolean108[9] = F;
    arrayOfBoolean108[10] = F;
    arrayOfBoolean108[11] = T;
    arrayOfBoolean108[12] = F;
    arrayOfBoolean108[13] = F;
    arrayOfBoolean108[14] = T;
    arrayOfBoolean108[15] = T;
    arrayOfBoolean108[16] = T;
    arrayOfBoolean108[17] = T;
    arrayOfBoolean108[18] = T;
    arrayOfBoolean108[19] = T;
    arrayOfBoolean108[20] = F;
    arrayOfBoolean108[21] = T;
    arrayOfBoolean108[22] = T;
    arrayOfBoolean108[23] = T;
    arrayOfBoolean108[24] = T;
    arrayOfBoolean108[25] = T;
    arrayOfBoolean108[26] = F;
    arrayOfBoolean86[21] = arrayOfBoolean108;
    boolean[] arrayOfBoolean109 = new boolean[27];
    arrayOfBoolean109[0] = T;
    arrayOfBoolean109[1] = T;
    arrayOfBoolean109[2] = F;
    arrayOfBoolean109[3] = F;
    arrayOfBoolean109[4] = T;
    arrayOfBoolean109[5] = T;
    arrayOfBoolean109[6] = F;
    arrayOfBoolean109[7] = F;
    arrayOfBoolean109[8] = T;
    arrayOfBoolean109[9] = T;
    arrayOfBoolean109[10] = T;
    arrayOfBoolean109[11] = T;
    arrayOfBoolean109[12] = F;
    arrayOfBoolean109[13] = F;
    arrayOfBoolean109[14] = T;
    arrayOfBoolean109[15] = T;
    arrayOfBoolean109[16] = F;
    arrayOfBoolean109[17] = F;
    arrayOfBoolean109[18] = F;
    arrayOfBoolean109[19] = F;
    arrayOfBoolean109[20] = F;
    arrayOfBoolean109[21] = T;
    arrayOfBoolean109[22] = T;
    arrayOfBoolean109[23] = T;
    arrayOfBoolean109[24] = F;
    arrayOfBoolean109[25] = F;
    arrayOfBoolean109[26] = F;
    arrayOfBoolean86[22] = arrayOfBoolean109;
    boolean[] arrayOfBoolean110 = new boolean[27];
    arrayOfBoolean110[0] = T;
    arrayOfBoolean110[1] = T;
    arrayOfBoolean110[2] = T;
    arrayOfBoolean110[3] = T;
    arrayOfBoolean110[4] = T;
    arrayOfBoolean110[5] = T;
    arrayOfBoolean110[6] = F;
    arrayOfBoolean110[7] = F;
    arrayOfBoolean110[8] = F;
    arrayOfBoolean110[9] = T;
    arrayOfBoolean110[10] = T;
    arrayOfBoolean110[11] = F;
    arrayOfBoolean110[12] = F;
    arrayOfBoolean110[13] = F;
    arrayOfBoolean110[14] = T;
    arrayOfBoolean110[15] = T;
    arrayOfBoolean110[16] = F;
    arrayOfBoolean110[17] = F;
    arrayOfBoolean110[18] = F;
    arrayOfBoolean110[19] = F;
    arrayOfBoolean110[20] = F;
    arrayOfBoolean110[21] = T;
    arrayOfBoolean110[22] = T;
    arrayOfBoolean110[23] = F;
    arrayOfBoolean110[24] = T;
    arrayOfBoolean110[25] = T;
    arrayOfBoolean110[26] = F;
    arrayOfBoolean86[23] = arrayOfBoolean110;
    boolean[] arrayOfBoolean111 = new boolean[27];
    arrayOfBoolean111[0] = F;
    arrayOfBoolean111[1] = T;
    arrayOfBoolean111[2] = T;
    arrayOfBoolean111[3] = T;
    arrayOfBoolean111[4] = T;
    arrayOfBoolean111[5] = F;
    arrayOfBoolean111[6] = F;
    arrayOfBoolean111[7] = F;
    arrayOfBoolean111[8] = F;
    arrayOfBoolean111[9] = T;
    arrayOfBoolean111[10] = T;
    arrayOfBoolean111[11] = F;
    arrayOfBoolean111[12] = F;
    arrayOfBoolean111[13] = F;
    arrayOfBoolean111[14] = T;
    arrayOfBoolean111[15] = T;
    arrayOfBoolean111[16] = T;
    arrayOfBoolean111[17] = T;
    arrayOfBoolean111[18] = T;
    arrayOfBoolean111[19] = T;
    arrayOfBoolean111[20] = F;
    arrayOfBoolean111[21] = T;
    arrayOfBoolean111[22] = T;
    arrayOfBoolean111[23] = F;
    arrayOfBoolean111[24] = F;
    arrayOfBoolean111[25] = F;
    arrayOfBoolean111[26] = T;
    arrayOfBoolean86[24] = arrayOfBoolean111;
    boolean[] arrayOfBoolean112 = new boolean[27];
    arrayOfBoolean112[0] = F;
    arrayOfBoolean112[1] = F;
    arrayOfBoolean112[2] = F;
    arrayOfBoolean112[3] = F;
    arrayOfBoolean112[4] = F;
    arrayOfBoolean112[5] = F;
    arrayOfBoolean112[6] = F;
    arrayOfBoolean112[7] = F;
    arrayOfBoolean112[8] = F;
    arrayOfBoolean112[9] = F;
    arrayOfBoolean112[10] = F;
    arrayOfBoolean112[11] = F;
    arrayOfBoolean112[12] = F;
    arrayOfBoolean112[13] = F;
    arrayOfBoolean112[14] = F;
    arrayOfBoolean112[15] = F;
    arrayOfBoolean112[16] = F;
    arrayOfBoolean112[17] = F;
    arrayOfBoolean112[18] = F;
    arrayOfBoolean112[19] = F;
    arrayOfBoolean112[20] = F;
    arrayOfBoolean112[21] = F;
    arrayOfBoolean112[22] = F;
    arrayOfBoolean112[23] = F;
    arrayOfBoolean112[24] = F;
    arrayOfBoolean112[25] = F;
    arrayOfBoolean112[26] = F;
    arrayOfBoolean86[25] = arrayOfBoolean112;
    boolean[] arrayOfBoolean113 = new boolean[27];
    arrayOfBoolean113[0] = F;
    arrayOfBoolean113[1] = F;
    arrayOfBoolean113[2] = F;
    arrayOfBoolean113[3] = F;
    arrayOfBoolean113[4] = F;
    arrayOfBoolean113[5] = F;
    arrayOfBoolean113[6] = F;
    arrayOfBoolean113[7] = F;
    arrayOfBoolean113[8] = F;
    arrayOfBoolean113[9] = F;
    arrayOfBoolean113[10] = F;
    arrayOfBoolean113[11] = F;
    arrayOfBoolean113[12] = F;
    arrayOfBoolean113[13] = F;
    arrayOfBoolean113[14] = F;
    arrayOfBoolean113[15] = F;
    arrayOfBoolean113[16] = F;
    arrayOfBoolean113[17] = F;
    arrayOfBoolean113[18] = F;
    arrayOfBoolean113[19] = F;
    arrayOfBoolean113[20] = F;
    arrayOfBoolean113[21] = F;
    arrayOfBoolean113[22] = F;
    arrayOfBoolean113[23] = F;
    arrayOfBoolean113[24] = F;
    arrayOfBoolean113[25] = F;
    arrayOfBoolean113[26] = F;
    arrayOfBoolean86[26] = arrayOfBoolean113;
    this.game_over_word = arrayOfBoolean86;
    boolean[][][] arrayOfBoolean114 = new boolean[4][][];
    boolean[][] arrayOfBoolean115 = new boolean[20][];
    boolean[] arrayOfBoolean116 = new boolean[27];
    arrayOfBoolean116[0] = F;
    arrayOfBoolean116[1] = F;
    arrayOfBoolean116[2] = F;
    arrayOfBoolean116[3] = F;
    arrayOfBoolean116[4] = F;
    arrayOfBoolean116[5] = F;
    arrayOfBoolean116[6] = F;
    arrayOfBoolean116[7] = F;
    arrayOfBoolean116[8] = F;
    arrayOfBoolean116[9] = F;
    arrayOfBoolean116[10] = F;
    arrayOfBoolean116[11] = F;
    arrayOfBoolean116[12] = F;
    arrayOfBoolean116[13] = F;
    arrayOfBoolean116[14] = F;
    arrayOfBoolean116[15] = F;
    arrayOfBoolean116[16] = F;
    arrayOfBoolean116[17] = F;
    arrayOfBoolean116[18] = F;
    arrayOfBoolean116[19] = F;
    arrayOfBoolean116[20] = F;
    arrayOfBoolean116[21] = F;
    arrayOfBoolean116[22] = F;
    arrayOfBoolean116[23] = F;
    arrayOfBoolean116[24] = F;
    arrayOfBoolean116[25] = F;
    arrayOfBoolean116[26] = F;
    arrayOfBoolean115[0] = arrayOfBoolean116;
    boolean[] arrayOfBoolean117 = new boolean[27];
    arrayOfBoolean117[0] = F;
    arrayOfBoolean117[1] = F;
    arrayOfBoolean117[2] = F;
    arrayOfBoolean117[3] = F;
    arrayOfBoolean117[4] = F;
    arrayOfBoolean117[5] = F;
    arrayOfBoolean117[6] = F;
    arrayOfBoolean117[7] = F;
    arrayOfBoolean117[8] = F;
    arrayOfBoolean117[9] = F;
    arrayOfBoolean117[10] = F;
    arrayOfBoolean117[11] = F;
    arrayOfBoolean117[12] = F;
    arrayOfBoolean117[13] = F;
    arrayOfBoolean117[14] = F;
    arrayOfBoolean117[15] = F;
    arrayOfBoolean117[16] = F;
    arrayOfBoolean117[17] = F;
    arrayOfBoolean117[18] = F;
    arrayOfBoolean117[19] = F;
    arrayOfBoolean117[20] = F;
    arrayOfBoolean117[21] = F;
    arrayOfBoolean117[22] = F;
    arrayOfBoolean117[23] = F;
    arrayOfBoolean117[24] = F;
    arrayOfBoolean117[25] = F;
    arrayOfBoolean117[26] = F;
    arrayOfBoolean115[1] = arrayOfBoolean117;
    boolean[] arrayOfBoolean118 = new boolean[27];
    arrayOfBoolean118[0] = F;
    arrayOfBoolean118[1] = F;
    arrayOfBoolean118[2] = F;
    arrayOfBoolean118[3] = F;
    arrayOfBoolean118[4] = F;
    arrayOfBoolean118[5] = F;
    arrayOfBoolean118[6] = F;
    arrayOfBoolean118[7] = F;
    arrayOfBoolean118[8] = F;
    arrayOfBoolean118[9] = F;
    arrayOfBoolean118[10] = F;
    arrayOfBoolean118[11] = F;
    arrayOfBoolean118[12] = F;
    arrayOfBoolean118[13] = F;
    arrayOfBoolean118[14] = F;
    arrayOfBoolean118[15] = F;
    arrayOfBoolean118[16] = F;
    arrayOfBoolean118[17] = F;
    arrayOfBoolean118[18] = F;
    arrayOfBoolean118[19] = F;
    arrayOfBoolean118[20] = F;
    arrayOfBoolean118[21] = F;
    arrayOfBoolean118[22] = F;
    arrayOfBoolean118[23] = F;
    arrayOfBoolean118[24] = F;
    arrayOfBoolean118[25] = F;
    arrayOfBoolean118[26] = F;
    arrayOfBoolean115[2] = arrayOfBoolean118;
    boolean[] arrayOfBoolean119 = new boolean[27];
    arrayOfBoolean119[0] = F;
    arrayOfBoolean119[1] = F;
    arrayOfBoolean119[2] = F;
    arrayOfBoolean119[3] = F;
    arrayOfBoolean119[4] = F;
    arrayOfBoolean119[5] = F;
    arrayOfBoolean119[6] = F;
    arrayOfBoolean119[7] = F;
    arrayOfBoolean119[8] = F;
    arrayOfBoolean119[9] = F;
    arrayOfBoolean119[10] = F;
    arrayOfBoolean119[11] = F;
    arrayOfBoolean119[12] = F;
    arrayOfBoolean119[13] = F;
    arrayOfBoolean119[14] = F;
    arrayOfBoolean119[15] = F;
    arrayOfBoolean119[16] = F;
    arrayOfBoolean119[17] = F;
    arrayOfBoolean119[18] = F;
    arrayOfBoolean119[19] = F;
    arrayOfBoolean119[20] = F;
    arrayOfBoolean119[21] = F;
    arrayOfBoolean119[22] = F;
    arrayOfBoolean119[23] = F;
    arrayOfBoolean119[24] = F;
    arrayOfBoolean119[25] = F;
    arrayOfBoolean119[26] = F;
    arrayOfBoolean115[3] = arrayOfBoolean119;
    boolean[] arrayOfBoolean120 = new boolean[27];
    arrayOfBoolean120[0] = F;
    arrayOfBoolean120[1] = F;
    arrayOfBoolean120[2] = F;
    arrayOfBoolean120[3] = F;
    arrayOfBoolean120[4] = F;
    arrayOfBoolean120[5] = F;
    arrayOfBoolean120[6] = F;
    arrayOfBoolean120[7] = F;
    arrayOfBoolean120[8] = F;
    arrayOfBoolean120[9] = F;
    arrayOfBoolean120[10] = F;
    arrayOfBoolean120[11] = F;
    arrayOfBoolean120[12] = F;
    arrayOfBoolean120[13] = F;
    arrayOfBoolean120[14] = F;
    arrayOfBoolean120[15] = F;
    arrayOfBoolean120[16] = F;
    arrayOfBoolean120[17] = F;
    arrayOfBoolean120[18] = F;
    arrayOfBoolean120[19] = F;
    arrayOfBoolean120[20] = F;
    arrayOfBoolean120[21] = F;
    arrayOfBoolean120[22] = F;
    arrayOfBoolean120[23] = F;
    arrayOfBoolean120[24] = F;
    arrayOfBoolean120[25] = F;
    arrayOfBoolean120[26] = F;
    arrayOfBoolean115[4] = arrayOfBoolean120;
    boolean[] arrayOfBoolean121 = new boolean[27];
    arrayOfBoolean121[0] = F;
    arrayOfBoolean121[1] = F;
    arrayOfBoolean121[2] = F;
    arrayOfBoolean121[3] = F;
    arrayOfBoolean121[4] = F;
    arrayOfBoolean121[5] = F;
    arrayOfBoolean121[6] = F;
    arrayOfBoolean121[7] = F;
    arrayOfBoolean121[8] = F;
    arrayOfBoolean121[9] = F;
    arrayOfBoolean121[10] = F;
    arrayOfBoolean121[11] = F;
    arrayOfBoolean121[12] = F;
    arrayOfBoolean121[13] = F;
    arrayOfBoolean121[14] = F;
    arrayOfBoolean121[15] = F;
    arrayOfBoolean121[16] = F;
    arrayOfBoolean121[17] = F;
    arrayOfBoolean121[18] = F;
    arrayOfBoolean121[19] = F;
    arrayOfBoolean121[20] = F;
    arrayOfBoolean121[21] = F;
    arrayOfBoolean121[22] = F;
    arrayOfBoolean121[23] = F;
    arrayOfBoolean121[24] = F;
    arrayOfBoolean121[25] = F;
    arrayOfBoolean121[26] = F;
    arrayOfBoolean115[5] = arrayOfBoolean121;
    boolean[] arrayOfBoolean122 = new boolean[27];
    arrayOfBoolean122[0] = F;
    arrayOfBoolean122[1] = F;
    arrayOfBoolean122[2] = F;
    arrayOfBoolean122[3] = F;
    arrayOfBoolean122[4] = F;
    arrayOfBoolean122[5] = F;
    arrayOfBoolean122[6] = F;
    arrayOfBoolean122[7] = F;
    arrayOfBoolean122[8] = F;
    arrayOfBoolean122[9] = F;
    arrayOfBoolean122[10] = F;
    arrayOfBoolean122[11] = F;
    arrayOfBoolean122[12] = F;
    arrayOfBoolean122[13] = F;
    arrayOfBoolean122[14] = F;
    arrayOfBoolean122[15] = F;
    arrayOfBoolean122[16] = F;
    arrayOfBoolean122[17] = F;
    arrayOfBoolean122[18] = F;
    arrayOfBoolean122[19] = F;
    arrayOfBoolean122[20] = F;
    arrayOfBoolean122[21] = F;
    arrayOfBoolean122[22] = F;
    arrayOfBoolean122[23] = F;
    arrayOfBoolean122[24] = F;
    arrayOfBoolean122[25] = F;
    arrayOfBoolean122[26] = F;
    arrayOfBoolean115[6] = arrayOfBoolean122;
    boolean[] arrayOfBoolean123 = new boolean[27];
    arrayOfBoolean123[0] = F;
    arrayOfBoolean123[1] = F;
    arrayOfBoolean123[2] = F;
    arrayOfBoolean123[3] = F;
    arrayOfBoolean123[4] = F;
    arrayOfBoolean123[5] = F;
    arrayOfBoolean123[6] = F;
    arrayOfBoolean123[7] = F;
    arrayOfBoolean123[8] = F;
    arrayOfBoolean123[9] = F;
    arrayOfBoolean123[10] = F;
    arrayOfBoolean123[11] = F;
    arrayOfBoolean123[12] = F;
    arrayOfBoolean123[13] = F;
    arrayOfBoolean123[14] = F;
    arrayOfBoolean123[15] = F;
    arrayOfBoolean123[16] = F;
    arrayOfBoolean123[17] = F;
    arrayOfBoolean123[18] = F;
    arrayOfBoolean123[19] = F;
    arrayOfBoolean123[20] = F;
    arrayOfBoolean123[21] = F;
    arrayOfBoolean123[22] = F;
    arrayOfBoolean123[23] = F;
    arrayOfBoolean123[24] = F;
    arrayOfBoolean123[25] = F;
    arrayOfBoolean123[26] = F;
    arrayOfBoolean115[7] = arrayOfBoolean123;
    boolean[] arrayOfBoolean124 = new boolean[27];
    arrayOfBoolean124[0] = F;
    arrayOfBoolean124[1] = F;
    arrayOfBoolean124[2] = F;
    arrayOfBoolean124[3] = F;
    arrayOfBoolean124[4] = F;
    arrayOfBoolean124[5] = F;
    arrayOfBoolean124[6] = F;
    arrayOfBoolean124[7] = F;
    arrayOfBoolean124[8] = F;
    arrayOfBoolean124[9] = F;
    arrayOfBoolean124[10] = F;
    arrayOfBoolean124[11] = F;
    arrayOfBoolean124[12] = F;
    arrayOfBoolean124[13] = F;
    arrayOfBoolean124[14] = F;
    arrayOfBoolean124[15] = F;
    arrayOfBoolean124[16] = F;
    arrayOfBoolean124[17] = F;
    arrayOfBoolean124[18] = F;
    arrayOfBoolean124[19] = F;
    arrayOfBoolean124[20] = F;
    arrayOfBoolean124[21] = F;
    arrayOfBoolean124[22] = F;
    arrayOfBoolean124[23] = F;
    arrayOfBoolean124[24] = F;
    arrayOfBoolean124[25] = F;
    arrayOfBoolean124[26] = F;
    arrayOfBoolean115[8] = arrayOfBoolean124;
    boolean[] arrayOfBoolean125 = new boolean[27];
    arrayOfBoolean125[0] = F;
    arrayOfBoolean125[1] = F;
    arrayOfBoolean125[2] = F;
    arrayOfBoolean125[3] = F;
    arrayOfBoolean125[4] = F;
    arrayOfBoolean125[5] = F;
    arrayOfBoolean125[6] = F;
    arrayOfBoolean125[7] = F;
    arrayOfBoolean125[8] = F;
    arrayOfBoolean125[9] = F;
    arrayOfBoolean125[10] = F;
    arrayOfBoolean125[11] = F;
    arrayOfBoolean125[12] = F;
    arrayOfBoolean125[13] = F;
    arrayOfBoolean125[14] = F;
    arrayOfBoolean125[15] = F;
    arrayOfBoolean125[16] = F;
    arrayOfBoolean125[17] = F;
    arrayOfBoolean125[18] = F;
    arrayOfBoolean125[19] = F;
    arrayOfBoolean125[20] = F;
    arrayOfBoolean125[21] = F;
    arrayOfBoolean125[22] = F;
    arrayOfBoolean125[23] = F;
    arrayOfBoolean125[24] = F;
    arrayOfBoolean125[25] = F;
    arrayOfBoolean125[26] = F;
    arrayOfBoolean115[9] = arrayOfBoolean125;
    boolean[] arrayOfBoolean126 = new boolean[27];
    arrayOfBoolean126[0] = F;
    arrayOfBoolean126[1] = F;
    arrayOfBoolean126[2] = F;
    arrayOfBoolean126[3] = F;
    arrayOfBoolean126[4] = F;
    arrayOfBoolean126[5] = F;
    arrayOfBoolean126[6] = F;
    arrayOfBoolean126[7] = F;
    arrayOfBoolean126[8] = F;
    arrayOfBoolean126[9] = F;
    arrayOfBoolean126[10] = F;
    arrayOfBoolean126[11] = F;
    arrayOfBoolean126[12] = F;
    arrayOfBoolean126[13] = F;
    arrayOfBoolean126[14] = F;
    arrayOfBoolean126[15] = F;
    arrayOfBoolean126[16] = F;
    arrayOfBoolean126[17] = F;
    arrayOfBoolean126[18] = F;
    arrayOfBoolean126[19] = F;
    arrayOfBoolean126[20] = F;
    arrayOfBoolean126[21] = F;
    arrayOfBoolean126[22] = F;
    arrayOfBoolean126[23] = F;
    arrayOfBoolean126[24] = F;
    arrayOfBoolean126[25] = F;
    arrayOfBoolean126[26] = F;
    arrayOfBoolean115[10] = arrayOfBoolean126;
    boolean[] arrayOfBoolean127 = new boolean[27];
    arrayOfBoolean127[0] = F;
    arrayOfBoolean127[1] = F;
    arrayOfBoolean127[2] = F;
    arrayOfBoolean127[3] = F;
    arrayOfBoolean127[4] = F;
    arrayOfBoolean127[5] = F;
    arrayOfBoolean127[6] = F;
    arrayOfBoolean127[7] = F;
    arrayOfBoolean127[8] = F;
    arrayOfBoolean127[9] = F;
    arrayOfBoolean127[10] = F;
    arrayOfBoolean127[11] = F;
    arrayOfBoolean127[12] = F;
    arrayOfBoolean127[13] = F;
    arrayOfBoolean127[14] = F;
    arrayOfBoolean127[15] = F;
    arrayOfBoolean127[16] = F;
    arrayOfBoolean127[17] = F;
    arrayOfBoolean127[18] = F;
    arrayOfBoolean127[19] = F;
    arrayOfBoolean127[20] = F;
    arrayOfBoolean127[21] = F;
    arrayOfBoolean127[22] = F;
    arrayOfBoolean127[23] = F;
    arrayOfBoolean127[24] = F;
    arrayOfBoolean127[25] = F;
    arrayOfBoolean127[26] = F;
    arrayOfBoolean115[11] = arrayOfBoolean127;
    boolean[] arrayOfBoolean128 = new boolean[27];
    arrayOfBoolean128[0] = F;
    arrayOfBoolean128[1] = F;
    arrayOfBoolean128[2] = F;
    arrayOfBoolean128[3] = F;
    arrayOfBoolean128[4] = F;
    arrayOfBoolean128[5] = F;
    arrayOfBoolean128[6] = F;
    arrayOfBoolean128[7] = F;
    arrayOfBoolean128[8] = F;
    arrayOfBoolean128[9] = F;
    arrayOfBoolean128[10] = F;
    arrayOfBoolean128[11] = F;
    arrayOfBoolean128[12] = F;
    arrayOfBoolean128[13] = F;
    arrayOfBoolean128[14] = F;
    arrayOfBoolean128[15] = F;
    arrayOfBoolean128[16] = F;
    arrayOfBoolean128[17] = F;
    arrayOfBoolean128[18] = F;
    arrayOfBoolean128[19] = F;
    arrayOfBoolean128[20] = F;
    arrayOfBoolean128[21] = F;
    arrayOfBoolean128[22] = F;
    arrayOfBoolean128[23] = F;
    arrayOfBoolean128[24] = F;
    arrayOfBoolean128[25] = F;
    arrayOfBoolean128[26] = F;
    arrayOfBoolean115[12] = arrayOfBoolean128;
    boolean[] arrayOfBoolean129 = new boolean[27];
    arrayOfBoolean129[0] = F;
    arrayOfBoolean129[1] = F;
    arrayOfBoolean129[2] = F;
    arrayOfBoolean129[3] = F;
    arrayOfBoolean129[4] = F;
    arrayOfBoolean129[5] = F;
    arrayOfBoolean129[6] = F;
    arrayOfBoolean129[7] = F;
    arrayOfBoolean129[8] = F;
    arrayOfBoolean129[9] = F;
    arrayOfBoolean129[10] = F;
    arrayOfBoolean129[11] = F;
    arrayOfBoolean129[12] = F;
    arrayOfBoolean129[13] = F;
    arrayOfBoolean129[14] = F;
    arrayOfBoolean129[15] = F;
    arrayOfBoolean129[16] = F;
    arrayOfBoolean129[17] = F;
    arrayOfBoolean129[18] = F;
    arrayOfBoolean129[19] = F;
    arrayOfBoolean129[20] = F;
    arrayOfBoolean129[21] = F;
    arrayOfBoolean129[22] = F;
    arrayOfBoolean129[23] = F;
    arrayOfBoolean129[24] = T;
    arrayOfBoolean129[25] = F;
    arrayOfBoolean129[26] = F;
    arrayOfBoolean115[13] = arrayOfBoolean129;
    boolean[] arrayOfBoolean130 = new boolean[27];
    arrayOfBoolean130[0] = F;
    arrayOfBoolean130[1] = F;
    arrayOfBoolean130[2] = F;
    arrayOfBoolean130[3] = F;
    arrayOfBoolean130[4] = F;
    arrayOfBoolean130[5] = F;
    arrayOfBoolean130[6] = F;
    arrayOfBoolean130[7] = F;
    arrayOfBoolean130[8] = F;
    arrayOfBoolean130[9] = F;
    arrayOfBoolean130[10] = F;
    arrayOfBoolean130[11] = F;
    arrayOfBoolean130[12] = F;
    arrayOfBoolean130[13] = F;
    arrayOfBoolean130[14] = F;
    arrayOfBoolean130[15] = F;
    arrayOfBoolean130[16] = F;
    arrayOfBoolean130[17] = F;
    arrayOfBoolean130[18] = F;
    arrayOfBoolean130[19] = F;
    arrayOfBoolean130[20] = F;
    arrayOfBoolean130[21] = F;
    arrayOfBoolean130[22] = F;
    arrayOfBoolean130[23] = F;
    arrayOfBoolean130[24] = F;
    arrayOfBoolean130[25] = F;
    arrayOfBoolean130[26] = T;
    arrayOfBoolean115[14] = arrayOfBoolean130;
    boolean[] arrayOfBoolean131 = new boolean[27];
    arrayOfBoolean131[0] = F;
    arrayOfBoolean131[1] = F;
    arrayOfBoolean131[2] = F;
    arrayOfBoolean131[3] = F;
    arrayOfBoolean131[4] = F;
    arrayOfBoolean131[5] = F;
    arrayOfBoolean131[6] = F;
    arrayOfBoolean131[7] = F;
    arrayOfBoolean131[8] = F;
    arrayOfBoolean131[9] = F;
    arrayOfBoolean131[10] = F;
    arrayOfBoolean131[11] = F;
    arrayOfBoolean131[12] = F;
    arrayOfBoolean131[13] = F;
    arrayOfBoolean131[14] = F;
    arrayOfBoolean131[15] = F;
    arrayOfBoolean131[16] = F;
    arrayOfBoolean131[17] = F;
    arrayOfBoolean131[18] = F;
    arrayOfBoolean131[19] = F;
    arrayOfBoolean131[20] = F;
    arrayOfBoolean131[21] = F;
    arrayOfBoolean131[22] = F;
    arrayOfBoolean131[23] = F;
    arrayOfBoolean131[24] = F;
    arrayOfBoolean131[25] = F;
    arrayOfBoolean131[26] = T;
    arrayOfBoolean115[15] = arrayOfBoolean131;
    boolean[] arrayOfBoolean132 = new boolean[27];
    arrayOfBoolean132[0] = F;
    arrayOfBoolean132[1] = F;
    arrayOfBoolean132[2] = F;
    arrayOfBoolean132[3] = F;
    arrayOfBoolean132[4] = F;
    arrayOfBoolean132[5] = F;
    arrayOfBoolean132[6] = F;
    arrayOfBoolean132[7] = F;
    arrayOfBoolean132[8] = F;
    arrayOfBoolean132[9] = F;
    arrayOfBoolean132[10] = F;
    arrayOfBoolean132[11] = F;
    arrayOfBoolean132[12] = F;
    arrayOfBoolean132[13] = F;
    arrayOfBoolean132[14] = F;
    arrayOfBoolean132[15] = F;
    arrayOfBoolean132[16] = F;
    arrayOfBoolean132[17] = F;
    arrayOfBoolean132[18] = F;
    arrayOfBoolean132[19] = F;
    arrayOfBoolean132[20] = F;
    arrayOfBoolean132[21] = F;
    arrayOfBoolean132[22] = F;
    arrayOfBoolean132[23] = F;
    arrayOfBoolean132[24] = F;
    arrayOfBoolean132[25] = F;
    arrayOfBoolean132[26] = F;
    arrayOfBoolean115[16] = arrayOfBoolean132;
    boolean[] arrayOfBoolean133 = new boolean[27];
    arrayOfBoolean133[0] = F;
    arrayOfBoolean133[1] = F;
    arrayOfBoolean133[2] = F;
    arrayOfBoolean133[3] = F;
    arrayOfBoolean133[4] = F;
    arrayOfBoolean133[5] = F;
    arrayOfBoolean133[6] = F;
    arrayOfBoolean133[7] = F;
    arrayOfBoolean133[8] = F;
    arrayOfBoolean133[9] = F;
    arrayOfBoolean133[10] = F;
    arrayOfBoolean133[11] = F;
    arrayOfBoolean133[12] = F;
    arrayOfBoolean133[13] = F;
    arrayOfBoolean133[14] = F;
    arrayOfBoolean133[15] = F;
    arrayOfBoolean133[16] = F;
    arrayOfBoolean133[17] = F;
    arrayOfBoolean133[18] = F;
    arrayOfBoolean133[19] = F;
    arrayOfBoolean133[20] = F;
    arrayOfBoolean133[21] = F;
    arrayOfBoolean133[22] = F;
    arrayOfBoolean133[23] = F;
    arrayOfBoolean133[24] = F;
    arrayOfBoolean133[25] = F;
    arrayOfBoolean133[26] = F;
    arrayOfBoolean115[17] = arrayOfBoolean133;
    boolean[] arrayOfBoolean134 = new boolean[27];
    arrayOfBoolean134[0] = F;
    arrayOfBoolean134[1] = F;
    arrayOfBoolean134[2] = F;
    arrayOfBoolean134[3] = F;
    arrayOfBoolean134[4] = F;
    arrayOfBoolean134[5] = F;
    arrayOfBoolean134[6] = F;
    arrayOfBoolean134[7] = F;
    arrayOfBoolean134[8] = F;
    arrayOfBoolean134[9] = F;
    arrayOfBoolean134[10] = F;
    arrayOfBoolean134[11] = F;
    arrayOfBoolean134[12] = F;
    arrayOfBoolean134[13] = F;
    arrayOfBoolean134[14] = F;
    arrayOfBoolean134[15] = F;
    arrayOfBoolean134[16] = F;
    arrayOfBoolean134[17] = F;
    arrayOfBoolean134[18] = F;
    arrayOfBoolean134[19] = F;
    arrayOfBoolean134[20] = F;
    arrayOfBoolean134[21] = F;
    arrayOfBoolean134[22] = F;
    arrayOfBoolean134[23] = F;
    arrayOfBoolean134[24] = F;
    arrayOfBoolean134[25] = F;
    arrayOfBoolean134[26] = F;
    arrayOfBoolean115[18] = arrayOfBoolean134;
    boolean[] arrayOfBoolean135 = new boolean[27];
    arrayOfBoolean135[0] = F;
    arrayOfBoolean135[1] = F;
    arrayOfBoolean135[2] = F;
    arrayOfBoolean135[3] = F;
    arrayOfBoolean135[4] = F;
    arrayOfBoolean135[5] = F;
    arrayOfBoolean135[6] = F;
    arrayOfBoolean135[7] = F;
    arrayOfBoolean135[8] = F;
    arrayOfBoolean135[9] = F;
    arrayOfBoolean135[10] = F;
    arrayOfBoolean135[11] = F;
    arrayOfBoolean135[12] = F;
    arrayOfBoolean135[13] = F;
    arrayOfBoolean135[14] = F;
    arrayOfBoolean135[15] = F;
    arrayOfBoolean135[16] = F;
    arrayOfBoolean135[17] = F;
    arrayOfBoolean135[18] = F;
    arrayOfBoolean135[19] = F;
    arrayOfBoolean135[20] = F;
    arrayOfBoolean135[21] = F;
    arrayOfBoolean135[22] = F;
    arrayOfBoolean135[23] = F;
    arrayOfBoolean135[24] = F;
    arrayOfBoolean135[25] = F;
    arrayOfBoolean135[26] = F;
    arrayOfBoolean115[19] = arrayOfBoolean135;
    arrayOfBoolean114[0] = arrayOfBoolean115;
    boolean[][] arrayOfBoolean136 = new boolean[30][];
    boolean[] arrayOfBoolean137 = new boolean[27];
    arrayOfBoolean137[0] = F;
    arrayOfBoolean137[1] = F;
    arrayOfBoolean137[2] = F;
    arrayOfBoolean137[3] = F;
    arrayOfBoolean137[4] = F;
    arrayOfBoolean137[5] = F;
    arrayOfBoolean137[6] = F;
    arrayOfBoolean137[7] = F;
    arrayOfBoolean137[8] = F;
    arrayOfBoolean137[9] = F;
    arrayOfBoolean137[10] = F;
    arrayOfBoolean137[11] = F;
    arrayOfBoolean137[12] = F;
    arrayOfBoolean137[13] = F;
    arrayOfBoolean137[14] = F;
    arrayOfBoolean137[15] = F;
    arrayOfBoolean137[16] = F;
    arrayOfBoolean137[17] = F;
    arrayOfBoolean137[18] = F;
    arrayOfBoolean137[19] = F;
    arrayOfBoolean137[20] = F;
    arrayOfBoolean137[21] = F;
    arrayOfBoolean137[22] = F;
    arrayOfBoolean137[23] = F;
    arrayOfBoolean137[24] = F;
    arrayOfBoolean137[25] = F;
    arrayOfBoolean137[26] = F;
    arrayOfBoolean136[0] = arrayOfBoolean137;
    boolean[] arrayOfBoolean138 = new boolean[27];
    arrayOfBoolean138[0] = F;
    arrayOfBoolean138[1] = F;
    arrayOfBoolean138[2] = F;
    arrayOfBoolean138[3] = F;
    arrayOfBoolean138[4] = F;
    arrayOfBoolean138[5] = F;
    arrayOfBoolean138[6] = F;
    arrayOfBoolean138[7] = F;
    arrayOfBoolean138[8] = F;
    arrayOfBoolean138[9] = F;
    arrayOfBoolean138[10] = F;
    arrayOfBoolean138[11] = F;
    arrayOfBoolean138[12] = F;
    arrayOfBoolean138[13] = F;
    arrayOfBoolean138[14] = F;
    arrayOfBoolean138[15] = F;
    arrayOfBoolean138[16] = F;
    arrayOfBoolean138[17] = F;
    arrayOfBoolean138[18] = F;
    arrayOfBoolean138[19] = F;
    arrayOfBoolean138[20] = F;
    arrayOfBoolean138[21] = F;
    arrayOfBoolean138[22] = F;
    arrayOfBoolean138[23] = F;
    arrayOfBoolean138[24] = F;
    arrayOfBoolean138[25] = F;
    arrayOfBoolean138[26] = F;
    arrayOfBoolean136[1] = arrayOfBoolean138;
    boolean[] arrayOfBoolean139 = new boolean[27];
    arrayOfBoolean139[0] = F;
    arrayOfBoolean139[1] = F;
    arrayOfBoolean139[2] = F;
    arrayOfBoolean139[3] = F;
    arrayOfBoolean139[4] = F;
    arrayOfBoolean139[5] = F;
    arrayOfBoolean139[6] = F;
    arrayOfBoolean139[7] = F;
    arrayOfBoolean139[8] = F;
    arrayOfBoolean139[9] = F;
    arrayOfBoolean139[10] = F;
    arrayOfBoolean139[11] = F;
    arrayOfBoolean139[12] = F;
    arrayOfBoolean139[13] = F;
    arrayOfBoolean139[14] = F;
    arrayOfBoolean139[15] = F;
    arrayOfBoolean139[16] = F;
    arrayOfBoolean139[17] = F;
    arrayOfBoolean139[18] = F;
    arrayOfBoolean139[19] = F;
    arrayOfBoolean139[20] = F;
    arrayOfBoolean139[21] = F;
    arrayOfBoolean139[22] = F;
    arrayOfBoolean139[23] = F;
    arrayOfBoolean139[24] = F;
    arrayOfBoolean139[25] = F;
    arrayOfBoolean139[26] = F;
    arrayOfBoolean136[2] = arrayOfBoolean139;
    boolean[] arrayOfBoolean140 = new boolean[27];
    arrayOfBoolean140[0] = F;
    arrayOfBoolean140[1] = F;
    arrayOfBoolean140[2] = F;
    arrayOfBoolean140[3] = F;
    arrayOfBoolean140[4] = F;
    arrayOfBoolean140[5] = F;
    arrayOfBoolean140[6] = F;
    arrayOfBoolean140[7] = F;
    arrayOfBoolean140[8] = F;
    arrayOfBoolean140[9] = F;
    arrayOfBoolean140[10] = F;
    arrayOfBoolean140[11] = F;
    arrayOfBoolean140[12] = F;
    arrayOfBoolean140[13] = F;
    arrayOfBoolean140[14] = F;
    arrayOfBoolean140[15] = F;
    arrayOfBoolean140[16] = F;
    arrayOfBoolean140[17] = F;
    arrayOfBoolean140[18] = F;
    arrayOfBoolean140[19] = F;
    arrayOfBoolean140[20] = F;
    arrayOfBoolean140[21] = F;
    arrayOfBoolean140[22] = F;
    arrayOfBoolean140[23] = F;
    arrayOfBoolean140[24] = F;
    arrayOfBoolean140[25] = F;
    arrayOfBoolean140[26] = F;
    arrayOfBoolean136[3] = arrayOfBoolean140;
    boolean[] arrayOfBoolean141 = new boolean[27];
    arrayOfBoolean141[0] = F;
    arrayOfBoolean141[1] = F;
    arrayOfBoolean141[2] = F;
    arrayOfBoolean141[3] = F;
    arrayOfBoolean141[4] = F;
    arrayOfBoolean141[5] = F;
    arrayOfBoolean141[6] = F;
    arrayOfBoolean141[7] = F;
    arrayOfBoolean141[8] = F;
    arrayOfBoolean141[9] = F;
    arrayOfBoolean141[10] = F;
    arrayOfBoolean141[11] = F;
    arrayOfBoolean141[12] = F;
    arrayOfBoolean141[13] = F;
    arrayOfBoolean141[14] = F;
    arrayOfBoolean141[15] = F;
    arrayOfBoolean141[16] = F;
    arrayOfBoolean141[17] = F;
    arrayOfBoolean141[18] = F;
    arrayOfBoolean141[19] = F;
    arrayOfBoolean141[20] = F;
    arrayOfBoolean141[21] = F;
    arrayOfBoolean141[22] = F;
    arrayOfBoolean141[23] = F;
    arrayOfBoolean141[24] = F;
    arrayOfBoolean141[25] = F;
    arrayOfBoolean141[26] = F;
    arrayOfBoolean136[4] = arrayOfBoolean141;
    boolean[] arrayOfBoolean142 = new boolean[27];
    arrayOfBoolean142[0] = F;
    arrayOfBoolean142[1] = F;
    arrayOfBoolean142[2] = F;
    arrayOfBoolean142[3] = F;
    arrayOfBoolean142[4] = F;
    arrayOfBoolean142[5] = F;
    arrayOfBoolean142[6] = F;
    arrayOfBoolean142[7] = F;
    arrayOfBoolean142[8] = F;
    arrayOfBoolean142[9] = F;
    arrayOfBoolean142[10] = F;
    arrayOfBoolean142[11] = F;
    arrayOfBoolean142[12] = F;
    arrayOfBoolean142[13] = F;
    arrayOfBoolean142[14] = F;
    arrayOfBoolean142[15] = F;
    arrayOfBoolean142[16] = F;
    arrayOfBoolean142[17] = F;
    arrayOfBoolean142[18] = F;
    arrayOfBoolean142[19] = F;
    arrayOfBoolean142[20] = F;
    arrayOfBoolean142[21] = F;
    arrayOfBoolean142[22] = F;
    arrayOfBoolean142[23] = F;
    arrayOfBoolean142[24] = F;
    arrayOfBoolean142[25] = F;
    arrayOfBoolean142[26] = F;
    arrayOfBoolean136[5] = arrayOfBoolean142;
    boolean[] arrayOfBoolean143 = new boolean[27];
    arrayOfBoolean143[0] = F;
    arrayOfBoolean143[1] = F;
    arrayOfBoolean143[2] = F;
    arrayOfBoolean143[3] = T;
    arrayOfBoolean143[4] = T;
    arrayOfBoolean143[5] = F;
    arrayOfBoolean143[6] = F;
    arrayOfBoolean143[7] = F;
    arrayOfBoolean143[8] = F;
    arrayOfBoolean143[9] = F;
    arrayOfBoolean143[10] = F;
    arrayOfBoolean143[11] = F;
    arrayOfBoolean143[12] = F;
    arrayOfBoolean143[13] = F;
    arrayOfBoolean143[14] = F;
    arrayOfBoolean143[15] = F;
    arrayOfBoolean143[16] = F;
    arrayOfBoolean143[17] = F;
    arrayOfBoolean143[18] = F;
    arrayOfBoolean143[19] = F;
    arrayOfBoolean143[20] = F;
    arrayOfBoolean143[21] = F;
    arrayOfBoolean143[22] = F;
    arrayOfBoolean143[23] = F;
    arrayOfBoolean143[24] = F;
    arrayOfBoolean143[25] = F;
    arrayOfBoolean143[26] = F;
    arrayOfBoolean136[6] = arrayOfBoolean143;
    boolean[] arrayOfBoolean144 = new boolean[27];
    arrayOfBoolean144[0] = F;
    arrayOfBoolean144[1] = F;
    arrayOfBoolean144[2] = F;
    arrayOfBoolean144[3] = T;
    arrayOfBoolean144[4] = T;
    arrayOfBoolean144[5] = F;
    arrayOfBoolean144[6] = F;
    arrayOfBoolean144[7] = F;
    arrayOfBoolean144[8] = F;
    arrayOfBoolean144[9] = F;
    arrayOfBoolean144[10] = F;
    arrayOfBoolean144[11] = F;
    arrayOfBoolean144[12] = F;
    arrayOfBoolean144[13] = F;
    arrayOfBoolean144[14] = F;
    arrayOfBoolean144[15] = F;
    arrayOfBoolean144[16] = F;
    arrayOfBoolean144[17] = F;
    arrayOfBoolean144[18] = F;
    arrayOfBoolean144[19] = F;
    arrayOfBoolean144[20] = F;
    arrayOfBoolean144[21] = F;
    arrayOfBoolean144[22] = F;
    arrayOfBoolean144[23] = F;
    arrayOfBoolean144[24] = F;
    arrayOfBoolean144[25] = F;
    arrayOfBoolean144[26] = F;
    arrayOfBoolean136[7] = arrayOfBoolean144;
    boolean[] arrayOfBoolean145 = new boolean[27];
    arrayOfBoolean145[0] = F;
    arrayOfBoolean145[1] = F;
    arrayOfBoolean145[2] = F;
    arrayOfBoolean145[3] = T;
    arrayOfBoolean145[4] = T;
    arrayOfBoolean145[5] = F;
    arrayOfBoolean145[6] = F;
    arrayOfBoolean145[7] = F;
    arrayOfBoolean145[8] = F;
    arrayOfBoolean145[9] = F;
    arrayOfBoolean145[10] = F;
    arrayOfBoolean145[11] = F;
    arrayOfBoolean145[12] = F;
    arrayOfBoolean145[13] = F;
    arrayOfBoolean145[14] = F;
    arrayOfBoolean145[15] = F;
    arrayOfBoolean145[16] = F;
    arrayOfBoolean145[17] = F;
    arrayOfBoolean145[18] = F;
    arrayOfBoolean145[19] = F;
    arrayOfBoolean145[20] = F;
    arrayOfBoolean145[21] = F;
    arrayOfBoolean145[22] = F;
    arrayOfBoolean145[23] = F;
    arrayOfBoolean145[24] = F;
    arrayOfBoolean145[25] = F;
    arrayOfBoolean145[26] = F;
    arrayOfBoolean136[8] = arrayOfBoolean145;
    boolean[] arrayOfBoolean146 = new boolean[27];
    arrayOfBoolean146[0] = F;
    arrayOfBoolean146[1] = F;
    arrayOfBoolean146[2] = F;
    arrayOfBoolean146[3] = T;
    arrayOfBoolean146[4] = T;
    arrayOfBoolean146[5] = T;
    arrayOfBoolean146[6] = T;
    arrayOfBoolean146[7] = T;
    arrayOfBoolean146[8] = F;
    arrayOfBoolean146[9] = F;
    arrayOfBoolean146[10] = T;
    arrayOfBoolean146[11] = T;
    arrayOfBoolean146[12] = T;
    arrayOfBoolean146[13] = T;
    arrayOfBoolean146[14] = T;
    arrayOfBoolean146[15] = T;
    arrayOfBoolean146[16] = F;
    arrayOfBoolean146[17] = F;
    arrayOfBoolean146[18] = T;
    arrayOfBoolean146[19] = T;
    arrayOfBoolean146[20] = T;
    arrayOfBoolean146[21] = T;
    arrayOfBoolean146[22] = T;
    arrayOfBoolean146[23] = F;
    arrayOfBoolean146[24] = F;
    arrayOfBoolean146[25] = F;
    arrayOfBoolean146[26] = F;
    arrayOfBoolean136[9] = arrayOfBoolean146;
    boolean[] arrayOfBoolean147 = new boolean[27];
    arrayOfBoolean147[0] = F;
    arrayOfBoolean147[1] = F;
    arrayOfBoolean147[2] = F;
    arrayOfBoolean147[3] = T;
    arrayOfBoolean147[4] = T;
    arrayOfBoolean147[5] = T;
    arrayOfBoolean147[6] = T;
    arrayOfBoolean147[7] = T;
    arrayOfBoolean147[8] = T;
    arrayOfBoolean147[9] = F;
    arrayOfBoolean147[10] = T;
    arrayOfBoolean147[11] = T;
    arrayOfBoolean147[12] = T;
    arrayOfBoolean147[13] = T;
    arrayOfBoolean147[14] = T;
    arrayOfBoolean147[15] = T;
    arrayOfBoolean147[16] = F;
    arrayOfBoolean147[17] = T;
    arrayOfBoolean147[18] = T;
    arrayOfBoolean147[19] = T;
    arrayOfBoolean147[20] = T;
    arrayOfBoolean147[21] = T;
    arrayOfBoolean147[22] = T;
    arrayOfBoolean147[23] = F;
    arrayOfBoolean147[24] = F;
    arrayOfBoolean147[25] = F;
    arrayOfBoolean147[26] = F;
    arrayOfBoolean136[10] = arrayOfBoolean147;
    boolean[] arrayOfBoolean148 = new boolean[27];
    arrayOfBoolean148[0] = F;
    arrayOfBoolean148[1] = F;
    arrayOfBoolean148[2] = F;
    arrayOfBoolean148[3] = T;
    arrayOfBoolean148[4] = T;
    arrayOfBoolean148[5] = F;
    arrayOfBoolean148[6] = F;
    arrayOfBoolean148[7] = T;
    arrayOfBoolean148[8] = T;
    arrayOfBoolean148[9] = F;
    arrayOfBoolean148[10] = F;
    arrayOfBoolean148[11] = F;
    arrayOfBoolean148[12] = T;
    arrayOfBoolean148[13] = T;
    arrayOfBoolean148[14] = F;
    arrayOfBoolean148[15] = F;
    arrayOfBoolean148[16] = F;
    arrayOfBoolean148[17] = T;
    arrayOfBoolean148[18] = T;
    arrayOfBoolean148[19] = F;
    arrayOfBoolean148[20] = F;
    arrayOfBoolean148[21] = F;
    arrayOfBoolean148[22] = F;
    arrayOfBoolean148[23] = F;
    arrayOfBoolean148[24] = F;
    arrayOfBoolean148[25] = F;
    arrayOfBoolean148[26] = F;
    arrayOfBoolean136[11] = arrayOfBoolean148;
    boolean[] arrayOfBoolean149 = new boolean[27];
    arrayOfBoolean149[0] = F;
    arrayOfBoolean149[1] = F;
    arrayOfBoolean149[2] = F;
    arrayOfBoolean149[3] = T;
    arrayOfBoolean149[4] = T;
    arrayOfBoolean149[5] = F;
    arrayOfBoolean149[6] = F;
    arrayOfBoolean149[7] = T;
    arrayOfBoolean149[8] = T;
    arrayOfBoolean149[9] = F;
    arrayOfBoolean149[10] = F;
    arrayOfBoolean149[11] = F;
    arrayOfBoolean149[12] = T;
    arrayOfBoolean149[13] = T;
    arrayOfBoolean149[14] = F;
    arrayOfBoolean149[15] = F;
    arrayOfBoolean149[16] = F;
    arrayOfBoolean149[17] = T;
    arrayOfBoolean149[18] = T;
    arrayOfBoolean149[19] = F;
    arrayOfBoolean149[20] = F;
    arrayOfBoolean149[21] = F;
    arrayOfBoolean149[22] = F;
    arrayOfBoolean149[23] = F;
    arrayOfBoolean149[24] = F;
    arrayOfBoolean149[25] = F;
    arrayOfBoolean149[26] = F;
    arrayOfBoolean136[12] = arrayOfBoolean149;
    boolean[] arrayOfBoolean150 = new boolean[27];
    arrayOfBoolean150[0] = F;
    arrayOfBoolean150[1] = F;
    arrayOfBoolean150[2] = F;
    arrayOfBoolean150[3] = T;
    arrayOfBoolean150[4] = T;
    arrayOfBoolean150[5] = F;
    arrayOfBoolean150[6] = F;
    arrayOfBoolean150[7] = T;
    arrayOfBoolean150[8] = T;
    arrayOfBoolean150[9] = F;
    arrayOfBoolean150[10] = F;
    arrayOfBoolean150[11] = F;
    arrayOfBoolean150[12] = T;
    arrayOfBoolean150[13] = T;
    arrayOfBoolean150[14] = F;
    arrayOfBoolean150[15] = F;
    arrayOfBoolean150[16] = F;
    arrayOfBoolean150[17] = T;
    arrayOfBoolean150[18] = T;
    arrayOfBoolean150[19] = F;
    arrayOfBoolean150[20] = F;
    arrayOfBoolean150[21] = F;
    arrayOfBoolean150[22] = F;
    arrayOfBoolean150[23] = F;
    arrayOfBoolean150[24] = F;
    arrayOfBoolean150[25] = F;
    arrayOfBoolean150[26] = F;
    arrayOfBoolean136[13] = arrayOfBoolean150;
    boolean[] arrayOfBoolean151 = new boolean[27];
    arrayOfBoolean151[0] = F;
    arrayOfBoolean151[1] = F;
    arrayOfBoolean151[2] = F;
    arrayOfBoolean151[3] = T;
    arrayOfBoolean151[4] = T;
    arrayOfBoolean151[5] = F;
    arrayOfBoolean151[6] = F;
    arrayOfBoolean151[7] = T;
    arrayOfBoolean151[8] = T;
    arrayOfBoolean151[9] = F;
    arrayOfBoolean151[10] = F;
    arrayOfBoolean151[11] = F;
    arrayOfBoolean151[12] = T;
    arrayOfBoolean151[13] = T;
    arrayOfBoolean151[14] = F;
    arrayOfBoolean151[15] = F;
    arrayOfBoolean151[16] = F;
    arrayOfBoolean151[17] = T;
    arrayOfBoolean151[18] = T;
    arrayOfBoolean151[19] = T;
    arrayOfBoolean151[20] = T;
    arrayOfBoolean151[21] = T;
    arrayOfBoolean151[22] = T;
    arrayOfBoolean151[23] = F;
    arrayOfBoolean151[24] = F;
    arrayOfBoolean151[25] = F;
    arrayOfBoolean151[26] = F;
    arrayOfBoolean136[14] = arrayOfBoolean151;
    boolean[] arrayOfBoolean152 = new boolean[27];
    arrayOfBoolean152[0] = F;
    arrayOfBoolean152[1] = F;
    arrayOfBoolean152[2] = F;
    arrayOfBoolean152[3] = T;
    arrayOfBoolean152[4] = T;
    arrayOfBoolean152[5] = F;
    arrayOfBoolean152[6] = F;
    arrayOfBoolean152[7] = T;
    arrayOfBoolean152[8] = T;
    arrayOfBoolean152[9] = F;
    arrayOfBoolean152[10] = F;
    arrayOfBoolean152[11] = F;
    arrayOfBoolean152[12] = T;
    arrayOfBoolean152[13] = T;
    arrayOfBoolean152[14] = F;
    arrayOfBoolean152[15] = F;
    arrayOfBoolean152[16] = F;
    arrayOfBoolean152[17] = F;
    arrayOfBoolean152[18] = T;
    arrayOfBoolean152[19] = T;
    arrayOfBoolean152[20] = T;
    arrayOfBoolean152[21] = T;
    arrayOfBoolean152[22] = T;
    arrayOfBoolean152[23] = F;
    arrayOfBoolean152[24] = F;
    arrayOfBoolean152[25] = F;
    arrayOfBoolean152[26] = F;
    arrayOfBoolean136[15] = arrayOfBoolean152;
    boolean[] arrayOfBoolean153 = new boolean[27];
    arrayOfBoolean153[0] = F;
    arrayOfBoolean153[1] = F;
    arrayOfBoolean153[2] = F;
    arrayOfBoolean153[3] = F;
    arrayOfBoolean153[4] = F;
    arrayOfBoolean153[5] = F;
    arrayOfBoolean153[6] = F;
    arrayOfBoolean153[7] = F;
    arrayOfBoolean153[8] = F;
    arrayOfBoolean153[9] = F;
    arrayOfBoolean153[10] = F;
    arrayOfBoolean153[11] = F;
    arrayOfBoolean153[12] = F;
    arrayOfBoolean153[13] = F;
    arrayOfBoolean153[14] = F;
    arrayOfBoolean153[15] = F;
    arrayOfBoolean153[16] = F;
    arrayOfBoolean153[17] = F;
    arrayOfBoolean153[18] = F;
    arrayOfBoolean153[19] = F;
    arrayOfBoolean153[20] = F;
    arrayOfBoolean153[21] = F;
    arrayOfBoolean153[22] = F;
    arrayOfBoolean153[23] = F;
    arrayOfBoolean153[24] = F;
    arrayOfBoolean153[25] = F;
    arrayOfBoolean153[26] = F;
    arrayOfBoolean136[16] = arrayOfBoolean153;
    boolean[] arrayOfBoolean154 = new boolean[27];
    arrayOfBoolean154[0] = F;
    arrayOfBoolean154[1] = F;
    arrayOfBoolean154[2] = F;
    arrayOfBoolean154[3] = F;
    arrayOfBoolean154[4] = F;
    arrayOfBoolean154[5] = F;
    arrayOfBoolean154[6] = F;
    arrayOfBoolean154[7] = F;
    arrayOfBoolean154[8] = F;
    arrayOfBoolean154[9] = F;
    arrayOfBoolean154[10] = F;
    arrayOfBoolean154[11] = F;
    arrayOfBoolean154[12] = F;
    arrayOfBoolean154[13] = F;
    arrayOfBoolean154[14] = F;
    arrayOfBoolean154[15] = F;
    arrayOfBoolean154[16] = F;
    arrayOfBoolean154[17] = F;
    arrayOfBoolean154[18] = F;
    arrayOfBoolean154[19] = F;
    arrayOfBoolean154[20] = F;
    arrayOfBoolean154[21] = F;
    arrayOfBoolean154[22] = F;
    arrayOfBoolean154[23] = F;
    arrayOfBoolean154[24] = F;
    arrayOfBoolean154[25] = F;
    arrayOfBoolean154[26] = F;
    arrayOfBoolean136[17] = arrayOfBoolean154;
    boolean[] arrayOfBoolean155 = new boolean[27];
    arrayOfBoolean155[0] = F;
    arrayOfBoolean155[1] = F;
    arrayOfBoolean155[2] = F;
    arrayOfBoolean155[3] = F;
    arrayOfBoolean155[4] = F;
    arrayOfBoolean155[5] = F;
    arrayOfBoolean155[6] = F;
    arrayOfBoolean155[7] = F;
    arrayOfBoolean155[8] = F;
    arrayOfBoolean155[9] = F;
    arrayOfBoolean155[10] = F;
    arrayOfBoolean155[11] = F;
    arrayOfBoolean155[12] = F;
    arrayOfBoolean155[13] = F;
    arrayOfBoolean155[14] = F;
    arrayOfBoolean155[15] = F;
    arrayOfBoolean155[16] = F;
    arrayOfBoolean155[17] = F;
    arrayOfBoolean155[18] = F;
    arrayOfBoolean155[19] = F;
    arrayOfBoolean155[20] = F;
    arrayOfBoolean155[21] = F;
    arrayOfBoolean155[22] = F;
    arrayOfBoolean155[23] = F;
    arrayOfBoolean155[24] = F;
    arrayOfBoolean155[25] = F;
    arrayOfBoolean155[26] = F;
    arrayOfBoolean136[18] = arrayOfBoolean155;
    boolean[] arrayOfBoolean156 = new boolean[27];
    arrayOfBoolean156[0] = F;
    arrayOfBoolean156[1] = F;
    arrayOfBoolean156[2] = F;
    arrayOfBoolean156[3] = F;
    arrayOfBoolean156[4] = F;
    arrayOfBoolean156[5] = F;
    arrayOfBoolean156[6] = F;
    arrayOfBoolean156[7] = F;
    arrayOfBoolean156[8] = F;
    arrayOfBoolean156[9] = F;
    arrayOfBoolean156[10] = F;
    arrayOfBoolean156[11] = F;
    arrayOfBoolean156[12] = F;
    arrayOfBoolean156[13] = F;
    arrayOfBoolean156[14] = F;
    arrayOfBoolean156[15] = F;
    arrayOfBoolean156[16] = F;
    arrayOfBoolean156[17] = F;
    arrayOfBoolean156[18] = F;
    arrayOfBoolean156[19] = F;
    arrayOfBoolean156[20] = F;
    arrayOfBoolean156[21] = F;
    arrayOfBoolean156[22] = F;
    arrayOfBoolean156[23] = F;
    arrayOfBoolean156[24] = F;
    arrayOfBoolean156[25] = F;
    arrayOfBoolean156[26] = F;
    arrayOfBoolean136[19] = arrayOfBoolean156;
    boolean[] arrayOfBoolean157 = new boolean[27];
    arrayOfBoolean157[0] = F;
    arrayOfBoolean157[1] = F;
    arrayOfBoolean157[2] = F;
    arrayOfBoolean157[3] = F;
    arrayOfBoolean157[4] = F;
    arrayOfBoolean157[5] = F;
    arrayOfBoolean157[6] = F;
    arrayOfBoolean157[7] = F;
    arrayOfBoolean157[8] = F;
    arrayOfBoolean157[9] = F;
    arrayOfBoolean157[10] = F;
    arrayOfBoolean157[11] = F;
    arrayOfBoolean157[12] = F;
    arrayOfBoolean157[13] = F;
    arrayOfBoolean157[14] = F;
    arrayOfBoolean157[15] = F;
    arrayOfBoolean157[16] = F;
    arrayOfBoolean157[17] = F;
    arrayOfBoolean157[18] = F;
    arrayOfBoolean157[19] = F;
    arrayOfBoolean157[20] = F;
    arrayOfBoolean157[21] = F;
    arrayOfBoolean157[22] = F;
    arrayOfBoolean157[23] = F;
    arrayOfBoolean157[24] = F;
    arrayOfBoolean157[25] = F;
    arrayOfBoolean157[26] = F;
    arrayOfBoolean136[20] = arrayOfBoolean157;
    boolean[] arrayOfBoolean158 = new boolean[27];
    arrayOfBoolean158[0] = F;
    arrayOfBoolean158[1] = F;
    arrayOfBoolean158[2] = F;
    arrayOfBoolean158[3] = F;
    arrayOfBoolean158[4] = F;
    arrayOfBoolean158[5] = F;
    arrayOfBoolean158[6] = F;
    arrayOfBoolean158[7] = F;
    arrayOfBoolean158[8] = F;
    arrayOfBoolean158[9] = F;
    arrayOfBoolean158[10] = F;
    arrayOfBoolean158[11] = F;
    arrayOfBoolean158[12] = F;
    arrayOfBoolean158[13] = F;
    arrayOfBoolean158[14] = F;
    arrayOfBoolean158[15] = F;
    arrayOfBoolean158[16] = F;
    arrayOfBoolean158[17] = F;
    arrayOfBoolean158[18] = F;
    arrayOfBoolean158[19] = F;
    arrayOfBoolean158[20] = F;
    arrayOfBoolean158[21] = F;
    arrayOfBoolean158[22] = F;
    arrayOfBoolean158[23] = F;
    arrayOfBoolean158[24] = F;
    arrayOfBoolean158[25] = F;
    arrayOfBoolean158[26] = F;
    arrayOfBoolean136[21] = arrayOfBoolean158;
    boolean[] arrayOfBoolean159 = new boolean[27];
    arrayOfBoolean159[0] = F;
    arrayOfBoolean159[1] = F;
    arrayOfBoolean159[2] = F;
    arrayOfBoolean159[3] = F;
    arrayOfBoolean159[4] = F;
    arrayOfBoolean159[5] = F;
    arrayOfBoolean159[6] = F;
    arrayOfBoolean159[7] = F;
    arrayOfBoolean159[8] = F;
    arrayOfBoolean159[9] = F;
    arrayOfBoolean159[10] = F;
    arrayOfBoolean159[11] = F;
    arrayOfBoolean159[12] = F;
    arrayOfBoolean159[13] = F;
    arrayOfBoolean159[14] = F;
    arrayOfBoolean159[15] = F;
    arrayOfBoolean159[16] = F;
    arrayOfBoolean159[17] = F;
    arrayOfBoolean159[18] = F;
    arrayOfBoolean159[19] = F;
    arrayOfBoolean159[20] = F;
    arrayOfBoolean159[21] = F;
    arrayOfBoolean159[22] = F;
    arrayOfBoolean159[23] = F;
    arrayOfBoolean159[24] = F;
    arrayOfBoolean159[25] = F;
    arrayOfBoolean159[26] = F;
    arrayOfBoolean136[22] = arrayOfBoolean159;
    boolean[] arrayOfBoolean160 = new boolean[27];
    arrayOfBoolean160[0] = F;
    arrayOfBoolean160[1] = F;
    arrayOfBoolean160[2] = F;
    arrayOfBoolean160[3] = F;
    arrayOfBoolean160[4] = F;
    arrayOfBoolean160[5] = F;
    arrayOfBoolean160[6] = F;
    arrayOfBoolean160[7] = F;
    arrayOfBoolean160[8] = F;
    arrayOfBoolean160[9] = F;
    arrayOfBoolean160[10] = F;
    arrayOfBoolean160[11] = F;
    arrayOfBoolean160[12] = F;
    arrayOfBoolean160[13] = F;
    arrayOfBoolean160[14] = F;
    arrayOfBoolean160[15] = F;
    arrayOfBoolean160[16] = F;
    arrayOfBoolean160[17] = F;
    arrayOfBoolean160[18] = F;
    arrayOfBoolean160[19] = F;
    arrayOfBoolean160[20] = F;
    arrayOfBoolean160[21] = F;
    arrayOfBoolean160[22] = F;
    arrayOfBoolean160[23] = F;
    arrayOfBoolean160[24] = F;
    arrayOfBoolean160[25] = F;
    arrayOfBoolean160[26] = F;
    arrayOfBoolean136[23] = arrayOfBoolean160;
    boolean[] arrayOfBoolean161 = new boolean[27];
    arrayOfBoolean161[0] = F;
    arrayOfBoolean161[1] = F;
    arrayOfBoolean161[2] = F;
    arrayOfBoolean161[3] = F;
    arrayOfBoolean161[4] = F;
    arrayOfBoolean161[5] = F;
    arrayOfBoolean161[6] = F;
    arrayOfBoolean161[7] = F;
    arrayOfBoolean161[8] = F;
    arrayOfBoolean161[9] = F;
    arrayOfBoolean161[10] = F;
    arrayOfBoolean161[11] = F;
    arrayOfBoolean161[12] = F;
    arrayOfBoolean161[13] = F;
    arrayOfBoolean161[14] = F;
    arrayOfBoolean161[15] = F;
    arrayOfBoolean161[16] = F;
    arrayOfBoolean161[17] = F;
    arrayOfBoolean161[18] = F;
    arrayOfBoolean161[19] = F;
    arrayOfBoolean161[20] = F;
    arrayOfBoolean161[21] = F;
    arrayOfBoolean161[22] = F;
    arrayOfBoolean161[23] = F;
    arrayOfBoolean161[24] = F;
    arrayOfBoolean161[25] = F;
    arrayOfBoolean161[26] = F;
    arrayOfBoolean136[24] = arrayOfBoolean161;
    boolean[] arrayOfBoolean162 = new boolean[27];
    arrayOfBoolean162[0] = F;
    arrayOfBoolean162[1] = F;
    arrayOfBoolean162[2] = F;
    arrayOfBoolean162[3] = F;
    arrayOfBoolean162[4] = F;
    arrayOfBoolean162[5] = F;
    arrayOfBoolean162[6] = F;
    arrayOfBoolean162[7] = F;
    arrayOfBoolean162[8] = F;
    arrayOfBoolean162[9] = F;
    arrayOfBoolean162[10] = F;
    arrayOfBoolean162[11] = F;
    arrayOfBoolean162[12] = F;
    arrayOfBoolean162[13] = F;
    arrayOfBoolean162[14] = F;
    arrayOfBoolean162[15] = F;
    arrayOfBoolean162[16] = F;
    arrayOfBoolean162[17] = F;
    arrayOfBoolean162[18] = F;
    arrayOfBoolean162[19] = F;
    arrayOfBoolean162[20] = F;
    arrayOfBoolean162[21] = F;
    arrayOfBoolean162[22] = F;
    arrayOfBoolean162[23] = F;
    arrayOfBoolean162[24] = F;
    arrayOfBoolean162[25] = F;
    arrayOfBoolean162[26] = F;
    arrayOfBoolean136[25] = arrayOfBoolean162;
    boolean[] arrayOfBoolean163 = new boolean[27];
    arrayOfBoolean163[0] = F;
    arrayOfBoolean163[1] = F;
    arrayOfBoolean163[2] = F;
    arrayOfBoolean163[3] = F;
    arrayOfBoolean163[4] = F;
    arrayOfBoolean163[5] = F;
    arrayOfBoolean163[6] = F;
    arrayOfBoolean163[7] = F;
    arrayOfBoolean163[8] = F;
    arrayOfBoolean163[9] = F;
    arrayOfBoolean163[10] = F;
    arrayOfBoolean163[11] = F;
    arrayOfBoolean163[12] = F;
    arrayOfBoolean163[13] = F;
    arrayOfBoolean163[14] = F;
    arrayOfBoolean163[15] = F;
    arrayOfBoolean163[16] = F;
    arrayOfBoolean163[17] = F;
    arrayOfBoolean163[18] = F;
    arrayOfBoolean163[19] = F;
    arrayOfBoolean163[20] = F;
    arrayOfBoolean163[21] = F;
    arrayOfBoolean163[22] = F;
    arrayOfBoolean163[23] = F;
    arrayOfBoolean163[24] = F;
    arrayOfBoolean163[25] = F;
    arrayOfBoolean163[26] = F;
    arrayOfBoolean136[26] = arrayOfBoolean163;
    boolean[] arrayOfBoolean164 = new boolean[27];
    arrayOfBoolean164[0] = F;
    arrayOfBoolean164[1] = F;
    arrayOfBoolean164[2] = F;
    arrayOfBoolean164[3] = F;
    arrayOfBoolean164[4] = F;
    arrayOfBoolean164[5] = F;
    arrayOfBoolean164[6] = F;
    arrayOfBoolean164[7] = F;
    arrayOfBoolean164[8] = F;
    arrayOfBoolean164[9] = F;
    arrayOfBoolean164[10] = F;
    arrayOfBoolean164[11] = F;
    arrayOfBoolean164[12] = F;
    arrayOfBoolean164[13] = F;
    arrayOfBoolean164[14] = F;
    arrayOfBoolean164[15] = F;
    arrayOfBoolean164[16] = F;
    arrayOfBoolean164[17] = F;
    arrayOfBoolean164[18] = F;
    arrayOfBoolean164[19] = F;
    arrayOfBoolean164[20] = F;
    arrayOfBoolean164[21] = F;
    arrayOfBoolean164[22] = F;
    arrayOfBoolean164[23] = F;
    arrayOfBoolean164[24] = F;
    arrayOfBoolean164[25] = F;
    arrayOfBoolean164[26] = F;
    arrayOfBoolean136[27] = arrayOfBoolean164;
    boolean[] arrayOfBoolean165 = new boolean[27];
    arrayOfBoolean165[0] = F;
    arrayOfBoolean165[1] = F;
    arrayOfBoolean165[2] = F;
    arrayOfBoolean165[3] = F;
    arrayOfBoolean165[4] = F;
    arrayOfBoolean165[5] = F;
    arrayOfBoolean165[6] = F;
    arrayOfBoolean165[7] = F;
    arrayOfBoolean165[8] = F;
    arrayOfBoolean165[9] = F;
    arrayOfBoolean165[10] = F;
    arrayOfBoolean165[11] = F;
    arrayOfBoolean165[12] = F;
    arrayOfBoolean165[13] = F;
    arrayOfBoolean165[14] = F;
    arrayOfBoolean165[15] = F;
    arrayOfBoolean165[16] = F;
    arrayOfBoolean165[17] = F;
    arrayOfBoolean165[18] = F;
    arrayOfBoolean165[19] = F;
    arrayOfBoolean165[20] = F;
    arrayOfBoolean165[21] = F;
    arrayOfBoolean165[22] = F;
    arrayOfBoolean165[23] = F;
    arrayOfBoolean165[24] = F;
    arrayOfBoolean165[25] = F;
    arrayOfBoolean165[26] = F;
    arrayOfBoolean136[28] = arrayOfBoolean165;
    boolean[] arrayOfBoolean166 = new boolean[27];
    arrayOfBoolean166[0] = F;
    arrayOfBoolean166[1] = F;
    arrayOfBoolean166[2] = F;
    arrayOfBoolean166[3] = F;
    arrayOfBoolean166[4] = F;
    arrayOfBoolean166[5] = F;
    arrayOfBoolean166[6] = F;
    arrayOfBoolean166[7] = F;
    arrayOfBoolean166[8] = F;
    arrayOfBoolean166[9] = F;
    arrayOfBoolean166[10] = F;
    arrayOfBoolean166[11] = F;
    arrayOfBoolean166[12] = F;
    arrayOfBoolean166[13] = F;
    arrayOfBoolean166[14] = F;
    arrayOfBoolean166[15] = F;
    arrayOfBoolean166[16] = F;
    arrayOfBoolean166[17] = F;
    arrayOfBoolean166[18] = F;
    arrayOfBoolean166[19] = F;
    arrayOfBoolean166[20] = F;
    arrayOfBoolean166[21] = F;
    arrayOfBoolean166[22] = F;
    arrayOfBoolean166[23] = F;
    arrayOfBoolean166[24] = F;
    arrayOfBoolean166[25] = F;
    arrayOfBoolean166[26] = F;
    arrayOfBoolean136[29] = arrayOfBoolean166;
    arrayOfBoolean114[1] = arrayOfBoolean136;
    boolean[][] arrayOfBoolean167 = new boolean[30][];
    boolean[] arrayOfBoolean168 = new boolean[27];
    arrayOfBoolean168[0] = T;
    arrayOfBoolean168[1] = T;
    arrayOfBoolean168[2] = T;
    arrayOfBoolean168[3] = T;
    arrayOfBoolean168[4] = T;
    arrayOfBoolean168[5] = T;
    arrayOfBoolean168[6] = T;
    arrayOfBoolean168[7] = T;
    arrayOfBoolean168[8] = T;
    arrayOfBoolean168[9] = T;
    arrayOfBoolean168[10] = T;
    arrayOfBoolean168[11] = T;
    arrayOfBoolean168[12] = T;
    arrayOfBoolean168[13] = T;
    arrayOfBoolean168[14] = T;
    arrayOfBoolean168[15] = T;
    arrayOfBoolean168[16] = T;
    arrayOfBoolean168[17] = T;
    arrayOfBoolean168[18] = T;
    arrayOfBoolean168[19] = T;
    arrayOfBoolean168[20] = T;
    arrayOfBoolean168[21] = T;
    arrayOfBoolean168[22] = T;
    arrayOfBoolean168[23] = T;
    arrayOfBoolean168[24] = T;
    arrayOfBoolean168[25] = T;
    arrayOfBoolean168[26] = T;
    arrayOfBoolean167[0] = arrayOfBoolean168;
    boolean[] arrayOfBoolean169 = new boolean[27];
    arrayOfBoolean169[0] = T;
    arrayOfBoolean169[1] = T;
    arrayOfBoolean169[2] = T;
    arrayOfBoolean169[3] = T;
    arrayOfBoolean169[4] = T;
    arrayOfBoolean169[5] = T;
    arrayOfBoolean169[6] = T;
    arrayOfBoolean169[7] = T;
    arrayOfBoolean169[8] = T;
    arrayOfBoolean169[9] = T;
    arrayOfBoolean169[10] = T;
    arrayOfBoolean169[11] = T;
    arrayOfBoolean169[12] = T;
    arrayOfBoolean169[13] = T;
    arrayOfBoolean169[14] = T;
    arrayOfBoolean169[15] = T;
    arrayOfBoolean169[16] = T;
    arrayOfBoolean169[17] = T;
    arrayOfBoolean169[18] = T;
    arrayOfBoolean169[19] = T;
    arrayOfBoolean169[20] = T;
    arrayOfBoolean169[21] = T;
    arrayOfBoolean169[22] = T;
    arrayOfBoolean169[23] = T;
    arrayOfBoolean169[24] = T;
    arrayOfBoolean169[25] = T;
    arrayOfBoolean169[26] = T;
    arrayOfBoolean167[1] = arrayOfBoolean169;
    boolean[] arrayOfBoolean170 = new boolean[27];
    arrayOfBoolean170[0] = T;
    arrayOfBoolean170[1] = T;
    arrayOfBoolean170[2] = T;
    arrayOfBoolean170[3] = T;
    arrayOfBoolean170[4] = T;
    arrayOfBoolean170[5] = T;
    arrayOfBoolean170[6] = T;
    arrayOfBoolean170[7] = T;
    arrayOfBoolean170[8] = T;
    arrayOfBoolean170[9] = T;
    arrayOfBoolean170[10] = T;
    arrayOfBoolean170[11] = T;
    arrayOfBoolean170[12] = T;
    arrayOfBoolean170[13] = T;
    arrayOfBoolean170[14] = T;
    arrayOfBoolean170[15] = T;
    arrayOfBoolean170[16] = T;
    arrayOfBoolean170[17] = T;
    arrayOfBoolean170[18] = T;
    arrayOfBoolean170[19] = T;
    arrayOfBoolean170[20] = T;
    arrayOfBoolean170[21] = T;
    arrayOfBoolean170[22] = T;
    arrayOfBoolean170[23] = T;
    arrayOfBoolean170[24] = T;
    arrayOfBoolean170[25] = T;
    arrayOfBoolean170[26] = T;
    arrayOfBoolean167[2] = arrayOfBoolean170;
    boolean[] arrayOfBoolean171 = new boolean[27];
    arrayOfBoolean171[0] = T;
    arrayOfBoolean171[1] = T;
    arrayOfBoolean171[2] = T;
    arrayOfBoolean171[3] = T;
    arrayOfBoolean171[4] = T;
    arrayOfBoolean171[5] = T;
    arrayOfBoolean171[6] = T;
    arrayOfBoolean171[7] = T;
    arrayOfBoolean171[8] = T;
    arrayOfBoolean171[9] = T;
    arrayOfBoolean171[10] = T;
    arrayOfBoolean171[11] = T;
    arrayOfBoolean171[12] = T;
    arrayOfBoolean171[13] = T;
    arrayOfBoolean171[14] = T;
    arrayOfBoolean171[15] = T;
    arrayOfBoolean171[16] = T;
    arrayOfBoolean171[17] = T;
    arrayOfBoolean171[18] = T;
    arrayOfBoolean171[19] = T;
    arrayOfBoolean171[20] = T;
    arrayOfBoolean171[21] = T;
    arrayOfBoolean171[22] = T;
    arrayOfBoolean171[23] = T;
    arrayOfBoolean171[24] = T;
    arrayOfBoolean171[25] = T;
    arrayOfBoolean171[26] = T;
    arrayOfBoolean167[3] = arrayOfBoolean171;
    boolean[] arrayOfBoolean172 = new boolean[27];
    arrayOfBoolean172[0] = T;
    arrayOfBoolean172[1] = T;
    arrayOfBoolean172[2] = T;
    arrayOfBoolean172[3] = T;
    arrayOfBoolean172[4] = T;
    arrayOfBoolean172[5] = T;
    arrayOfBoolean172[6] = T;
    arrayOfBoolean172[7] = T;
    arrayOfBoolean172[8] = T;
    arrayOfBoolean172[9] = T;
    arrayOfBoolean172[10] = T;
    arrayOfBoolean172[11] = T;
    arrayOfBoolean172[12] = T;
    arrayOfBoolean172[13] = T;
    arrayOfBoolean172[14] = T;
    arrayOfBoolean172[15] = T;
    arrayOfBoolean172[16] = T;
    arrayOfBoolean172[17] = T;
    arrayOfBoolean172[18] = T;
    arrayOfBoolean172[19] = T;
    arrayOfBoolean172[20] = T;
    arrayOfBoolean172[21] = T;
    arrayOfBoolean172[22] = T;
    arrayOfBoolean172[23] = T;
    arrayOfBoolean172[24] = T;
    arrayOfBoolean172[25] = T;
    arrayOfBoolean172[26] = T;
    arrayOfBoolean167[4] = arrayOfBoolean172;
    boolean[] arrayOfBoolean173 = new boolean[27];
    arrayOfBoolean173[0] = T;
    arrayOfBoolean173[1] = T;
    arrayOfBoolean173[2] = T;
    arrayOfBoolean173[3] = T;
    arrayOfBoolean173[4] = T;
    arrayOfBoolean173[5] = T;
    arrayOfBoolean173[6] = T;
    arrayOfBoolean173[7] = T;
    arrayOfBoolean173[8] = T;
    arrayOfBoolean173[9] = T;
    arrayOfBoolean173[10] = T;
    arrayOfBoolean173[11] = T;
    arrayOfBoolean173[12] = T;
    arrayOfBoolean173[13] = T;
    arrayOfBoolean173[14] = T;
    arrayOfBoolean173[15] = T;
    arrayOfBoolean173[16] = T;
    arrayOfBoolean173[17] = T;
    arrayOfBoolean173[18] = T;
    arrayOfBoolean173[19] = T;
    arrayOfBoolean173[20] = T;
    arrayOfBoolean173[21] = T;
    arrayOfBoolean173[22] = T;
    arrayOfBoolean173[23] = T;
    arrayOfBoolean173[24] = T;
    arrayOfBoolean173[25] = T;
    arrayOfBoolean173[26] = T;
    arrayOfBoolean167[5] = arrayOfBoolean173;
    boolean[] arrayOfBoolean174 = new boolean[27];
    arrayOfBoolean174[0] = T;
    arrayOfBoolean174[1] = T;
    arrayOfBoolean174[2] = T;
    arrayOfBoolean174[3] = T;
    arrayOfBoolean174[4] = T;
    arrayOfBoolean174[5] = T;
    arrayOfBoolean174[6] = T;
    arrayOfBoolean174[7] = T;
    arrayOfBoolean174[8] = T;
    arrayOfBoolean174[9] = T;
    arrayOfBoolean174[10] = T;
    arrayOfBoolean174[11] = T;
    arrayOfBoolean174[12] = T;
    arrayOfBoolean174[13] = T;
    arrayOfBoolean174[14] = T;
    arrayOfBoolean174[15] = T;
    arrayOfBoolean174[16] = T;
    arrayOfBoolean174[17] = T;
    arrayOfBoolean174[18] = T;
    arrayOfBoolean174[19] = T;
    arrayOfBoolean174[20] = T;
    arrayOfBoolean174[21] = T;
    arrayOfBoolean174[22] = T;
    arrayOfBoolean174[23] = T;
    arrayOfBoolean174[24] = T;
    arrayOfBoolean174[25] = T;
    arrayOfBoolean174[26] = T;
    arrayOfBoolean167[6] = arrayOfBoolean174;
    boolean[] arrayOfBoolean175 = new boolean[27];
    arrayOfBoolean175[0] = T;
    arrayOfBoolean175[1] = T;
    arrayOfBoolean175[2] = T;
    arrayOfBoolean175[3] = T;
    arrayOfBoolean175[4] = T;
    arrayOfBoolean175[5] = T;
    arrayOfBoolean175[6] = T;
    arrayOfBoolean175[7] = T;
    arrayOfBoolean175[8] = T;
    arrayOfBoolean175[9] = T;
    arrayOfBoolean175[10] = T;
    arrayOfBoolean175[11] = T;
    arrayOfBoolean175[12] = T;
    arrayOfBoolean175[13] = T;
    arrayOfBoolean175[14] = T;
    arrayOfBoolean175[15] = T;
    arrayOfBoolean175[16] = T;
    arrayOfBoolean175[17] = T;
    arrayOfBoolean175[18] = T;
    arrayOfBoolean175[19] = T;
    arrayOfBoolean175[20] = T;
    arrayOfBoolean175[21] = T;
    arrayOfBoolean175[22] = T;
    arrayOfBoolean175[23] = T;
    arrayOfBoolean175[24] = T;
    arrayOfBoolean175[25] = T;
    arrayOfBoolean175[26] = T;
    arrayOfBoolean167[7] = arrayOfBoolean175;
    boolean[] arrayOfBoolean176 = new boolean[27];
    arrayOfBoolean176[0] = T;
    arrayOfBoolean176[1] = T;
    arrayOfBoolean176[2] = T;
    arrayOfBoolean176[3] = T;
    arrayOfBoolean176[4] = T;
    arrayOfBoolean176[5] = T;
    arrayOfBoolean176[6] = T;
    arrayOfBoolean176[7] = T;
    arrayOfBoolean176[8] = T;
    arrayOfBoolean176[9] = T;
    arrayOfBoolean176[10] = T;
    arrayOfBoolean176[11] = T;
    arrayOfBoolean176[12] = T;
    arrayOfBoolean176[13] = T;
    arrayOfBoolean176[14] = T;
    arrayOfBoolean176[15] = T;
    arrayOfBoolean176[16] = T;
    arrayOfBoolean176[17] = T;
    arrayOfBoolean176[18] = T;
    arrayOfBoolean176[19] = T;
    arrayOfBoolean176[20] = T;
    arrayOfBoolean176[21] = T;
    arrayOfBoolean176[22] = T;
    arrayOfBoolean176[23] = T;
    arrayOfBoolean176[24] = T;
    arrayOfBoolean176[25] = T;
    arrayOfBoolean176[26] = T;
    arrayOfBoolean167[8] = arrayOfBoolean176;
    boolean[] arrayOfBoolean177 = new boolean[27];
    arrayOfBoolean177[0] = T;
    arrayOfBoolean177[1] = T;
    arrayOfBoolean177[2] = T;
    arrayOfBoolean177[3] = T;
    arrayOfBoolean177[4] = T;
    arrayOfBoolean177[5] = T;
    arrayOfBoolean177[6] = T;
    arrayOfBoolean177[7] = T;
    arrayOfBoolean177[8] = T;
    arrayOfBoolean177[9] = T;
    arrayOfBoolean177[10] = T;
    arrayOfBoolean177[11] = T;
    arrayOfBoolean177[12] = T;
    arrayOfBoolean177[13] = T;
    arrayOfBoolean177[14] = T;
    arrayOfBoolean177[15] = T;
    arrayOfBoolean177[16] = T;
    arrayOfBoolean177[17] = T;
    arrayOfBoolean177[18] = T;
    arrayOfBoolean177[19] = T;
    arrayOfBoolean177[20] = T;
    arrayOfBoolean177[21] = T;
    arrayOfBoolean177[22] = T;
    arrayOfBoolean177[23] = T;
    arrayOfBoolean177[24] = T;
    arrayOfBoolean177[25] = T;
    arrayOfBoolean177[26] = T;
    arrayOfBoolean167[9] = arrayOfBoolean177;
    boolean[] arrayOfBoolean178 = new boolean[27];
    arrayOfBoolean178[0] = T;
    arrayOfBoolean178[1] = T;
    arrayOfBoolean178[2] = T;
    arrayOfBoolean178[3] = T;
    arrayOfBoolean178[4] = T;
    arrayOfBoolean178[5] = T;
    arrayOfBoolean178[6] = T;
    arrayOfBoolean178[7] = T;
    arrayOfBoolean178[8] = T;
    arrayOfBoolean178[9] = T;
    arrayOfBoolean178[10] = T;
    arrayOfBoolean178[11] = T;
    arrayOfBoolean178[12] = T;
    arrayOfBoolean178[13] = T;
    arrayOfBoolean178[14] = T;
    arrayOfBoolean178[15] = T;
    arrayOfBoolean178[16] = T;
    arrayOfBoolean178[17] = T;
    arrayOfBoolean178[18] = T;
    arrayOfBoolean178[19] = T;
    arrayOfBoolean178[20] = T;
    arrayOfBoolean178[21] = T;
    arrayOfBoolean178[22] = T;
    arrayOfBoolean178[23] = T;
    arrayOfBoolean178[24] = T;
    arrayOfBoolean178[25] = T;
    arrayOfBoolean178[26] = T;
    arrayOfBoolean167[10] = arrayOfBoolean178;
    boolean[] arrayOfBoolean179 = new boolean[27];
    arrayOfBoolean179[0] = T;
    arrayOfBoolean179[1] = T;
    arrayOfBoolean179[2] = T;
    arrayOfBoolean179[3] = T;
    arrayOfBoolean179[4] = T;
    arrayOfBoolean179[5] = T;
    arrayOfBoolean179[6] = T;
    arrayOfBoolean179[7] = T;
    arrayOfBoolean179[8] = T;
    arrayOfBoolean179[9] = T;
    arrayOfBoolean179[10] = T;
    arrayOfBoolean179[11] = T;
    arrayOfBoolean179[12] = T;
    arrayOfBoolean179[13] = T;
    arrayOfBoolean179[14] = T;
    arrayOfBoolean179[15] = T;
    arrayOfBoolean179[16] = T;
    arrayOfBoolean179[17] = T;
    arrayOfBoolean179[18] = T;
    arrayOfBoolean179[19] = T;
    arrayOfBoolean179[20] = T;
    arrayOfBoolean179[21] = T;
    arrayOfBoolean179[22] = T;
    arrayOfBoolean179[23] = T;
    arrayOfBoolean179[24] = T;
    arrayOfBoolean179[25] = T;
    arrayOfBoolean179[26] = T;
    arrayOfBoolean167[11] = arrayOfBoolean179;
    boolean[] arrayOfBoolean180 = new boolean[27];
    arrayOfBoolean180[0] = T;
    arrayOfBoolean180[1] = T;
    arrayOfBoolean180[2] = T;
    arrayOfBoolean180[3] = T;
    arrayOfBoolean180[4] = T;
    arrayOfBoolean180[5] = T;
    arrayOfBoolean180[6] = T;
    arrayOfBoolean180[7] = T;
    arrayOfBoolean180[8] = T;
    arrayOfBoolean180[9] = T;
    arrayOfBoolean180[10] = T;
    arrayOfBoolean180[11] = T;
    arrayOfBoolean180[12] = T;
    arrayOfBoolean180[13] = T;
    arrayOfBoolean180[14] = T;
    arrayOfBoolean180[15] = T;
    arrayOfBoolean180[16] = T;
    arrayOfBoolean180[17] = T;
    arrayOfBoolean180[18] = T;
    arrayOfBoolean180[19] = T;
    arrayOfBoolean180[20] = T;
    arrayOfBoolean180[21] = T;
    arrayOfBoolean180[22] = T;
    arrayOfBoolean180[23] = T;
    arrayOfBoolean180[24] = T;
    arrayOfBoolean180[25] = T;
    arrayOfBoolean180[26] = T;
    arrayOfBoolean167[12] = arrayOfBoolean180;
    boolean[] arrayOfBoolean181 = new boolean[27];
    arrayOfBoolean181[0] = T;
    arrayOfBoolean181[1] = T;
    arrayOfBoolean181[2] = T;
    arrayOfBoolean181[3] = T;
    arrayOfBoolean181[4] = T;
    arrayOfBoolean181[5] = T;
    arrayOfBoolean181[6] = T;
    arrayOfBoolean181[7] = T;
    arrayOfBoolean181[8] = T;
    arrayOfBoolean181[9] = T;
    arrayOfBoolean181[10] = T;
    arrayOfBoolean181[11] = T;
    arrayOfBoolean181[12] = T;
    arrayOfBoolean181[13] = T;
    arrayOfBoolean181[14] = T;
    arrayOfBoolean181[15] = T;
    arrayOfBoolean181[16] = T;
    arrayOfBoolean181[17] = T;
    arrayOfBoolean181[18] = T;
    arrayOfBoolean181[19] = T;
    arrayOfBoolean181[20] = T;
    arrayOfBoolean181[21] = T;
    arrayOfBoolean181[22] = T;
    arrayOfBoolean181[23] = T;
    arrayOfBoolean181[24] = T;
    arrayOfBoolean181[25] = T;
    arrayOfBoolean181[26] = T;
    arrayOfBoolean167[13] = arrayOfBoolean181;
    boolean[] arrayOfBoolean182 = new boolean[27];
    arrayOfBoolean182[0] = T;
    arrayOfBoolean182[1] = T;
    arrayOfBoolean182[2] = T;
    arrayOfBoolean182[3] = T;
    arrayOfBoolean182[4] = T;
    arrayOfBoolean182[5] = T;
    arrayOfBoolean182[6] = T;
    arrayOfBoolean182[7] = T;
    arrayOfBoolean182[8] = T;
    arrayOfBoolean182[9] = T;
    arrayOfBoolean182[10] = T;
    arrayOfBoolean182[11] = T;
    arrayOfBoolean182[12] = T;
    arrayOfBoolean182[13] = T;
    arrayOfBoolean182[14] = T;
    arrayOfBoolean182[15] = T;
    arrayOfBoolean182[16] = T;
    arrayOfBoolean182[17] = T;
    arrayOfBoolean182[18] = T;
    arrayOfBoolean182[19] = T;
    arrayOfBoolean182[20] = T;
    arrayOfBoolean182[21] = T;
    arrayOfBoolean182[22] = T;
    arrayOfBoolean182[23] = T;
    arrayOfBoolean182[24] = T;
    arrayOfBoolean182[25] = T;
    arrayOfBoolean182[26] = T;
    arrayOfBoolean167[14] = arrayOfBoolean182;
    boolean[] arrayOfBoolean183 = new boolean[27];
    arrayOfBoolean183[0] = T;
    arrayOfBoolean183[1] = T;
    arrayOfBoolean183[2] = T;
    arrayOfBoolean183[3] = T;
    arrayOfBoolean183[4] = T;
    arrayOfBoolean183[5] = T;
    arrayOfBoolean183[6] = T;
    arrayOfBoolean183[7] = T;
    arrayOfBoolean183[8] = T;
    arrayOfBoolean183[9] = T;
    arrayOfBoolean183[10] = T;
    arrayOfBoolean183[11] = T;
    arrayOfBoolean183[12] = T;
    arrayOfBoolean183[13] = T;
    arrayOfBoolean183[14] = T;
    arrayOfBoolean183[15] = T;
    arrayOfBoolean183[16] = T;
    arrayOfBoolean183[17] = T;
    arrayOfBoolean183[18] = T;
    arrayOfBoolean183[19] = T;
    arrayOfBoolean183[20] = T;
    arrayOfBoolean183[21] = T;
    arrayOfBoolean183[22] = T;
    arrayOfBoolean183[23] = T;
    arrayOfBoolean183[24] = T;
    arrayOfBoolean183[25] = T;
    arrayOfBoolean183[26] = T;
    arrayOfBoolean167[15] = arrayOfBoolean183;
    boolean[] arrayOfBoolean184 = new boolean[27];
    arrayOfBoolean184[0] = T;
    arrayOfBoolean184[1] = T;
    arrayOfBoolean184[2] = T;
    arrayOfBoolean184[3] = T;
    arrayOfBoolean184[4] = T;
    arrayOfBoolean184[5] = T;
    arrayOfBoolean184[6] = T;
    arrayOfBoolean184[7] = T;
    arrayOfBoolean184[8] = T;
    arrayOfBoolean184[9] = T;
    arrayOfBoolean184[10] = T;
    arrayOfBoolean184[11] = T;
    arrayOfBoolean184[12] = T;
    arrayOfBoolean184[13] = T;
    arrayOfBoolean184[14] = T;
    arrayOfBoolean184[15] = T;
    arrayOfBoolean184[16] = T;
    arrayOfBoolean184[17] = T;
    arrayOfBoolean184[18] = T;
    arrayOfBoolean184[19] = T;
    arrayOfBoolean184[20] = T;
    arrayOfBoolean184[21] = T;
    arrayOfBoolean184[22] = T;
    arrayOfBoolean184[23] = T;
    arrayOfBoolean184[24] = T;
    arrayOfBoolean184[25] = T;
    arrayOfBoolean184[26] = T;
    arrayOfBoolean167[16] = arrayOfBoolean184;
    boolean[] arrayOfBoolean185 = new boolean[27];
    arrayOfBoolean185[0] = T;
    arrayOfBoolean185[1] = T;
    arrayOfBoolean185[2] = T;
    arrayOfBoolean185[3] = T;
    arrayOfBoolean185[4] = T;
    arrayOfBoolean185[5] = T;
    arrayOfBoolean185[6] = T;
    arrayOfBoolean185[7] = T;
    arrayOfBoolean185[8] = T;
    arrayOfBoolean185[9] = T;
    arrayOfBoolean185[10] = T;
    arrayOfBoolean185[11] = T;
    arrayOfBoolean185[12] = T;
    arrayOfBoolean185[13] = T;
    arrayOfBoolean185[14] = T;
    arrayOfBoolean185[15] = T;
    arrayOfBoolean185[16] = T;
    arrayOfBoolean185[17] = T;
    arrayOfBoolean185[18] = T;
    arrayOfBoolean185[19] = T;
    arrayOfBoolean185[20] = T;
    arrayOfBoolean185[21] = T;
    arrayOfBoolean185[22] = T;
    arrayOfBoolean185[23] = T;
    arrayOfBoolean185[24] = T;
    arrayOfBoolean185[25] = T;
    arrayOfBoolean185[26] = T;
    arrayOfBoolean167[17] = arrayOfBoolean185;
    boolean[] arrayOfBoolean186 = new boolean[27];
    arrayOfBoolean186[0] = T;
    arrayOfBoolean186[1] = T;
    arrayOfBoolean186[2] = T;
    arrayOfBoolean186[3] = T;
    arrayOfBoolean186[4] = T;
    arrayOfBoolean186[5] = T;
    arrayOfBoolean186[6] = T;
    arrayOfBoolean186[7] = T;
    arrayOfBoolean186[8] = T;
    arrayOfBoolean186[9] = T;
    arrayOfBoolean186[10] = T;
    arrayOfBoolean186[11] = T;
    arrayOfBoolean186[12] = T;
    arrayOfBoolean186[13] = T;
    arrayOfBoolean186[14] = T;
    arrayOfBoolean186[15] = T;
    arrayOfBoolean186[16] = T;
    arrayOfBoolean186[17] = T;
    arrayOfBoolean186[18] = T;
    arrayOfBoolean186[19] = T;
    arrayOfBoolean186[20] = T;
    arrayOfBoolean186[21] = T;
    arrayOfBoolean186[22] = T;
    arrayOfBoolean186[23] = T;
    arrayOfBoolean186[24] = T;
    arrayOfBoolean186[25] = T;
    arrayOfBoolean186[26] = T;
    arrayOfBoolean167[18] = arrayOfBoolean186;
    boolean[] arrayOfBoolean187 = new boolean[27];
    arrayOfBoolean187[0] = T;
    arrayOfBoolean187[1] = T;
    arrayOfBoolean187[2] = T;
    arrayOfBoolean187[3] = T;
    arrayOfBoolean187[4] = T;
    arrayOfBoolean187[5] = T;
    arrayOfBoolean187[6] = T;
    arrayOfBoolean187[7] = T;
    arrayOfBoolean187[8] = T;
    arrayOfBoolean187[9] = T;
    arrayOfBoolean187[10] = T;
    arrayOfBoolean187[11] = T;
    arrayOfBoolean187[12] = T;
    arrayOfBoolean187[13] = T;
    arrayOfBoolean187[14] = T;
    arrayOfBoolean187[15] = T;
    arrayOfBoolean187[16] = T;
    arrayOfBoolean187[17] = T;
    arrayOfBoolean187[18] = T;
    arrayOfBoolean187[19] = T;
    arrayOfBoolean187[20] = T;
    arrayOfBoolean187[21] = T;
    arrayOfBoolean187[22] = T;
    arrayOfBoolean187[23] = T;
    arrayOfBoolean187[24] = T;
    arrayOfBoolean187[25] = T;
    arrayOfBoolean187[26] = T;
    arrayOfBoolean167[19] = arrayOfBoolean187;
    boolean[] arrayOfBoolean188 = new boolean[27];
    arrayOfBoolean188[0] = T;
    arrayOfBoolean188[1] = T;
    arrayOfBoolean188[2] = T;
    arrayOfBoolean188[3] = T;
    arrayOfBoolean188[4] = T;
    arrayOfBoolean188[5] = T;
    arrayOfBoolean188[6] = T;
    arrayOfBoolean188[7] = T;
    arrayOfBoolean188[8] = T;
    arrayOfBoolean188[9] = T;
    arrayOfBoolean188[10] = T;
    arrayOfBoolean188[11] = T;
    arrayOfBoolean188[12] = T;
    arrayOfBoolean188[13] = T;
    arrayOfBoolean188[14] = T;
    arrayOfBoolean188[15] = T;
    arrayOfBoolean188[16] = T;
    arrayOfBoolean188[17] = T;
    arrayOfBoolean188[18] = T;
    arrayOfBoolean188[19] = T;
    arrayOfBoolean188[20] = T;
    arrayOfBoolean188[21] = T;
    arrayOfBoolean188[22] = T;
    arrayOfBoolean188[23] = T;
    arrayOfBoolean188[24] = T;
    arrayOfBoolean188[25] = T;
    arrayOfBoolean188[26] = T;
    arrayOfBoolean167[20] = arrayOfBoolean188;
    boolean[] arrayOfBoolean189 = new boolean[27];
    arrayOfBoolean189[0] = T;
    arrayOfBoolean189[1] = T;
    arrayOfBoolean189[2] = T;
    arrayOfBoolean189[3] = T;
    arrayOfBoolean189[4] = T;
    arrayOfBoolean189[5] = T;
    arrayOfBoolean189[6] = T;
    arrayOfBoolean189[7] = T;
    arrayOfBoolean189[8] = T;
    arrayOfBoolean189[9] = T;
    arrayOfBoolean189[10] = T;
    arrayOfBoolean189[11] = T;
    arrayOfBoolean189[12] = T;
    arrayOfBoolean189[13] = T;
    arrayOfBoolean189[14] = T;
    arrayOfBoolean189[15] = T;
    arrayOfBoolean189[16] = T;
    arrayOfBoolean189[17] = T;
    arrayOfBoolean189[18] = T;
    arrayOfBoolean189[19] = T;
    arrayOfBoolean189[20] = T;
    arrayOfBoolean189[21] = T;
    arrayOfBoolean189[22] = T;
    arrayOfBoolean189[23] = T;
    arrayOfBoolean189[24] = T;
    arrayOfBoolean189[25] = T;
    arrayOfBoolean189[26] = T;
    arrayOfBoolean167[21] = arrayOfBoolean189;
    boolean[] arrayOfBoolean190 = new boolean[27];
    arrayOfBoolean190[0] = T;
    arrayOfBoolean190[1] = T;
    arrayOfBoolean190[2] = T;
    arrayOfBoolean190[3] = T;
    arrayOfBoolean190[4] = T;
    arrayOfBoolean190[5] = T;
    arrayOfBoolean190[6] = T;
    arrayOfBoolean190[7] = T;
    arrayOfBoolean190[8] = T;
    arrayOfBoolean190[9] = T;
    arrayOfBoolean190[10] = T;
    arrayOfBoolean190[11] = T;
    arrayOfBoolean190[12] = T;
    arrayOfBoolean190[13] = T;
    arrayOfBoolean190[14] = T;
    arrayOfBoolean190[15] = T;
    arrayOfBoolean190[16] = T;
    arrayOfBoolean190[17] = T;
    arrayOfBoolean190[18] = T;
    arrayOfBoolean190[19] = T;
    arrayOfBoolean190[20] = T;
    arrayOfBoolean190[21] = T;
    arrayOfBoolean190[22] = T;
    arrayOfBoolean190[23] = T;
    arrayOfBoolean190[24] = T;
    arrayOfBoolean190[25] = T;
    arrayOfBoolean190[26] = T;
    arrayOfBoolean167[22] = arrayOfBoolean190;
    boolean[] arrayOfBoolean191 = new boolean[27];
    arrayOfBoolean191[0] = T;
    arrayOfBoolean191[1] = T;
    arrayOfBoolean191[2] = T;
    arrayOfBoolean191[3] = T;
    arrayOfBoolean191[4] = T;
    arrayOfBoolean191[5] = T;
    arrayOfBoolean191[6] = T;
    arrayOfBoolean191[7] = T;
    arrayOfBoolean191[8] = T;
    arrayOfBoolean191[9] = T;
    arrayOfBoolean191[10] = T;
    arrayOfBoolean191[11] = T;
    arrayOfBoolean191[12] = T;
    arrayOfBoolean191[13] = T;
    arrayOfBoolean191[14] = T;
    arrayOfBoolean191[15] = T;
    arrayOfBoolean191[16] = T;
    arrayOfBoolean191[17] = T;
    arrayOfBoolean191[18] = T;
    arrayOfBoolean191[19] = T;
    arrayOfBoolean191[20] = T;
    arrayOfBoolean191[21] = T;
    arrayOfBoolean191[22] = T;
    arrayOfBoolean191[23] = T;
    arrayOfBoolean191[24] = T;
    arrayOfBoolean191[25] = T;
    arrayOfBoolean191[26] = T;
    arrayOfBoolean167[23] = arrayOfBoolean191;
    boolean[] arrayOfBoolean192 = new boolean[27];
    arrayOfBoolean192[0] = T;
    arrayOfBoolean192[1] = T;
    arrayOfBoolean192[2] = T;
    arrayOfBoolean192[3] = T;
    arrayOfBoolean192[4] = T;
    arrayOfBoolean192[5] = T;
    arrayOfBoolean192[6] = T;
    arrayOfBoolean192[7] = T;
    arrayOfBoolean192[8] = T;
    arrayOfBoolean192[9] = T;
    arrayOfBoolean192[10] = T;
    arrayOfBoolean192[11] = T;
    arrayOfBoolean192[12] = T;
    arrayOfBoolean192[13] = T;
    arrayOfBoolean192[14] = T;
    arrayOfBoolean192[15] = T;
    arrayOfBoolean192[16] = T;
    arrayOfBoolean192[17] = T;
    arrayOfBoolean192[18] = T;
    arrayOfBoolean192[19] = T;
    arrayOfBoolean192[20] = T;
    arrayOfBoolean192[21] = T;
    arrayOfBoolean192[22] = T;
    arrayOfBoolean192[23] = T;
    arrayOfBoolean192[24] = T;
    arrayOfBoolean192[25] = T;
    arrayOfBoolean192[26] = T;
    arrayOfBoolean167[24] = arrayOfBoolean192;
    boolean[] arrayOfBoolean193 = new boolean[27];
    arrayOfBoolean193[0] = T;
    arrayOfBoolean193[1] = T;
    arrayOfBoolean193[2] = T;
    arrayOfBoolean193[3] = T;
    arrayOfBoolean193[4] = T;
    arrayOfBoolean193[5] = T;
    arrayOfBoolean193[6] = T;
    arrayOfBoolean193[7] = T;
    arrayOfBoolean193[8] = T;
    arrayOfBoolean193[9] = T;
    arrayOfBoolean193[10] = T;
    arrayOfBoolean193[11] = T;
    arrayOfBoolean193[12] = T;
    arrayOfBoolean193[13] = T;
    arrayOfBoolean193[14] = T;
    arrayOfBoolean193[15] = T;
    arrayOfBoolean193[16] = T;
    arrayOfBoolean193[17] = T;
    arrayOfBoolean193[18] = T;
    arrayOfBoolean193[19] = T;
    arrayOfBoolean193[20] = T;
    arrayOfBoolean193[21] = T;
    arrayOfBoolean193[22] = T;
    arrayOfBoolean193[23] = T;
    arrayOfBoolean193[24] = T;
    arrayOfBoolean193[25] = T;
    arrayOfBoolean193[26] = T;
    arrayOfBoolean167[25] = arrayOfBoolean193;
    boolean[] arrayOfBoolean194 = new boolean[27];
    arrayOfBoolean194[0] = T;
    arrayOfBoolean194[1] = T;
    arrayOfBoolean194[2] = T;
    arrayOfBoolean194[3] = T;
    arrayOfBoolean194[4] = T;
    arrayOfBoolean194[5] = T;
    arrayOfBoolean194[6] = T;
    arrayOfBoolean194[7] = T;
    arrayOfBoolean194[8] = T;
    arrayOfBoolean194[9] = T;
    arrayOfBoolean194[10] = T;
    arrayOfBoolean194[11] = T;
    arrayOfBoolean194[12] = T;
    arrayOfBoolean194[13] = T;
    arrayOfBoolean194[14] = T;
    arrayOfBoolean194[15] = T;
    arrayOfBoolean194[16] = T;
    arrayOfBoolean194[17] = T;
    arrayOfBoolean194[18] = T;
    arrayOfBoolean194[19] = T;
    arrayOfBoolean194[20] = T;
    arrayOfBoolean194[21] = T;
    arrayOfBoolean194[22] = T;
    arrayOfBoolean194[23] = T;
    arrayOfBoolean194[24] = T;
    arrayOfBoolean194[25] = T;
    arrayOfBoolean194[26] = T;
    arrayOfBoolean167[26] = arrayOfBoolean194;
    boolean[] arrayOfBoolean195 = new boolean[27];
    arrayOfBoolean195[0] = T;
    arrayOfBoolean195[1] = T;
    arrayOfBoolean195[2] = T;
    arrayOfBoolean195[3] = T;
    arrayOfBoolean195[4] = T;
    arrayOfBoolean195[5] = T;
    arrayOfBoolean195[6] = T;
    arrayOfBoolean195[7] = T;
    arrayOfBoolean195[8] = T;
    arrayOfBoolean195[9] = T;
    arrayOfBoolean195[10] = T;
    arrayOfBoolean195[11] = T;
    arrayOfBoolean195[12] = T;
    arrayOfBoolean195[13] = T;
    arrayOfBoolean195[14] = T;
    arrayOfBoolean195[15] = T;
    arrayOfBoolean195[16] = T;
    arrayOfBoolean195[17] = T;
    arrayOfBoolean195[18] = T;
    arrayOfBoolean195[19] = T;
    arrayOfBoolean195[20] = T;
    arrayOfBoolean195[21] = T;
    arrayOfBoolean195[22] = T;
    arrayOfBoolean195[23] = T;
    arrayOfBoolean195[24] = T;
    arrayOfBoolean195[25] = T;
    arrayOfBoolean195[26] = T;
    arrayOfBoolean167[27] = arrayOfBoolean195;
    boolean[] arrayOfBoolean196 = new boolean[27];
    arrayOfBoolean196[0] = T;
    arrayOfBoolean196[1] = T;
    arrayOfBoolean196[2] = T;
    arrayOfBoolean196[3] = T;
    arrayOfBoolean196[4] = T;
    arrayOfBoolean196[5] = T;
    arrayOfBoolean196[6] = T;
    arrayOfBoolean196[7] = T;
    arrayOfBoolean196[8] = T;
    arrayOfBoolean196[9] = T;
    arrayOfBoolean196[10] = T;
    arrayOfBoolean196[11] = T;
    arrayOfBoolean196[12] = T;
    arrayOfBoolean196[13] = T;
    arrayOfBoolean196[14] = T;
    arrayOfBoolean196[15] = T;
    arrayOfBoolean196[16] = T;
    arrayOfBoolean196[17] = T;
    arrayOfBoolean196[18] = T;
    arrayOfBoolean196[19] = T;
    arrayOfBoolean196[20] = T;
    arrayOfBoolean196[21] = T;
    arrayOfBoolean196[22] = T;
    arrayOfBoolean196[23] = T;
    arrayOfBoolean196[24] = T;
    arrayOfBoolean196[25] = T;
    arrayOfBoolean196[26] = T;
    arrayOfBoolean167[28] = arrayOfBoolean196;
    boolean[] arrayOfBoolean197 = new boolean[27];
    arrayOfBoolean197[0] = T;
    arrayOfBoolean197[1] = T;
    arrayOfBoolean197[2] = T;
    arrayOfBoolean197[3] = T;
    arrayOfBoolean197[4] = T;
    arrayOfBoolean197[5] = T;
    arrayOfBoolean197[6] = T;
    arrayOfBoolean197[7] = T;
    arrayOfBoolean197[8] = T;
    arrayOfBoolean197[9] = T;
    arrayOfBoolean197[10] = T;
    arrayOfBoolean197[11] = T;
    arrayOfBoolean197[12] = T;
    arrayOfBoolean197[13] = T;
    arrayOfBoolean197[14] = T;
    arrayOfBoolean197[15] = T;
    arrayOfBoolean197[16] = T;
    arrayOfBoolean197[17] = T;
    arrayOfBoolean197[18] = T;
    arrayOfBoolean197[19] = T;
    arrayOfBoolean197[20] = T;
    arrayOfBoolean197[21] = T;
    arrayOfBoolean197[22] = T;
    arrayOfBoolean197[23] = T;
    arrayOfBoolean197[24] = T;
    arrayOfBoolean197[25] = T;
    arrayOfBoolean197[26] = T;
    arrayOfBoolean167[29] = arrayOfBoolean197;
    arrayOfBoolean114[2] = arrayOfBoolean167;
    boolean[][] arrayOfBoolean198 = new boolean[30][];
    boolean[] arrayOfBoolean199 = new boolean[27];
    arrayOfBoolean199[0] = F;
    arrayOfBoolean199[1] = F;
    arrayOfBoolean199[2] = F;
    arrayOfBoolean199[3] = F;
    arrayOfBoolean199[4] = F;
    arrayOfBoolean199[5] = F;
    arrayOfBoolean199[6] = F;
    arrayOfBoolean199[7] = F;
    arrayOfBoolean199[8] = F;
    arrayOfBoolean199[9] = F;
    arrayOfBoolean199[10] = F;
    arrayOfBoolean199[11] = F;
    arrayOfBoolean199[12] = F;
    arrayOfBoolean199[13] = F;
    arrayOfBoolean199[14] = F;
    arrayOfBoolean199[15] = F;
    arrayOfBoolean199[16] = F;
    arrayOfBoolean199[17] = F;
    arrayOfBoolean199[18] = F;
    arrayOfBoolean199[19] = F;
    arrayOfBoolean199[20] = F;
    arrayOfBoolean199[21] = F;
    arrayOfBoolean199[22] = F;
    arrayOfBoolean199[23] = F;
    arrayOfBoolean199[24] = F;
    arrayOfBoolean199[25] = F;
    arrayOfBoolean199[26] = F;
    arrayOfBoolean198[0] = arrayOfBoolean199;
    boolean[] arrayOfBoolean200 = new boolean[27];
    arrayOfBoolean200[0] = F;
    arrayOfBoolean200[1] = F;
    arrayOfBoolean200[2] = F;
    arrayOfBoolean200[3] = F;
    arrayOfBoolean200[4] = F;
    arrayOfBoolean200[5] = F;
    arrayOfBoolean200[6] = F;
    arrayOfBoolean200[7] = F;
    arrayOfBoolean200[8] = F;
    arrayOfBoolean200[9] = F;
    arrayOfBoolean200[10] = F;
    arrayOfBoolean200[11] = F;
    arrayOfBoolean200[12] = F;
    arrayOfBoolean200[13] = F;
    arrayOfBoolean200[14] = F;
    arrayOfBoolean200[15] = F;
    arrayOfBoolean200[16] = F;
    arrayOfBoolean200[17] = F;
    arrayOfBoolean200[18] = F;
    arrayOfBoolean200[19] = F;
    arrayOfBoolean200[20] = F;
    arrayOfBoolean200[21] = F;
    arrayOfBoolean200[22] = F;
    arrayOfBoolean200[23] = F;
    arrayOfBoolean200[24] = F;
    arrayOfBoolean200[25] = F;
    arrayOfBoolean200[26] = F;
    arrayOfBoolean198[1] = arrayOfBoolean200;
    boolean[] arrayOfBoolean201 = new boolean[27];
    arrayOfBoolean201[0] = F;
    arrayOfBoolean201[1] = F;
    arrayOfBoolean201[2] = F;
    arrayOfBoolean201[3] = F;
    arrayOfBoolean201[4] = F;
    arrayOfBoolean201[5] = F;
    arrayOfBoolean201[6] = F;
    arrayOfBoolean201[7] = F;
    arrayOfBoolean201[8] = F;
    arrayOfBoolean201[9] = F;
    arrayOfBoolean201[10] = F;
    arrayOfBoolean201[11] = F;
    arrayOfBoolean201[12] = F;
    arrayOfBoolean201[13] = F;
    arrayOfBoolean201[14] = F;
    arrayOfBoolean201[15] = F;
    arrayOfBoolean201[16] = F;
    arrayOfBoolean201[17] = F;
    arrayOfBoolean201[18] = F;
    arrayOfBoolean201[19] = F;
    arrayOfBoolean201[20] = F;
    arrayOfBoolean201[21] = F;
    arrayOfBoolean201[22] = F;
    arrayOfBoolean201[23] = F;
    arrayOfBoolean201[24] = F;
    arrayOfBoolean201[25] = F;
    arrayOfBoolean201[26] = F;
    arrayOfBoolean198[2] = arrayOfBoolean201;
    boolean[] arrayOfBoolean202 = new boolean[27];
    arrayOfBoolean202[0] = F;
    arrayOfBoolean202[1] = F;
    arrayOfBoolean202[2] = F;
    arrayOfBoolean202[3] = F;
    arrayOfBoolean202[4] = F;
    arrayOfBoolean202[5] = F;
    arrayOfBoolean202[6] = F;
    arrayOfBoolean202[7] = F;
    arrayOfBoolean202[8] = F;
    arrayOfBoolean202[9] = F;
    arrayOfBoolean202[10] = F;
    arrayOfBoolean202[11] = F;
    arrayOfBoolean202[12] = F;
    arrayOfBoolean202[13] = F;
    arrayOfBoolean202[14] = F;
    arrayOfBoolean202[15] = F;
    arrayOfBoolean202[16] = F;
    arrayOfBoolean202[17] = F;
    arrayOfBoolean202[18] = F;
    arrayOfBoolean202[19] = F;
    arrayOfBoolean202[20] = F;
    arrayOfBoolean202[21] = F;
    arrayOfBoolean202[22] = F;
    arrayOfBoolean202[23] = F;
    arrayOfBoolean202[24] = F;
    arrayOfBoolean202[25] = F;
    arrayOfBoolean202[26] = F;
    arrayOfBoolean198[3] = arrayOfBoolean202;
    boolean[] arrayOfBoolean203 = new boolean[27];
    arrayOfBoolean203[0] = F;
    arrayOfBoolean203[1] = F;
    arrayOfBoolean203[2] = F;
    arrayOfBoolean203[3] = F;
    arrayOfBoolean203[4] = F;
    arrayOfBoolean203[5] = F;
    arrayOfBoolean203[6] = F;
    arrayOfBoolean203[7] = F;
    arrayOfBoolean203[8] = F;
    arrayOfBoolean203[9] = F;
    arrayOfBoolean203[10] = F;
    arrayOfBoolean203[11] = F;
    arrayOfBoolean203[12] = F;
    arrayOfBoolean203[13] = F;
    arrayOfBoolean203[14] = F;
    arrayOfBoolean203[15] = F;
    arrayOfBoolean203[16] = F;
    arrayOfBoolean203[17] = F;
    arrayOfBoolean203[18] = F;
    arrayOfBoolean203[19] = F;
    arrayOfBoolean203[20] = F;
    arrayOfBoolean203[21] = F;
    arrayOfBoolean203[22] = F;
    arrayOfBoolean203[23] = F;
    arrayOfBoolean203[24] = F;
    arrayOfBoolean203[25] = F;
    arrayOfBoolean203[26] = F;
    arrayOfBoolean198[4] = arrayOfBoolean203;
    boolean[] arrayOfBoolean204 = new boolean[27];
    arrayOfBoolean204[0] = F;
    arrayOfBoolean204[1] = F;
    arrayOfBoolean204[2] = F;
    arrayOfBoolean204[3] = F;
    arrayOfBoolean204[4] = F;
    arrayOfBoolean204[5] = F;
    arrayOfBoolean204[6] = F;
    arrayOfBoolean204[7] = F;
    arrayOfBoolean204[8] = F;
    arrayOfBoolean204[9] = F;
    arrayOfBoolean204[10] = F;
    arrayOfBoolean204[11] = F;
    arrayOfBoolean204[12] = F;
    arrayOfBoolean204[13] = F;
    arrayOfBoolean204[14] = F;
    arrayOfBoolean204[15] = F;
    arrayOfBoolean204[16] = F;
    arrayOfBoolean204[17] = F;
    arrayOfBoolean204[18] = F;
    arrayOfBoolean204[19] = F;
    arrayOfBoolean204[20] = F;
    arrayOfBoolean204[21] = F;
    arrayOfBoolean204[22] = F;
    arrayOfBoolean204[23] = F;
    arrayOfBoolean204[24] = F;
    arrayOfBoolean204[25] = F;
    arrayOfBoolean204[26] = F;
    arrayOfBoolean198[5] = arrayOfBoolean204;
    boolean[] arrayOfBoolean205 = new boolean[27];
    arrayOfBoolean205[0] = F;
    arrayOfBoolean205[1] = F;
    arrayOfBoolean205[2] = F;
    arrayOfBoolean205[3] = F;
    arrayOfBoolean205[4] = F;
    arrayOfBoolean205[5] = F;
    arrayOfBoolean205[6] = F;
    arrayOfBoolean205[7] = F;
    arrayOfBoolean205[8] = F;
    arrayOfBoolean205[9] = F;
    arrayOfBoolean205[10] = F;
    arrayOfBoolean205[11] = F;
    arrayOfBoolean205[12] = F;
    arrayOfBoolean205[13] = F;
    arrayOfBoolean205[14] = F;
    arrayOfBoolean205[15] = F;
    arrayOfBoolean205[16] = F;
    arrayOfBoolean205[17] = F;
    arrayOfBoolean205[18] = F;
    arrayOfBoolean205[19] = F;
    arrayOfBoolean205[20] = F;
    arrayOfBoolean205[21] = F;
    arrayOfBoolean205[22] = F;
    arrayOfBoolean205[23] = F;
    arrayOfBoolean205[24] = F;
    arrayOfBoolean205[25] = F;
    arrayOfBoolean205[26] = F;
    arrayOfBoolean198[6] = arrayOfBoolean205;
    boolean[] arrayOfBoolean206 = new boolean[27];
    arrayOfBoolean206[0] = F;
    arrayOfBoolean206[1] = F;
    arrayOfBoolean206[2] = F;
    arrayOfBoolean206[3] = F;
    arrayOfBoolean206[4] = F;
    arrayOfBoolean206[5] = F;
    arrayOfBoolean206[6] = F;
    arrayOfBoolean206[7] = F;
    arrayOfBoolean206[8] = F;
    arrayOfBoolean206[9] = F;
    arrayOfBoolean206[10] = F;
    arrayOfBoolean206[11] = F;
    arrayOfBoolean206[12] = F;
    arrayOfBoolean206[13] = F;
    arrayOfBoolean206[14] = F;
    arrayOfBoolean206[15] = F;
    arrayOfBoolean206[16] = F;
    arrayOfBoolean206[17] = F;
    arrayOfBoolean206[18] = F;
    arrayOfBoolean206[19] = F;
    arrayOfBoolean206[20] = F;
    arrayOfBoolean206[21] = F;
    arrayOfBoolean206[22] = F;
    arrayOfBoolean206[23] = F;
    arrayOfBoolean206[24] = F;
    arrayOfBoolean206[25] = F;
    arrayOfBoolean206[26] = F;
    arrayOfBoolean198[7] = arrayOfBoolean206;
    boolean[] arrayOfBoolean207 = new boolean[27];
    arrayOfBoolean207[0] = F;
    arrayOfBoolean207[1] = F;
    arrayOfBoolean207[2] = F;
    arrayOfBoolean207[3] = F;
    arrayOfBoolean207[4] = F;
    arrayOfBoolean207[5] = F;
    arrayOfBoolean207[6] = F;
    arrayOfBoolean207[7] = F;
    arrayOfBoolean207[8] = F;
    arrayOfBoolean207[9] = F;
    arrayOfBoolean207[10] = F;
    arrayOfBoolean207[11] = F;
    arrayOfBoolean207[12] = F;
    arrayOfBoolean207[13] = F;
    arrayOfBoolean207[14] = F;
    arrayOfBoolean207[15] = F;
    arrayOfBoolean207[16] = F;
    arrayOfBoolean207[17] = F;
    arrayOfBoolean207[18] = F;
    arrayOfBoolean207[19] = F;
    arrayOfBoolean207[20] = F;
    arrayOfBoolean207[21] = F;
    arrayOfBoolean207[22] = F;
    arrayOfBoolean207[23] = F;
    arrayOfBoolean207[24] = F;
    arrayOfBoolean207[25] = F;
    arrayOfBoolean207[26] = F;
    arrayOfBoolean198[8] = arrayOfBoolean207;
    boolean[] arrayOfBoolean208 = new boolean[27];
    arrayOfBoolean208[0] = F;
    arrayOfBoolean208[1] = F;
    arrayOfBoolean208[2] = F;
    arrayOfBoolean208[3] = F;
    arrayOfBoolean208[4] = F;
    arrayOfBoolean208[5] = F;
    arrayOfBoolean208[6] = F;
    arrayOfBoolean208[7] = F;
    arrayOfBoolean208[8] = F;
    arrayOfBoolean208[9] = F;
    arrayOfBoolean208[10] = F;
    arrayOfBoolean208[11] = F;
    arrayOfBoolean208[12] = F;
    arrayOfBoolean208[13] = F;
    arrayOfBoolean208[14] = F;
    arrayOfBoolean208[15] = F;
    arrayOfBoolean208[16] = F;
    arrayOfBoolean208[17] = F;
    arrayOfBoolean208[18] = F;
    arrayOfBoolean208[19] = F;
    arrayOfBoolean208[20] = F;
    arrayOfBoolean208[21] = F;
    arrayOfBoolean208[22] = F;
    arrayOfBoolean208[23] = F;
    arrayOfBoolean208[24] = F;
    arrayOfBoolean208[25] = F;
    arrayOfBoolean208[26] = F;
    arrayOfBoolean198[9] = arrayOfBoolean208;
    boolean[] arrayOfBoolean209 = new boolean[27];
    arrayOfBoolean209[0] = F;
    arrayOfBoolean209[1] = F;
    arrayOfBoolean209[2] = F;
    arrayOfBoolean209[3] = F;
    arrayOfBoolean209[4] = F;
    arrayOfBoolean209[5] = F;
    arrayOfBoolean209[6] = F;
    arrayOfBoolean209[7] = F;
    arrayOfBoolean209[8] = F;
    arrayOfBoolean209[9] = F;
    arrayOfBoolean209[10] = F;
    arrayOfBoolean209[11] = F;
    arrayOfBoolean209[12] = F;
    arrayOfBoolean209[13] = F;
    arrayOfBoolean209[14] = F;
    arrayOfBoolean209[15] = F;
    arrayOfBoolean209[16] = F;
    arrayOfBoolean209[17] = F;
    arrayOfBoolean209[18] = F;
    arrayOfBoolean209[19] = F;
    arrayOfBoolean209[20] = F;
    arrayOfBoolean209[21] = F;
    arrayOfBoolean209[22] = F;
    arrayOfBoolean209[23] = F;
    arrayOfBoolean209[24] = F;
    arrayOfBoolean209[25] = F;
    arrayOfBoolean209[26] = F;
    arrayOfBoolean198[10] = arrayOfBoolean209;
    boolean[] arrayOfBoolean210 = new boolean[27];
    arrayOfBoolean210[0] = F;
    arrayOfBoolean210[1] = F;
    arrayOfBoolean210[2] = F;
    arrayOfBoolean210[3] = F;
    arrayOfBoolean210[4] = F;
    arrayOfBoolean210[5] = F;
    arrayOfBoolean210[6] = F;
    arrayOfBoolean210[7] = F;
    arrayOfBoolean210[8] = F;
    arrayOfBoolean210[9] = F;
    arrayOfBoolean210[10] = F;
    arrayOfBoolean210[11] = F;
    arrayOfBoolean210[12] = F;
    arrayOfBoolean210[13] = F;
    arrayOfBoolean210[14] = F;
    arrayOfBoolean210[15] = F;
    arrayOfBoolean210[16] = F;
    arrayOfBoolean210[17] = F;
    arrayOfBoolean210[18] = F;
    arrayOfBoolean210[19] = F;
    arrayOfBoolean210[20] = F;
    arrayOfBoolean210[21] = F;
    arrayOfBoolean210[22] = F;
    arrayOfBoolean210[23] = F;
    arrayOfBoolean210[24] = F;
    arrayOfBoolean210[25] = F;
    arrayOfBoolean210[26] = F;
    arrayOfBoolean198[11] = arrayOfBoolean210;
    boolean[] arrayOfBoolean211 = new boolean[27];
    arrayOfBoolean211[0] = F;
    arrayOfBoolean211[1] = F;
    arrayOfBoolean211[2] = F;
    arrayOfBoolean211[3] = F;
    arrayOfBoolean211[4] = F;
    arrayOfBoolean211[5] = F;
    arrayOfBoolean211[6] = F;
    arrayOfBoolean211[7] = F;
    arrayOfBoolean211[8] = F;
    arrayOfBoolean211[9] = F;
    arrayOfBoolean211[10] = F;
    arrayOfBoolean211[11] = F;
    arrayOfBoolean211[12] = F;
    arrayOfBoolean211[13] = F;
    arrayOfBoolean211[14] = F;
    arrayOfBoolean211[15] = F;
    arrayOfBoolean211[16] = F;
    arrayOfBoolean211[17] = F;
    arrayOfBoolean211[18] = F;
    arrayOfBoolean211[19] = F;
    arrayOfBoolean211[20] = F;
    arrayOfBoolean211[21] = F;
    arrayOfBoolean211[22] = F;
    arrayOfBoolean211[23] = F;
    arrayOfBoolean211[24] = F;
    arrayOfBoolean211[25] = F;
    arrayOfBoolean211[26] = F;
    arrayOfBoolean198[12] = arrayOfBoolean211;
    boolean[] arrayOfBoolean212 = new boolean[27];
    arrayOfBoolean212[0] = F;
    arrayOfBoolean212[1] = F;
    arrayOfBoolean212[2] = F;
    arrayOfBoolean212[3] = F;
    arrayOfBoolean212[4] = F;
    arrayOfBoolean212[5] = F;
    arrayOfBoolean212[6] = F;
    arrayOfBoolean212[7] = F;
    arrayOfBoolean212[8] = F;
    arrayOfBoolean212[9] = F;
    arrayOfBoolean212[10] = F;
    arrayOfBoolean212[11] = F;
    arrayOfBoolean212[12] = F;
    arrayOfBoolean212[13] = F;
    arrayOfBoolean212[14] = F;
    arrayOfBoolean212[15] = F;
    arrayOfBoolean212[16] = F;
    arrayOfBoolean212[17] = F;
    arrayOfBoolean212[18] = F;
    arrayOfBoolean212[19] = F;
    arrayOfBoolean212[20] = F;
    arrayOfBoolean212[21] = F;
    arrayOfBoolean212[22] = F;
    arrayOfBoolean212[23] = F;
    arrayOfBoolean212[24] = F;
    arrayOfBoolean212[25] = F;
    arrayOfBoolean212[26] = F;
    arrayOfBoolean198[13] = arrayOfBoolean212;
    boolean[] arrayOfBoolean213 = new boolean[27];
    arrayOfBoolean213[0] = F;
    arrayOfBoolean213[1] = F;
    arrayOfBoolean213[2] = F;
    arrayOfBoolean213[3] = F;
    arrayOfBoolean213[4] = F;
    arrayOfBoolean213[5] = F;
    arrayOfBoolean213[6] = F;
    arrayOfBoolean213[7] = F;
    arrayOfBoolean213[8] = F;
    arrayOfBoolean213[9] = F;
    arrayOfBoolean213[10] = F;
    arrayOfBoolean213[11] = F;
    arrayOfBoolean213[12] = F;
    arrayOfBoolean213[13] = F;
    arrayOfBoolean213[14] = F;
    arrayOfBoolean213[15] = F;
    arrayOfBoolean213[16] = F;
    arrayOfBoolean213[17] = F;
    arrayOfBoolean213[18] = F;
    arrayOfBoolean213[19] = F;
    arrayOfBoolean213[20] = F;
    arrayOfBoolean213[21] = F;
    arrayOfBoolean213[22] = F;
    arrayOfBoolean213[23] = F;
    arrayOfBoolean213[24] = F;
    arrayOfBoolean213[25] = F;
    arrayOfBoolean213[26] = F;
    arrayOfBoolean198[14] = arrayOfBoolean213;
    boolean[] arrayOfBoolean214 = new boolean[27];
    arrayOfBoolean214[0] = F;
    arrayOfBoolean214[1] = F;
    arrayOfBoolean214[2] = F;
    arrayOfBoolean214[3] = F;
    arrayOfBoolean214[4] = F;
    arrayOfBoolean214[5] = F;
    arrayOfBoolean214[6] = F;
    arrayOfBoolean214[7] = F;
    arrayOfBoolean214[8] = F;
    arrayOfBoolean214[9] = F;
    arrayOfBoolean214[10] = F;
    arrayOfBoolean214[11] = F;
    arrayOfBoolean214[12] = F;
    arrayOfBoolean214[13] = F;
    arrayOfBoolean214[14] = F;
    arrayOfBoolean214[15] = F;
    arrayOfBoolean214[16] = F;
    arrayOfBoolean214[17] = F;
    arrayOfBoolean214[18] = F;
    arrayOfBoolean214[19] = F;
    arrayOfBoolean214[20] = F;
    arrayOfBoolean214[21] = F;
    arrayOfBoolean214[22] = F;
    arrayOfBoolean214[23] = F;
    arrayOfBoolean214[24] = F;
    arrayOfBoolean214[25] = F;
    arrayOfBoolean214[26] = F;
    arrayOfBoolean198[15] = arrayOfBoolean214;
    boolean[] arrayOfBoolean215 = new boolean[27];
    arrayOfBoolean215[0] = F;
    arrayOfBoolean215[1] = F;
    arrayOfBoolean215[2] = F;
    arrayOfBoolean215[3] = F;
    arrayOfBoolean215[4] = F;
    arrayOfBoolean215[5] = F;
    arrayOfBoolean215[6] = F;
    arrayOfBoolean215[7] = F;
    arrayOfBoolean215[8] = F;
    arrayOfBoolean215[9] = F;
    arrayOfBoolean215[10] = F;
    arrayOfBoolean215[11] = F;
    arrayOfBoolean215[12] = F;
    arrayOfBoolean215[13] = F;
    arrayOfBoolean215[14] = F;
    arrayOfBoolean215[15] = F;
    arrayOfBoolean215[16] = F;
    arrayOfBoolean215[17] = F;
    arrayOfBoolean215[18] = F;
    arrayOfBoolean215[19] = F;
    arrayOfBoolean215[20] = F;
    arrayOfBoolean215[21] = F;
    arrayOfBoolean215[22] = F;
    arrayOfBoolean215[23] = F;
    arrayOfBoolean215[24] = F;
    arrayOfBoolean215[25] = F;
    arrayOfBoolean215[26] = F;
    arrayOfBoolean198[16] = arrayOfBoolean215;
    boolean[] arrayOfBoolean216 = new boolean[27];
    arrayOfBoolean216[0] = F;
    arrayOfBoolean216[1] = F;
    arrayOfBoolean216[2] = F;
    arrayOfBoolean216[3] = F;
    arrayOfBoolean216[4] = F;
    arrayOfBoolean216[5] = F;
    arrayOfBoolean216[6] = F;
    arrayOfBoolean216[7] = F;
    arrayOfBoolean216[8] = F;
    arrayOfBoolean216[9] = F;
    arrayOfBoolean216[10] = F;
    arrayOfBoolean216[11] = F;
    arrayOfBoolean216[12] = F;
    arrayOfBoolean216[13] = F;
    arrayOfBoolean216[14] = F;
    arrayOfBoolean216[15] = F;
    arrayOfBoolean216[16] = F;
    arrayOfBoolean216[17] = F;
    arrayOfBoolean216[18] = F;
    arrayOfBoolean216[19] = F;
    arrayOfBoolean216[20] = F;
    arrayOfBoolean216[21] = F;
    arrayOfBoolean216[22] = F;
    arrayOfBoolean216[23] = F;
    arrayOfBoolean216[24] = F;
    arrayOfBoolean216[25] = F;
    arrayOfBoolean216[26] = F;
    arrayOfBoolean198[17] = arrayOfBoolean216;
    boolean[] arrayOfBoolean217 = new boolean[27];
    arrayOfBoolean217[0] = F;
    arrayOfBoolean217[1] = F;
    arrayOfBoolean217[2] = F;
    arrayOfBoolean217[3] = F;
    arrayOfBoolean217[4] = F;
    arrayOfBoolean217[5] = F;
    arrayOfBoolean217[6] = F;
    arrayOfBoolean217[7] = F;
    arrayOfBoolean217[8] = F;
    arrayOfBoolean217[9] = F;
    arrayOfBoolean217[10] = F;
    arrayOfBoolean217[11] = F;
    arrayOfBoolean217[12] = F;
    arrayOfBoolean217[13] = F;
    arrayOfBoolean217[14] = F;
    arrayOfBoolean217[15] = F;
    arrayOfBoolean217[16] = F;
    arrayOfBoolean217[17] = F;
    arrayOfBoolean217[18] = F;
    arrayOfBoolean217[19] = F;
    arrayOfBoolean217[20] = F;
    arrayOfBoolean217[21] = F;
    arrayOfBoolean217[22] = F;
    arrayOfBoolean217[23] = F;
    arrayOfBoolean217[24] = F;
    arrayOfBoolean217[25] = F;
    arrayOfBoolean217[26] = F;
    arrayOfBoolean198[18] = arrayOfBoolean217;
    boolean[] arrayOfBoolean218 = new boolean[27];
    arrayOfBoolean218[0] = F;
    arrayOfBoolean218[1] = F;
    arrayOfBoolean218[2] = F;
    arrayOfBoolean218[3] = F;
    arrayOfBoolean218[4] = F;
    arrayOfBoolean218[5] = F;
    arrayOfBoolean218[6] = F;
    arrayOfBoolean218[7] = F;
    arrayOfBoolean218[8] = F;
    arrayOfBoolean218[9] = F;
    arrayOfBoolean218[10] = F;
    arrayOfBoolean218[11] = F;
    arrayOfBoolean218[12] = F;
    arrayOfBoolean218[13] = F;
    arrayOfBoolean218[14] = F;
    arrayOfBoolean218[15] = F;
    arrayOfBoolean218[16] = F;
    arrayOfBoolean218[17] = F;
    arrayOfBoolean218[18] = F;
    arrayOfBoolean218[19] = F;
    arrayOfBoolean218[20] = F;
    arrayOfBoolean218[21] = F;
    arrayOfBoolean218[22] = F;
    arrayOfBoolean218[23] = F;
    arrayOfBoolean218[24] = F;
    arrayOfBoolean218[25] = F;
    arrayOfBoolean218[26] = F;
    arrayOfBoolean198[19] = arrayOfBoolean218;
    boolean[] arrayOfBoolean219 = new boolean[27];
    arrayOfBoolean219[0] = F;
    arrayOfBoolean219[1] = F;
    arrayOfBoolean219[2] = F;
    arrayOfBoolean219[3] = F;
    arrayOfBoolean219[4] = F;
    arrayOfBoolean219[5] = F;
    arrayOfBoolean219[6] = F;
    arrayOfBoolean219[7] = F;
    arrayOfBoolean219[8] = F;
    arrayOfBoolean219[9] = F;
    arrayOfBoolean219[10] = F;
    arrayOfBoolean219[11] = F;
    arrayOfBoolean219[12] = F;
    arrayOfBoolean219[13] = F;
    arrayOfBoolean219[14] = F;
    arrayOfBoolean219[15] = F;
    arrayOfBoolean219[16] = F;
    arrayOfBoolean219[17] = F;
    arrayOfBoolean219[18] = F;
    arrayOfBoolean219[19] = F;
    arrayOfBoolean219[20] = F;
    arrayOfBoolean219[21] = F;
    arrayOfBoolean219[22] = F;
    arrayOfBoolean219[23] = F;
    arrayOfBoolean219[24] = F;
    arrayOfBoolean219[25] = F;
    arrayOfBoolean219[26] = F;
    arrayOfBoolean198[20] = arrayOfBoolean219;
    boolean[] arrayOfBoolean220 = new boolean[27];
    arrayOfBoolean220[0] = F;
    arrayOfBoolean220[1] = F;
    arrayOfBoolean220[2] = F;
    arrayOfBoolean220[3] = F;
    arrayOfBoolean220[4] = F;
    arrayOfBoolean220[5] = F;
    arrayOfBoolean220[6] = F;
    arrayOfBoolean220[7] = F;
    arrayOfBoolean220[8] = F;
    arrayOfBoolean220[9] = F;
    arrayOfBoolean220[10] = F;
    arrayOfBoolean220[11] = F;
    arrayOfBoolean220[12] = F;
    arrayOfBoolean220[13] = F;
    arrayOfBoolean220[14] = F;
    arrayOfBoolean220[15] = F;
    arrayOfBoolean220[16] = F;
    arrayOfBoolean220[17] = F;
    arrayOfBoolean220[18] = F;
    arrayOfBoolean220[19] = F;
    arrayOfBoolean220[20] = F;
    arrayOfBoolean220[21] = F;
    arrayOfBoolean220[22] = F;
    arrayOfBoolean220[23] = F;
    arrayOfBoolean220[24] = F;
    arrayOfBoolean220[25] = F;
    arrayOfBoolean220[26] = F;
    arrayOfBoolean198[21] = arrayOfBoolean220;
    boolean[] arrayOfBoolean221 = new boolean[27];
    arrayOfBoolean221[0] = F;
    arrayOfBoolean221[1] = F;
    arrayOfBoolean221[2] = F;
    arrayOfBoolean221[3] = F;
    arrayOfBoolean221[4] = F;
    arrayOfBoolean221[5] = F;
    arrayOfBoolean221[6] = F;
    arrayOfBoolean221[7] = F;
    arrayOfBoolean221[8] = F;
    arrayOfBoolean221[9] = F;
    arrayOfBoolean221[10] = F;
    arrayOfBoolean221[11] = F;
    arrayOfBoolean221[12] = F;
    arrayOfBoolean221[13] = F;
    arrayOfBoolean221[14] = F;
    arrayOfBoolean221[15] = F;
    arrayOfBoolean221[16] = F;
    arrayOfBoolean221[17] = F;
    arrayOfBoolean221[18] = F;
    arrayOfBoolean221[19] = F;
    arrayOfBoolean221[20] = F;
    arrayOfBoolean221[21] = F;
    arrayOfBoolean221[22] = F;
    arrayOfBoolean221[23] = F;
    arrayOfBoolean221[24] = F;
    arrayOfBoolean221[25] = F;
    arrayOfBoolean221[26] = F;
    arrayOfBoolean198[22] = arrayOfBoolean221;
    boolean[] arrayOfBoolean222 = new boolean[27];
    arrayOfBoolean222[0] = F;
    arrayOfBoolean222[1] = F;
    arrayOfBoolean222[2] = F;
    arrayOfBoolean222[3] = F;
    arrayOfBoolean222[4] = F;
    arrayOfBoolean222[5] = F;
    arrayOfBoolean222[6] = F;
    arrayOfBoolean222[7] = F;
    arrayOfBoolean222[8] = F;
    arrayOfBoolean222[9] = F;
    arrayOfBoolean222[10] = F;
    arrayOfBoolean222[11] = F;
    arrayOfBoolean222[12] = F;
    arrayOfBoolean222[13] = F;
    arrayOfBoolean222[14] = F;
    arrayOfBoolean222[15] = F;
    arrayOfBoolean222[16] = F;
    arrayOfBoolean222[17] = F;
    arrayOfBoolean222[18] = F;
    arrayOfBoolean222[19] = F;
    arrayOfBoolean222[20] = F;
    arrayOfBoolean222[21] = F;
    arrayOfBoolean222[22] = F;
    arrayOfBoolean222[23] = F;
    arrayOfBoolean222[24] = F;
    arrayOfBoolean222[25] = F;
    arrayOfBoolean222[26] = F;
    arrayOfBoolean198[23] = arrayOfBoolean222;
    boolean[] arrayOfBoolean223 = new boolean[27];
    arrayOfBoolean223[0] = F;
    arrayOfBoolean223[1] = F;
    arrayOfBoolean223[2] = F;
    arrayOfBoolean223[3] = F;
    arrayOfBoolean223[4] = F;
    arrayOfBoolean223[5] = F;
    arrayOfBoolean223[6] = F;
    arrayOfBoolean223[7] = F;
    arrayOfBoolean223[8] = F;
    arrayOfBoolean223[9] = F;
    arrayOfBoolean223[10] = F;
    arrayOfBoolean223[11] = F;
    arrayOfBoolean223[12] = F;
    arrayOfBoolean223[13] = F;
    arrayOfBoolean223[14] = F;
    arrayOfBoolean223[15] = F;
    arrayOfBoolean223[16] = F;
    arrayOfBoolean223[17] = F;
    arrayOfBoolean223[18] = F;
    arrayOfBoolean223[19] = F;
    arrayOfBoolean223[20] = F;
    arrayOfBoolean223[21] = F;
    arrayOfBoolean223[22] = F;
    arrayOfBoolean223[23] = F;
    arrayOfBoolean223[24] = F;
    arrayOfBoolean223[25] = F;
    arrayOfBoolean223[26] = F;
    arrayOfBoolean198[24] = arrayOfBoolean223;
    boolean[] arrayOfBoolean224 = new boolean[27];
    arrayOfBoolean224[0] = F;
    arrayOfBoolean224[1] = F;
    arrayOfBoolean224[2] = F;
    arrayOfBoolean224[3] = F;
    arrayOfBoolean224[4] = F;
    arrayOfBoolean224[5] = F;
    arrayOfBoolean224[6] = F;
    arrayOfBoolean224[7] = F;
    arrayOfBoolean224[8] = F;
    arrayOfBoolean224[9] = F;
    arrayOfBoolean224[10] = F;
    arrayOfBoolean224[11] = F;
    arrayOfBoolean224[12] = F;
    arrayOfBoolean224[13] = F;
    arrayOfBoolean224[14] = F;
    arrayOfBoolean224[15] = F;
    arrayOfBoolean224[16] = F;
    arrayOfBoolean224[17] = F;
    arrayOfBoolean224[18] = F;
    arrayOfBoolean224[19] = F;
    arrayOfBoolean224[20] = F;
    arrayOfBoolean224[21] = F;
    arrayOfBoolean224[22] = F;
    arrayOfBoolean224[23] = F;
    arrayOfBoolean224[24] = F;
    arrayOfBoolean224[25] = F;
    arrayOfBoolean224[26] = F;
    arrayOfBoolean198[25] = arrayOfBoolean224;
    boolean[] arrayOfBoolean225 = new boolean[27];
    arrayOfBoolean225[0] = F;
    arrayOfBoolean225[1] = F;
    arrayOfBoolean225[2] = F;
    arrayOfBoolean225[3] = F;
    arrayOfBoolean225[4] = F;
    arrayOfBoolean225[5] = F;
    arrayOfBoolean225[6] = F;
    arrayOfBoolean225[7] = F;
    arrayOfBoolean225[8] = F;
    arrayOfBoolean225[9] = F;
    arrayOfBoolean225[10] = F;
    arrayOfBoolean225[11] = F;
    arrayOfBoolean225[12] = F;
    arrayOfBoolean225[13] = F;
    arrayOfBoolean225[14] = F;
    arrayOfBoolean225[15] = F;
    arrayOfBoolean225[16] = F;
    arrayOfBoolean225[17] = F;
    arrayOfBoolean225[18] = F;
    arrayOfBoolean225[19] = F;
    arrayOfBoolean225[20] = F;
    arrayOfBoolean225[21] = F;
    arrayOfBoolean225[22] = F;
    arrayOfBoolean225[23] = F;
    arrayOfBoolean225[24] = F;
    arrayOfBoolean225[25] = F;
    arrayOfBoolean225[26] = F;
    arrayOfBoolean198[26] = arrayOfBoolean225;
    boolean[] arrayOfBoolean226 = new boolean[27];
    arrayOfBoolean226[0] = F;
    arrayOfBoolean226[1] = F;
    arrayOfBoolean226[2] = F;
    arrayOfBoolean226[3] = F;
    arrayOfBoolean226[4] = F;
    arrayOfBoolean226[5] = F;
    arrayOfBoolean226[6] = F;
    arrayOfBoolean226[7] = F;
    arrayOfBoolean226[8] = F;
    arrayOfBoolean226[9] = F;
    arrayOfBoolean226[10] = F;
    arrayOfBoolean226[11] = F;
    arrayOfBoolean226[12] = F;
    arrayOfBoolean226[13] = F;
    arrayOfBoolean226[14] = F;
    arrayOfBoolean226[15] = F;
    arrayOfBoolean226[16] = F;
    arrayOfBoolean226[17] = F;
    arrayOfBoolean226[18] = F;
    arrayOfBoolean226[19] = F;
    arrayOfBoolean226[20] = F;
    arrayOfBoolean226[21] = F;
    arrayOfBoolean226[22] = F;
    arrayOfBoolean226[23] = F;
    arrayOfBoolean226[24] = F;
    arrayOfBoolean226[25] = F;
    arrayOfBoolean226[26] = F;
    arrayOfBoolean198[27] = arrayOfBoolean226;
    boolean[] arrayOfBoolean227 = new boolean[27];
    arrayOfBoolean227[0] = F;
    arrayOfBoolean227[1] = F;
    arrayOfBoolean227[2] = F;
    arrayOfBoolean227[3] = F;
    arrayOfBoolean227[4] = F;
    arrayOfBoolean227[5] = F;
    arrayOfBoolean227[6] = F;
    arrayOfBoolean227[7] = F;
    arrayOfBoolean227[8] = F;
    arrayOfBoolean227[9] = F;
    arrayOfBoolean227[10] = F;
    arrayOfBoolean227[11] = F;
    arrayOfBoolean227[12] = F;
    arrayOfBoolean227[13] = F;
    arrayOfBoolean227[14] = F;
    arrayOfBoolean227[15] = F;
    arrayOfBoolean227[16] = F;
    arrayOfBoolean227[17] = F;
    arrayOfBoolean227[18] = F;
    arrayOfBoolean227[19] = F;
    arrayOfBoolean227[20] = F;
    arrayOfBoolean227[21] = F;
    arrayOfBoolean227[22] = F;
    arrayOfBoolean227[23] = F;
    arrayOfBoolean227[24] = F;
    arrayOfBoolean227[25] = F;
    arrayOfBoolean227[26] = F;
    arrayOfBoolean198[28] = arrayOfBoolean227;
    boolean[] arrayOfBoolean228 = new boolean[27];
    arrayOfBoolean228[0] = F;
    arrayOfBoolean228[1] = F;
    arrayOfBoolean228[2] = F;
    arrayOfBoolean228[3] = F;
    arrayOfBoolean228[4] = F;
    arrayOfBoolean228[5] = F;
    arrayOfBoolean228[6] = F;
    arrayOfBoolean228[7] = F;
    arrayOfBoolean228[8] = F;
    arrayOfBoolean228[9] = F;
    arrayOfBoolean228[10] = F;
    arrayOfBoolean228[11] = F;
    arrayOfBoolean228[12] = F;
    arrayOfBoolean228[13] = T;
    arrayOfBoolean228[14] = F;
    arrayOfBoolean228[15] = F;
    arrayOfBoolean228[16] = F;
    arrayOfBoolean228[17] = F;
    arrayOfBoolean228[18] = F;
    arrayOfBoolean228[19] = F;
    arrayOfBoolean228[20] = F;
    arrayOfBoolean228[21] = F;
    arrayOfBoolean228[22] = F;
    arrayOfBoolean228[23] = F;
    arrayOfBoolean228[24] = F;
    arrayOfBoolean228[25] = F;
    arrayOfBoolean228[26] = F;
    arrayOfBoolean198[29] = arrayOfBoolean228;
    arrayOfBoolean114[3] = arrayOfBoolean198;
    this.a_Bricks_exist = arrayOfBoolean114;
    this.start_animation_timer = new CountDownTimer(2010L, 200L)
    {
      public void onFinish()
      {
        BrickView.this.start_animation = false;
      }

      public void onTick(long paramAnonymousLong)
      {
        BrickView.start_animation_number = 1 + BrickView.start_animation_number;
        BrickView.start_animation_number %= 5;
      }
    };
    this.hint_animation_timer = new CountDownTimer(2100L, 200L)
    {
      public void onFinish()
      {
        BrickView.hint_animation = false;
        BrickView.this.invalidate();
      }

      public void onTick(long paramAnonymousLong)
      {
        BrickView.hint_animation_number = 1 + BrickView.hint_animation_number;
        BrickView.hint_animation_number %= 10;
        BrickView.this.invalidate();
      }
    };
    this.tmp_playTime = 0;
    this.playTime_timer = new CountDownTimer(1000L, 1000L)
    {
      public void onFinish()
      {
        BrickView localBrickView = BrickView.this;
        localBrickView.tmp_playTime = (1 + localBrickView.tmp_playTime);
        start();
      }

      public void onTick(long paramAnonymousLong)
      {
      }
    };
    this.board_TimerTask_Handler = new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        super.handleMessage(paramAnonymousMessage);
        switch (paramAnonymousMessage.what)
        {
        default:
        case 1:
        }
        do
          return;
        while (BrickView.game_over);
        BrickView.this.board_move(GameActivity.gravity_sensor[0]);
        BrickView.this.invalidate();
      }
    };
    this.ball_TimerTask_Handler = new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        super.handleMessage(paramAnonymousMessage);
        switch (paramAnonymousMessage.what)
        {
        default:
        case 1:
        }
        do
          return;
        while (!BrickView.this.ball_alive);
        BrickView.this.ball_move_and_touchcheck();
        BrickView.this.invalidate();
      }
    };
    Log.d("HtcDotBreaker", "BrickView Constructor:\tbegin");
    this.current_game_level = brick_game_level;
    init_margins();
    if (!set_static_variable)
    {
      init_dot_screen_size();
      init_mark_loc();
      init_Paint_color();
      set_static_variable = true;
    }
    StringBuilder localStringBuilder = new StringBuilder().append("dot_breaker_level_");
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(brick_game_level);
    String str = String.format(localLocale, "%03d", arrayOfObject) + ".png";
    Log.d("HtcDotBreaker", "game_level_string = " + str);
    this.mBitmap4827 = DotBreakerUtils.getBitmapFromAsset(paramContext.getAssets(), str);
    ball_speed = GameActivity.preferencesGet("SpeedOfBall", 70);
    board_speed = GameActivity.preferencesGet("SpeedOfBoard", 40);
    board_moveAngle = GameActivity.preferencesGet("MoveAngleOfBoard", 0.4F);
    boolean bool = isResumeGame();
    if (bool)
      this.game_pause = true;
    int i = this.res.getDimensionPixelSize(2131099671);
    int j = this.res.getDimensionPixelSize(2131099672);
    int m;
    int n;
    int i2;
    int i3;
    int i4;
    int i5;
    int i6;
    if (bool)
    {
      Log.d("Alvis", "score = new Numeric().\tkeep_score=" + keep_score + "\tin if().");
      this.score = new Numeric(GameActivity.preferencesGet("Current_Score", 0), this.WALL_LEFT + i * BLOCK_WIDTH, this.WALL_TOP + j * BLOCK_HEIGHT);
      keep_score = this.score.numeric;
      m = this.res.getDimensionPixelSize(2131099675);
      n = this.res.getDimensionPixelSize(2131099676);
      if (!bool)
        break label34975;
      Log.d("Alvis", "life = new Numeric().\tkeep_life=" + keep_life + "\tin if().");
      this.life = new Numeric(GameActivity.preferencesGet("Current_Life", 5), this.WALL_LEFT + m * BLOCK_WIDTH, this.WALL_TOP + n * BLOCK_HEIGHT);
      keep_life = this.life.numeric;
      this.BRICKS_AREA_LEFT = (this.WALL_LEFT + this.res.getDimensionPixelSize(2131099679) * BLOCK_WIDTH);
      this.BRICKS_AREA_TOP = (this.WALL_TOP + this.res.getDimensionPixelSize(2131099680) * BLOCK_HEIGHT);
      this.BRICKS_AREA_RIGHT = (this.WALL_RIGHT - this.res.getDimensionPixelSize(2131099681) * BLOCK_WIDTH);
      this.BRICKS_AREA_BOTTOM = (this.WALL_BOTTOM - this.res.getDimensionPixelSize(2131099682) * BLOCK_HEIGHT);
      bricks_initial(brick_game_level);
      i2 = this.res.getDimensionPixelSize(2131099683);
      i3 = this.res.getDimensionPixelSize(2131099684);
      i4 = this.res.getDimensionPixelSize(2131099685);
      i5 = this.res.getDimensionPixelSize(2131099686);
      i6 = this.res.getDimensionPixelSize(2131099687);
      if (!bool)
        break label35065;
    }
    int i7;
    int i8;
    label34975: label35065: for (this.board = new Board(true, a_Paint[6], GameActivity.preferencesGet("Current_Board_Left", this.WALL_LEFT + i2 * BLOCK_WIDTH), GameActivity.preferencesGet("Current_Board_Top", this.WALL_TOP + i3 * BLOCK_HEIGHT), GameActivity.preferencesGet("Current_Board_Right", this.WALL_LEFT + i4 * BLOCK_WIDTH), GameActivity.preferencesGet("Current_Board_Bottom", this.WALL_TOP + i5 * BLOCK_HEIGHT), GameActivity.preferencesGet("Current_Board_Width", i6 * BLOCK_WIDTH), GameActivity.preferencesGet("Current_Board_MoveUnitHorizontal", BLOCK_WIDTH)); ; this.board = new Board(T, a_Paint[6], this.WALL_LEFT + i2 * BLOCK_WIDTH, this.WALL_TOP + i3 * BLOCK_HEIGHT, this.WALL_LEFT + i4 * BLOCK_WIDTH, this.WALL_TOP + i5 * BLOCK_HEIGHT, i6 * BLOCK_WIDTH, BLOCK_WIDTH))
    {
      this.BALL_MOVE_UNIT_HORIZONTAL_POSITIVE = (1 * BLOCK_WIDTH);
      this.BALL_MOVE_UNIT_HORIZONTAL_NEGATIVE = (-1 * BLOCK_WIDTH);
      this.BALL_MOVE_UNIT_VERTICAL_POSITIVE = (1 * BLOCK_HEIGHT);
      this.BALL_MOVE_UNIT_VERTICAL_NEGATIVE = (-1 * BLOCK_HEIGHT);
      i7 = this.res.getDimensionPixelSize(2131099688);
      i8 = this.res.getDimensionPixelSize(2131099689);
      if (!bool)
        break label35213;
      for (int i9 = 0; i9 < 4; i9++)
        if (GameActivity.preferencesGet("Current_Ball[" + i9 + "]_Exist", false))
          this.a_ball[i9] = new Ball(GameActivity.preferencesGet("Current_Ball[" + i9 + "]_Exist", false), a_Paint[10], GameActivity.preferencesGet("Current_Ball[" + i9 + "]_Left", this.board.left + this.board.width / 2 + i7 / 2 * BLOCK_WIDTH), GameActivity.preferencesGet("Current_Ball[" + i9 + "]_Top", this.board.top - i8 * BLOCK_HEIGHT), GameActivity.preferencesGet("Current_Ball[" + i9 + "]_Right", this.board.left + this.board.width / 2 + i7 / 2 * BLOCK_WIDTH + i7 * BLOCK_WIDTH), GameActivity.preferencesGet("Current_Ball[" + i9 + "]_Bottom", this.board.top), GameActivity.preferencesGet("Current_Ball_Width", i7 * BLOCK_WIDTH), GameActivity.preferencesGet("Current_Ball_Height", i8 * BLOCK_HEIGHT), GameActivity.preferencesGet("Current_Ball[" + i9 + "]_MoveUnitHorizontal", this.BALL_MOVE_UNIT_HORIZONTAL_POSITIVE), GameActivity.preferencesGet("Current_Ball[" + i9 + "]_MoveUnitVertical", this.BALL_MOVE_UNIT_VERTICAL_NEGATIVE));
      Log.d("Alvis", "score = new Numeric().\tkeep_score=" + keep_score + "\tin else().");
      if (keep_score == -1);
      for (int k = 0; ; k = keep_score)
      {
        this.score = new Numeric(k, this.WALL_LEFT + i * BLOCK_WIDTH, this.WALL_TOP + j * BLOCK_HEIGHT);
        break;
      }
      Log.d("Alvis", "life = new Numeric().\tkeep_life=" + keep_life + "\tin else().");
      if (keep_life <= 0);
      for (int i1 = 5; ; i1 = keep_life)
      {
        this.life = new Numeric(i1, this.WALL_LEFT + m * BLOCK_WIDTH, this.WALL_TOP + n * BLOCK_HEIGHT);
        break;
      }
    }
    for (this.ball_alive = GameActivity.preferencesGet("Current_Ball_Alive", false); ; this.ball_alive = false)
    {
      this.START_ANIMATION_LOC_X = (this.WALL_LEFT + this.res.getDimensionPixelSize(2131099662) * BLOCK_WIDTH);
      this.START_ANIMATION_LOC_Y = (this.WALL_LEFT + this.res.getDimensionPixelSize(2131099663) * BLOCK_HEIGHT);
      gameStart();
      Log.d("HtcDotBreaker", "BrickView Constructor:\tend");
      return;
      label35213: this.a_ball[0] = new Ball(T, a_Paint[10], this.board.left + this.board.width / 2 + i7 / 2 * BLOCK_WIDTH, this.board.top - i8 * BLOCK_HEIGHT, this.board.left + this.board.width / 2 + i7 / 2 * BLOCK_WIDTH + i7 * BLOCK_WIDTH, this.board.top, i7 * BLOCK_WIDTH, i8 * BLOCK_HEIGHT, this.BALL_MOVE_UNIT_HORIZONTAL_POSITIVE, this.BALL_MOVE_UNIT_VERTICAL_NEGATIVE);
    }
  }

  private void add_life()
  {
    if (this.life.numeric < 20)
    {
      Numeric localNumeric = this.life;
      int i = 1 + localNumeric.numeric;
      localNumeric.numeric = i;
      keep_life = i;
    }
  }

  private void ball_initial()
  {
    this.a_ball[0].width = (this.res.getDimensionPixelSize(2131099688) * BLOCK_WIDTH);
    this.a_ball[0].height = (this.res.getDimensionPixelSize(2131099689) * BLOCK_WIDTH);
    this.bonus_22YELLOW = false;
    this.ball_alive = false;
    for (int i = 1; i < 4; i++)
      if ((this.a_ball[i] != null) && (this.a_ball[i].exist))
        this.a_ball[i].exist = false;
    this.a_ball[0].exist = true;
    ball_move_up(0);
    this.number_of_balls_alive = 1;
    ball_reset_location();
  }

  private void ball_move(int paramInt)
  {
    Ball localBall1 = this.a_ball[paramInt];
    localBall1.left += this.a_ball[paramInt].move_unit_horizontal;
    Ball localBall2 = this.a_ball[paramInt];
    localBall2.right += this.a_ball[paramInt].move_unit_horizontal;
    Ball localBall3 = this.a_ball[paramInt];
    localBall3.top += this.a_ball[paramInt].move_unit_vertical;
    Ball localBall4 = this.a_ball[paramInt];
    localBall4.bottom += this.a_ball[paramInt].move_unit_vertical;
    this.ball_travelled = (1 + this.ball_travelled);
  }

  private void ball_move_down(int paramInt)
  {
    this.a_ball[paramInt].move_unit_vertical = this.BALL_MOVE_UNIT_VERTICAL_POSITIVE;
  }

  private void ball_move_left(int paramInt)
  {
    this.a_ball[paramInt].move_unit_horizontal = this.BALL_MOVE_UNIT_HORIZONTAL_NEGATIVE;
  }

  private void ball_move_right(int paramInt)
  {
    this.a_ball[paramInt].move_unit_horizontal = this.BALL_MOVE_UNIT_HORIZONTAL_POSITIVE;
  }

  private void ball_move_up(int paramInt)
  {
    this.a_ball[paramInt].move_unit_vertical = this.BALL_MOVE_UNIT_VERTICAL_NEGATIVE;
  }

  private void ball_reset_location()
  {
    for (int i = 0; i < 4; i++)
      if ((this.a_ball[i] != null) && (this.a_ball[i].exist))
      {
        this.a_ball[i].left = (this.board.left + this.board.width / BLOCK_WIDTH / 2 * BLOCK_WIDTH - this.a_ball[i].width / BLOCK_WIDTH / 2 * BLOCK_WIDTH);
        this.a_ball[i].right = (this.a_ball[i].left + this.a_ball[i].width);
        this.a_ball[i].top = (this.board.top - this.a_ball[i].height);
        this.a_ball[i].bottom = this.board.top;
      }
  }

  private void ball_touch_brick(int paramInt1, int paramInt2, int paramInt3)
  {
    new Random().nextInt(10);
    if (this.bonus_22YELLOW)
    {
      ball_touch_brick_and_clear_brick(paramInt2 - 3, paramInt3);
      ball_touch_brick_and_clear_brick(paramInt2 + 3, paramInt3);
      ball_touch_brick_and_clear_brick(paramInt2, paramInt3 - 3);
      ball_touch_brick_and_clear_brick(paramInt2, paramInt3 + 3);
    }
    ball_touch_brick_and_clear_brick(paramInt2, paramInt3);
    if (!this.bonus_22RED)
    {
      if ((this.a_ball[paramInt1].left >= this.a_bricks[paramInt2][paramInt3].left) || (this.a_ball[paramInt1].right >= this.a_bricks[paramInt2][paramInt3].right))
        break label168;
      ball_move_left(paramInt1);
    }
    label168: 
    do
      while ((this.a_ball[paramInt1].top < this.a_bricks[paramInt2][paramInt3].top) && (this.a_ball[paramInt1].bottom < this.a_bricks[paramInt2][paramInt3].bottom))
      {
        ball_move_up(paramInt1);
        return;
        if ((this.a_ball[paramInt1].left > this.a_bricks[paramInt2][paramInt3].left) && (this.a_ball[paramInt1].right > this.a_bricks[paramInt2][paramInt3].right))
          ball_move_right(paramInt1);
      }
    while ((this.a_ball[paramInt1].top <= this.a_bricks[paramInt2][paramInt3].top) || (this.a_ball[paramInt1].bottom <= this.a_bricks[paramInt2][paramInt3].bottom));
    ball_move_down(paramInt1);
  }

  private void ball_touch_brick_and_clear_brick(int paramInt1, int paramInt2)
  {
    if (mode_setting == true)
    {
      this.a_bricks[paramInt1][paramInt2].exist = false;
      Numeric localNumeric2 = this.score;
      int m = 1 + localNumeric2.numeric;
      localNumeric2.numeric = m;
      keep_score = m;
      this.mBricksHit = (1 + this.mBricksHit);
    }
    while (true)
    {
      return;
      for (int i = paramInt1 - 1; i < paramInt1 + 2; i++)
      {
        int j = paramInt2 - 1;
        if (j < paramInt2 + 2)
        {
          if ((i < 0) || (i >= this.a_bricks.length) || (j < 0) || (j >= this.a_bricks[0].length) || (!this.a_bricks[i][j].exist));
          while (true)
          {
            j++;
            break;
            this.a_bricks[i][j].exist = false;
            Numeric localNumeric1 = this.score;
            int k = 1 + localNumeric1.numeric;
            localNumeric1.numeric = k;
            keep_score = k;
            check_create_life_bonus(this.score.numeric, paramInt1, paramInt2);
            this.mBricksHit = (1 + this.mBricksHit);
          }
        }
      }
    }
  }

  private void board_initial()
  {
    this.board.width = (this.res.getDimensionPixelSize(2131099687) * BLOCK_WIDTH);
  }

  private void bonus_22BLUE_change_color(BrickItem paramBrickItem)
  {
    if (this.bonus_22BLUE_change_color)
    {
      int i = 1 + this.bonus_22BLUE;
      this.bonus_22BLUE = i;
      this.bonus_22BLUE = (i % 7);
      paramBrickItem.paint = a_Paint[(28 + this.bonus_22BLUE)];
    }
  }

  private void bricks_initial(int paramInt)
  {
    Log.d("Alvis", "bircks_initial()\tbegin.");
    Log.d("Alvis", "bircks_initial()\t1");
    if (isResumeGame())
    {
      int m = GameActivity.preferencesGet("Current_Bricks_Location_Left", this.BRICKS_AREA_LEFT);
      int n = GameActivity.preferencesGet("Current_Bricks_Location_Top", this.BRICKS_AREA_TOP);
      int i1 = GameActivity.preferencesGet("Current_Bricks_Width", BLOCK_WIDTH);
      int i2 = GameActivity.preferencesGet("Current_Bricks_Height", BLOCK_HEIGHT);
      for (int i3 = 0; i3 < this.a_bricks.length; i3++)
        for (int i4 = 0; i4 < this.a_bricks[i3].length; i4++)
        {
          Paint localPaint2 = new Paint();
          localPaint2.setColor(GameActivity.preferencesGet("Current_Bricks[" + i3 + "][" + i4 + "]_PaintColor", -16777216));
          this.a_bricks[i3][i4] = new BrickItem(GameActivity.preferencesGet("Current_Bricks[" + i3 + "][" + i4 + "]_Exist", true), localPaint2, m + i3 * i1, n + i4 * i2, m + i1 * (i3 + 1), n + i2 * (i4 + 1));
        }
    }
    int i = -16777216;
    for (int j = 0; j < this.a_bricks.length; j++)
      for (int k = 0; k < this.a_bricks[j].length; k++)
      {
        if (this.mBitmap4827 != null)
          i = this.mBitmap4827.getPixel(j, k + 7);
        Log.d("testcolor", "color[" + j + "][" + k + "]=" + i);
        Paint localPaint1 = new Paint();
        localPaint1.setColor(i);
        this.a_bricks[j][k] = new BrickItem(true, localPaint1, this.BRICKS_AREA_LEFT + j * BLOCK_WIDTH, this.BRICKS_AREA_TOP + k * BLOCK_HEIGHT, this.BRICKS_AREA_LEFT + (j + 1) * BLOCK_WIDTH, this.BRICKS_AREA_TOP + (k + 1) * BLOCK_HEIGHT);
        if ((0xFFFFFF & i) == 0)
          this.a_bricks[j][k].exist = false;
      }
    Log.d("Alvis", "bircks_initial()\tend.");
  }

  private void bricks_nextlevel()
  {
    int i = 20;
    Log.d("HtcDotBreaker", "[BrickView] bricks_nextlevel\tbegin.");
    int j = this.current_game_level;
    if (this.current_game_level >= i);
    while (true)
    {
      brick_game_level = i;
      if (j == i)
        GameActivity.preferencesEditor("CongratulationMessage", true);
      GameActivity.preferencesEditor("GameStatus[" + j + "]", 2);
      GameActivity.preferencesEditor("GameStatus[" + i + "]", 1);
      GameActivity.preferencesEditor("PlayedGameLevel", i);
      GameActivity.preferencesEditorCommit();
      Log.d("HtcDotBreaker", "[BrickView] bricks_nextlevel\tend.");
      return;
      i = j + 1;
    }
  }

  private void bricks_repositioning(int paramInt1, int paramInt2)
  {
    for (int i = 0; i < this.a_bricks.length; i++)
      for (int j = 0; j < this.a_bricks[i].length; j++)
      {
        BrickItem localBrickItem1 = this.a_bricks[i][j];
        localBrickItem1.left += paramInt1 * BLOCK_WIDTH;
        BrickItem localBrickItem2 = this.a_bricks[i][j];
        localBrickItem2.right += paramInt1 * BLOCK_WIDTH;
        BrickItem localBrickItem3 = this.a_bricks[i][j];
        localBrickItem3.top += paramInt2 * BLOCK_HEIGHT;
        BrickItem localBrickItem4 = this.a_bricks[i][j];
        localBrickItem4.bottom += paramInt2 * BLOCK_HEIGHT;
      }
  }

  private void cancelTimer()
  {
    Log.d("HtcDotBreaker", "[BrickView] cancelTimer()\tbegin");
    ball_TimerTask.cancel();
    board_TimerTask.cancel();
    for (int i = 0; i < this.a_bonus.length; i++)
    {
      Log.d("HtcDotBreaker", "[BrickView] cancelTimer()\ttype=" + i);
      if (this.a_bonus_timer[i] != null)
      {
        Log.d("HtcDotBreaker", "[BrickView] cancelTimer()\ttype=" + i + "  a_bonus_timer[type] != null");
        this.a_bonus_timer[i].cancel();
      }
    }
    if (this.bonus_item_timer_change_color != null)
      this.bonus_item_timer_change_color.cancel();
    Log.d("HtcDotBreaker", "[BrickView] cancelTimer()\tend");
  }

  private boolean check_bricks_move_with_ball(int paramInt1, int paramInt2)
  {
    for (int i = 0; i < this.a_bricks.length; i++)
      for (int j = 0; j < this.a_bricks[i].length; j++)
        for (int k = 0; k < 4; k++)
          if ((this.a_ball[k] != null) && (this.a_ball[k].exist) && (this.a_bricks[i][j].left + paramInt1 * BLOCK_WIDTH == this.a_ball[k].left) && (this.a_bricks[i][j].top + paramInt2 * BLOCK_HEIGHT == this.a_ball[k].top) && (this.a_bricks[i][j].exist == true))
            return true;
    return false;
  }

  private void check_create_life_bonus(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = this.tmp_score_add_life_check;
    this.tmp_score_add_life_check = (paramInt1 % 50);
    if ((this.tmp_score_add_life_check < i) && (this.number_of_bonus < this.MAX_NUMBER_OF_BONUS) && (this.a_bonus[6] == null))
      create_bonus(6, paramInt2, paramInt3);
  }

  private boolean check_level_complete()
  {
    for (int i = 0; i < this.a_bricks.length; i++)
      for (int j = 0; j < this.a_bricks[i].length; j++)
        if (this.a_bricks[i][j].exist == true)
          return false;
    invalidate();
    return true;
  }

  private void check_touch_ball_and_board(int paramInt)
  {
    if (this.a_ball[paramInt].bottom == this.board.top)
    {
      if ((this.a_ball[paramInt].right >= this.board.left) && (this.a_ball[paramInt].left <= this.board.right))
        ball_move_up(paramInt);
      if ((this.a_ball[paramInt].right == this.board.left) || (this.a_ball[paramInt].right == this.board.left + BLOCK_WIDTH))
        ball_move_left(paramInt);
      if ((this.a_ball[paramInt].left == this.board.right) || (this.a_ball[paramInt].left == this.board.right - BLOCK_WIDTH))
        ball_move_right(paramInt);
    }
    while (true)
    {
      check_touch_ball_and_wall(paramInt);
      return;
      if ((this.a_ball[paramInt].bottom == this.board.bottom) || (this.a_ball[paramInt].bottom == this.board.bottom - BLOCK_HEIGHT))
      {
        if (this.a_ball[paramInt].right == this.board.left)
          ball_move_left(paramInt);
        while (true)
        {
          if ((this.a_ball[paramInt].left < this.board.left) || (this.a_ball[paramInt].left > this.board.left + 2 * BLOCK_WIDTH))
            break label323;
          this.board.left = this.a_ball[paramInt].right;
          ball_move_left(paramInt);
          break;
          if (this.a_ball[paramInt].left == this.board.right)
            ball_move_right(paramInt);
        }
        label323: if ((this.a_ball[paramInt].left >= this.board.left + 3 * BLOCK_WIDTH) && (this.a_ball[paramInt].left <= this.board.left + 5 * BLOCK_WIDTH))
        {
          this.board.left = (this.a_ball[paramInt].left - this.board.width);
          ball_move_right(paramInt);
        }
      }
    }
  }

  private void check_touch_ball_and_bricks(int paramInt)
  {
    for (int i = 0; i < this.a_bricks.length; i++)
    {
      int j = 0;
      if (j < this.a_bricks[i].length)
      {
        if (!this.a_bricks[i][j].exist);
        while (true)
        {
          j++;
          break;
          if ((this.a_ball[paramInt].right == this.a_bricks[i][j].left) && (this.a_ball[paramInt].top < this.a_bricks[i][j].bottom) && (this.a_ball[paramInt].bottom > this.a_bricks[i][j].top))
          {
            ball_touch_brick(paramInt, i, j);
          }
          else if ((this.a_ball[paramInt].left == this.a_bricks[i][j].right) && (this.a_ball[paramInt].top < this.a_bricks[i][j].bottom) && (this.a_ball[paramInt].bottom > this.a_bricks[i][j].top))
          {
            ball_touch_brick(paramInt, i, j);
          }
          else if ((this.a_ball[paramInt].bottom == this.a_bricks[i][j].top) && (this.a_ball[paramInt].left < this.a_bricks[i][j].right) && (this.a_ball[paramInt].right > this.a_bricks[i][j].left))
          {
            ball_touch_brick(paramInt, i, j);
          }
          else if ((this.a_ball[paramInt].top == this.a_bricks[i][j].bottom) && (this.a_ball[paramInt].left < this.a_bricks[i][j].right) && (this.a_ball[paramInt].right > this.a_bricks[i][j].left))
          {
            ball_touch_brick(paramInt, i, j);
          }
          else if ((this.a_ball[paramInt].bottom == this.a_bricks[i][j].top) && (this.a_ball[paramInt].right == this.a_bricks[i][j].left) && (this.a_ball[paramInt].move_unit_horizontal == this.BALL_MOVE_UNIT_HORIZONTAL_POSITIVE) && (this.a_ball[paramInt].move_unit_vertical == this.BALL_MOVE_UNIT_VERTICAL_POSITIVE) && ((i - 1 < 0) || ((i - 1 >= 0) && (!this.a_bricks[(i - 1)][j].exist))) && ((j - 1 < 0) || ((j - 1 >= 0) && (!this.a_bricks[i][(j - 1)].exist))))
          {
            ball_touch_brick(paramInt, i, j);
            if (this.a_ball[paramInt].left <= this.WALL_LEFT)
              ball_move_right(paramInt);
            if (this.a_ball[paramInt].top <= this.WALL_TOP)
              ball_move_down(paramInt);
          }
          else if ((this.a_ball[paramInt].bottom == this.a_bricks[i][j].top) && (this.a_ball[paramInt].left == this.a_bricks[i][j].right) && (this.a_ball[paramInt].move_unit_horizontal == this.BALL_MOVE_UNIT_HORIZONTAL_NEGATIVE) && (this.a_ball[paramInt].move_unit_vertical == this.BALL_MOVE_UNIT_VERTICAL_POSITIVE) && ((i + 1 == this.a_bricks.length) || ((i + 1 < this.a_bricks.length) && (!this.a_bricks[(i + 1)][j].exist))) && ((j - 1 < 0) || ((j - 1 >= 0) && (!this.a_bricks[i][(j - 1)].exist))))
          {
            ball_touch_brick(paramInt, i, j);
            if (this.a_ball[paramInt].right >= this.WALL_RIGHT)
              ball_move_left(paramInt);
            if (this.a_ball[paramInt].top <= this.WALL_TOP)
              ball_move_down(paramInt);
          }
          else if ((this.a_ball[paramInt].top == this.a_bricks[i][j].bottom) && (this.a_ball[paramInt].right == this.a_bricks[i][j].left) && (this.a_ball[paramInt].move_unit_horizontal == this.BALL_MOVE_UNIT_HORIZONTAL_POSITIVE) && (this.a_ball[paramInt].move_unit_vertical == this.BALL_MOVE_UNIT_VERTICAL_NEGATIVE) && ((i - 1 < 0) || ((i - 1 >= 0) && (!this.a_bricks[(i - 1)][j].exist))) && ((j + 1 == this.a_bricks[i].length) || ((j + 1 < this.a_bricks[i].length) && (!this.a_bricks[i][(j + 1)].exist))))
          {
            ball_touch_brick(paramInt, i, j);
            if (this.a_ball[paramInt].left <= this.WALL_LEFT)
              ball_move_right(paramInt);
            if (this.a_ball[paramInt].bottom >= this.WALL_BOTTOM)
              ball_move_up(paramInt);
          }
          else if ((this.a_ball[paramInt].top == this.a_bricks[i][j].bottom) && (this.a_ball[paramInt].left == this.a_bricks[i][j].right) && (this.a_ball[paramInt].move_unit_horizontal == this.BALL_MOVE_UNIT_HORIZONTAL_NEGATIVE) && (this.a_ball[paramInt].move_unit_vertical == this.BALL_MOVE_UNIT_VERTICAL_NEGATIVE) && ((i + 1 == this.a_bricks.length) || ((i + 1 < this.a_bricks.length) && (!this.a_bricks[(i + 1)][j].exist))) && ((j + 1 == this.a_bricks[i].length) || ((j + 1 < this.a_bricks[i].length) && (!this.a_bricks[i][(j + 1)].exist))))
          {
            ball_touch_brick(paramInt, i, j);
            if (this.a_ball[paramInt].right >= this.WALL_RIGHT)
              ball_move_left(paramInt);
            if (this.a_ball[paramInt].bottom >= this.WALL_BOTTOM)
              ball_move_up(paramInt);
          }
        }
      }
    }
  }

  private void check_touch_ball_and_wall(int paramInt)
  {
    if ((this.a_ball[paramInt].left <= this.BRICKS_AREA_LEFT) && (this.a_ball[paramInt].move_unit_horizontal < 0))
      ball_move_right(paramInt);
    do
      while ((this.a_ball[paramInt].top <= this.BRICKS_AREA_TOP) && (this.a_ball[paramInt].move_unit_vertical < 0))
      {
        ball_move_down(paramInt);
        return;
        if ((this.a_ball[paramInt].right >= this.BRICKS_AREA_RIGHT) && (this.a_ball[paramInt].move_unit_horizontal > 0))
          ball_move_left(paramInt);
      }
    while (this.a_ball[paramInt].top < this.WALL_BOTTOM);
    this.number_of_balls_alive = (-1 + this.number_of_balls_alive);
    this.a_ball[paramInt].exist = false;
    if (this.number_of_balls_alive == 0)
    {
      board_initial();
      ball_initial();
      Numeric localNumeric = this.life;
      int i = -1 + localNumeric.numeric;
      localNumeric.numeric = i;
      keep_life = i;
      Log.d("HtcDotBreaker", "BrickView.check_touch_ball_and_wall\t--life.numeric.\t\tindex_of_ball = " + paramInt);
      if (this.life.numeric == 0)
        game_over = true;
    }
    Log.d("HtcDotBreaker", "life.numeric=" + this.life.numeric + " keep_life=" + keep_life + " game_over=" + game_over);
  }

  private void check_touch_bonusitems_and_board(int paramInt)
  {
    if ((this.a_bonus[paramInt].bottom == this.board.top) && (this.a_bonus[paramInt].paint != a_Paint[9]))
      if ((this.a_bonus[paramInt].left < this.board.right) && (this.a_bonus[paramInt].right > this.board.left))
        touch_bonusitems_and_board(paramInt);
    while ((this.a_bonus[paramInt].top >= this.board.bottom) || (this.a_bonus[paramInt].bottom <= this.board.top) || (this.a_bonus[paramInt].paint == a_Paint[9]) || (this.a_bonus[paramInt].left >= this.board.right) || (this.a_bonus[paramInt].right <= this.board.left))
      return;
    touch_bonusitems_and_board(paramInt);
  }

  private void create_bonus(int paramInt1, int paramInt2, int paramInt3)
  {
    this.number_of_bonus = (1 + this.number_of_bonus);
    Log.d("Alvis", "number_of_bonus = " + this.number_of_bonus + " type = " + paramInt1);
    if ((paramInt1 >= 0) && (paramInt1 <= 3))
    {
      int k = -2 + this.a_bricks.length;
      if (paramInt2 > k)
        paramInt2 = k;
      if (paramInt1 == 0)
        this.a_bonus[paramInt1] = toNewBonusTypeA(a_Paint[11], paramInt2, paramInt3);
    }
    while (true)
    {
      toSetCountDownTimer(paramInt1, this.a_bonus[paramInt1].top / BLOCK_HEIGHT);
      return;
      if (paramInt1 == 1)
      {
        this.a_bonus[paramInt1] = toNewBonusTypeA(a_Paint[1], paramInt2, paramInt3);
      }
      else if (paramInt1 == 2)
      {
        this.a_bonus[paramInt1] = toNewBonusTypeA(a_Paint[8], paramInt2, paramInt3);
      }
      else if (paramInt1 == 3)
      {
        this.a_bonus[paramInt1] = toNewBonusTypeA(a_Paint[2], paramInt2, paramInt3);
        continue;
        if ((paramInt1 >= 4) && (paramInt1 <= 5))
        {
          int j = -3 + this.a_bricks.length;
          if (paramInt2 > j)
            paramInt2 = j;
          if (paramInt1 == 4)
            this.a_bonus[paramInt1] = toNewBonusTypeB(a_Paint[5], paramInt2, paramInt3);
          else if (paramInt1 == 5)
            this.a_bonus[paramInt1] = toNewBonusTypeB(a_Paint[8], paramInt2, paramInt3);
        }
        else if (paramInt1 == 6)
        {
          int i = -5 + this.a_bricks.length;
          if (paramInt2 > i)
            paramInt2 = i;
          this.a_bonus[paramInt1] = toNewBonusTypeHeart(a_Paint[8], paramInt2, paramInt3);
        }
      }
    }
  }

  private void failing_down(BrickItem paramBrickItem)
  {
    paramBrickItem.top += BLOCK_HEIGHT;
    paramBrickItem.bottom += BLOCK_HEIGHT;
  }

  private int find_Bricks_Current_Rectangle_Boundary_Bottom(int paramInt1, int paramInt2)
  {
    for (int i = paramInt1; i >= 0; i--)
      for (int j = 0; j < this.a_bricks.length; j++)
        if (this.a_bricks[j][i].exist == true)
        {
          this.bricks_Current_Rectangle_Boundary_Bottom = i;
          if (this.a_bricks[j][i].bottom >= this.BRICKS_AREA_BOTTOM)
            paramInt2 = 0;
          return paramInt2;
        }
    return 0;
  }

  private int find_Bricks_Current_Rectangle_Boundary_Left(int paramInt1, int paramInt2)
  {
    for (int i = paramInt1; i <= -1 + this.a_bricks.length; i++)
      for (int j = 0; j < this.a_bricks[i].length; j++)
        if (this.a_bricks[i][j].exist == true)
        {
          this.bricks_Current_Rectangle_Boundary_Left = i;
          if (this.a_bricks[i][j].left <= this.BRICKS_AREA_LEFT)
            paramInt2 = 0;
          return paramInt2;
        }
    return 0;
  }

  private int find_Bricks_Current_Rectangle_Boundary_Right(int paramInt1, int paramInt2)
  {
    for (int i = paramInt1; i >= 0; i--)
      for (int j = 0; j < this.a_bricks[i].length; j++)
        if (this.a_bricks[i][j].exist == true)
        {
          this.bricks_Current_Rectangle_Boundary_Right = i;
          if (this.a_bricks[i][j].right >= this.BRICKS_AREA_RIGHT)
            paramInt2 = 0;
          return paramInt2;
        }
    return 0;
  }

  private int find_Bricks_Current_Rectangle_Boundary_Top(int paramInt1, int paramInt2)
  {
    for (int i = paramInt1; i <= -1 + this.a_bricks[0].length; i++)
      for (int j = 0; j < this.a_bricks.length; j++)
        if (this.a_bricks[j][i].exist == true)
        {
          this.bricks_Current_Rectangle_Boundary_Top = i;
          if (this.a_bricks[j][i].top <= this.BRICKS_AREA_TOP)
            paramInt2 = 0;
          return paramInt2;
        }
    return 0;
  }

  private void gameStart()
  {
    startMove();
    this.playTime_timer.start();
  }

  private void init_Paint_color()
  {
    Log.d("HtcDotBreaker", "init_Paint_color()\tbegin.");
    for (int i = 0; i < a_Paint.length; i++)
      a_Paint[i] = new Paint();
    a_Paint[0].setColor(-16777216);
    a_Paint[1].setColor(-16776961);
    a_Paint[2].setColor(-16711681);
    a_Paint[3].setColor(-12303292);
    a_Paint[4].setColor(-7829368);
    a_Paint[5].setColor(-16711936);
    a_Paint[6].setColor(-3355444);
    a_Paint[7].setColor(-65281);
    a_Paint[8].setColor(-65536);
    a_Paint[9].setColor(0);
    a_Paint[10].setColor(-1);
    a_Paint[11].setColor(-256);
    a_Paint[12].setColor(-8388480);
    a_Paint[13].setColor(-65281);
    a_Paint[14].setColor(-11861886);
    a_Paint[15].setColor(-7722014);
    a_Paint[16].setColor(-16776961);
    a_Paint[17].setColor(-16728065);
    a_Paint[18].setColor(-12525360);
    a_Paint[19].setColor(-16711681);
    a_Paint[20].setColor(-16744448);
    a_Paint[21].setColor(-16711809);
    a_Paint[22].setColor(-6632142);
    a_Paint[23].setColor(-989556);
    a_Paint[24].setColor(-7650029);
    a_Paint[25].setColor(-29696);
    a_Paint[26].setColor(-7667712);
    a_Paint[27].setColor(-65536);
    a_Paint[28] = a_Paint[8];
    a_Paint[29].setColor(-23296);
    a_Paint[30] = a_Paint[11];
    a_Paint[31] = a_Paint[5];
    a_Paint[32] = a_Paint[1];
    a_Paint[33].setColor(-11861886);
    a_Paint[34].setColor(-1146130);
    a_Paint[35].setColor(-2013265920);
    a_Paint[36].setColor(this.Word_GameOver_Color);
    a_Paint[37].setColor(this.Word_Restart_Color);
    a_Paint[38].setColor(this.Word_YouWin_Color);
    a_Paint[39].setColor(this.Word_Next_Color);
    a_Paint[40].setColor(this.Mark_Pause_Color);
    a_Paint[41].setColor(this.Mark_Start_Color);
    a_Paint[42].setColor(this.Mark_Exit_Color);
    a_Paint[43].setColor(this.Score_Life_Line_Color);
    Log.d("HtcDotBreaker", "init_Paint_color()\tend.");
  }

  private void init_dot_screen_size()
  {
    Log.d("HtcDotBreaker", "init_dot_screen_size\tbegin.");
    BLOCK_WIDTH = this.dot_pixel_width;
    BLOCK_HEIGHT = this.dot_pixel_height;
    SCREEN_WALL_RIGHT = this.inner_frame_margin_left + this.inner_frame_width + this.inner_frame_margin_right;
    SCREEN_WALL_BOTTOM = this.inner_frame_margin_top + this.inner_frame_height + this.inner_frame_margin_bottom;
    Log.d("HtcDotBreaker", "init_dot_screen_size\tend.");
  }

  private void init_margins()
  {
    Log.d("HtcDotBreaker", "init_margins\tbegin.");
    this.WALL_LEFT = this.inner_frame_margin_left;
    this.WALL_TOP = this.inner_frame_margin_top;
    this.WALL_RIGHT = (this.inner_frame_margin_left + this.inner_frame_width);
    this.WALL_BOTTOM = (this.inner_frame_margin_top + this.inner_frame_height);
    Log.d("HtcDotBreaker", "init_margins\tend.");
  }

  private void init_mark_loc()
  {
    Log.d("HtcDotBreaker", "init_mark_loc\tbegin.");
    MARK_PAUSE_LOC_X = this.WALL_LEFT + this.res.getDimensionPixelSize(2131099654) * BLOCK_WIDTH;
    MARK_PAUSE_LOC_Y = this.WALL_TOP + this.res.getDimensionPixelSize(2131099655) * BLOCK_HEIGHT;
    MARK_START_LOC_X = this.WALL_LEFT + this.res.getDimensionPixelSize(2131099656) * BLOCK_WIDTH;
    MARK_START_LOC_Y = this.WALL_TOP + this.res.getDimensionPixelSize(2131099657) * BLOCK_HEIGHT;
    MARK_EXIT_LOC_X = this.WALL_LEFT + this.res.getDimensionPixelSize(2131099658) * BLOCK_WIDTH;
    MARK_EXIT_LOC_Y = this.WALL_TOP + this.res.getDimensionPixelSize(2131099659) * BLOCK_HEIGHT;
    HINT_ANIMATION_LOC_X = this.WALL_LEFT + this.res.getDimensionPixelSize(2131099660) * BLOCK_WIDTH;
    HINT_ANIMATION_LOC_Y = this.WALL_TOP + this.res.getDimensionPixelSize(2131099661) * BLOCK_HEIGHT;
    Log.d("HtcDotBreaker", "init_mark_loc\tend.");
  }

  private boolean isResumeGame()
  {
    int i = GameActivity.preferencesGet("Current_GameLevel", -1);
    int j = brick_game_level;
    boolean bool1 = false;
    if (i == j)
    {
      boolean bool2 = GameActivity.preferencesGet("Current_GameState", false);
      bool1 = false;
      if (bool2)
        bool1 = true;
    }
    return bool1;
  }

  protected static void onDrawMatrix(Canvas paramCanvas, boolean[][] paramArrayOfBoolean, int paramInt1, int paramInt2, int paramInt3)
  {
    for (int i = 0; i < paramArrayOfBoolean.length; i++)
      for (int j = 0; j < paramArrayOfBoolean[i].length; j++)
        if (paramArrayOfBoolean[i][j] != 0)
          paramCanvas.drawRect(paramInt1 + i * BLOCK_WIDTH, paramInt2 + j * BLOCK_HEIGHT, paramInt1 + (i + 1) * BLOCK_WIDTH, paramInt2 + (j + 1) * BLOCK_HEIGHT, a_Paint[paramInt3]);
  }

  public static Bitmap resizeBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    int i;
    int j;
    if (paramBitmap != null)
    {
      i = paramBitmap.getWidth();
      j = paramBitmap.getHeight();
      Log.d("HtcDotBreaker", "[BrickView] resizeBitmap, bitmap width = " + i);
      Log.d("HtcDotBreaker", "[BrickView] resizeBitmap, bitmap height = " + j);
      if ((i != paramInt2) || (j != paramInt1));
    }
    else
    {
      return paramBitmap;
    }
    if ((i > paramInt2) && (j > paramInt1))
    {
      Log.d("HtcDotBreaker", "[BrickView] resizeBitmap, start");
      WallpaperMaker localWallpaperMaker = new WallpaperMaker();
      if (localWallpaperMaker != null)
        paramBitmap = localWallpaperMaker.convertDotViewWallpaper(paramBitmap, paramInt2, paramInt1);
      Log.d("HtcDotBreaker", "[BrickView] resizeBitmap, end");
      return paramBitmap;
    }
    Log.d("HtcDotBreaker", "[BrickView] resizeBitmap, input bitmap is invalid, size is not enough");
    return null;
  }

  private void startMove()
  {
    board_Timer = new Timer();
    boolean bool = isResumeGame();
    if (bool);
    while (true)
    {
      ball_Timer = new Timer();
      if (!bool)
        break;
      return;
      Timer localTimer1 = board_Timer;
      TimerTask localTimerTask1 = board_TimerTask_renew(board_TimerTask);
      board_TimerTask = localTimerTask1;
      localTimer1.schedule(localTimerTask1, 0L, board_speed);
    }
    Timer localTimer2 = ball_Timer;
    TimerTask localTimerTask2 = ball_TimerTask_renew(ball_TimerTask);
    ball_TimerTask = localTimerTask2;
    localTimer2.schedule(localTimerTask2, 0L, ball_speed);
  }

  protected static boolean[][] toConvertMatrix(boolean[][] paramArrayOfBoolean)
  {
    int[] arrayOfInt = { paramArrayOfBoolean[0].length, paramArrayOfBoolean.length };
    boolean[][] arrayOfBoolean = (boolean[][])Array.newInstance(Boolean.TYPE, arrayOfInt);
    for (int i = 0; i < paramArrayOfBoolean.length; i++)
      for (int j = 0; j < paramArrayOfBoolean[i].length; j++)
        arrayOfBoolean[j][i] = paramArrayOfBoolean[i][j];
    return arrayOfBoolean;
  }

  private BrickItem toNewBonusTypeA(Paint paramPaint, int paramInt1, int paramInt2)
  {
    return new BrickItem(T, paramPaint, this.a_bricks[paramInt1][paramInt2].left, this.a_bricks[paramInt1][paramInt2].top + BLOCK_HEIGHT, this.a_bricks[paramInt1][paramInt2].left + 2 * BLOCK_WIDTH, this.a_bricks[paramInt1][paramInt2].top + 3 * BLOCK_HEIGHT);
  }

  private BrickItem toNewBonusTypeB(Paint paramPaint, int paramInt1, int paramInt2)
  {
    return new BrickItem(T, paramPaint, this.a_bricks[paramInt1][paramInt2].left, this.a_bricks[paramInt1][paramInt2].top + BLOCK_HEIGHT, this.a_bricks[paramInt1][paramInt2].left + 3 * BLOCK_WIDTH, this.a_bricks[paramInt1][paramInt2].top + 2 * BLOCK_HEIGHT);
  }

  private BrickItem toNewBonusTypeHeart(Paint paramPaint, int paramInt1, int paramInt2)
  {
    return new BrickItem(T, paramPaint, this.a_bricks[paramInt1][paramInt2].left, this.a_bricks[paramInt1][paramInt2].top + BLOCK_HEIGHT, this.a_bricks[paramInt1][paramInt2].left + 5 * BLOCK_WIDTH, this.a_bricks[paramInt1][paramInt2].top + 5 * BLOCK_HEIGHT);
  }

  private void touch_bonusitems_and_board(int paramInt)
  {
    this.a_bonus[paramInt].paint = a_Paint[9];
    if (paramInt == 0)
    {
      if (!this.bonus_22YELLOW)
      {
        this.bonus_22YELLOW = T;
        for (int m = 0; m < 4; m++)
          if ((this.a_ball[m] != null) && (this.a_ball[m].exist != F))
          {
            Ball localBall1 = this.a_ball[m];
            localBall1.width += BLOCK_WIDTH;
            Ball localBall2 = this.a_ball[m];
            localBall2.height += BLOCK_HEIGHT;
            Ball localBall3 = this.a_ball[m];
            localBall3.right += BLOCK_WIDTH;
            Ball localBall4 = this.a_ball[m];
            localBall4.bottom += BLOCK_HEIGHT;
          }
      }
    }
    else if (paramInt == 1)
    {
      this.bonus_22BLUE_change_color = F;
      this.number_of_balls_alive = 4;
      int i = 0;
      for (int j = 0; j < 4; j++)
        if ((this.a_ball[j] != null) && (this.a_ball[j].exist != F))
          i = j;
      int k = 0;
      if (k < 4)
      {
        if ((this.a_ball[k] == null) || (this.a_ball[k].exist == F))
        {
          this.a_ball[k] = new Ball(T, this.a_ball[i].paint, this.a_ball[i].left, this.a_ball[i].top, this.a_ball[i].right, this.a_ball[i].bottom, this.a_ball[i].width, this.a_ball[i].height, this.a_ball[i].move_unit_horizontal, this.a_ball[i].move_unit_vertical);
          if (k != 1)
            break label406;
          this.a_ball[k].move_unit_horizontal = (-this.a_ball[k].move_unit_horizontal);
        }
        while (true)
        {
          k++;
          break;
          label406: if (k == 2)
          {
            this.a_ball[k].move_unit_vertical = (-this.a_ball[k].move_unit_vertical);
          }
          else if (k == 3)
          {
            this.a_ball[k].move_unit_horizontal = (-this.a_ball[k].move_unit_horizontal);
            this.a_ball[k].move_unit_vertical = (-this.a_ball[k].move_unit_vertical);
          }
        }
      }
    }
    else
    {
      if (paramInt != 2)
        break label518;
      this.bonus_22RED = T;
      new CountDownTimer(10000L, 10000L)
      {
        public void onFinish()
        {
          BrickView.access$602(BrickView.this, BrickView.F);
        }

        public void onTick(long paramAnonymousLong)
        {
        }
      }
      .start();
    }
    label518: 
    do
    {
      do
      {
        do
        {
          return;
          if (paramInt == 3)
          {
            ball_TimerTask.cancel();
            Timer localTimer = ball_Timer;
            TimerTask localTimerTask = ball_TimerTask_renew(ball_TimerTask);
            ball_TimerTask = localTimerTask;
            localTimer.schedule(localTimerTask, 0L, (int)(ball_speed / 1.5D));
            new CountDownTimer(10000L, 10000L)
            {
              public void onFinish()
              {
                BrickView.ball_TimerTask.cancel();
                if (!BrickView.this.game_pause)
                {
                  Timer localTimer = BrickView.ball_Timer;
                  TimerTask localTimerTask = BrickView.this.ball_TimerTask_renew(BrickView.ball_TimerTask);
                  BrickView.ball_TimerTask = localTimerTask;
                  localTimer.schedule(localTimerTask, 0L, BrickView.ball_speed);
                }
              }

              public void onTick(long paramAnonymousLong)
              {
              }
            }
            .start();
            return;
          }
          if (paramInt != 4)
            break;
        }
        while (this.board.width >= 10 * BLOCK_WIDTH);
        Board localBoard2 = this.board;
        localBoard2.width += 2 * BLOCK_WIDTH;
        return;
        if (paramInt != 5)
          break;
      }
      while (this.board.width <= 4 * BLOCK_WIDTH);
      Board localBoard1 = this.board;
      localBoard1.width -= 2 * BLOCK_WIDTH;
      return;
    }
    while (paramInt != 6);
    add_life();
  }

  TimerTask ball_TimerTask_renew(TimerTask paramTimerTask)
  {
    return new TimerTask()
    {
      public void run()
      {
        Message localMessage = new Message();
        localMessage.what = 1;
        BrickView.this.ball_TimerTask_Handler.sendMessage(localMessage);
      }
    };
  }

  protected void ball_move_and_touchcheck()
  {
    for (int i = 0; i < 4; i++)
      if ((this.a_ball[i] != null) && (this.a_ball[i].exist))
      {
        ball_move(i);
        check_touch_ball_and_board(i);
        check_touch_ball_and_bricks(i);
      }
    if (check_level_complete())
    {
      bricks_nextlevel();
      cancelTimer();
      game_level_complete = true;
    }
  }

  TimerTask board_TimerTask_renew(TimerTask paramTimerTask)
  {
    return new TimerTask()
    {
      public void run()
      {
        Message localMessage = new Message();
        localMessage.what = 1;
        BrickView.this.board_TimerTask_Handler.sendMessage(localMessage);
      }
    };
  }

  protected void board_move(float paramFloat)
  {
    if (paramFloat > board_moveAngle)
    {
      Board localBoard2 = this.board;
      localBoard2.left -= 1 * this.board.move_unit_h;
      if (this.board.left > this.WALL_LEFT)
        break label133;
      this.board.left = this.WALL_LEFT;
    }
    while (true)
    {
      for (int i = 0; i < 4; i++)
        if ((this.a_ball[i] != null) && (this.a_ball[i].exist))
          check_touch_ball_and_board(i);
      if (paramFloat >= -board_moveAngle)
        break;
      Board localBoard1 = this.board;
      localBoard1.left += 1 * this.board.move_unit_h;
      break;
      label133: if (this.board.left >= this.WALL_RIGHT - this.board.width)
        this.board.left = (this.WALL_RIGHT - this.board.width);
    }
    if (!this.ball_alive)
      ball_reset_location();
    this.board.right = (this.board.left + this.board.width);
  }

  protected void bricks_move()
  {
    Random localRandom = new Random();
    int i = -1 + localRandom.nextInt(3);
    int j = -1 + localRandom.nextInt(3);
    if (check_bricks_move_with_ball(i, j))
      return;
    if (i == -1)
    {
      i = find_Bricks_Current_Rectangle_Boundary_Left(this.bricks_Current_Rectangle_Boundary_Left, i);
      if (this.bricks_Current_Rectangle_Boundary_Left + this.bricks_shiftoffset_horizontal > 0)
        this.bricks_shiftoffset_horizontal = (-1 + this.bricks_shiftoffset_horizontal);
      label71: if (j != -1)
        break label205;
      j = find_Bricks_Current_Rectangle_Boundary_Top(this.bricks_Current_Rectangle_Boundary_Top, j);
      if (this.bricks_Current_Rectangle_Boundary_Top + this.bricks_shiftoffset_vertical > 0)
        this.bricks_shiftoffset_vertical = (-1 + this.bricks_shiftoffset_vertical);
    }
    while (true)
    {
      bricks_repositioning(i, j);
      for (int k = 0; k < 4; k++)
        if ((this.a_ball[k] != null) && (this.a_ball[k].exist))
          check_touch_ball_and_bricks(k);
      break;
      if (i != 1)
        break label71;
      i = find_Bricks_Current_Rectangle_Boundary_Right(this.bricks_Current_Rectangle_Boundary_Right, i);
      if (this.bricks_Current_Rectangle_Boundary_Right + this.bricks_shiftoffset_horizontal >= -1 + this.a_bricks.length)
        break label71;
      this.bricks_shiftoffset_horizontal = (1 + this.bricks_shiftoffset_horizontal);
      break label71;
      label205: if (j == 1)
      {
        j = find_Bricks_Current_Rectangle_Boundary_Bottom(this.bricks_Current_Rectangle_Boundary_Bottom, j);
        if (this.bricks_Current_Rectangle_Boundary_Bottom + this.bricks_shiftoffset_vertical < -1 + this.a_bricks[0].length)
          this.bricks_shiftoffset_vertical = (1 + this.bricks_shiftoffset_vertical);
      }
    }
  }

  public void onBackPressed()
  {
    Log.d("Alvis", "GameActivity.onBackPressed()\tbegin.");
    this.start_animation_timer.cancel();
    this.hint_animation_timer.cancel();
    this.playTime_timer.cancel();
    hint_animation = false;
    toGamePause();
    GameActivity.preferencesEditor("BricksHit", GameActivity.preferencesGet("BricksHit", 0) + this.mBricksHit);
    GameActivity.preferencesEditor("BallTravelled", GameActivity.preferencesGet("BallTravelled", 0) + this.ball_travelled);
    GameActivity.preferencesEditor("PlayTime", GameActivity.preferencesGet("PlayTime", 0) + this.tmp_playTime);
    saveCurrentGameState();
    keep_score = -1;
    keep_life = 0;
    Log.d("Alvis", "GameActivity.onBackPressed()\tend.");
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    onDrawScore(paramCanvas, this.score.loc_x, this.score.loc_y, this.score.numeric);
    onDrawLife(paramCanvas, this.life.loc_x, this.life.loc_y, this.life.numeric);
    paramCanvas.drawRect(this.WALL_LEFT, this.BRICKS_AREA_TOP - BLOCK_HEIGHT, this.WALL_RIGHT, this.BRICKS_AREA_TOP, a_Paint[43]);
    paramCanvas.drawRect(this.board.left, this.board.top, this.board.right, this.board.bottom, this.board.paint);
    for (int i = 0; i < 4; i++)
      if ((this.a_ball[i] != null) && (this.a_ball[i].exist != F))
        paramCanvas.drawRect(this.a_ball[i].left, this.a_ball[i].top, this.a_ball[i].right, this.a_ball[i].bottom, this.a_ball[i].paint);
    if ((this.number_of_bonus > 0) && (this.number_of_bonus <= this.MAX_NUMBER_OF_BONUS))
    {
      int m = 0;
      if (m < -1 + this.a_bonus.length)
      {
        if (this.a_bonus[m] == null);
        while (true)
        {
          m++;
          break;
          paramCanvas.drawRect(this.a_bonus[m].left, this.a_bonus[m].top, this.a_bonus[m].right, this.a_bonus[m].bottom, this.a_bonus[m].paint);
        }
      }
      if ((this.a_bonus[6] != null) && (this.a_bonus[6].paint == a_Paint[8]))
        for (int n = 0; n < this.FAILING_DOWN_HEART.length; n++)
          for (int i1 = 0; i1 < this.FAILING_DOWN_HEART[0].length; i1++)
            if (this.FAILING_DOWN_HEART[n][i1] == T)
              paramCanvas.drawRect(this.a_bonus[6].left + n * BLOCK_WIDTH, this.a_bonus[6].top + i1 * BLOCK_HEIGHT, this.a_bonus[6].left + (n + 1) * BLOCK_WIDTH, this.a_bonus[6].top + (i1 + 1) * BLOCK_HEIGHT, a_Paint[8]);
    }
    for (int j = 0; j < this.a_bricks.length; j++)
      for (int k = 0; k < this.a_bricks[j].length; k++)
        if (this.a_bricks[j][k].exist == true)
          paramCanvas.drawRect(this.a_bricks[j][k].left, this.a_bricks[j][k].top, this.a_bricks[j][k].right, this.a_bricks[j][k].bottom, this.a_bricks[j][k].paint);
    if (this.start_animation)
      onDrawMatrix(paramCanvas, toConvertMatrix(START_ANIMATION[start_animation_number]), this.START_ANIMATION_LOC_X, this.START_ANIMATION_LOC_Y, 11);
    while (!this.game_pause)
      return;
    paramCanvas.drawRect(0.0F, 0.0F, SCREEN_WALL_RIGHT, SCREEN_WALL_BOTTOM, a_Paint[35]);
    if (hint_animation)
    {
      if (hint_animation_number < 5)
      {
        onDrawMatrix(paramCanvas, MARK_START, MARK_START_LOC_X, MARK_START_LOC_Y, 41);
        onDrawMatrix(paramCanvas, toConvertMatrix(HINT_ANIMATION[hint_animation_number]), HINT_ANIMATION_LOC_X, HINT_ANIMATION_LOC_Y, 41);
        return;
      }
      onDrawMatrix(paramCanvas, MARK_EXIT, MARK_EXIT_LOC_X, MARK_EXIT_LOC_Y, 42);
      onDrawMatrix(paramCanvas, toConvertMatrix(HINT_ANIMATION[hint_animation_number]), HINT_ANIMATION_LOC_X, HINT_ANIMATION_LOC_Y, 42);
      return;
    }
    onDrawMatrix(paramCanvas, MARK_PAUSE, MARK_PAUSE_LOC_X, MARK_PAUSE_LOC_Y, 40);
  }

  protected void onDrawLife(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt3 < 0)
    {
      Log.d("HtcDotBreaker", "BrickView.onDrawLife\tmlife < 0.\tmlife=" + paramInt3);
      this.life.numeric = 1;
    }
    for (int i = 0; i < this.LIFE_BITS; i++)
    {
      int j = paramInt3 % 10;
      boolean[][] arrayOfBoolean = toConvertMatrix(this.a_numeric[j]);
      Paint localPaint = a_Paint[8];
      onDrawNumeric(paramCanvas, paramInt1, paramInt2, arrayOfBoolean, localPaint);
      paramInt3 /= 10;
      paramInt1 -= 4 * BLOCK_WIDTH;
    }
  }

  protected void onDrawNumeric(Canvas paramCanvas, int paramInt1, int paramInt2, boolean[][] paramArrayOfBoolean, Paint paramPaint)
  {
    for (int i = 0; i < paramArrayOfBoolean.length; i++)
      for (int j = 0; j < paramArrayOfBoolean[i].length; j++)
        if (paramArrayOfBoolean[i][j] != 0)
          paramCanvas.drawRect(paramInt1 + i * BLOCK_WIDTH, paramInt2 + j * BLOCK_HEIGHT, paramInt1 + (i + 1) * BLOCK_WIDTH, paramInt2 + (j + 1) * BLOCK_HEIGHT, paramPaint);
  }

  protected void onDrawScore(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt3 < 0)
      Log.d("HtcDotBreaker", "BrickView.onDrawScore\tscore < 0.\tscore=" + paramInt3);
    while (true)
    {
      return;
      if (paramInt3 > 1000)
      {
        boolean[][] arrayOfBoolean2 = toConvertMatrix(this.NUMERIC_MORE_K[0]);
        Paint localPaint2 = a_Paint[40];
        onDrawNumeric(paramCanvas, paramInt1, paramInt2, arrayOfBoolean2, localPaint2);
        int k = paramInt1 - 4 * BLOCK_WIDTH;
        onDrawNumeric(paramCanvas, k, paramInt2, toConvertMatrix(this.NUMERIC_MORE_K[1]), a_Paint[40]);
        int m = k - 4 * BLOCK_WIDTH;
        if (paramInt3 < 9000)
        {
          int n = paramInt3 / 1000;
          onDrawNumeric(paramCanvas, m, paramInt2, toConvertMatrix(this.a_numeric[n]), a_Paint[40]);
          return;
        }
        onDrawNumeric(paramCanvas, m, paramInt2, toConvertMatrix(this.a_numeric[9]), a_Paint[40]);
        return;
      }
      for (int i = 0; i < this.SCORE_BITS; i++)
      {
        int j = paramInt3 % 10;
        boolean[][] arrayOfBoolean1 = toConvertMatrix(this.a_numeric[j]);
        Paint localPaint1 = a_Paint[40];
        onDrawNumeric(paramCanvas, paramInt1, paramInt2, arrayOfBoolean1, localPaint1);
        paramInt3 /= 10;
        paramInt1 -= 4 * BLOCK_WIDTH;
      }
    }
  }

  protected void saveCurrentGameState()
  {
    GameActivity.preferencesEditor("Current_GameLevel", brick_game_level);
    GameActivity.preferencesEditor("Current_GameState", true);
    GameActivity.preferencesEditor("Current_Ball_Alive", this.ball_alive);
    GameActivity.preferencesEditor("Current_Score", this.score.numeric);
    GameActivity.preferencesEditor("Current_Life", this.life.numeric);
    GameActivity.preferencesEditor("Current_Bricks_Location_Left", this.a_bricks[0][0].left);
    GameActivity.preferencesEditor("Current_Bricks_Location_Top", this.a_bricks[0][0].top);
    GameActivity.preferencesEditor("Current_Bricks_Width", BLOCK_WIDTH);
    GameActivity.preferencesEditor("Current_Bricks_Height", BLOCK_HEIGHT);
    for (int i = 0; i < this.a_bricks.length; i++)
      for (int m = 0; m < this.a_bricks[0].length; m++)
      {
        GameActivity.preferencesEditor("Current_Bricks[" + i + "][" + m + "]_Exist", this.a_bricks[i][m].exist);
        GameActivity.preferencesEditor("Current_Bricks[" + i + "][" + m + "]_PaintColor", this.a_bricks[i][m].paint.getColor());
      }
    GameActivity.preferencesEditor("Current_Ball_Width", this.a_ball[0].width);
    GameActivity.preferencesEditor("Current_Ball_Height", this.a_ball[0].height);
    int j = 0;
    if (j < 4)
    {
      if (this.a_ball[j] != null)
      {
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Exist", this.a_ball[j].exist);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Left", this.a_ball[j].left);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Top", this.a_ball[j].top);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Right", this.a_ball[j].right);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Bottom", this.a_ball[j].bottom);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_MoveUnitHorizontal", this.a_ball[j].move_unit_horizontal);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_MoveUnitVertical", this.a_ball[j].move_unit_vertical);
      }
      while (true)
      {
        j++;
        break;
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Exist", false);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Left", this.a_ball[0].left);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Top", this.a_ball[0].top);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Right", this.a_ball[0].right);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_Bottom", this.a_ball[0].bottom);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_MoveUnitHorizontal", this.a_ball[0].move_unit_horizontal);
        GameActivity.preferencesEditor("Current_Ball[" + j + "]_MoveUnitVertical", this.a_ball[0].move_unit_vertical);
      }
    }
    GameActivity.preferencesEditor("Current_Board_Exist", this.board.exist);
    GameActivity.preferencesEditor("Current_Board_PaintColor", this.board.paint.getColor());
    GameActivity.preferencesEditor("Current_Board_Left", this.board.left);
    GameActivity.preferencesEditor("Current_Board_Top", this.board.top);
    GameActivity.preferencesEditor("Current_Board_Right", this.board.right);
    GameActivity.preferencesEditor("Current_Board_Bottom", this.board.bottom);
    GameActivity.preferencesEditor("Current_Board_Width", this.board.width);
    GameActivity.preferencesEditor("Current_Board_MoveUnitHorizontal", this.board.move_unit_h);
    if (GameActivity.preferencesGet("GameStatus[" + brick_game_level + "]", 1) != 3)
    {
      int k = 1;
      while (k <= 20)
        if (GameActivity.preferencesGet("GameStatus[" + (k + 1) + "]", 1) != 4)
        {
          GameActivity.preferencesEditor("GameStatus[" + k + "]", 2);
          k++;
        }
        else
        {
          GameActivity.preferencesEditor("GameStatus[" + k + "]", 1);
        }
    }
    GameActivity.preferencesEditor("GameStatus[" + brick_game_level + "]", 3);
    Log.d("Alvis", "GameActivity.preferencesEditor(GameStatus[brick_game_level], GameLevel.ongoing);\tbrick_game_level=" + brick_game_level);
    GameActivity.preferencesEditorCommit();
  }

  protected void toExitBrick()
  {
    Log.d("Alvis", "toExitBrick()\tbegin.");
    this.start_animation_timer.cancel();
    this.hint_animation_timer.cancel();
    this.playTime_timer.cancel();
    hint_animation = false;
    toGamePause();
    GameActivity.preferencesEditor("BricksHit", GameActivity.preferencesGet("BricksHit", 0) + this.mBricksHit);
    GameActivity.preferencesEditor("BallTravelled", GameActivity.preferencesGet("BallTravelled", 0) + this.ball_travelled);
    GameActivity.preferencesEditor("PlayTime", GameActivity.preferencesGet("PlayTime", 0) + this.tmp_playTime);
    saveCurrentGameState();
    ActivityList.getInstance().exit();
    Log.d("Alvis", "toExitBrick()\tend.");
  }

  protected void toGamePause()
  {
    this.game_pause = true;
    if (ball_TimerTask != null)
      ball_TimerTask.cancel();
    if (board_TimerTask != null)
      board_TimerTask.cancel();
    for (int i = 0; i < this.a_bonus.length; i++)
      if (this.a_bonus[i] != null)
        this.a_bonus_timer[i].cancel();
    invalidate();
  }

  protected void toGameResume()
  {
    this.game_pause = false;
    this.hint_animation_timer.cancel();
    hint_animation = false;
    Timer localTimer1 = ball_Timer;
    TimerTask localTimerTask1 = ball_TimerTask_renew(ball_TimerTask);
    ball_TimerTask = localTimerTask1;
    localTimer1.schedule(localTimerTask1, 0L, ball_speed);
    Timer localTimer2 = board_Timer;
    TimerTask localTimerTask2 = board_TimerTask_renew(board_TimerTask);
    board_TimerTask = localTimerTask2;
    localTimer2.schedule(localTimerTask2, 0L, board_speed);
    for (int i = 0; i < this.a_bonus.length; i++)
      if (this.a_bonus[i] != null)
        toSetCountDownTimer(i, this.a_bonus[i].top / BLOCK_HEIGHT);
  }

  protected void toSetCountDownTimer(int paramInt1, int paramInt2)
  {
    this.a_bonus_timer[paramInt1] = new CountDownTimer(100 * (this.BLOCK_NUMBER_COLUMN - paramInt2), 100L)
    {
      public void onFinish()
      {
        BrickView.this.a_bonus[this.val$type] = null;
        BrickView.access$206(BrickView.this);
        BrickView.this.a_bonus_timer[this.val$type] = null;
      }

      public void onTick(long paramAnonymousLong)
      {
        BrickView.this.check_touch_bonusitems_and_board(this.val$type);
        BrickView.this.failing_down(BrickView.this.a_bonus[this.val$type]);
      }
    }
    .start();
    if (paramInt1 == 1)
    {
      this.bonus_22BLUE_change_color = true;
      this.bonus_item_timer_change_color = new CountDownTimer(100 * (1 + (this.BLOCK_NUMBER_COLUMN - paramInt2)), 100L)
      {
        public void onFinish()
        {
          if ((BrickView.this.a_bonus[1] != null) && (BrickView.this.a_bonus[1].bottom < BrickView.this.WALL_BOTTOM))
            start();
        }

        public void onTick(long paramAnonymousLong)
        {
          if (BrickView.this.a_bonus[1] == null)
          {
            BrickView.this.bonus_item_timer_change_color.cancel();
            BrickView.access$402(BrickView.this, false);
          }
          BrickView.this.bonus_22BLUE_change_color(BrickView.this.a_bonus[1]);
          BrickView.this.invalidate();
        }
      }
      .start();
    }
  }

  protected void toShowStartAnimation()
  {
    this.start_animation_timer.cancel();
    this.start_animation = true;
    start_animation_number = -1 + START_ANIMATION.length;
    this.start_animation_timer.start();
  }

  protected void toStartGame()
  {
    this.start_animation_timer.cancel();
    this.start_animation = false;
    this.ball_alive = true;
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.dotbreaker.BrickView
 * JD-Core Version:    0.6.2
 */