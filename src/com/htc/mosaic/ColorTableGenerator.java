package com.htc.mosaic;

import java.lang.reflect.Array;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ColorTableGenerator
{
  static char[][][] RGB332MappingTable = arrayOfChar;
  static int[] keycolor = { -65536, -42752, -30976, -19200, -11776, -2425088, -7209216, -16713985, -16729601, -16747009, -16763393, -14941953, -9371393, -4128513, -65301, -1, -16777216 };
  private static int thresholdBlack = 40;

  static
  {
    char[][][] arrayOfChar = new char[8][][];
    arrayOfChar[0] = { { 15, 11, 11, 11 }, { 15, 10, 10, 10 }, { 6, 10, 10, 10 }, { 6, 9, 9, 9 }, { 6, 9, 9, 9 }, { 6, 8, 8, 8 }, { 6, 6, 7, 7 }, { 6, 6, 7, 7 } };
    arrayOfChar[1] = { { 15, 11, 11, 11 }, { 15, 11, 11, 11 }, { 15, 10, 10, 10 }, { 6, 9, 9, 9 }, { 6, 6, 9, 9 }, { 6, 6, 8, 8 }, { 6, 6, 7, 7 }, { 6, 6, 7, 7 } };
    char[][] arrayOfChar1 = new char[8][];
    char[] arrayOfChar2 = new char[4];
    arrayOfChar2[1] = '\f';
    arrayOfChar2[2] = '\f';
    arrayOfChar2[3] = '\f';
    arrayOfChar1[0] = arrayOfChar2;
    char[] arrayOfChar3 = new char[4];
    arrayOfChar3[1] = '\017';
    arrayOfChar3[2] = '\f';
    arrayOfChar3[3] = '\f';
    arrayOfChar1[1] = arrayOfChar3;
    arrayOfChar1[2] = { 1, 15, 10, 10 };
    arrayOfChar1[3] = { 6, 15, 9, 9 };
    arrayOfChar1[4] = { 6, 6, 9, 9 };
    arrayOfChar1[5] = { 6, 6, 8, 8 };
    arrayOfChar1[6] = { 6, 6, 7, 7 };
    arrayOfChar1[7] = { 6, 6, 7, 7 };
    arrayOfChar[2] = arrayOfChar1;
    char[][] arrayOfChar4 = new char[8][];
    char[] arrayOfChar5 = new char[4];
    arrayOfChar5[2] = '\f';
    arrayOfChar5[3] = '\f';
    arrayOfChar4[0] = arrayOfChar5;
    char[] arrayOfChar6 = new char[4];
    arrayOfChar6[2] = '\f';
    arrayOfChar6[3] = '\f';
    arrayOfChar4[1] = arrayOfChar6;
    arrayOfChar4[2] = { 1, 15, 12, 12 };
    arrayOfChar4[3] = { 1, 1, 12, 12 };
    arrayOfChar4[4] = { 6, 6, 9, 9 };
    arrayOfChar4[5] = { 6, 6, 8, 8 };
    arrayOfChar4[6] = { 6, 6, 7, 7 };
    arrayOfChar4[7] = { 6, 6, 7, 7 };
    arrayOfChar[3] = arrayOfChar4;
    char[][] arrayOfChar7 = new char[8][];
    char[] arrayOfChar8 = new char[4];
    arrayOfChar8[2] = '\f';
    arrayOfChar8[3] = '\f';
    arrayOfChar7[0] = arrayOfChar8;
    char[] arrayOfChar9 = new char[4];
    arrayOfChar9[2] = '\f';
    arrayOfChar9[3] = '\f';
    arrayOfChar7[1] = arrayOfChar9;
    arrayOfChar7[2] = { 1, 1, 12, 12 };
    arrayOfChar7[3] = { 1, 1, 12, 12 };
    arrayOfChar7[4] = { 6, 6, 9, 9 };
    arrayOfChar7[5] = { 6, 6, 15, 15 };
    arrayOfChar7[6] = { 6, 6, 15, 15 };
    arrayOfChar7[7] = { 6, 6, 15, 15 };
    arrayOfChar[4] = arrayOfChar7;
    char[][] arrayOfChar10 = new char[8][];
    char[] arrayOfChar11 = new char[4];
    arrayOfChar11[2] = '\r';
    arrayOfChar11[3] = '\r';
    arrayOfChar10[0] = arrayOfChar11;
    char[] arrayOfChar12 = new char[4];
    arrayOfChar12[2] = '\r';
    arrayOfChar12[3] = '\r';
    arrayOfChar10[1] = arrayOfChar12;
    arrayOfChar10[2] = { 1, 1, 13, 13 };
    arrayOfChar10[3] = { 1, 1, 13, 13 };
    arrayOfChar10[4] = { 2, 2, 15, 15 };
    arrayOfChar10[5] = { 3, 3, 15, 15 };
    arrayOfChar10[6] = { 5, 5, 15, 15 };
    arrayOfChar10[7] = { 5, 5, 15, 15 };
    arrayOfChar[5] = arrayOfChar10;
    char[][] arrayOfChar13 = new char[8][];
    char[] arrayOfChar14 = new char[4];
    arrayOfChar14[2] = '\016';
    arrayOfChar14[3] = '\r';
    arrayOfChar13[0] = arrayOfChar14;
    char[] arrayOfChar15 = new char[4];
    arrayOfChar15[2] = '\016';
    arrayOfChar15[3] = '\r';
    arrayOfChar13[1] = arrayOfChar15;
    arrayOfChar13[2] = { 1, 1, 14, 13 };
    arrayOfChar13[3] = { 1, 1, 14, 13 };
    arrayOfChar13[4] = { 2, 2, 15, 15 };
    arrayOfChar13[5] = { 3, 3, 15, 15 };
    arrayOfChar13[6] = { 5, 5, 15, 15 };
    arrayOfChar13[7] = { 5, 5, 15, 15 };
    arrayOfChar[6] = arrayOfChar13;
    char[][] arrayOfChar16 = new char[8][];
    char[] arrayOfChar17 = new char[4];
    arrayOfChar17[2] = '\016';
    arrayOfChar17[3] = '\016';
    arrayOfChar16[0] = arrayOfChar17;
    char[] arrayOfChar18 = new char[4];
    arrayOfChar18[2] = '\016';
    arrayOfChar18[3] = '\016';
    arrayOfChar16[1] = arrayOfChar18;
    arrayOfChar16[2] = { 1, 1, 14, 14 };
    arrayOfChar16[3] = { 1, 1, 14, 14 };
    arrayOfChar16[4] = { 2, 2, 15, 15 };
    arrayOfChar16[5] = { 3, 3, 15, 15 };
    arrayOfChar16[6] = { 4, 4, 15, 15 };
    arrayOfChar16[7] = { 5, 5, 15, 15 };
    arrayOfChar[7] = arrayOfChar16;
  }

  static int RGB332toKeycolor(int paramInt1, int paramInt2, int paramInt3)
  {
    if (RGB332MappingTable == null)
      genFromRGB332();
    return keycolor[RGB332MappingTable[paramInt1][paramInt2][paramInt3]];
  }

  static int RGB332toKeycolorIndex(int paramInt1, int paramInt2, int paramInt3)
  {
    if (RGB332MappingTable == null)
      genFromRGB332();
    return RGB332MappingTable[paramInt1][paramInt2][paramInt3];
  }

  private static char _RGB332toKeyColorIdx(int paramInt, boolean paramBoolean)
  {
    HashMap localHashMap1 = new HashMap(keycolor.length);
    int i = -1 + keycolor.length;
    if (paramBoolean)
      i = keycolor.length;
    int j = 0;
    HashMap localHashMap2;
    float f2;
    HashMap localHashMap3;
    Iterator localIterator;
    if (j >= i)
    {
      localHashMap2 = sortByValue(localHashMap1, true);
      f2 = -1.0F;
      localHashMap3 = new HashMap(keycolor.length);
      localIterator = localHashMap2.entrySet().iterator();
    }
    while (true)
    {
      label73: if (!localIterator.hasNext());
      Map.Entry localEntry;
      do
      {
        LinkedList localLinkedList = new LinkedList(localHashMap3.keySet());
        Collections.sort(localLinkedList);
        int i7 = ((Integer)localLinkedList.get(0)).intValue();
        if (i7 == 16)
          i7 = 15;
        localHashMap2.clear();
        localHashMap3.clear();
        localLinkedList.clear();
        return (char)i7;
        int k = (0xFF0000 & paramInt) >> 16;
        int m = (0xFF00 & paramInt) >> 8;
        int n = paramInt & 0xFF;
        int i1 = (0xFF0000 & keycolor[j]) >> 16;
        int i2 = (0xFF00 & keycolor[j]) >> 8;
        int i3 = 0xFF & keycolor[j];
        int i4 = Math.abs(k - i1);
        int i5 = Math.abs(m - i2);
        int i6 = Math.abs(n - i3);
        float f1 = (float)Math.sqrt(i4 * i4 + i5 * i5 + i6 * i6);
        localHashMap1.put(Integer.valueOf(j), Float.valueOf(f1));
        j++;
        break;
        localEntry = (Map.Entry)localIterator.next();
        if (f2 == -1.0F)
        {
          f2 = ((Float)localEntry.getValue()).floatValue();
          localHashMap3.put((Integer)localEntry.getKey(), (Float)localEntry.getValue());
          break label73;
        }
      }
      while (((Float)localEntry.getValue()).floatValue() != f2);
      localHashMap3.put((Integer)localEntry.getKey(), (Float)localEntry.getValue());
    }
  }

  static void genFromRGB332()
  {
    if (RGB332MappingTable != null);
    int k;
    int m;
    int n;
    while (true)
    {
      return;
      int[] arrayOfInt = { 8, 8, 4 };
      RGB332MappingTable = (char[][][])Array.newInstance(Character.TYPE, arrayOfInt);
      int i = RGB332Quantizer.QCRed.length;
      int j = RGB332Quantizer.QCGreen.length;
      k = RGB332Quantizer.QCBlue.length;
      for (m = 0; m < i; m++)
      {
        n = 0;
        if (n < j)
          break label77;
      }
    }
    label77: for (int i1 = 0; ; i1++)
    {
      if (i1 >= k)
      {
        n++;
        break;
      }
      int i2 = -16777216 + (RGB332Quantizer.QCRed[m] << 16) + (RGB332Quantizer.QCGreen[n] << 8) + RGB332Quantizer.QCBlue[i1];
      int i3 = Math.abs(RGB332Quantizer.QCRed[m] - RGB332Quantizer.QCGreen[n]);
      int i4 = Math.abs(RGB332Quantizer.QCGreen[m] - RGB332Quantizer.QCBlue[i1]);
      int i5 = Math.abs(RGB332Quantizer.QCBlue[i1] - RGB332Quantizer.QCRed[m]);
      int i6 = thresholdBlack;
      boolean bool = false;
      if (i3 <= i6)
      {
        int i7 = thresholdBlack;
        bool = false;
        if (i4 <= i7)
        {
          int i8 = thresholdBlack;
          bool = false;
          if (i5 <= i8)
            bool = true;
        }
      }
      RGB332MappingTable[m][n][i1] = _RGB332toKeyColorIdx(i2, bool);
    }
  }

  private static HashMap<Integer, Float> sortByValue(Map<Integer, Float> paramMap, boolean paramBoolean)
  {
    LinkedList localLinkedList = new LinkedList(paramMap.entrySet());
    Collections.sort(localLinkedList, new Comparator()
    {
      public int compare(Map.Entry<Integer, Float> paramAnonymousEntry1, Map.Entry<Integer, Float> paramAnonymousEntry2)
      {
        if (this.val$order)
          return ((Float)paramAnonymousEntry1.getValue()).compareTo((Float)paramAnonymousEntry2.getValue());
        return ((Float)paramAnonymousEntry2.getValue()).compareTo((Float)paramAnonymousEntry1.getValue());
      }
    });
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    Iterator localIterator = localLinkedList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return localLinkedHashMap;
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localLinkedHashMap.put((Integer)localEntry.getKey(), (Float)localEntry.getValue());
    }
  }

  public int getKeycolor(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= keycolor.length))
      return 0;
    return keycolor[paramInt];
  }

  public void release()
  {
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.mosaic.ColorTableGenerator
 * JD-Core Version:    0.6.2
 */