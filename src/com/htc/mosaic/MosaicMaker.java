package com.htc.mosaic;

import android.graphics.Bitmap;
import android.os.SystemClock;
import android.util.Log;

public class MosaicMaker
{
  private int getCommonColor(Bitmap paramBitmap)
  {
    if (paramBitmap == null)
      return -16777216;
    RGB332Quantizer localRGB332Quantizer = new RGB332Quantizer();
    long l1 = SystemClock.currentThreadTimeMillis();
    int i = localRGB332Quantizer.getMostCommonColor(paramBitmap);
    long l2 = SystemClock.currentThreadTimeMillis() - l1;
    Log.i("DotView", "Get Common Color Cost: " + l2 + " ms");
    return i;
  }

  public LinearMosaic doLinearMosaic(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    if (paramBitmap == null)
      return null;
    LinearMosaic localLinearMosaic = new LinearMosaic();
    Bitmap localBitmap = Bitmap.createScaledBitmap(paramBitmap, paramInt1, paramInt2, false);
    localLinearMosaic.setMosaic(localBitmap);
    localLinearMosaic.setCommonColor(getCommonColor(localBitmap));
    return localLinearMosaic;
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.mosaic.MosaicMaker
 * JD-Core Version:    0.6.2
 */