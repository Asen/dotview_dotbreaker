package com.htc.mosaic;

import android.graphics.Bitmap;

public class LinearMosaic
{
  private Bitmap bmpMosaic = null;
  private int mCommonColor = -16777216;

  public void destroy()
  {
    if (this.bmpMosaic != null)
    {
      this.bmpMosaic.recycle();
      this.bmpMosaic = null;
    }
  }

  public int getCommonColor()
  {
    return this.mCommonColor;
  }

  public Bitmap getMosaic()
  {
    return this.bmpMosaic;
  }

  public void setCommonColor(int paramInt)
  {
    this.mCommonColor = paramInt;
  }

  public void setMosaic(Bitmap paramBitmap)
  {
    this.bmpMosaic = paramBitmap;
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.mosaic.LinearMosaic
 * JD-Core Version:    0.6.2
 */