package com.htc.mosaic;

import android.graphics.Bitmap;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class RGB332Quantizer
{
  static int[] QCBlue = arrayOfInt3;
  static int[] QCGreen;
  static int[] QCRed;
  static int[] maskRGB = { 16711680, 65280, 255 };
  static int[] maskRGB332 = { 224, 28, 3 };
  private Bitmap bmpRGB332 = null;
  private HashMap<Integer, Integer> colorStatisticsTable = new HashMap(256);

  static
  {
    int[] arrayOfInt1 = new int[8];
    arrayOfInt1[1] = 36;
    arrayOfInt1[2] = 73;
    arrayOfInt1[3] = 109;
    arrayOfInt1[4] = 146;
    arrayOfInt1[5] = 182;
    arrayOfInt1[6] = 219;
    arrayOfInt1[7] = 255;
    QCRed = arrayOfInt1;
    int[] arrayOfInt2 = new int[8];
    arrayOfInt2[1] = 36;
    arrayOfInt2[2] = 73;
    arrayOfInt2[3] = 109;
    arrayOfInt2[4] = 146;
    arrayOfInt2[5] = 182;
    arrayOfInt2[6] = 219;
    arrayOfInt2[7] = 255;
    QCGreen = arrayOfInt2;
    int[] arrayOfInt3 = new int[4];
    arrayOfInt3[1] = 85;
    arrayOfInt3[2] = 170;
    arrayOfInt3[3] = 255;
  }

  private static HashMap<Integer, Integer> sortByValue(Map<Integer, Integer> paramMap, boolean paramBoolean)
  {
    Object localObject;
    if (paramMap.isEmpty())
      localObject = null;
    while (true)
    {
      return localObject;
      LinkedList localLinkedList = new LinkedList(paramMap.entrySet());
      Collections.sort(localLinkedList, new Comparator()
      {
        public int compare(Map.Entry<Integer, Integer> paramAnonymousEntry1, Map.Entry<Integer, Integer> paramAnonymousEntry2)
        {
          if (this.val$order)
            return ((Integer)paramAnonymousEntry1.getValue()).compareTo((Integer)paramAnonymousEntry2.getValue());
          return ((Integer)paramAnonymousEntry2.getValue()).compareTo((Integer)paramAnonymousEntry1.getValue());
        }
      });
      localObject = new LinkedHashMap();
      Iterator localIterator = localLinkedList.iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        ((HashMap)localObject).put((Integer)localEntry.getKey(), (Integer)localEntry.getValue());
      }
    }
  }

  int getMostCommonColor(Bitmap paramBitmap)
  {
    if (paramBitmap == null)
      return -16777216;
    int i = 0;
    int i2;
    HashMap localHashMap;
    Iterator localIterator;
    if (i >= paramBitmap.getHeight())
    {
      this.colorStatisticsTable = sortByValue(this.colorStatisticsTable, false);
      i2 = -1;
      localHashMap = new HashMap(this.colorStatisticsTable.size());
      localIterator = this.colorStatisticsTable.entrySet().iterator();
    }
    while (true)
    {
      label62: if (!localIterator.hasNext());
      Map.Entry localEntry;
      int i3;
      do
      {
        LinkedList localLinkedList = new LinkedList(localHashMap.keySet());
        Collections.sort(localLinkedList);
        int i4 = ColorTableGenerator.keycolor[((Integer)localLinkedList.get(0)).intValue()];
        localHashMap.clear();
        localLinkedList.clear();
        return i4;
        int j = 0;
        if (j >= paramBitmap.getWidth())
        {
          i++;
          break;
        }
        int k = toRGB332(paramBitmap.getPixel(j, i));
        int m = (k & maskRGB[0]) >> 16 >> 5;
        int n = (k & maskRGB[1]) >> 8 >> 5;
        int i1 = ((k & maskRGB[2]) >> 6) + ((m << 5) + (n << 2));
        if (this.colorStatisticsTable.containsKey(Integer.valueOf(i1)))
          this.colorStatisticsTable.put(Integer.valueOf(i1), Integer.valueOf(1 + ((Integer)this.colorStatisticsTable.get(Integer.valueOf(i1))).intValue()));
        while (true)
        {
          j++;
          break;
          this.colorStatisticsTable.put(Integer.valueOf(i1), Integer.valueOf(1));
        }
        localEntry = (Map.Entry)localIterator.next();
        i3 = ColorTableGenerator.RGB332toKeycolorIndex((((Integer)localEntry.getKey()).intValue() & maskRGB332[0]) >> 5, (((Integer)localEntry.getKey()).intValue() & maskRGB332[1]) >> 2, ((Integer)localEntry.getKey()).intValue() & maskRGB332[2]);
        if (i2 == -1)
        {
          i2 = ((Integer)localEntry.getValue()).intValue();
          localHashMap.put(Integer.valueOf(i3), (Integer)localEntry.getValue());
          break label62;
        }
      }
      while (((Integer)localEntry.getValue()).intValue() != i2);
      if (localHashMap.get(Integer.valueOf(i3)) != null)
        localHashMap.put(Integer.valueOf(i3), Integer.valueOf(((Integer)localEntry.getValue()).intValue() + ((Integer)localEntry.getValue()).intValue()));
      else
        localHashMap.put(Integer.valueOf(i3), (Integer)localEntry.getValue());
    }
  }

  int toRGB332(int paramInt)
  {
    int i = (paramInt & maskRGB[0]) >> 16 >> 5;
    int j = (paramInt & maskRGB[1]) >> 8 >> 5;
    int k = (paramInt & maskRGB[2]) >> 6;
    return -16777216 + ((QCRed[i] << 16) + (QCGreen[j] << 8) + QCBlue[k]);
  }

  Bitmap toRGB332(Bitmap paramBitmap)
  {
    if (paramBitmap == null)
      return null;
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    this.bmpRGB332 = Bitmap.createBitmap(i, j, paramBitmap.getConfig());
    int k = 0;
    if (k >= j)
      return this.bmpRGB332;
    for (int m = 0; ; m++)
    {
      if (m >= i)
      {
        k++;
        break;
      }
      int n = toRGB332(paramBitmap.getPixel(m, k));
      this.bmpRGB332.setPixel(m, k, n);
    }
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.mosaic.RGB332Quantizer
 * JD-Core Version:    0.6.2
 */