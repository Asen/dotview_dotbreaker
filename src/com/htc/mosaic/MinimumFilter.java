package com.htc.mosaic;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.SystemClock;
import android.util.Log;

public class MinimumFilter
{
  static int[] maskParam = null;
  static int maskRadius = 0;
  String TAG = "MinimumFilter";

  public MinimumFilter(int paramInt)
  {
    if (paramInt < 3)
      return;
    maskRadius = paramInt / 2;
    maskParam = createMask(maskRadius);
  }

  private int[] createMask(int paramInt)
  {
    int[] arrayOfInt = new int[2 * (int)Math.pow(1.0D + 2.0D * paramInt, 2.0D)];
    int i = 0;
    int j = -paramInt;
    if (j > paramInt)
      return arrayOfInt;
    for (int k = -paramInt; ; k++)
    {
      if (k > paramInt)
      {
        j++;
        break;
      }
      arrayOfInt[(i * 2)] = k;
      arrayOfInt[(1 + i * 2)] = j;
      i++;
    }
  }

  public Bitmap doMinimumFilter(Bitmap paramBitmap)
  {
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    Bitmap localBitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
    int[] arrayOfInt1 = new int[i * j];
    paramBitmap.getPixels(arrayOfInt1, 0, i, 0, 0, i, j);
    byte[] arrayOfByte1 = new byte[i * j];
    int[] arrayOfInt2;
    byte[] arrayOfByte2;
    int i2;
    int i12;
    for (int k = 0; ; k++)
    {
      int m = i * j;
      if (k >= m)
      {
        long l1 = SystemClock.currentThreadTimeMillis();
        arrayOfInt2 = new int[i * j];
        arrayOfByte2 = new byte[i * j];
        i2 = 0;
        if (i2 < j)
          break;
        i12 = 0;
        if (i12 < j)
          break label400;
        localBitmap.setPixels(arrayOfInt1, 0, i, 0, 0, i, j);
        long l2 = SystemClock.currentThreadTimeMillis() - l1;
        Log.i(this.TAG, "[doMinimumFilter]Performance= " + l2 + "ms(" + i + " x " + j + ")");
        System.gc();
        return localBitmap;
      }
      int n = (0xFF0000 & arrayOfInt1[k]) >> 16;
      int i1 = (0xFF00 & arrayOfInt1[k]) >> 8;
      arrayOfByte1[k] = ((byte)(((0xFF & arrayOfInt1[k]) + (n + i1)) / 3));
    }
    int i5;
    int i6;
    int i7;
    for (int i3 = 0; ; i3++)
    {
      if (i3 >= i)
      {
        i2++;
        break;
      }
      int i4 = i3 + i2 * i;
      i5 = -1;
      i6 = 255;
      i7 = -maskRadius;
      int i8 = maskRadius;
      if (i7 <= i8)
        break label302;
      arrayOfByte2[i4] = ((byte)i6);
      arrayOfInt2[i4] = arrayOfInt1[i5];
    }
    label302: int i9 = i3 + i7;
    int i10;
    int i11;
    if ((i9 >= 0) && (i9 < i))
    {
      i10 = i9 + i2 * i;
      if (arrayOfByte1[i10] >= 0)
        break label372;
      i11 = 256 + (0xFF & arrayOfByte1[i10]);
      label352: if (i5 != -1)
        break label382;
      i5 = i10;
      i6 = i11;
    }
    while (true)
    {
      i7++;
      break;
      label372: i11 = arrayOfByte1[i10];
      break label352;
      label382: if (i11 < i6)
      {
        i5 = i10;
        i6 = i11;
      }
    }
    label400: int i15;
    int i16;
    int i17;
    for (int i13 = 0; ; i13++)
    {
      if (i13 >= i)
      {
        i12++;
        break;
      }
      int i14 = i13 + i12 * i;
      i15 = -1;
      i16 = 255;
      i17 = -maskRadius;
      int i18 = maskRadius;
      if (i17 <= i18)
        break label466;
      arrayOfInt1[i14] = arrayOfInt2[i15];
    }
    label466: int i19 = i12 + i17;
    int i20;
    int i21;
    if ((i19 >= 0) && (i19 < j))
    {
      i20 = i13 + i19 * i;
      if (arrayOfByte2[i20] >= 0)
        break label536;
      i21 = 256 + (0xFF & arrayOfByte2[i20]);
      label516: if (i15 != -1)
        break label546;
      i15 = i20;
      i16 = i21;
    }
    while (true)
    {
      i17++;
      break;
      label536: i21 = arrayOfByte2[i20];
      break label516;
      label546: if (i21 < i16)
      {
        i15 = i20;
        i16 = i21;
      }
    }
  }
}

/* Location:           F:\Android\android\debug\dotbreaker\classes-dex2jar.jar
 * Qualified Name:     com.htc.mosaic.MinimumFilter
 * JD-Core Version:    0.6.2
 */